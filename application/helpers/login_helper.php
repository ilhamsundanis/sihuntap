<?php

function already_login()
{
    $ci = &get_instance();
    $user_session = $ci->session->userdata('id_user');
    if ($user_session) {
        redirect('main');
    }
}

function not_login()
{
    $ci = &get_instance();
    $user_session = $ci->session->userdata('id_user');
    if (!$user_session) {
        redirect('auth');
    }
}

function not_admin()
{
    $ci = &get_instance();
    $user_session = $ci->session->userdata('role');
    if ($user_session != 1) {
        redirect('main');
    }
}
