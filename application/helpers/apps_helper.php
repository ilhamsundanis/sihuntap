<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('UploadImage') ) {
	function UploadImage($file, $folder) {
		$explodeName = explode(".", $file['name']);
		$encode_name = md5(uniqid(rand(), true));
		$new_name    = "FILE" . $encode_name . "." . $explodeName[count($explodeName) - 1];
		$upload_path = "./assets/upload/" . $folder . "/";
		$fileExtension = strtolower(end($explodeName));
		$valid_ext = array('jpg','jpeg','png','gif','bmp', 'pdf','docx','doc');
		if(in_array($fileExtension,$valid_ext)){
			if(!is_dir($upload_path)) mkdir($upload_path, 7777, TRUE);
			move_uploaded_file($file['tmp_name'], $upload_path . $new_name);
			return base_url() . "assets/upload/" . $folder . "/" . $new_name;
		}else{
			return base_url() . "assets/fe/images/logo_candidate.png";
		}
	}
}

if ( ! function_exists('UploadImageByName') ) {
	function UploadImageByName($file, $folder, $name) {
		$explodeName = explode(".", $file['name']);
		$new_name    = "FILE-" . $name . "." . $explodeName[count($explodeName) - 1];
		$upload_path = "./assets/upload/" . $folder . "/";
		$fileExtension = strtolower(end($explodeName));
		$valid_ext = array('jpg','jpeg','png','gif','bmp');
		if(in_array($fileExtension,$valid_ext)){
			if(!is_dir($upload_path)) mkdir($upload_path, 7777, TRUE);
			move_uploaded_file($file['tmp_name'], $upload_path . $new_name);
			return base_url() . "assets/upload/" . $folder . "/" . $new_name;
		}else{
			return base_url() . "assets/fe/images/logo_candidate.png";
		}
	}
}
if ( ! function_exists('BackgroundProcessAPI') ) {
	function BackgroundProcessAPI($method, $url, $data = array()) {
		$ci =& get_instance();

		$raw_data = json_encode($data);
		$command = "curl -k --header \"Content-Type: application/json\" --request " . $method . " '".base_url()."api/".$url."' --data '".$raw_data."' > /dev/null &";
		shell_exec($command);
	}
}
if (!function_exists('load_apps')) {
	function load_apps()
	{

		$ci = get_instance();
		$queryCompany = $ci->db->get('settings')->row_array();
		return $queryCompany;
	}
}
if (!function_exists('upload_file')) {
	function upload_file($name_file, $path, $field_name)
	{

		$ci = get_instance();

		$config['upload_path']          = './assets/images/' . $path;
		$config['allowed_types']        = '*';
		$config['file_name']            = $name_file;
		$config['overwrite']			= true;
		// $config['max_size']             = 1024; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$ci->load->library('upload', $config);
		$ci->upload->initialize($config);
		if ($ci->upload->do_upload($field_name)) {
			return base_url() . 'assets/images/' . $path . $ci->upload->data("file_name");
		} else {
			// return $ci->upload->display_errors();
		}

		return "default.jpg";
	}
}

if (!function_exists('upload_all_file')) {
	function upload_all_file($name_file, $path, $field_name)
	{

		$ci = get_instance();

		$config['upload_path']          = './images/' . $path;
		$config['allowed_types']        = '*';
		$config['file_name']            = $name_file;
		$config['overwrite']			= true;
		// $config['max_size']             = 1024; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$ci->load->library('upload', $config);
		$ci->upload->initialize($config);
		if ($ci->upload->do_upload($field_name)) {
			$temp 		 = $ci->upload->data();
			$temp['url'] = base_url() . 'images/' . $path . $ci->upload->data("file_name");
			return $temp;
			// return base_url() . 'images/' . $path . $ci->upload->data("file_name");
		} else {
			// return $ci->upload->display_errors();
			$temp['url'] = "default.jpg";
		}

		return $temp;
	}
}
if ( ! function_exists('UploadDocumentByName') ) {
	function UploadDocumentByName($file, $folder, $name) {
		$explodeName = explode(".", $file['name']);
		$new_name    = "FILE-" . $name . "." . $explodeName[count($explodeName) - 1];
		$upload_path = "./assets/upload/" . $folder . "/";
		$fileExtension = strtolower(end($explodeName));
		$valid_ext = array('jpg','jpeg','png','gif','bmp', 'pdf','docx','doc','xlx','xlxs','ppt','pptx');
		if(in_array($fileExtension,$valid_ext)){
			if(!is_dir($upload_path)) mkdir($upload_path, 7777, TRUE);
			move_uploaded_file($file['tmp_name'], $upload_path . $new_name);
			return base_url() . "assets/upload/" . $folder . "/" . $new_name;
		}else{
			return "";
		}
	}
}


if ( ! function_exists('date_format_indo') ) {
	function date_format_indo($date, $print_day = false, $time = false){
		if ( ! empty($date) AND $date != "0000-00-00 00:00:00" AND $date != "0000-00-00" ) {
			// pre(ok);
			$day = array ( 1 =>    'Senin',
						'Selasa',
						'Rabu',
						'Kamis',
						'Jumat',
						'Sabtu',
						'Minggu'
					);
					
			$bulan = array (1 =>   'Januari',
						'Februari',
						'Maret',
						'April',
						'Mei',
						'Juni',
						'Juli',
						'Agustus',
						'September',
						'Oktober',
						'November',
						'Desember'
					);
			$split 	  = explode('-', $date);
			$date_indonesia = substr($split[2], 0, 2) . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
			
			if ($print_day) {
				$num = date('N', strtotime($date));
				if ($time) {
					return $day[$num] . ', ' . $date_indonesia . ', ' . date('H:i', strtotime($date)).' WIB ';
				} else {
					return $day[$num] . ', ' . $date_indonesia;
				}
			}
			return $date_indonesia;
		}
	}
}


if (!function_exists('kelas_bangunan')) {
	function kelas_bangunan()
	{
		return array('SEDERHANA', 'MENENGAH', 'MEWAH', 'HUNIAN BERIMBANG');
	}
}

if (!function_exists('jenis_perumahan')) {
	function jenis_perumahan()
	{
		return array('SUBSIDI', 'NON SUBSIDI');
	}
}

if (!function_exists('status_yatim')) {
	function status_yatim()
	{
		return array('TIDAK', 'YATIM', 'PIATU', 'YATIM PIATU');
	}
}

if (!function_exists('pekerjaan')) {
	function pekerjaan()
	{
		return array(
			'BELUM/TIDAK BEKERJA',
			'MENGURUS RUMAH TANGGA',

			'PEGAWAI NEGERI SIPIL',
			'TENTARA NASIONAL INDONESIA',
			'KEPOLISIAN RI',
			'PEDAGANG',
			'PETANI / PEKEBUN',
			'PETERNAK',
			'NELAYAN/PERIKANAN',
			'INDUSTRI',
			'KONSTRUKSI',
			'TRANSPORTASI',
			'KARYAWAN SWASTA',
			'KARYAWAN BUMN',
			'KARYAWAN BUMD',
			'KARYAWAN HONORER',
			'BURUH HARIAN LEPAS',
			'BURUH TANI/PERKEBUNAN',
			'PEMBANTU RUMAH TANGGA',
			'WARTAWAN',
			'USTADZ / MUBALIGH',
			'GURU',
			'DOSEN',
			'WIRASWASTA',
			'BIDAN',
			'PERAWAT',
			'PENSIUNAN'
		);
	}
}

if (!function_exists('kewarganegaraan')) {
	function kewarganegaraan()
	{
		return array(
			'WNI' => "WNI",
			'WNA' => "WNA"
		);
	}
}

if (!function_exists('status_siswa')) {
	function status_siswa()
	{
		return array(
			'BARU' => "BARU",
			'PINDAHAN' => "PINDAHAN"
		);
	}
}

if (!function_exists('alert')) {
	function alert()
	{
		$ci = &get_instance();

		if ($ci->session->flashdata('success')) {
			echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
			echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
			echo '<span aria-hidden="true">&times;</span></button>';
			echo $ci->session->flashdata('success');
			echo '</div>';
		} elseif ($ci->session->flashdata('info')) {
			echo '<div class="alert alert-info alert-dismissible fade show" role="alert">';
			echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
			echo '<span aria-hidden="true">&times;</span></button>';
			echo $ci->session->flashdata('info');
			echo '</div>';
		} elseif ($ci->session->flashdata('warning')) {
			echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">';
			echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
			echo '<span aria-hidden="true">&times;</span></button>';
			echo $ci->session->flashdata('warning');
			echo '</div>';
		} elseif ($ci->session->flashdata('danger')) {
			echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
			echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
			echo '<span aria-hidden="true">&times;</span></button>';
			echo $ci->session->flashdata('danger');
			echo '</div>';
		}
	}
}

if (!function_exists('total_slider')) {
	function total_slider()
	{
		$ci = &get_instance();
		$ci->db->select("id");
		$query = $ci->db->get('page_slider');
		return $query->num_rows();
	}
}
if (!function_exists('total_users')) {
	function total_users()
	{
		$ci = &get_instance();
		$ci->db->select("id");
		$query = $ci->db->get('users');
		return $query->num_rows();
	}
}

if (!function_exists('total_news')) {
	function total_news()
	{
		$ci = &get_instance();
		$ci->db->select("id");
		$query = $ci->db->get('page_news');
		return $query->num_rows();
	}
}
if (!function_exists('total_document')) {
	function total_document()
	{
		$ci = &get_instance();
		$ci->db->select("id");
		$query = $ci->db->get('page_document');
		return $query->num_rows();
	}
}

if (!function_exists('data_kecamatan')) {
	function data_kecamatan()
	{

		$ci = get_instance();

		if ($ci->session->userdata('role') == 1) {
			$ci->db->where('id_kab', 3201);
			$ci->db->order_by('id_kec', 'ASC');
			$querySiswa = $ci->db->get('tr_kecamatan')->result_array();
			return $querySiswa;
		} else {
			$ci->db->where('id_kab', 3201);
			$ci->db->where('id_kec', $ci->session->userdata('kecamatan'));
			$ci->db->order_by('id_kec', 'ASC');
			$querySiswa = $ci->db->get('tr_kecamatan')->result_array();
			return $querySiswa;
		}
	}
}

if (!function_exists('get_desa')) {
	function get_desa($id_kec)
	{

		$ci = get_instance();
		$ci->db->where('id_kec', $id_kec);
		$ci->db->order_by('id_kel', 'ASC');
		$querySiswa = $ci->db->get('tr_kelurahan')->result_array();
		return $querySiswa;
	}
}

if (!function_exists('get_uri1')) {
	function get_uri1()
	{
		$ci = get_instance();
		$url = $ci->uri->segment(1);
		return $url;
	}
}


if ( ! function_exists('RawQuery') ) {
	function RawQuery($query, $params=array(), $page = 0, $limit = 0, $order, $sort, $groupBy = "") {
		$result = "";
		if ( count($params) > 0 ) {
			$result .= " WHERE ";
			foreach ($params as $val) {
				$result .= $val . " AND ";
			}

			$result = rtrim($result, " AND ");
		}
		if ( !empty( $groupBy ) ) {
			$result .= " GROUP BY " . $groupBy . " ";
		}
		if ( !empty($order) ) {
			$result .= " ORDER BY " . $order . " " . $sort;
		}
		if ( $limit > 0 && $page > 0 ) {
			$offset = ($page - 1) * $limit;
			$result .= " LIMIT " . $limit . " OFFSET " . $offset;
		}
		return $result;
	}
}

if (!function_exists('count_penerima')) {
	function count_penerima($id)
	{

		$ci = get_instance();
		$ci->db->where('proposal_id',$id);
		$queryCount = $ci->db->get('residence_recipient')->num_rows();
		return $queryCount;
	}
}

if (!function_exists('alamat_huntara')) {
	function alamat_huntara($id)
	{

		$ci = get_instance();
		$ci->db->where('proposal_id',$id);
		$query = $ci->db->get('huntara')->row_array();
		if ($query > 0) {
			return $query;
		}else{
			return FALSE;
		}
		
	}
}
if (!function_exists('total_penerima')) {
	function total_penerima()
	{

		$ci = get_instance();
		$queryCount = $ci->db->get('residence_recipient')->num_rows();
		return $queryCount;
	}
}

if (!function_exists('total_data')) {
	function total_data($id)
	{

		$ci = get_instance();
		$ci->db->where('status',$id);
		$queryCount = $ci->db->get('residence_recipient')->num_rows();
		return $queryCount;
	}
}
if (!function_exists('total_rehabilitasi')) {
	function total_rehabilitasi($id)
	{

		$ci = get_instance();
		$ci->db->where('status',$id);
		$queryCount = $ci->db->get('disaster_victims')->num_rows();
		return $queryCount;
	}
}
if ( ! function_exists('Response') ) {
	function Response($meta = false) {
		$resp = array(
			"success"   => false,
			"code"      => 400,
			"message"   => "",
			"meta"      => array(
				"page"      => 1,
				"limit"     => 10,
				"offset"    => 0,
				"totalData" => 0,
				"totalPage" => 0,
				"orderBy"   => "",
				"sort"      => "",
			),
			"data"      => array(),
		);

		if ( !$meta ) {
			unset($resp['meta']);
		}

		return $resp;
	}
}
if ( ! function_exists('secret_key') ) {
	function secret_key() {
		$key = "c9b8d7183d6500ccda444b353bb93889c6679a95bddb10221e24f7d425c63bdb";
        return $key;
    }
}

if ( ! function_exists('guidv4') ) {
	function guidv4($data = null) {
		// Generate 16 bytes (128 bits) of random data or use the data passed into the function.
		$data = (!empty($data)) ? $data : random_bytes(16);
		// assert(strlen($data) == 16);
		$data = PHP_MAJOR_VERSION < 7 ? openssl_random_pseudo_bytes(16) : random_bytes(16);
		// Set version to 0100
		$data[6] = chr(ord($data[6]) & 0x0f | 0x40);
		// Set bits 6-7 to 10
		$data[8] = chr(ord($data[8]) & 0x3f | 0x80);
	
		// Output the 36 character UUID.
		return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
	}
}

if ( ! function_exists('validate_token') ) {
	function validate_token($token) {
		if ( empty($token) ) {
			return array("message" => "Authorization is empty", "status" => 401);
		}

		$token = str_replace("Bearer ", "", $token);

		$jwt            = new JWT();
        $jwt_secret_key = secret_key();
		$payload        = (array)$jwt->decode($token, $jwt_secret_key, true);

		if ( empty( $payload["exp"] ) ) {
			return array("message" => $payload[0], "status" => 401);
		}

		$date_now = date("Y-m-d H:i:s");
		$date_exp = date("Y-m-d H:i:s", $payload["exp"]);
		if ($date_now > $date_exp) {
			return array("message" => "Token Akses Sudah Kedaluarsa, Silahkan Login Kembali.", "status" => 401);
		}

		return array("message" => "success", "status" => 200, "data" => $payload);
	}
}