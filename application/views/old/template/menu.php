<body class="fixed-left">
    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>
    <!-- Begin page -->
    <div id="wrapper">

        <!-- ========== Left Sidebar Start ========== -->
        <div class="left side-menu">
            <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                <i class="ion-close"></i>
            </button>

            <!-- LOGO -->
            <div class="topbar-left">
                <div class="text-center">
                    <!-- <a href="index.html" class="logo" style="font-size: 30px;color: #fff;">SIHUNTAP</a> -->
                    <a href="<?php echo base_url(); ?>" class="logo">
                        <img src="<?php echo base_url(); ?>template/assets/images/logo.png" alt="" class="logo-large">
                    </a>
                </div>
            </div>

            <div class="sidebar-inner niceScrollleft">

                <div id="sidebar-menu">
                    <ul>
                        <li class="menu-title">Main</li>

                        <li>
                            <a href="<?php echo base_url('main'); ?>" class="waves-effect">
                                <i class="mdi mdi-home"></i><span>Beranda</span>
                            </a>
                        </li>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-layers"></i><span>Permohonan</span> <span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="<?= base_url('permohonan'); ?>">Data Usulan</a></li>
                                <li><a href="<?= base_url('verifikasi'); ?>">Data Verifikasi</a></li>
                                <li><a href="<?= base_url('huntara');?>">Relokasi Huntara</a></li>
                            </ul>
                        </li>

                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-layers"></i><span>Pem Perumahan</span> <span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="<?= base_url('huntap'); ?>">Data Hunian Tetap</a></li>
                                <li><a href="<?= base_url('perumahan'); ?>">Data Perumahan</a></li>
                            </ul>
                        </li>
                        <!-- <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-layers"></i><span>Ka. Pemukiman</span> <span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="list-unstyled">
                                
                                <li><a href="<?= base_url('rutilahu'); ?>">Data Rutilahu</a></li>
                            </ul>
                        </li>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-layers"></i><span>Data PSU</span> <span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="<?= base_url('pemakaman'); ?>">Data Pemakaman</a></li>
                                <li><a href="<?= base_url('pertanahan'); ?>">Data Pertanahan</a></li>
                                <li><a href="<?= base_url('psu'); ?>">Data Pertanahan</a></li>
                            </ul>
                        </li>

                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-layers"></i><span>Pen. Bangunan</span> <span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="<?= base_url('bangunan_pemerintah'); ?>">Bangunan Pemerintah</a></li>
                                <li><a href="<?= base_url('perizinan'); ?>">Perizinan</a></li>
                                <li><a href="<?= base_url('reklame'); ?>">Reklame</a></li>
                            </ul>
                        </li>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-layers"></i><span>Pertanahan</span> <span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="list-unstyled">
                            <li><a href="<?= base_url('tanah_aset'); ?>">Tanah Aset Pemda</a></li>
                            </ul>
                        </li> -->



                        <li class="menu-title">Laporan</li>

                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-bullseye"></i> <span> Laporan </span> <span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="<?php echo base_url('report');?>">Hunian Tetap</a></li>

                            </ul>
                        </li>
                        


                        <?php if ($this->session->userdata('role') == 1) { ?>

                        <li class="menu-title">Extra</li>

                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-map-marker-multiple"></i><span> Manajemen web </span> <span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="<?php echo base_url('news');?>"> Artikel Berita</a></li>
                                <li><a href="<?php echo base_url('galeri');?>"> Galeri Kegiatan</a></li>
                            </ul>
                        </li>

                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-google-pages"></i><span> Pengaturan </span> <span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="<?php echo base_url('apps'); ?>">Aplikasi</a></li>
                                <li><a href="<?php echo base_url('apps/kec'); ?>">Master Kecamatan</a></li>
                                <li><a href="<?php echo base_url('apps/desa'); ?>">Master Desa</a></li>
                                <li><a href="<?php echo base_url('apps/desa'); ?>">Master Jenis Bencana</a></li>
                                <li><a href="<?php echo base_url('user'); ?>">Pengguna</a></li>

                            </ul>
                        </li>

                        <?php } ?>
                        <li>
                            <a href="<?php echo base_url('auth/logout'); ?>" class="waves-effect">
                               <i class="mdi mdi-logout-variant"></i><span>Log Out</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div> <!-- end sidebarinner -->
        </div>
        <!-- Left Sidebar End -->

        <!-- Start right Content here -->

        <div class="content-page">
            <!-- Start content -->
            <div class="content">

                <!-- Top Bar Start -->
                <div class="topbar">

                    <nav class="navbar-custom">

                        <ul class="list-inline float-right mb-0">



                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"><?= $this->session->userdata('nama_user'); ?>  
                                    <img src="<?php echo base_url(); ?>template/assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle"> 
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5><?= $this->session->userdata('nama_user'); ?></h5>
                                    </div>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5 text-muted"></i> Profil</a>
                                    <!-- <a class="dropdown-item" href="#"><i class="mdi mdi-wallet m-r-5 text-muted"></i> My Wallet</a> -->
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-settings m-r-5 text-muted"></i> Pengaturan</a>
                                    <!-- <a class="dropdown-item" href="#"><i class="mdi mdi-lock-open-outline m-r-5 text-muted"></i> Lock screen</a> -->
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="<?= base_url('auth/logout') ?>"><i class="mdi mdi-logout m-r-5 text-muted"></i> Logout</a>
                                </div>
                            </li>

                        </ul>

                        <ul class="list-inline menu-left mb-0">
                            <li class="float-left">
                                <button class="button-menu-mobile open-left waves-light waves-effect">
                                    <i class="mdi mdi-menu"></i>
                                </button>
                            </li>
                        </ul>

                        <div class="clearfix"></div>

                    </nav>

                </div>
                <!-- Top Bar End -->