<div class="page-content-wrapper ">

    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                            <li class="breadcrumb-item"><a href="#">Data Pemakaman</a></li>
                            <li class="breadcrumb-item active">Form Tambah</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Data Pemakaman</h4>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>pemakaman/save_data" method="POST">
                            <h4 class="mt-0 header-title">Form Tambah Data</h4>
                            <p class="text-muted mb-4 font-14">Silahkan lengkapi data dibawah ini</p>
                            <div class="form-group row">
                                <label for="nama_pemakaman" class="col-sm-2 col-form-label">Nama Pemakaman</label>
                                <div class="col-sm-4">
                                    <input class="form-control" name="nama_pemakaman" type="text" id="nama_pemakaman" required placeholder="Input nama pemakaman">
                                </div>
                                <label for="luas_pemakaman" class="col-sm-2 col-form-label">Luas Pemakaman</label>
                                <div class="col-sm-4">
                                    <input class="form-control" name="luas_pemakaman" type="text" id="luas_pemakaman" required placeholder="Input luas pemakaman dalam meter persegi">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-sm-2 col-form-label">Kecamatan</label>
                                <div class="col-sm-4">
                                    <select name="kecamatan" id="kecamatan" class="form-control" required>
                                        <option selected>Pilih Kecamatan</option>
                                        <?php foreach (data_kecamatan() as $key => $kec) { ?>
                                            <?php if ($this->session->userdata('role') != 1) { ?>
                                                <option value="<?= $kec['id_kec']; ?>"><?= $kec['nama']; ?></option>
                                            <?php } else { ?>
                                                <option value="<?= $kec['id_kec']; ?>"><?= $kec['nama']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <label for="example-email-input" class="col-sm-2 col-form-label">Desa</label>
                                <div class="col-sm-4">
                                    <select name="kelurahan" id="kelurahan" class="form-control" required>
                                        <option selected>Pilih Desa</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-url-input" class="col-sm-2 col-form-label">Alamat</label>
                                <div class="col-sm-10">
                                    <textarea name="alamat" id="alamat" class="form-control" placeholder="Input alamat lengkap pemakaman" required></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <br />
                                    <button type="submit" class="btn btn-primary" name="save_pemakaman">Tambah Data</button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div><!-- container -->
</div> <!-- Page content Wrapper -->

<script type="text/javascript" src="<?php echo base_url(); ?>template/assets/js/jquery-3.3.1.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#kecamatan').change(function() {
            var id = $(this).val();
            $.ajax({
                url: "<?php echo base_url(); ?>huntap/get_desa_by_kec",
                method: "POST",
                data: {
                    id: id
                },
                async: false,
                dataType: 'json',
                success: function(data) {
                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_kel + '>' + data[i].nama_desa + '</option>';
                    }
                    $('#kelurahan').html(html);

                }
            });
        });
    });
</script>