<div class="page-content-wrapper ">

    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a>Beranda</a></li>
                            <li class="breadcrumb-item"><a><?php echo $title; ?></a></li>
                        </ol>
                    </div>
                    <h4 class="page-title"><?php echo $title; ?></h4>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class="card">
                    <div class="card-body table-responsive">
                        <h5 class="header-title"><?php echo $title; ?></h5>
                        <p class="text-muted mb-4"><?php echo $description; ?></p>
                        <p>
                            <a href="<?php echo base_url(); ?>pemakaman/add"><button type="submit" class="btn btn-primary waves-effect waves-light">Tambah Data</button></a>
                        </p>
                        <?= alert(); ?>
                        <div class="data-pemakaman">
                            <table id="pemakaman-datatable" class="table">
                                <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>NAMA PEMAKAMAN</th>
                                        <th>LUAS</th>
                                        <th>ALAMAT</th>
                                        <th>DESA</th>
                                        <th>KECAMATAN</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($GetData as $key => $data) { ?>
                                        <tr>
                                            <td><?= $no; ?></td>
                                            <td><?= $data['nama_pemakaman']; ?></td>
                                            <td><?= number_format($data['luas_pemakaman']); ?> M<sup>2</sup></td>
                                            <td><?= $data['alamat_pemakaman']; ?></td>
                                            <td><?= $data['nama_desa']; ?></td>
                                            <td><?= $data['nama']; ?></td>
                                            <td>
                                                <a href="<?php echo base_url('pemakaman/view/' . $data['id_pemakaman']); ?>">
                                                    <i class="fa fa-edit"></i>
                                                </a> |
                                                <a href="<?php echo base_url('pemakaman/delete/' . $data['id_pemakaman']); ?>" onclick="return confirm('Anda yakin ingin menghapus data?')">
                                                    <i class="fa fa-eraser"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php $no++;
                                    } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->

    </div><!-- container -->

</div> <!-- Page content Wrapper -->

<script src="<?= base_url() ?>template/assets/js/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#pemakaman-datatable').DataTable();
    });
</script>