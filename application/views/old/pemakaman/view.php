    <?php foreach ($GetData as $key => $value) {
        // code...
    } ?>
    <div class="page-content-wrapper ">
        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group float-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                                <li class="breadcrumb-item"><a href="#">Data Pemakaman</a></li>
                                <li class="breadcrumb-item active"><?php echo $title_form; ?></li>
                            </ol>
                        </div>
                        <h4 class="page-title"><?php echo $title; ?></h4>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="<?php echo base_url(); ?>pemakaman/update_pemakaman" method="POST">
                                <h4 class="mt-0 header-title"><?php echo $title_form; ?></h4>
                                <p class="text-muted mb-4 font-14"><?php echo $description_form; ?></p>
                                <input type="hidden" value="<?php echo $value['id_pemakaman']; ?>" name="id_pemakaman">
                                <div class="form-group row">
                                    <label for="nama_pemakaman" class="col-sm-2 col-form-label">Nama Pemakaman</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="text" name="nama_pemakaman" placeholder="Input nama lengkap penerima" value="<?php echo $value['nama_pemakaman']; ?>" id="nama_pemakaman" required>
                                    </div>
                                    <label for="luas_pemakaman" class="col-sm-2 col-form-label">Luas Pemakaman</label>
                                    <div class="col-sm-3">
                                        <input class="form-control" type="text" id="luas_pemakaman" value="<?php echo $value['luas_pemakaman']; ?>" name="luas_pemakaman" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-email-input" class="col-sm-2 col-form-label">Kecamatan</label>
                                    <div class="col-sm-4">
                                        <select name="kecamatan" id="kecamatan" class="form-control" required>
                                            <?php foreach (data_kecamatan() as $key => $kec) {
                                            ?>
                                                <option value="<?php echo $kec['id_kec']; ?>" <?= $kec['id_kec'] == $value['id_kec'] ? "selected" : "" ?>><?php echo $kec['nama']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <label for="example-email-input" class="col-sm-2 col-form-label" required>Desa</label>
                                    <div class="col-sm-4">
                                        <select name="kelurahan" id="kelurahan" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-url-input" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <textarea name="alamat" id="alamat" class="form-control" placeholder="Input alamat lengkap penerima" required><?php echo $value['alamat_pemakaman']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <br />
                                        <button type="submit" class="btn btn-primary" name="edit_pemakaman">Edit Data</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->

    <script type="text/javascript" src="<?php echo base_url(); ?>template/assets/js/jquery-3.3.1.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            const kecamatan_id = <?= $value['id_kec']; ?>;
            const desa_id = <?= $value['desa_id']; ?>;
            $.ajax({
                url: "<?php echo base_url(); ?>pemakaman/get_desa_by_kec",
                method: "POST",
                data: {
                    id: kecamatan_id
                },
                async: false,
                dataType: 'json',
                success: function(data) {
                    let html = '';
                    let i;
                    let temp = "";
                    for (i = 0; i < data.length; i++) {
                        if (data[i].id_kel == desa_id) {
                            temp = "selected";
                        } else {
                            temp = "";
                        }
                        html += `<option value="${data[i].id_kel}" ${temp}>${data[i].nama_desa}</option>`;
                    }
                    $('#kelurahan').html(html);
                }
            });

            $('#kecamatan').change(function() {
                const id = $(this).val();
                $.ajax({
                    url: "<?php echo base_url(); ?>pemakaman/get_desa_by_kec",
                    method: "POST",
                    data: {
                        id: id
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        let html = '';
                        let i;
                        for (i = 0; i < data.length; i++) {
                            html += '<option value=' + data[i].id_kel + '>' + data[i].nama_desa + '</option>';
                        }
                        $('#kelurahan').html(html);
                    }
                });
            });
        });
    </script>