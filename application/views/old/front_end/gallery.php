<section class="section-padding">
    <div class="container">
        <div class="text-center mb-50">
            <h2 class="section-title">Galeri Kegiatan</h2>
            <p class="section-sub">Kegiatan DPKPP Kabupaten Bogor.</p>
        </div>


        <div class="portfolio-container text-center">
            <ul class="portfolio-filter brand-filter">
                <li class="active waves-effect waves-light" data-group="all">Semua</li>
                <li class=" waves-effect waves-light" data-group="survei">Survei</li>
                <li class=" waves-effect waves-light" data-group="huntap">Huntap</li>
                <li class=" waves-effect waves-light" data-group="rutilahu">Rutilahu</li>
                <li class=" waves-effect waves-light" data-group="perumahan">Perumahan</li>
                <li class=" waves-effect waves-light" data-group="pertanahan">Pertanahan</li>
            </ul>


            <div class="portfolio portfolio-masonry col-3 gutter mtb-50">
                <!-- remove "gutter" class for no spacing -->

                <div class="portfolio-item" data-groups='["all", "huntap", "pertanahan"]'>
                    <div class="portfolio-wrapper">
                        <div class="thumb">
                            <div class="bg-overlay brand-overlay"></div>
                            <img src="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-1.jpeg" alt="">
                            <div class="portfolio-intro">
                                <div class="action-btn">
                                    <a href="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-1.jpeg" class="tt-lightbox" title=""><i class="fa fa-search"></i></a>
                                </div>
                                <h2><a href="#">Galeri Kegiatan</a></h2>
                                <p><a href="#">huntap</a></p>
                            </div>
                        </div><!-- thumb -->
                    </div><!-- /.portfolio-wrapper -->
                </div><!-- /.portfolio-item -->

                <div class="portfolio-item" data-groups='["all", "rutilahu", "survei"]'>
                    <div class="portfolio-wrapper">

                        <div class="thumb">
                            <div class="bg-overlay brand-overlay"></div>
                            <img src="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-2.jpeg" alt="">

                            <div class="portfolio-intro">
                                <div class="action-btn">
                                    <a href="#"> <i class="fa fa-link"></i> </a>
                                </div>
                                <h2><a href="#">Galeri Kegiatan</a></h2>
                                <p><a href="#">rutilahu</a></p>
                            </div>
                        </div><!-- thumb -->

                    </div><!-- /.portfolio-wrapper -->
                </div><!-- /.portfolio-item -->

                <div class="portfolio-item" data-groups='["all", "pertanahan", "huntap"]'>

                    <div class="portfolio-wrapper">
                        <div class="thumb">
                            <div class="bg-overlay brand-overlay"></div>

                            <div class="portfolio-slider" data-direction="vertical">
                                <ul class="slides">
                                    <li>
                                        <a href="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-3.jpeg" title="materialize Unique Design">
                                            <img src="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-3.jpeg" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-4.jpeg" title="materialize Clean and Elegant">
                                            <img src="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-4.jpeg" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="portfolio-intro">
                                <div class="action-btn">
                                    <a href="#"> <i class="fa fa-search"></i> </a>
                                </div>
                                <h2><a href="#">Galeri Kegiatan</a></h2>
                                <p><a href="#">Gallery</a></p>
                            </div>

                        </div><!-- thumb -->

                    </div><!-- /.portfolio-wrapper -->
                </div><!-- /.portfolio-item -->

                <div class="portfolio-item" data-groups='["all", "survei", "huntap"]'>
                    <div class="portfolio-wrapper">
                        <div class="thumb">
                            <div class="bg-overlay brand-overlay"></div>
                            <img src="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-5.jpeg" alt="">

                            <div class="portfolio-intro">
                                <div class="action-btn">
                                    <a class="popup-video" href="https://www.youtube.com/watch?v=XVfOe5mFbAE"> <i class="fa fa-play"></i> </a>
                                </div>
                                <h2><a href="#">Galeri Kegiatan</a></h2>
                                <p><a href="#">Video</a></p>
                            </div>
                        </div><!-- thumb -->

                    </div><!-- /.portfolio-wrapper -->
                </div><!-- /.portfolio-item -->

                <div class="portfolio-item" data-groups='["all", "pertanahan", "rutilahu"]'>
                    <div class="portfolio-wrapper">
                        <div class="thumb">
                            <div class="bg-overlay brand-overlay"></div>
                            <img src="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-6.jpeg" alt="">

                            <div class="portfolio-intro">
                                <div class="action-btn">
                                    <a href="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-6.jpeg" class="tt-lightbox" title=""> <i class="fa fa-search"></i> </a>
                                </div>
                                <h2><a href="#">Galeri Kegiatan</a></h2>
                                <p><a href="#">huntap</a></p>
                            </div>
                        </div><!-- /.thumb -->

                    </div><!-- /.portfolio-wrapper -->
                </div><!-- /.portfolio-item -->

                <div class="portfolio-item" data-groups='["all", "survei",  "rutilahu"]'>
                    <div class="portfolio-wrapper">
                        <div class="thumb">
                            <div class="bg-overlay brand-overlay"></div>
                            <img src="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-7.jpeg" alt="">

                            <div class="portfolio-intro">
                                <div class="action-btn">
                                    <a href="portfolio-single.html"> <i class="fa fa-link"></i></a>
                                </div>
                                <h2><a href="#">Galeri Kegiatan</a></h2>
                                <p><a href="#">rutilahu</a></p>
                            </div>
                        </div><!-- /.thumb -->

                    </div><!-- /.portfolio-wrapper -->
                </div><!-- /.portfolio-item -->

                <div class="portfolio-item" data-groups='["all", "survei", "pertanahan"]'>
                    <div class="portfolio-wrapper">
                        <div class="thumb">
                            <div class="bg-overlay brand-overlay"></div>
                            <img src="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-8.jpeg" alt="">

                            <div class="portfolio-intro">
                                <div class="action-btn">
                                    <a href="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-8.jpeg" class="tt-lightbox" title=""><i class="fa fa-search"></i></a>
                                </div>
                                <h2><a href="#">Galeri Kegiatan</a></h2>
                                <p><a href="#">pertanahan</a></p>
                            </div>
                        </div><!-- /.thumb -->

                    </div><!-- /.portfolio-wrapper -->
                </div><!-- /.portfolio-item -->

                <div class="portfolio-item" data-groups='["all", "huntap", "rutilahu"]'>
                    <div class="portfolio-wrapper">
                        <div class="thumb">
                            <div class="bg-overlay brand-overlay"></div>
                            <img src="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-9.jpeg" alt="">

                            <div class="portfolio-intro">
                                <div class="action-btn">
                                    <a href="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-9.jpeg" class="tt-lightbox" title=""><i class="fa fa-search"></i></a>
                                </div>
                                <h2><a href="#">Galeri Kegiatan</a></h2>
                                <p><a href="#">huntap</a></p>
                            </div>
                        </div><!-- /.thumb -->

                    </div><!-- /.portfolio-wrapper -->
                </div><!-- /.portfolio-item -->


                <div class="portfolio-item" data-groups='["all", "survei", "huntap"]'>
                    <div class="portfolio-wrapper">

                        <div class="thumb">
                            <div class="bg-overlay brand-overlay"></div>

                            <img src="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-5.jpeg" alt="">

                            <div class="portfolio-intro">
                                <div class="action-btn">
                                    <a href="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-5.jpeg" class="tt-lightbox" title=""><i class="fa fa-search"></i></a>
                                </div>
                                <h2><a href="#">Galeri Kegiatan</a></h2>
                                <p><a href="#">huntap</a></p>
                            </div>

                        </div><!-- thumb -->

                    </div><!-- /.portfolio-wrapper -->
                </div><!-- /.portfolio-item -->


                <div class="portfolio-item" data-groups='["all", "rutilahu", "pertanahan","perumahan"]'>
                    <div class="portfolio-wrapper">

                        <div class="thumb">
                            <div class="bg-overlay brand-overlay"></div>

                            <img src="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-2.jpeg" alt="">

                            <div class="portfolio-intro">
                                <div class="action-btn">
                                    <a href="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-2.jpeg" class="tt-lightbox" title=""><i class="fa fa-search"></i></a>
                                </div>
                                <h2><a href="#">Galeri Kegiatan</a></h2>
                                <p><a href="#">huntap</a></p>
                            </div>

                        </div><!-- thumb -->

                    </div><!-- /.portfolio-wrapper -->
                </div><!-- /.portfolio-item -->


                <div class="portfolio-item" data-groups='["all", "survei", "pertanahan"]'>
                    <div class="portfolio-wrapper">

                        <div class="thumb">
                            <div class="bg-overlay brand-overlay"></div>

                            <img src="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-5.jpeg" alt="">

                            <div class="portfolio-intro">
                                <div class="action-btn">
                                    <a href="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-5.jpeg" class="tt-lightbox" title=""><i class="fa fa-search"></i></a>
                                </div>
                                <h2><a href="#">Galeri Kegiatan</a></h2>
                                <p><a href="#">huntap</a></p>
                            </div>

                        </div><!-- thumb -->

                    </div><!-- /.portfolio-wrapper -->
                </div><!-- /.portfolio-item -->


                <div class="portfolio-item" data-groups='["all", "huntap", "rutilahu"]'>
                    <div class="portfolio-wrapper">

                        <div class="thumb">
                            <div class="bg-overlay brand-overlay"></div>

                            <img src="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-1.jpeg" alt="">

                            <div class="portfolio-intro">
                                <div class="action-btn">
                                    <a href="<?= base_url('template/front_end'); ?>/img/portfolio/galeri-1.jpeg" class="tt-lightbox" title=""><i class="fa fa-search"></i></a>
                                </div>
                                <h2><a href="#">Galeri Kegiatan</a></h2>
                                <p><a href="#">huntap</a></p>
                            </div>

                        </div><!-- thumb -->

                    </div><!-- /.portfolio-wrapper -->
                </div><!-- /.portfolio-item -->

            </div><!-- /.portfolio -->


            <!-- <div class="load-more-button text-center">
                <a class="waves-effect waves-light btn btn-large pink"> <i class="fa fa-spinner left"></i> Muat Lainnya</a>
            </div> -->

        </div><!-- portfolio-container -->
    </div>
</section>