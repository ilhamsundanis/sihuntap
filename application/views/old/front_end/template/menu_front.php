<body id="top" class="has-header-search">
    <!--header start-->
    <header id="header" class="tt-nav nav-border-bottom">
        <div class="header-sticky light-header ">
            <div class="container">
                <div id="materialize-menu" class="menuzord">
                    <!--logo start-->
                    <a href="<?= base_url('beranda') ?>" class="logo-brand">
                        <img class="retina" src="<?= base_url('template/front_end'); ?>/img/logo.png" alt="" />
                    </a>
                    <!--logo end-->
                    <!--mega menu start-->
                    <ul class="menuzord-menu pull-right">
                        <li class="<?= $title == "Beranda" ? "active" : "" ?>">
                            <a href="<?= base_url('beranda') ?>">Beranda</a>
                        </li>
                        <li class="<?= $this->uri->segment(1) == "profil" ? "active" : "" ?>"><a href="javascript:void(0)">Profil</a>
                            <ul class="dropdown">
                                <li class="<?= $title == "Profil Dinas" ? "active" : "" ?>"><a href="<?= base_url('profil/profil_dinas') ?>">Profile Dinas</a></li>
                                <li class="<?= $title == "Regulasi & Prosedur" ? "active" : "" ?>"><a href="<?= base_url('profil/regulasi') ?>">Regulasi &amp; Prosedur </a></li>
                            </ul>
                        </li>
                        <li class="<?= $this->uri->segment(1) == "galeri" ? "active" : "" ?>">
                            <a href="<?= base_url('galeri') ?>">Galeri Kegiatan</a>
                        </li>
                        <li class="<?= $this->uri->segment(1) == "artikel" ? "active" : "" ?>">
                            <a href="<?= base_url('artikel') ?>">Artikel & Berita</a>
                        </li>
                        <li class="<?= $this->uri->segment(1) == "kontak" ? "active" : "" ?>">
                            <a href="<?= base_url('kontak') ?>">Kontak Kami</a>
                        </li>
                        <li>
                            <a href="<?= base_url('auth') ?>">
                                <button class="btn btn-info">Login SIHUNTAP</button>
                            </a>
                        </li>
                    </ul>
                    <!--mega menu end-->
                </div>
            </div>
        </div>
    </header>
    <!--header end-->