<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="materialize is a material design based mutipurpose responsive template">
    <meta name="keywords" content="material design, card style, material template, portfolio, corporate, business, creative, agency">
    <meta name="author" content="trendytheme.net">

    <title><?= $title; ?></title>

    <!--  favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>template/assets/images/favicon.png">


    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,900' rel='stylesheet' type='text/css'>
    <!-- Material Icons CSS -->
    <link href="<?= base_url('template/front_end'); ?>/fonts/iconfont/material-icons.css" rel="stylesheet">
    <!-- FontAwesome CSS -->
    <link href="<?= base_url('template/front_end'); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- magnific-popup -->
    <link href="<?= base_url('template/front_end'); ?>/magnific-popup/magnific-popup.css" rel="stylesheet">
    <!-- owl.carousel -->
    <link href="<?= base_url('template/front_end'); ?>/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
    <link href="<?= base_url('template/front_end'); ?>/owl.carousel/assets/owl.theme.default.min.css" rel="stylesheet">
    <!-- flexslider -->
    <link href="<?= base_url('template/front_end'); ?>/flexSlider/flexslider.css" rel="stylesheet">
    <!-- materialize -->
    <link href="<?= base_url('template/front_end'); ?>/materialize/css/materialize.min.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?= base_url('template/front_end'); ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- shortcodes -->
    <link href="<?= base_url('template/front_end'); ?>/css/shortcodes/shortcodes.css" rel="stylesheet">
    <!-- Style CSS -->
    <link href="<?= base_url('template/front_end'); ?>/style.css" rel="stylesheet">


    <!-- RS5.0 Main Stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('template/front_end'); ?>/revolution/css/settings.css">
    <!-- RS5.0 Layers and Navigation Styles -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('template/front_end'); ?>/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('template/front_end'); ?>/revolution/css/navigation.css">


</head>