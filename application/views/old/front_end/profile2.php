<!--page title start-->
<section class="page-title ptb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Regulasi & Prosedure</h2>
                <ol class="breadcrumb">
                    <li><a href="#">Profil</a></li>
                    <li class="active">Regulasi & Prosedure</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!--page title end-->


<section class="padding-top-90 padding-bottom-70">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="border-bottom-tab">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab-1" role="tab" class="waves-effect waves-dark" data-toggle="tab">Hunian Tetap</a></li>
                        <li role="presentation"><a href="#tab-2" role="tab" class="waves-effect waves-dark" data-toggle="tab">Rumah Tidak Layak Huni</a></li>
                        <li role="presentation"><a href="#tab-3" role="tab" class="waves-effect waves-dark" data-toggle="tab">Perumahan</a></li>
                        <li role="presentation"><a href="#tab-4" role="tab" class="waves-effect waves-dark" data-toggle="tab">Pertanahan</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="panel-body">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="tab-1">
                                <img class="alignleft" src="<?= base_url('template/front_end'); ?>/img/huntap.png" alt="" width="300">
                                <p>Hunian tetap (huntap) adalah tempat tinggal para korban bencana pasca tinggal dari hunian sementara yang bersifat permanen. Bangunan huntap berbeda dengan huntara. Huntara bangunannya bersifat non-permanen dari sisi materialnya. Sedangkan huntap bangunannya permanen.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-2">
                                <img class="alignleft" src="<?= base_url('template/front_end'); ?>/img/rutilahu.png" alt="" width="300">
                                <p>Secara umum, rumah tidak layak huni (Rutilahu) adalah rumah yang tidak memenuhi persyaratan rumah layak huni, di mana konstruksi bangunannya tidak handal, luasnya tidak sesuai standar per orang di dalamnya, dan tidak menyehatkan bagi penghuninya dan/atau membahayakan bagi penghuninya.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-3">
                                <img class="alignleft" src="<?= base_url('template/front_end'); ?>/img/perumahan.png" alt="" width="300">
                                <p>Perumahan adalah sekelompok rumah atau bangunan lainnya yang dibangun bersamaan sebagai sebuah pengembangan tunggal. Bentuknya bervariasi di negara-negara manapun. Perumahan biasanya dibangun oleh seorang kontraktor tunggal dengan hanya beberapa gaya rancangan rumah atau bangunan, sehingga penampilannya menjadi seragam. Pada umumnya, perumahan adalah monotenur.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-4">
                                <img class="alignleft" src="<?= base_url('template/front_end'); ?>/img/pertanahan.png" alt="" width="300">
                                <p>Hukum pertanahan mengacu pada norma hukum bagi negara untuk menyesuaikan berbagai hubungan sosial dan ekonomi dalam kepemilikan, penguasaan, pengoperasian, penggunaan, perlindungan, dan pengelolaan tanah. Tujuannya adalah untuk melindungi kepentingan ekonomi tanah dari kelas penguasa dan membantu menstabilkan tatanan sosial ekonomi dan aturan politik kelas penguasa.</p>
                            </div>
                        </div>
                    </div>

                </div><!-- /.border-bottom-tab -->

            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>

<hr>

<section class="section-padding">
    <div class="container">

        <div class="text-center mb-80">
            <h2 class="section-title text-uppercase">Tugas Pokok &amp; Fungsi</h2>
            <p class="section-sub">Membantu Bupati dalam melaksanakan urusan pemerintahan di bidang perumahan, kawasan permukiman dan pertanahan.</p>
        </div>

        <div class="row equal-height-row">
            <div class="col-md-4 mb-30">
                <div class="featured-item hover-outline brand-hover radius-4 equal-height-column">
                    <div class="icon">
                        <i class="material-icons colored brand-icon">&#xE3B7;</i>
                    </div>
                    <div class="desc">
                        <h2>01</h2>
                        <p>Perumusan kebijakan bidang perumahan, kawasan permukiman dan bidang pertanahan;</p>
                    </div>
                </div><!-- /.featured-item -->
            </div><!-- /.col-md-4 -->

            <div class="col-md-4 mb-30">
                <div class="featured-item hover-outline brand-hover radius-4 equal-height-column">
                    <div class="icon">
                        <i class="material-icons colored brand-icon">&#xE326;</i>
                    </div>
                    <div class="desc">
                        <h2>02</h2>
                        <p>Pelaksanaan kebijakan bidang perumahan, kawasan permukiman dan bidang pertanahan;</p>
                    </div>
                </div><!-- /.featured-item -->
            </div><!-- /.col-md-4 -->

            <div class="col-md-4 mb-30">
                <div class="featured-item hover-outline brand-hover radius-4 equal-height-column">
                    <div class="icon">
                        <i class="material-icons colored brand-icon">&#xE8B8;</i>
                    </div>
                    <div class="desc">
                        <h2>03</h2>
                        <p>Pelaksanaan evaluasi, dan pelaporan di bidang perumahan, kawasan permukiman dan bidang pertanahan;</p>
                    </div>
                </div><!-- /.featured-item -->
            </div><!-- /.col-md-4 -->

            <div class="col-md-4">
                <div class="featured-item hover-outline brand-hover radius-4 equal-height-column">
                    <div class="icon">
                        <i class="material-icons colored brand-icon">&#xE325;</i>
                    </div>
                    <div class="desc">
                        <h2>04</h2>
                        <p>PPelaksanaan reformasi birokrasi;</p>
                    </div>
                </div><!-- /.featured-item -->
            </div><!-- /.col-md-4 -->

            <div class="col-md-4">
                <div class="featured-item hover-outline brand-hover radius-4 equal-height-column">
                    <div class="icon">
                        <i class="material-icons colored brand-icon">&#xE3B0;</i>
                    </div>
                    <div class="desc">
                        <h2>05</h2>
                        <p>Pelaksanaan administrasi Dinas;</p>
                    </div>
                </div><!-- /.featured-item -->
            </div><!-- /.col-md-4 -->

            <div class="col-md-4">
                <div class="featured-item hover-outline brand-hover radius-4 equal-height-column">
                    <div class="icon">
                        <i class="material-icons colored brand-icon">&#xE62E;</i>
                    </div>
                    <div class="desc">
                        <h2>06</h2>
                        <p>Pelaksanaan fungsi lain yang diberikan oleh Bupati sesuai dengan bidang tugasnya.</p>
                    </div>
                </div><!-- /.featured-item -->
            </div><!-- /.col-md-4 -->
        </div><!-- /.row -->

    </div><!-- /.container -->
</section>

<hr>

<hr>