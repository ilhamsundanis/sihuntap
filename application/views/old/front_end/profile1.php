<!--page title start-->
<section class="page-title ptb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Profile Dinas</h2>
                <ol class="breadcrumb">
                    <li><a href="#">Profil</a></li>
                    <li class="active">Profil Dinas</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!--page title end-->

<section class="section-padding">
    <div class="container">

        <div class="text-center mb-80">
            <h2 class="section-title text-uppercase">Profile Dinas</h2>
        </div>

        <div class="team-tab" role="tabpanel">
            <!-- Nav tabs -->
           <!--  <ul class="nav nav-tabs nav-justified" role="tablist">
                <li class="active">
                    <a href="#team-1" data-toggle="tab">
                        <img src="<?= base_url('template/front_end'); ?>/img/team/team-1.jpg" class="img-responsive" alt="Image">
                    </a>
                </li>

                <li>
                    <a href="#team-2" data-toggle="tab">
                        <img src="<?= base_url('template/front_end'); ?>/img/team/team-2.jpg" class="img-responsive" alt="Image">
                    </a>
                </li>

                <li>
                    <a href="#team-3" data-toggle="tab">
                        <img src="<?= base_url('template/front_end'); ?>/img/team/team-3.jpg" class="img-responsive" alt="Image">
                    </a>
                </li>

                <li>
                    <a href="#team-4" data-toggle="tab">
                        <img src="<?= base_url('template/front_end'); ?>/img/team/team-4.jpg" class="img-responsive" alt="Image">
                    </a>
                </li>
            </ul> -->

            <!-- Tab panes -->
            <div class="panel-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="team-1">
                        <div class="row">
                            <div class="col-md-4 col-sm-3">
                                <figure class="team-img text-center">
                                    <img src="<?= base_url('template/front_end'); ?>/img/team/team-large-1.png" class="img-responsive" alt="Image">
                                </figure>
                            </div><!-- /.col-md-4 -->

                            <div class="col-md-8 col-sm-9">

                                <div class="team-intro">
                                    <h3>Nama Kepala Dinas <small>Kepala Dinas</small></h3>
                                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Soluta, tenetur impedit! Consectetur quibusdam error voluptas incidunt unde sit eveniet voluptate fugit, voluptatum sint soluta corporis reprehenderit, doloribus dolorem repellendus ducimus.</p>
                                </div>
                                <!--team-skill end -->
                            </div> <!-- col-md-8 -->
                        </div> <!-- row -->
                    </div>
                    <!--team-1 end-->

                    <div role="tabpanel" class="tab-pane fade" id="team-2">
                        <div class="row">
                            <div class="col-md-4 col-sm-3">
                                <figure class="team-img text-center">
                                    <img src="<?= base_url('template/front_end'); ?>/img/team/team-large-2.png" class="img-responsive" alt="Image">
                                </figure>
                            </div><!-- /.col-md-4 -->
                            <div class="col-md-8 col-sm-9">
                                <div class="team-intro">
                                    <h3>Nama Wakil Kepala Dinas <small>Wakil Kepala Dinas</small></h3>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore minima molestias dolor porro nisi nam deserunt quisquam repellendus? Ipsum saepe ratione atque error explicabo eligendi nisi nulla harum dolorum earum.</p>
                                </div>
                                <!--team-skill end -->
                            </div> <!-- col-md-8 -->
                        </div> <!-- row -->
                    </div>
                    <!--team-2 end -->

                    <div role="tabpanel" class="tab-pane fade" id="team-3">
                        <div class="row">
                            <div class="col-md-4 col-sm-3">
                                <figure class="team-img text-center">
                                    <img src="<?= base_url('template/front_end'); ?>/img/team/team-large-3.png" class="img-responsive" alt="Image">
                                </figure>
                            </div><!-- /.col-md-4 -->

                            <div class="col-md-8 col-sm-9">
                                <div class="team-intro">
                                    <h3>Nama Sekretaris Dinas <small>Sekretaris</small></h3>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis aut velit ipsa facere saepe temporibus voluptates nesciunt qui commodi vero. Sed incidunt sequi perspiciatis tempora nulla reprehenderit dignissimos fugiat! Fugiat.</p>
                                </div>
                                <!--team-skill end -->
                            </div> <!-- col-md-8 -->
                        </div> <!-- row -->
                    </div>
                    <!--team-3 end -->

                    <div role="tabpanel" class="tab-pane fade" id="team-4">
                        <div class="row">
                            <div class="col-md-4 col-sm-3">
                                <figure class="team-img text-center">
                                    <img src="<?= base_url('template/front_end'); ?>/img/team/team-large-4.png" class="img-responsive" alt="Image">
                                </figure>
                            </div><!-- /.col-md-4 -->

                            <div class="col-md-8 col-sm-9">

                                <div class="team-intro">
                                    <h3>Lainnya <small>Lainnya</small></h3>
                                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempora ullam maxime atque fugit quidem iusto mollitia dolorem asperiores placeat, quibusdam itaque similique et! Mollitia labore consequuntur placeat rerum, error pariatur?.</p>
                                </div>
                                <!--team-skill end -->
                            </div> <!-- col-md-8 -->
                        </div> <!-- row -->
                    </div>
                    <!--team-4 end -->

                </div>
                <!--tab-content end -->
            </div>

        </div>
        <!--tab-pan end -->

    </div><!-- /.container -->
</section>

<hr>

<section class="section-padding banner-6 bg-fixed parallax-bg overlay light-9" data-stellar-background-ratio="0.5">
    <div class="container">

        <div class="text-center mb-80">
            <h2 class="section-title text-uppercase">Struktur Organisasi</h2>
        </div>

    </div><!-- /.container -->
</section>


<hr>

<section class="section-padding banner-6 bg-fixed parallax-bg overlay light-9" data-stellar-background-ratio="0.5">
    <div class="container">

        <div class="text-center mb-80">
            <h2 class="section-title text-uppercase">Bidang-bidang pada dinas</h2>
        </div>

        <div class="featured-carousel brand-dot">
            <div class="featured-item border-box radius-4 hover brand-hover">
                <div class="icon mb-30">
                    <i class="material-icons brand-icon">&#xE32A;</i>
                </div>
                <div class="desc">
                    <h2>Bidang Perumahan</h2>
                </div>
            </div><!-- /.featured-item -->

            <div class="featured-item border-box radius-4 hover brand-hover">
                <div class="icon mb-30">
                    <i class="material-icons brand-icon">&#xE851;</i>
                </div>
                <div class="desc">
                    <h2>Bidang Kawasan Permukiman</h2>
                </div>
            </div><!-- /.featured-item -->

            <div class="featured-item border-box radius-4 hover brand-hover">
                <div class="icon mb-30">
                    <i class="material-icons brand-icon">&#xE8AF;</i>
                </div>
                <div class="desc">
                    <h2>Bidang Prasaranan, Sarana dan Utilitas Umum</h2>
                </div>
            </div><!-- /.featured-item -->

            <div class="featured-item border-box radius-4 hover brand-hover">
                <div class="icon mb-30">
                    <i class="material-icons brand-icon">&#xE91D;</i>
                </div>
                <div class="desc">
                    <h2>Bidang Penataan Bangunan</h2>
                </div>
            </div><!-- /.featured-item -->

            <div class="featured-item border-box radius-4 hover brand-hover">
                <div class="icon mb-30">
                    <i class="material-icons brand-icon">&#xE8CB;</i>
                </div>
                <div class="desc">
                    <h2>Bidang Pertanahan</h2>
                </div>
            </div><!-- /.featured-item -->
        </div>

    </div><!-- /.container -->
</section>

<hr>