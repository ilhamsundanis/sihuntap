<!--page title start-->
<section class="page-title ptb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Kontak</h2>
                <ol class="breadcrumb">
                    <li><a href="#">Kontak</a></li>
                    <li class="active">Kontak Kami</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!--page title end-->


<!-- contact-form-section -->
<section class="section-padding">

    <div class="container">

        <div class="text-center mb-80">
            <h2 class="section-title text-uppercase">Hubungi Kami</h2>
            <p class="section-sub">Silahkan hubungi kami untuk lebih lanjut</p>
        </div>

        <div class="row">
            <div class="col-md-8">
                <form name="contact-form" id="contactForm" action="" method="POST">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-field">
                                <input type="text" name="name" class="validate" id="name">
                                <label for="name">Nama</label>
                            </div>

                        </div><!-- /.col-md-6 -->

                        <div class="col-md-6">
                            <div class="input-field">
                                <label class="sr-only" for="email">Email</label>
                                <input id="email" type="email" name="email" class="validate">
                                <label for="email" data-error="wrong" data-success="right">Email</label>
                            </div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-field">
                                <input id="phone" type="tel" name="phone" class="validate">
                                <label for="phone">Nomor Telepon</label>
                            </div>
                        </div><!-- /.col-md-6 -->

                        <div class="col-md-6">
                            <div class="input-field">
                                <input id="website" type="text" name="website" class="validate">
                                <label for="website">Website Anda</label>
                            </div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row -->

                    <div class="input-field">
                        <textarea name="message" id="message" class="materialize-textarea"></textarea>
                        <label for="message">Isi Pesan</label>
                    </div>

                    <button type="submit" name="submit" class="waves-effect waves-light btn submit-button pink mt-30">Kirim Pesan</button>
                </form>
            </div><!-- /.col-md-8 -->

            <div class="col-md-4 contact-info">

                <address>
                    <i class="material-icons brand-color">&#xE55F;</i>
                    <div class="address">
                        <p>Jl. Tegar Beriman, Tengah,<br> Cibinong, Bogor 16914</p>
                    </div>

                    <i class="material-icons brand-color">&#xE61C;</i>
                    <div class="phone">
                        <p>Nomor Telepon: (021) 8758968<br>
                            Fax: (021) 8758968</p>
                    </div>

                    <i class="material-icons brand-color">&#xE0E1;</i>
                    <div class="mail">
                        <p><a href="mailto:#">sihuntap@bogorkab.go.id</a><br>
                            <a href="#">sihuntap.bogorkab.go.id</a>
                        </p>
                    </div>
                </address>

            </div><!-- /.col-md-4 -->
        </div><!-- /.row -->
    </div>
</section>
<!-- contact-form-section End -->


<!-- map-section -->
<div id="myMap" class="height-350"></div>
<!-- /.map-section -->