<!-- start revolution slider 5.0 -->
<section class="rev_slider_wrapper">
    <div class="rev_slider materialize-slider">
        <ul>

            <!-- slide 1 start -->
            <li data-transition="fade" data-slotamount="default" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-thumb="<?= base_url('template/front_end'); ?>/img/banner-1/bg.jpg" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="materialize Material" data-description="">

                <!-- MAIN IMAGE -->
                <img src="<?= base_url('template/front_end'); ?>/img/banner-1/bg.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>


                <!-- LAYER NR. 1 -->
                <!-- <div class="tp-caption tp-resizeme" data-basealign="slide" data-x="['right','right','right','right']" data-hoffset="['0','0','-70','-70']" data-y="['top','top','top','top']" data-voffset="['0','0','-30','-30']" data-transform_idle="o:1;" data-transform_in="x:50px;opacity:0;s:600;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2000" style="z-index: 5;">
                    <div><img src="<?= base_url('template/front_end'); ?>/img/banner-1/dummy.png" alt="" data-lazyload="<?= base_url('template/front_end'); ?>/img/banner-1/book.png">
                    </div>
                </div>

                <div class="tp-caption tp-resizeme" data-x="left" data-y="center" data-voffset="-150" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:600;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2000" style="z-index: 5;">
                    <div><img src="<?= base_url('template/front_end'); ?>/img/banner-1/dummy.png" alt="" data-lazyload="<?= base_url('template/front_end'); ?>/img/banner-1/glasses.png">
                    </div>
                </div>


                <div class="tp-caption tp-resizeme" data-x="left" data-y="bottom" data-voffset="100" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:600;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2400" style="z-index: 5;">
                    <div><img src="<?= base_url('template/front_end'); ?>/img/banner-1/dummy.png" alt="" data-lazyload="<?= base_url('template/front_end'); ?>/img/banner-1/earphones.png">
                    </div>
                </div>


                <div class="tp-caption tp-resizeme" data-basealign="slide" data-x="['right','right','right','right']" data-hoffset="['0','0','-50','-50']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','0','0']" data-transform_idle="o:1;" data-transform_in="x:50px;opacity:0;s:600;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2400" style="z-index: 5;">
                    <div><img src="<?= base_url('template/front_end'); ?>/img/banner-1/dummy.png" alt="" data-lazyload="<?= base_url('template/front_end'); ?>/img/banner-1/cup.png">
                    </div>
                </div> -->

                <!-- LAYER NR. 5 -->
                <div class="tp-caption NotGeneric-Title tp-resizeme" data-x="center" data-y="center" data-voffset="-100" data-fontsize="['70','70','70','45']" data-lineheight="['70','70','70','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 5; white-space: nowrap;">SIHUNTAP
                </div>

                <!-- LAYER NR. 6 -->
                <div class="tp-caption tp-resizeme rev-subheading white-text text-center" data-x="center" data-y="center" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">Sistem Informasi Hunian Tetap <br> Kabupaten Bogor.
                </div>

                <!-- LAYER NR. 7 -->
                <div class="tp-caption tp-resizeme rev-btn" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['100','100','100','100']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-style_hover="cursor:default;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1200" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;">
                    <a href="#counter" class="btn btn-lg  waves-effect waves-light">Selengkapnya</a>
                </div>


                <!-- LAYER NR. 8 -->
                <div class="tp-caption tp-resizeme" data-basealign="slide" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['-70','-70','-170','-170']" data-transform_idle="o:1;" data-transform_in="y:50px;opacity:0;s:600;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2400" style="z-index: 5;">
                    <div><img src="<?= base_url('template/front_end'); ?>/img/banner-1/dummy.png" alt="" data-lazyload="<?= base_url('template/front_end'); ?>/img/banner-1/macbook.png">
                    </div>
                </div>
            </li>
            <!-- slide 1 end -->

            <!-- slide 2 start -->
            <li data-transition="fade" data-slotamount="default" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-thumb="<?= base_url('template/front_end'); ?>/img/banner-2/bg.jpg" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Unique Design" data-description="">

                <!-- MAIN IMAGE -->
                <img src="<?= base_url('template/front_end'); ?>/img/banner-2/bg.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>



                <!-- <div class="tp-caption tp-resizeme" data-basealign="slide" data-x="['right','right','right','right']" data-hoffset="['0','0','-170','-170']" data-y="['top','top','top','top']" data-voffset="['0','0','-90','-90']" data-transform_idle="o:1;" data-transform_in="x:50px;opacity:0;s:600;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2000" style="z-index: 5;">
                    <div><img src="<?= base_url('template/front_end'); ?>/img/banner-1/dummy.png" alt="" data-lazyload="<?= base_url('template/front_end'); ?>/img/banner-2/book.png">
                    </div>
                </div>


                <div class="tp-caption tp-resizeme" data-basealign="slide" data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" data-y="['center','center','center','center']" data-voffset="['0','0','0','0']" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:600;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2000" style="z-index: 5;">
                    <div><img src="<?= base_url('template/front_end'); ?>/img/banner-1/dummy.png" alt="" data-lazyload="<?= base_url('template/front_end'); ?>/img/banner-2/flower.png">
                    </div>
                </div>


                <div class="tp-caption tp-resizeme" data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','-90','-90']" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:600;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2400" style="z-index: 5;">
                    <div><img src="<?= base_url('template/front_end'); ?>/img/banner-1/dummy.png" alt="" data-lazyload="<?= base_url('template/front_end'); ?>/img/banner-2/box.png">
                    </div>
                </div>


                <div class="tp-caption tp-resizeme" data-basealign="slide" data-x="['right','right','right','right']" data-hoffset="['0','0','-150','-150']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','0','0']" data-transform_idle="o:1;" data-transform_in="x:50px;opacity:0;s:600;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2400" style="z-index: 5;">
                    <div><img src="<?= base_url('template/front_end'); ?>/img/banner-1/dummy.png" alt="" data-lazyload="<?= base_url('template/front_end'); ?>/img/banner-2/tube.png">
                    </div>
                </div> -->

                <!-- LAYER NR. 5 -->
                <div class="tp-caption NotGeneric-Title tp-resizeme" data-x="center" data-y="center" data-voffset="-100" data-fontsize="['70','70','70','45']" data-lineheight="['70','70','70','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 5; white-space: nowrap;">Perumahan & Pemukiman
                </div>

                <!-- LAYER NR. 6 -->
                <div class="tp-caption tp-resizeme rev-subheading white-text text-center" data-x="center" data-y="center" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">Menyajikan Data Hunian Tetap, Rutilahu, Perumahan, dan Pertanahan Terkini<br>
                </div>

                <!-- LAYER NR. 7 -->
                <div class="tp-caption tp-resizeme rev-btn" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['100','100','100','100']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-style_hover="cursor:default;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1200" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;">
                    <a href="#counter" class="btn btn-lg  waves-effect waves-light">Selengkapnya</a>
                </div>


                <!-- LAYER NR. 8 -->
                <div class="tp-caption tp-resizeme" data-basealign="slide" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['-120','-120','-170','-170']" data-transform_idle="o:1;" data-transform_in="y:50px;opacity:0;s:600;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2400" style="z-index: 5;">
                    <div><img src="<?= base_url('template/front_end'); ?>/img/banner-1/dummy.png" alt="" data-lazyload="<?= base_url('template/front_end'); ?>/img/banner-2/ipad.png">
                    </div>
                </div>
            </li>
            <!-- slide 2 end -->

        </ul>
    </div><!-- end revolution slider -->
</section><!-- end of slider wrapper -->

<section id="counter" class="counter-section facts-two banner-10 parallax-bg bg-fixed overlay light-9 section-padding" data-stellar-background-ratio="0.5">
    <div class="container">

        <div class="row text-center">
            <div class="col-sm-3 counter-wrap">
                <i class="material-icons brand-color">&#xe88a;</i>
                <span class="timer">545</span>
                <span class="count-description">Usulan Permohonan</span>
            </div> <!-- /.col-sm-3 -->

            <div class="col-sm-3 counter-wrap">
                <i class="material-icons brand-color">&#xe84f;</i>
                <span class="timer">535</span>
                <span class="count-description">Terverifikasi</span>
            </div><!-- /.col-sm-3 -->

            <div class="col-sm-3 counter-wrap">
                <i class="material-icons brand-color">&#xe88a;</i>
                <span class="timer">1544</span>
                <span class="count-description">Hunian Sementara</span>
            </div><!-- /.col-sm-3 -->

            <div class="col-sm-3 counter-wrap">
                <i class="material-icons brand-color">&#xe88b;</i>
                <span class="timer">111</span>
                <span class="count-description">Hunian Tetap</span>
            </div><!-- /.col-sm-3 -->
        </div>
    </div><!-- /.container -->
</section>

<section class="section-padding">
    <div class="container">

        <div class="text-center mb-80">
            <h2 class="section-title text-uppercase">Apa Itu Sihuntap?</h2>
            <p class="section-sub">SIHUNTAP adalah sistem informasi yang dirancang guna menyajikan data tentang Hunian Tetap, Rumah Tidak Layak Huni, Perumahan, dan Pertanahan di wilayah Kabupaten Bogor</p>
        </div>

        <div class="vertical-tab">
            <div class="row">
                <div class="col-sm-3">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-stacked" role="tablist">
                        <li role="presentation" class="active"><a href="#tab-5" class="waves-effect waves-light" role="tab" data-toggle="tab">Hunian Tetap</a></li>
                        <li role="presentation"><a href="#tab-6" class="waves-effect waves-light" role="tab" data-toggle="tab">Rumah Tidak Layak Huni</a></li>
                        <li role="presentation"><a href="#tab-7" class="waves-effect waves-light" role="tab" data-toggle="tab">Perumahan</a></li>
                        <li role="presentation"><a href="#tab-8" class="waves-effect waves-light" role="tab" data-toggle="tab">Pertanahan</a></li>
                    </ul>
                </div><!-- /.col-md-3 -->

                <div class="col-sm-9">
                    <!-- Tab panes -->
                    <div class="panel-body">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="tab-5">
                                <h2>Hunian Tetap</h2>

                                <img class="alignright" src="<?= base_url('template/front_end'); ?>/img/huntap.png" alt="" width="300">

                                <p>Hunian tetap (huntap) adalah tempat tinggal para korban bencana pasca tinggal dari hunian sementara yang bersifat permanen. Bangunan huntap berbeda dengan huntara. Huntara bangunannya bersifat non-permanen dari sisi materialnya. Sedangkan huntap bangunannya permanen.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-6">
                                <h2>Rumah Tidak Layak Huni</h2>

                                <img class="alignright" src="<?= base_url('template/front_end'); ?>/img/rutilahu.png" alt="" width="300">

                                <p>Secara umum, rumah tidak layak huni (Rutilahu) adalah rumah yang tidak memenuhi persyaratan rumah layak huni, di mana konstruksi bangunannya tidak handal, luasnya tidak sesuai standar per orang di dalamnya, dan tidak menyehatkan bagi penghuninya dan/atau membahayakan bagi penghuninya.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-7">
                                <h2>Perumahan</h2>

                                <img class="alignright" src="<?= base_url('template/front_end'); ?>/img/perumahan.png" alt="" width="300">

                                <p>Perumahan adalah sekelompok rumah atau bangunan lainnya yang dibangun bersamaan sebagai sebuah pengembangan tunggal. Bentuknya bervariasi di negara-negara manapun. Perumahan biasanya dibangun oleh seorang kontraktor tunggal dengan hanya beberapa gaya rancangan rumah atau bangunan, sehingga penampilannya menjadi seragam. Pada umumnya, perumahan adalah monotenur.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-8">
                                <h2>Pertanahan</h2>

                                <img class="alignright" src="<?= base_url('template/front_end'); ?>/img/pertanahan.png" alt="" width="300">

                                <p>Hukum pertanahan mengacu pada norma hukum bagi negara untuk menyesuaikan berbagai hubungan sosial dan ekonomi dalam kepemilikan, penguasaan, pengoperasian, penggunaan, perlindungan, dan pengelolaan tanah. Tujuannya adalah untuk melindungi kepentingan ekonomi tanah dari kelas penguasa dan membantu menstabilkan tatanan sosial ekonomi dan aturan politik kelas penguasa.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.vertical-tab -->

        
    </div><!-- /.container -->
</section>


<section class="padding-top-110 padding-bottom-70 brand-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="quote-carousel text-center">

                    <div class="carousel-item">
                        <div class="content">
                            <h2 class="white-text line-height-40">"Kata pengantar tentang Sistem Informasi Hunian Tetap."</h2>

                            <div class="testimonial-meta font-20 text-extrabold white-text">
                                Ade Yasin - Bupati Bogor
                            </div>
                        </div><!-- /.content -->
                    </div><!-- /.carousel-item -->

                    <div class="carousel-item">
                        <div class="content">
                            <h2 class="white-text line-height-40">"Kata pengantar tentang Sistem Informasi Hunian Tetap."</h2>

                            <div class="testimonial-meta font-20 text-extrabold white-text">
                                Iwan Setiawan - Wakil Bupati Bogor
                            </div>
                        </div><!-- /.content -->
                    </div><!-- /.carousel-item -->

                    <div class="carousel-item">
                        <div class="content">
                            <h2 class="white-text line-height-40">"Kata pengantar tentang Sistem Informasi Hunian Tetap."</h2>

                            <div class="testimonial-meta font-20 text-extrabold white-text">
                                Kepala DPKPP
                            </div>
                        </div><!-- /.content -->
                    </div><!-- /.carousel-item -->
                </div>
            </div><!-- /.col-md-8 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>


<section class="section-padding">
    <div class="container">

        <div class="text-center mb-80">
            <h2 class="section-title text-uppercase">Link Terkait</h2>
        </div>

        <div class="clients-grid gutter">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="border-box">
                        <a href="https://bogorkab.go.id/" target="_blank">
                            <img src="<?= base_url('template/front_end'); ?>/img/client-logo/kab-bogor.png" alt="clients">
                        </a>
                    </div><!-- /.border-box -->
                </div><!-- /.col-md-3 -->

                <div class="col-md-3 col-sm-6">
                    <div class="border-box">
                        <a href="https://beasiswapancakarsa.bogorkab.go.id/" target="_blank">
                            <img src="<?= base_url('template/front_end'); ?>/img/client-logo/beasiswa.png" alt="clients">
                        </a>
                    </div><!-- /.border-box -->
                </div><!-- /.col-md-3 -->

                <div class="col-md-3 col-sm-6">
                    <div class="border-box">
                        <a href="http://www.pengaduan.bogorkab.go.id/" target="_blank">
                            <img src="<?= base_url('template/front_end'); ?>/img/client-logo/laras-online.png" alt="clients">
                        </a>
                    </div><!-- /.border-box -->
                </div><!-- /.col-md-3 -->

                <div class="col-md-3 col-sm-6">
                    <div class="border-box">
                        <a href="https://ppid.bogorkab.go.id/" target="_blank">
                            <img src="<?= base_url('template/front_end'); ?>/img/client-logo/ppid.png" alt="clients">
                        </a>
                    </div><!-- /.border-box -->
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
            <div class="row mt-10">
                <div class="col-md-3 col-sm-6">
                    <div class="border-box">
                        <a href="https://covid-19.bogorkab.go.id/" target="_blank">
                            <img src="<?= base_url('template/front_end'); ?>/img/client-logo/info-covid19.png" alt="clients">
                        </a>
                    </div><!-- /.border-box -->
                </div><!-- /.col-md-3 -->

                <div class="col-md-3 col-sm-6">
                    <div class="border-box">
                        <a href="https://ppid.bogorkab.go.id/" target="_blank">
                            <img src="<?= base_url('template/front_end'); ?>/img/client-logo/sebaran-covid19.png" alt="clients">
                        </a>
                    </div><!-- /.border-box -->
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.clients-grid -->

    </div><!-- /.container -->
</section>