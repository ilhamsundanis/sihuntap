<?php foreach ($GetArtikelById as $key => $value) {
    // code...
}?>
<!--page title start-->
<section class="page-title ptb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Artikel</h2>
                <ol class="breadcrumb">
                    <li><a href="#">Artikel</a></li>
                    <li class="active">Detail Artikel</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!--page title end-->

<!-- blog section start -->
<section class="blog-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="posts-content single-post">

                    <article class="post-wrapper">

                        <header class="entry-header-wrapper clearfix">
                            <div class="entry-header">
                                <h2 class="entry-title"><?= $value['judul_berita'];?></h2>

                                <div class="entry-meta">
                                    <ul class="list-inline">
                                        <li>
                                            <i class="fa fa-user"></i><a><?= $value['nama_user'];?></a>
                                        </li>

                                        <li>
                                            <i class="fa fa-clock-o"></i><a><?= date_format_indo($value['created_dt'],true,true);?></a>
                                        </li>
                                        <li>
                                            <i class="fa fa-comment-o"></i><a href="#">3</a>
                                        </li>
                                    </ul>
                                </div><!-- .entry-meta -->
                            </div>

                        </header><!-- /.entry-header-wrapper -->

                        <div class="thumb-wrapper">
                            <img src="<?= $value['gambar_berita'];?>" class="img-responsive" alt="">
                        </div><!-- .post-thumb -->


                        <div class="entry-content">
                            <?php echo $value['isi_berita'];?>
                        </div><!-- .entry-content -->


                        <footer class="entry-footer">
                            <div class="post-tags">
                                <span class="tags-links">
                                    <i class="fa fa-tags"></i><a><?php echo $value['kategori_berita'];?></a>
                                </span>
                            </div> <!-- .post-tags -->
                        </footer>

                    </article><!-- /.post-wrapper -->

                    <!-- <nav class="single-post-navigation" role="navigation">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="previous-post-link">
                                    <a class="waves-effect waves-light" href="#"><i class="fa fa-long-arrow-left"></i>Baca Posting Sebelumnya</a>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="next-post-link">
                                    <a class="waves-effect waves-light" href="#">Baca Postingan Selanjutnya<i class="fa fa-long-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </nav> -->

                    <!-- <div class="comments-wrapper">
                        <div class="comments-tab">

                            <ul class="nav nav-tabs nav-justified" role="tablist">
                                <li role="presentation" class="active"><a href="#default-comment" class="waves-effect waves-light" role="tab" data-toggle="tab">Leave a comment now</a></li>
                                <li role="presentation"><a href="#facebook-comment" class="waves-effect waves-light" role="tab" data-toggle="tab">Facebook Comment</a></li>
                            </ul>

                            <div class="panel-body">
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="default-comment">
                                        <div class="comment-respond">

                                            <form action="#" method="post" id="commentform" novalidate="" role="form">
                                                <div class="form-group">
                                                    <div class="row">

                                                        <div class="col-sm-4 comment-form-author">
                                                            <input class="form-control" id="author" placeholder="Your Name" name="author" type="text" value="" aria-required="true">
                                                        </div>

                                                        <div class="col-sm-4 comment-form-email">
                                                            <input id="email" class="form-control" name="email" placeholder="Email Address" type="email" value="" aria-required="true">
                                                        </div>

                                                        <div class="col-sm-4 comment-form-subject">
                                                            <input class="form-control" placeholder="Subject" id="subject" name="subject" type="text" value="">
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="form-group comment-form-comment">
                                                    <textarea class="form-control" id="comment" name="comment" placeholder="Comment" rows="8" aria-required="true"></textarea>
                                                </div>

                                                <div class="form-submit">
                                                    <button class="btn btn-lg pink waves-effect waves-light" name="submit" type="submit" id="submit" value="">Submit</button>
                                                </div>
                                            </form>

                                        </div>

                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="facebook-comment">
                                        <p>Duis senectus mus condimentum nunc ac habitasse duis consectetur a arcu a accumsan cras et metus ultricies justo cum a bibendum. <a href="#">Egestas vestibulum blandit sem vestibulum curabitur</a> a vel aliquet gravida ullamcorper amet dictumst vestibulum a elementum proin id volutpat magna parturient. Id ac dui libero a ullamcorper euismod himenaeos a nam condimentum a adipiscing dapibus lobortis iaculis morbi.</p>

                                        <p>Himenaeos a vestibulum morbi. <a href="#">Ullamcorper cras scelerisque</a> taciti lorem metus feugiat est lacinia facilisis id nam leo condimentum praesent id diam. Vestibulum amet porta odio elementum convallis parturient tempor tortor tempus a mi ad parturient ad nulla mus amet in penatibus nascetur at vulputate euismod a est tristique scelerisque. Aliquet facilisis nisl vel vestibulum dignissim gravida ullamcorper hac quisque ad at nisl egestas adipiscing vel blandit.</p>
                                    </div>
                                </div><
                            </div>

                        </div>



                        <ul class="media-list comment-list mt-30">

                            <li class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="avatar" src="<?= base_url('template/front_end'); ?>/img/blog/commenter-1.jpg" alt="Jonathon Doe">
                                    </a>
                                </div>

                                <div class="media-body">
                                    <div class="comment-info">
                                        <h4 class="author-name">Jonathon Doe</h4>

                                        <div class="comment-meta">
                                            <a class="comment-report-link" href="#">Report</a>
                                            <a class="comment-reply-link" href="#">Reply</a>
                                        </div>
                                    </div>

                                    <p>Risus et cubilia lacus vestibulum conubia parturient scelerisque tincidunt ac habitant libero duis sagittis vestibulum dolor venenatis.Nostra cras in dis in a dignissim est in per sem consectetur.</p>

                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="avatar" src="<?= base_url('template/front_end'); ?>/img/blog/commenter-3.jpg" alt="Natalie Portman">
                                            </a>
                                        </div>

                                        <div class="media-body">
                                            <div class="comment-info">
                                                <h4 class="author-name">Natalie Portman</h4>

                                                <div class="comment-meta">
                                                    <a class="comment-report-link" href="#">Report</a>
                                                    <a class="comment-reply-link" href="#">Reply</a>
                                                </div>
                                            </div>

                                            <p>Risus et cubilia lacus vestibulum conubia parturient scelerisque tincidunt ac habitant libero duis sagittis.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="avatar" src="<?= base_url('template/front_end'); ?>/img/blog/commenter-1.jpg" alt="Jonathon Doe">
                                            </a>
                                        </div>

                                        <div class="media-body">
                                            <div class="comment-info">
                                                <h4 class="author-name">Jonathon Doe</h4>

                                                <div class="comment-meta">
                                                    <a class="comment-report-link" href="#">Report</a>
                                                    <a class="comment-reply-link" href="#">Reply</a>
                                                </div>
                                            </div>

                                            <p>Risus et cubilia lacus vestibulum conubia parturient scelerisque tincidunt ac habitant libero duis sagittis.</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="media">

                                <div class="media-left">
                                    <a href="#">
                                        <img class="avatar" src="<?= base_url('template/front_end'); ?>/img/blog/commenter-2.jpg" alt="Michael Ethan">
                                    </a>
                                </div>

                                <div class="media-body">

                                    <div class="comment-info">
                                        <h4 class="author-name">Michael Ethan</h4>

                                        <div class="comment-meta">
                                            <a class="comment-report-link" href="#">Report</a>
                                            <a class="comment-reply-link" href="#">Reply</a>
                                        </div>
                                    </div>

                                    <p>Risus et cubilia lacus vestibulum conubia parturient scelerisque tincidunt ac habitant libero duis sagittis.</p>

                                </div>
                            </li>
                        </ul>

                    </div> -->
                </div><!-- /.posts-content -->
            </div><!-- /.col-md-12 -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
<!-- blog section end -->