<!--page title start-->
<section class="page-title ptb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Artikel</h2>
                <ol class="breadcrumb">
                    <li><a href="#">Artikel</a></li>
                    <li class="active">Artikel & Berita</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!--page title end-->


<!-- Grid News -->
<section class="section-padding default-blog grid-blog">
    <div class="container">

        <div class="row">
            <?php foreach ($GetData as $key => $value) {?>
            <div class="col-md-4">
                <article class="post-wrapper">
                    <div class="thumb-wrapper">
                        <img src="<?= $value['gambar_berita'];?>" class="img-responsive" alt="">                       
                        <span class="post-comments-number">
                            <i class="fa fa-comments"></i><a href="<?= base_url('artikel/detail_artikel'); ?>">25</a>
                        </span>
                    </div><!-- .post-thumb -->
                    <div class="entry-content">
                        <h2 class="entry-title"><a href="<?= base_url('artikel/detail_artikel/'.$value['id_berita']); ?>"><?= $value['judul_berita'] ?></a></h2>
                        <?= $value['isi_berita'];?>
                    </div><!-- .entry-content -->

                </article><!-- /.post-wrapper -->
            </div><!-- /.col-md-4 -->
            <?php }?>
            

        </div>

        <ul class="pagination post-pagination text-center mt-50">
            <li><a href="#." class="waves-effect waves-light"><i class="fa fa-angle-left"></i></a></li>
            <li><span class="current waves-effect waves-light">1</span></li>
            <li><a href="#." class="waves-effect waves-light">2</a></li>
            <li><a href="#." class="waves-effect waves-light"><i class="fa fa-angle-right"></i></a></li>
        </ul>


    </div><!-- /.container -->
</section>
<!-- Grid News End -->