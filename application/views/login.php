<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Login - Sistem Informasi Hunian Tetap</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="Mannatthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="<?php echo base_url(); ?>template/assets/images/favicon.png">

    <link href="<?php echo base_url();?>template/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>template/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>template/assets/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>


    <!-- Begin page -->
    <div class="accountbg"></div>
    <div class="wrapper-page">

        <div class="card">
            <div class="card-body">
                <?= alert(); ?>
                <div class="text-center">
                    <a href="<?php echo base_url(); ?>" class="logo logo-admin"><img src="<?php echo base_url();?>template/assets/images/logo_warna.png" height="60" alt="logo"></a>
                </div>

                <div class="px-3 pb-3">
                    <form class="form-horizontal m-t-20" method="POST" action="<?php echo base_url('auth/proses_login'); ?>">

                        <div class="form-group row">
                            <div class="col-12">
                                <input class="form-control" type="text" name="username" required="" placeholder="Username" minlength="4">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <input class="form-control" type="password" name="password" required="" placeholder="Password" minlength="6">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">Remember me</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group text-center row m-t-20">
                            <div class="col-12">
                                <button class="btn btn-danger btn-block waves-effect waves-light" type="submit" name="btn_login">Log In</button>
                            </div>
                        </div>

                        <div class="form-group m-t-10 mb-0 row">
                            <div class="col-sm-7 m-t-20">
                                <a href="pages-recoverpw.html" class="text-muted"><i class="mdi mdi-lock"></i> <small>Lupa kata sandi ?</small></a>
                            </div>
                            <div class="col-sm-5 m-t-20">
                                <a href="pages-register.html" class="text-muted"><i class="mdi mdi-account-circle"></i> <small>Buat akun wilayah ?</small></a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>



    <!-- jQuery  -->
    <script src="<?php echo base_url();?>template/assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>template/assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>template/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>template/assets/js/modernizr.min.js"></script>
    <script src="<?php echo base_url();?>template/assets/js/detect.js"></script>
    <script src="<?php echo base_url();?>template/assets/js/fastclick.js"></script>
    <script src="<?php echo base_url();?>template/assets/js/jquery.blockUI.js"></script>
    <script src="<?php echo base_url();?>template/assets/js/waves.js"></script>
    <script src="<?php echo base_url();?>template/assets/js/jquery.nicescroll.js"></script>

    <!-- App js -->
    <script src="<?php echo base_url();?>template/assets/js/app.js"></script>

</body>

</html>