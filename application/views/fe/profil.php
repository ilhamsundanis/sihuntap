    <!-- ========================
       page title 
    =========================== -->
    <section class="page-title page-title-layout1 bg-overlay bg-parallax">
      <div class="bg-img"><img src="<?php echo base_url();?>template/fe/assets/images/banner_1.jpg" alt="background"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <span class="pagetitle__subheading">Cepat, Tepat, Akurat dan Mantap.</span>
            <h1 class="pagetitle__heading">Memberikan Hunian Tetap yang Layak</h1>
          </div><!-- /.col-xl-6 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.page-title -->

    
     <!-- =========================
       Banner layout 1
      =========================== -->
    <section class="banner-layout1 pb-100 bg-secondary">
      <div class="container-fluid col-padding-0">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 background-banner">
            <div class="bg-img">
              <img src="<?php echo base_url();?>template/fe/assets/images/banners/1.jpg" alt="banner">
            </div>
            <div class="cta-banner">
              <h4 class="cta-banner__subtitle ">40</h4>
              <h5 class="cta-banner__title ">Tahun Membangun </h5>
              <a href="#" class="btn btn__primary btn__link"><i class="icon-arrow-right1"></i></a>
            </div><!-- /.cta-banner -->
            <div class="video__box">
              <a class="video__btn video__btn-white popup-video" href="https://www.youtube.com/watch?v=DkPcvj99OrI">
                <div class="video__player">
                  <span class="video__player-animation"></span>
                  <span class="video__player-animation video__player-animation-2"></span>
                  <i class="fa fa-play"></i>
                </div>
              </a>
              <div class="video__box-text">
                <span class="color-white">Sihuntap di Visual!</span>
              </div>
            </div><!-- /.video__box -->
          </div><!-- /.col-xl-6 -->
          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="inner-padding py-0">
              <div class="heading-layout2 heading-light mb-40">
                <h3 class="heading__title">Hadir untuk membangun hunian yang layak untuk masyarakat.</h3>
                <p class="heading__desc">Sebagai manajemen konstruksi yang terdiversifikasi, desain-bangun, dan kontraktor umum
                  perusahaan.
                </p>
              </div><!-- /.heading -->
              <div class="d-flex flex-wrap justify-content-between fancybox-wrapper">
                <!-- fancybox item #1 -->
                <div class="fancybox-item">
                  <div class="fancybox-item__icon">
                    <i class="icon-measurement"></i>
                  </div><!-- /.fancybox-icon -->
                  <div class="fancybox-item__content">
                    <h4 class="fancybox-item__title">Penanganan Cepat dan Tepat</h4>
                  </div><!-- /.fancybox-content -->
                </div><!-- /.fancybox-item -->
                <!-- fancybox item #2 -->
                <div class="fancybox-item">
                  <div class="fancybox-item__icon">
                    <i class="icon-gloves"></i>
                  </div><!-- /.fancybox-icon -->
                  <div class="fancybox-item__content">
                    <h4 class="fancybox-item__title">Bekerja Bersama</h4>
                  </div><!-- /.fancybox-content -->
                </div><!-- /.col-lg-4 -->
                <!-- fancybox item #3 -->
                <div class="fancybox-item">
                  <div class="fancybox-item__icon">
                    <i class="icon-ladder"></i>
                  </div><!-- /.fancybox-icon -->
                  <div class="fancybox-item__content">
                    <h4 class="fancybox-item__title">Tata Kelola Pembangunan</h4>
                  </div><!-- /.fancybox-content -->
                </div><!-- /.fancybox-item -->
              </div><!-- /.row -->
              <p class="mb-20">Sistem Informasi Hunian Tetap (SIHUNTAP) adalah perwujudan Sistem Pemerintahan Berbasis Elektronik (SPBE) pada DPKPP Kabupaten Bogor yang dirancang untuk menyediakan sistem informasi tata kelola Hunian Tetap yang terpadu dan mendukung proses bisnis pengelolaan hunian tetap mulai dari tahap pengusulan, monitoring pelaksanaan pembangunan dan sebaran lokasinya, hingga akhirnya pembangunan hunian tetap tersebut dihuni dan/atau diserahterimakan ke calon penerima manfaat.
              </p>
              <p>
                  Dalam prakteknya, secara sistem SIHUNTAP data dilakukan oleh sistem backend yang saling terintegrasi. Prinsip dasarnya adalah memberikan kemudahan bagi pengguna dalam layanan satu pintu, sehingga SIHUNTAP dapat difungsikan sebagai satu-satunya portal pintu masuk yang dapat diakses oleh calon pengusul dalam menyampaikan usulan pembangunan Hunian Tetap. 
              </p>
            </div>
          </div><!-- /.col-xl-6 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.Banner layout 1 -->
