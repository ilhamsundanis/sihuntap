  <div>
    <section class="text-content-section pb-90" style="padding-top: 20px;">
      <h3 style="text-align: center;">Pemetaan Hunian Tetap</h3>
      <div id="map" style="width:100%;height:780px;"></div>
    </section>
  </div>
  <script src="http://maps.google.com/maps/api/js?key=AIzaSyAEP1L4EzuNYJR83hokTaPEqC8AXoklxmM" type="text/javascript"></script>
  <script type="text/javascript">
    var locations = [
      <?php foreach ($get_maps as $idx => $map) { ?>['<?php echo $map["name"]; ?><br>Nama Kecamatan : ', <?php echo $map['latlon']; ?>, <?= $idx + 1; ?>],
      <?php } ?>
      //   ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 11,
      center: new google.maps.LatLng(-6.5911, 106.8056),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>