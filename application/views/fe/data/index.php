<script src="http://maps.googleapis.com/maps/api/js"></script>
<script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDR5qarJ7U46r2roVyqF5VENWNGwtpvKQ">
</script>
<script>
function initialize() {
  var propertiPeta = {
    center:new google.maps.LatLng(-8.5830695,116.3202515),
    zoom:9,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  
  var peta = new google.maps.Map(document.getElementById("googleMap"), propertiPeta);
  
  // membuat Marker
  var marker=new google.maps.Marker({
      position: new google.maps.LatLng(-8.5830695,116.3202515),
      map: peta
  });

}

// event jendela di-load  
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div id="appContent">
    
    <section class="page-title page-title-layout7 bg-overlay bg-parallax text-center">
      <div class="bg-img"><img src="<?php echo base_url();?>assets/images/page-titles/3.jpg" alt="background"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 offset-xl-3">
            <h1 class="pagetitle__heading mb-0">Data Statistik</h1>
          </div><!-- /.col-xl-6 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.page-title -->

    <section class="text-content-section pb-90">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <h5 class="text__block-title">Berdasarkan Kecamatan</h5>
                    <table class="table">
                        <thead>
                            <th>No</th>
                            <th>Kecamatan</th>
                            <th>Jumlah Penerima</th>
                            <th>Detail</th>
                        </thead>
                        <tbody>
                            <tr v-for="(data, index) in list">
                                <td>{{index + 1}}</td>
                                <td>{{data.name}}</td>
                                <td style="text-align: center;">{{data.total_recipient}}</td>
                                <td><a :href="'<?php echo base_url('data/detail/');?>'+data.id" class="btn-download">Detail</a></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <th colspan="2">Total Jumlah</th>
                            <th>{{total}}</th>
                            <th></th>
                        </tfoot>
                    </table>
                    <div id="googleMap" style="width:100%;height:380px;"></div>
                </div>
            </div>
        </div>
    </section>
</div>


<script>
    var appContent = new Vue({
        el: '#appContent',
        data: {
            total: "",
            list: [],
        },
        mounted: function () {
            this.retrive();
        },
        methods: {    
            retrive() {
                
                axios("<?php echo base_url(); ?>api/content/statistic").then(response => {
                    this.list = response.data.data;
                    this.total = response.data.total;
                });
            },
        }
    });
</script>