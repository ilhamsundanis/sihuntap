
<div id="appContent">
    
    <section class="page-title page-title-layout7 bg-overlay bg-parallax text-center">
      <div class="bg-img"><img src="<?php echo base_url();?>assets/images/page-titles/3.jpg" alt="background"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 offset-xl-3">
            <h1 class="pagetitle__heading mb-0">Data Statistik</h1>
          </div><!-- /.col-xl-6 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.page-title -->

    <section class="text-content-section pb-90">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <h5 class="text__block-title">Berdasarkan Desa/Kelurahan</h5>
                    <table class="table">
                        <thead>
                            <th>No</th>
                            <th>Kecamatan</th>
                            <th>Desa / Kelurahan</th>
                            <th>Jumlah Penerima</th>
                        </thead>
                        <tbody>
                            <tr v-for="(data, index) in list">
                                <td>{{index + 1}}</td>
                                <td>{{data.master_district_name}}</td>
                                <td>{{data.name}}</td>
                                <td>{{data.total_recipient}}</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <th colspan="3">Total Jumlah</th>
                            <th>{{total}}</th>
                            <th></th>
                        </tfoot>
                    </table>
                    <div id="googleMap" style="width:100%;height:380px;"></div>
                </div>
            </div>
        </div>
    </section>
</div>


<script>
    var appContent = new Vue({
        el: '#appContent',
        data: {
            id: '<?php echo $id;?>',
            total: "",
            list: [],
        },
        mounted: function () {
            this.retrive();
        },
        methods: {    
            retrive() {
                
                axios("<?php echo base_url(); ?>api/content/statistic/"+ this.id).then(response => {
                    this.list = response.data.data;
                    this.total = response.data.total;
                });
            },
        }
    });
</script>