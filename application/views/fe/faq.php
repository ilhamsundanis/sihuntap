<div id="appContent">
    <section class="page-title page-title-layout5 bg-overlay bg-parallax text-center">
      <div class="bg-img"><img src="<?php echo base_url();?>template/fe/assets/images/banner_5.jpg" alt="background"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 offset-xl-3">
            <h1 class="pagetitle__heading mb-0">Layanan Bantuan SIHUNTAP </h1>
          </div><!-- /.col-xl-6 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.page-title -->

    <!-- ======================
       FAQ
    ========================= -->
    <section class="faq pt-100 pb-70">
      <div class="container">
        <div class="row" id="accordion">
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="accordion-item" v-for="(data, index) in list_faq">
              <div class="accordion-item__header" data-toggle="collapse" :data-target="'#collapse'+index">
                <a class="accordion-item__title">{{ data.question }}?</a>
              </div><!-- /.accordion-item-header -->
              <div :id="'collapse'+index" class="collapse" data-parent="#accordion">
                <div class="accordion-item__body">
                  <p>{{ data.answer }}</p>
                </div><!-- /.accordion-item-body -->
              </div>
            </div><!-- /.accordion-item -->

            
          </div><!-- /.col-lg-6 -->
          
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.FAQ -->

</div>
<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'paginate': VuejsPaginate,
        },
        data: {
            filter: {
                text_search: "",
            },

            list_faq: [],
            currentPage: <?php echo empty($this->uri->segment(4)) ? 1 : $this->uri->segment(4);?>,
            totalPage: 1,

            content_faq: [],
        },
        mounted: function () {
            this.retrive();

            axios("<?php echo base_url(); ?>api/content/faq").then(response => {
                this.content_faq = response.data.data;
			});
        },
        methods: {    
            clickCallback(pageNum) {
                window.location.href = "<?php echo base_url(); ?>content/faq/" + pageNum;
            },
            retrive() {
                var query = "?orderBy=id&sort=desc&limit=10&page=" + this.currentPage;
                <?php if ( !empty($get['ct']) ) { ?>
                    query += "&master_queue_id=<?php echo $get['ct']; ?>";
                <?php } ?>
                
                axios("<?php echo base_url(); ?>api/admin/page_faq" + query).then(response => {
                    this.list_faq = response.data.data;
                    this.totalPage = response.data.meta.totalPage;
                });
            },
            search() {
                window.location.href = "<?php echo base_url(); ?>content/faq/1?s=" + this.filter.text_search;
            }
        }
    });
</script>