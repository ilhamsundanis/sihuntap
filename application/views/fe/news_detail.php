<div id="appContent">
    <!-- ======================
      Blog Single
    ========================= -->
    <section class="blog blog-single pt-100 pb-40">
      <div class="container">
        <div class="row">
          
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="post-item mb-0">
              <div class="post-item__meta d-flex align-items-center">
                <div class="post-item__meta__cat">
                  <a href="#">{{data.master_category_news_name}}</a>
                </div><!-- /.blog-meta-cat -->
                <span class="post-item__meta__author">By: {{data.users_name}}</span>
                <span class="post-item__meta__date">{{data.created_at}}</span>
              </div><!-- /.blog-meta -->
              <h1 class="post-item__title">{{data.title}}</h1>
              <div class="post-item__img">
                <a href="#">
                  <img :src="data.image" :alt="data.title">
                </a>
              </div><!-- /.entry-img -->
              <div class="post-item__content">
                <div class="post-item__desc">
                  <p v-html="data.description"></p>
                </div><!-- /.blog-desc -->
              </div><!-- /.entry-content -->
            </div><!-- /.post-item -->
            

            
          </div><!-- /.col-lg-8 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.blog Single -->
</div>
<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'paginate': VuejsPaginate,
            'v-select': VueSelect.VueSelect,
        },
        data: {
            loading: false,
            id: '<?php echo $id;?>',          
            data: {},
            
        },
        mounted: function () {
         
            
            this.retrive();
        },
        methods: {
            
            retrive()
            {
                // var query = "?orderBy=id&sort=desc&limit=30&page=" + this.currentPage;
                // query += "&id=" + this.filter.id;
                // query += "&name=" + this.filter.name;
                // query += "&status=" + this.filter.status;
                // query += "&ktp_number=" + this.filter.ktp_number;
                axios("<?php echo base_url(); ?>api/admin/page_news/detail/" + this.id).then(response => {
                    this.data      = response.data.data;
                });
                
            },
           
            
        }
    });
</script>