    <!-- ============================
        Slider
    ============================== -->
    <div id="appContent">
      <section class="slider slider-layout1">
        <div class="slick-carousel carousel-arrows-light carousel-dots-light m-slides-0" data-slick='{"slidesToShow": 1, "arrows": true, "dots": true, "speed": 700,"fade": true,"cssEase": "linear"}'>
          <?php foreach ($GetSlider as $data) { ?>
            <div class="slide-item align-v-h bg-overlay">
              <div class="bg-img"><img src="<?php echo $data['image']; ?>" alt="<?php echo $data['name']; ?>"></div>
              <div class="container">
                <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                    <div class="slide__content">
                      <h2 class="slide__title"><?php echo $data['name']; ?></h2>
                      <p class="slide__desc"><?php echo $data['description']; ?> </p>

                    </div><!-- /.slide-content -->
                  </div><!-- /.col-xl-8 -->
                </div><!-- /.row -->
              </div><!-- /.container -->
            </div><!-- /.slide-item -->
          <?php } ?>


        </div><!-- /.carousel -->
      </section><!-- /.slider -->

      <!-- ========================
      About Layout 2
    =========================== -->
      <section class="about-layout2 pb-50">
        <div class="container">
          <div class="row mb-50 align-items-center">
            <div class="col-sm-12 col-md-12 col-lg-5">
              <div class="heading-layout2 mb-20">
                <h2 class="heading__subtitle">Selamat Datang di Web Bidang Perumahan</h2>
                <h3 class="heading__title">DPKPP Kabupaten Bogor Berkomitmen Untuk Membangun Bogor!
                </h3>
              </div><!-- /heading -->
            </div><!-- /.col-lg-6 -->
            <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-1 d-flex justify-content-end counters-wrapper">
              <div class="row">
                <!-- counter item #1 -->
                <div class="counter-item">
                  <h4 class="counter"><?php echo total_data(4); ?></h4>
                  <p class="counter-item__desc">Hunian Tetap</p>
                </div>
                <!-- counter item #2 -->
                <div class="counter-item">
                  <h4 class="counter"><?php echo total_data(3); ?></h4>
                  <p class="counter-item__desc">Hunian Sementara</p>
                </div>
                <div class="counter-item">
                  <h4 class="counter"><?php echo total_penerima(); ?></h4>
                  <p class="counter-item__desc">Terverifikasi</p>
                </div>
              </div>
              <div class="row">
                <!-- counter item #1 -->
                <div class="counter-item">
                  <h4 class="counter"><?php echo total_data(4); ?></h4>
                  <p class="counter-item__desc">Rehabilitasi</p>
                </div>
                <!-- counter item #2 -->
                <div class="counter-item">
                  <h4 class="counter"><?php echo total_data(3); ?></h4>
                  <p class="counter-item__desc">Sewa Rumah</p>
                </div>
                <div class="counter-item">
                  <h4 class="counter"><?php echo total_penerima(); ?></h4>
                  <p class="counter-item__desc">Belum Di Verifikasi</p>
                </div>
              </div>
              <div class="row">
                <!-- counter item #1 -->
                <div class="counter-item">
                  <h4 class="counter"><?php echo total_data(4); ?></h4>
                  <p class="counter-item__desc">Relokasi Mandiri</p>
                </div>
                <!-- counter item #2 -->
                <div class="counter-item">
                  <h4 class="counter"><?php echo total_data(3); ?></h4>
                  <p class="counter-item__desc">Lokasi/Wilayah</p>
                </div>
                <div class="counter-item">
                  <h4 class="counter"><?php echo total_penerima(); ?></h4>
                  <p class="counter-item__desc">Total Usulan</p>
                </div>
              </div>
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
          <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6">
              <p class="heading__desc mb-30" style="text-align:justify;">Sistem Informasi Hunian Tetap (SIHUNTAP) adalah perwujudan Sistem Pemerintahan Berbasis Elektronik (SPBE) pada DPKPP Kabupaten Bogor yang dirancang untuk menyediakan sistem informasi tata kelola Hunian Tetap yang terpadu dan mendukung proses bisnis pengelolaan hunian tetap mulai dari tahap pengusulan, monitoring pelaksanaan pembangunan dan sebaran lokasinya, hingga akhirnya pembangunan hunian tetap tersebut dihuni dan/atau diserahterimakan ke calon penerima manfaat..</p>
              <ul class="list-items list-unstyled mb-20 mt-40">
                <li>Penyediaan Rumah Yang Layak</li>
                <li>Desain Rumah Minimalis</li>
                <li>Fasilitas Umum Lengkap</li>
                <li>Rumah Produktif</li>
                <li>Lingkungan Yang Sehat dan Asri</li>
                <li>Professional dan Berkualitas</li>
                <li>Bangunan Tahan Gempa</li>
                <li>Sistem Kontrol Yang Baik</li>
              </ul>
              <a href="<?php echo base_url(); ?>" class="btn btn__primary btn__lg mb-30">
                <i class="icon-arrow-right1"></i><span>Tentang Lainnya</span>
              </a>
            </div><!-- /.col-lg-6 -->
            <div class="col-sm-12 col-md-12 col-lg-6">
              <div class="about__img">
                <img src="<?php echo base_url(); ?>template/fe/assets/images/deco-section-1.svg" alt="about" class="img-fluid">
                <div class="cta-banner">

                </div><!-- /.cta-banner -->
              </div><!-- /.about-img -->
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
      </section><!-- /.About Layout 2 -->
      <section class="banner-layout1 pb-100 bg-secondary">
        <div class="container-fluid col-padding-0">
          <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 background-banner">
              <div class="bg-img">
                <img src="<?php echo base_url(); ?>template/fe/assets/images/banners/1.jpg" alt="banner">
              </div>
              <div class="cta-banner">
                <h4 class="cta-banner__subtitle "></h4>
                <h5 class="cta-banner__title "></h5>
                <a href="#" class="btn btn__primary btn__link"><i class="icon-arrow-right1"></i></a>
              </div><!-- /.cta-banner -->
              <div class="video__box">
                <a class="video__btn video__btn-white popup-video" href="https://www.youtube.com/watch?v=noGkN2Mu-Zg">
                  <div class="video__player">
                    <span class="video__player-animation"></span>
                    <span class="video__player-animation video__player-animation-2"></span>
                    <i class="fa fa-play"></i>
                  </div>
                </a>
                <div class="video__box-text">
                  <span class="color-white">DPKPP On Video!</span>
                </div>
              </div><!-- /.video__box -->
            </div><!-- /.col-xl-6 -->
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
              <div class="inner-padding py-0">
                <div class="heading-layout2 heading-light mb-40">
                  <h3 class="heading__title">We Help You Build On Your Past And Prepare For The Future.</h3>
                  <p class="heading__desc">As a diversified construction management, design-build, and general contracting
                    firm, Eteon Group is recognized as one of the World's leading Industry and Manufacturing Corporation!
                  </p>
                </div><!-- /.heading -->
                <div class="d-flex flex-wrap justify-content-between fancybox-wrapper">
                  <!-- fancybox item #1 -->
                  <div class="fancybox-item">
                    <div class="fancybox-item__icon">
                      <i class="icon-measurement"></i>
                    </div><!-- /.fancybox-icon -->
                    <div class="fancybox-item__content">
                      <h4 class="fancybox-item__title">Perencanaan dan Pengembangan Perumahan</h4>
                    </div><!-- /.fancybox-content -->
                  </div><!-- /.fancybox-item -->
                  <!-- fancybox item #2 -->
                  <div class="fancybox-item">
                    <div class="fancybox-item__icon">
                      <i class="icon-gloves"></i>
                    </div><!-- /.fancybox-icon -->
                    <div class="fancybox-item__content">
                      <h4 class="fancybox-item__title">Pembangunan Perumahan</h4>
                    </div><!-- /.fancybox-content -->
                  </div><!-- /.col-lg-4 -->
                  <!-- fancybox item #3 -->
                  <div class="fancybox-item">
                    <div class="fancybox-item__icon">
                      <i class="icon-ladder"></i>
                    </div><!-- /.fancybox-icon -->
                    <div class="fancybox-item__content">
                      <h4 class="fancybox-item__title">Pemanfaatan dan Pengendalian Perumahan</h4>
                    </div><!-- /.fancybox-content -->
                  </div><!-- /.fancybox-item -->
                </div><!-- /.row -->
                <p class="color-white mb-20">Yet those that embrace change are thriving, building bigger, better, faster,
                  and stronger products than
                  ever before. You are helping to lead the charge; we can help you build on your past and prepare future.
                </p>
                <!-- <img src="<?php echo base_url(); ?>template/fe/assets/images/about/singnture.png" alt="singnture"> -->
              </div>
            </div><!-- /.col-xl-6 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
      </section><!-- /.Banner layout 1 -->
      <!-- ======================
     Work Process 
    ========================= -->
      <section class="work-process">
        <div class="bg-img"><img src="<?php echo base_url(); ?>template/fe/assets/images/backgrounds/1.jpg" alt="background"></div>
        <div class="container">
          <div class="row heading heading-layout3 mb-80">
            <div class="col-sm-12 col-md-12 col-lg-6">
              <h2 class="heading__subtitle">Bagaimana sistem bekerja!!</h2>
            </div><!-- /.col-lg-6 -->
            <div class="col-sm-12 col-md-12 col-lg-6">
              <h3 class="heading__title color-white">Manajemen Sistem Informasi Hunian Tetap</h3>
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
          <div class="row process-items-wrapper">
            <!-- process Item #1 -->
            <div class="col-sm-6 col-md-6 col-lg-3">
              <div class="process-item">
                <div class="process-item__icon">
                  <i class="icon-planning"></i>
                </div><!-- /.process-item__icon -->
                <h4 class="process-item__title">Pendataan dan Usulan dari Desa.</h4>
                <p class="process-item__desc">01. Permohonan</p>
              </div><!-- /.process-item -->
            </div><!-- /.col-lg-3 -->
            <!-- process Item #2 -->
            <div class="col-sm-6 col-md-6 col-lg-3">
              <div class="process-item">
                <div class="process-item__icon">
                  <i class="icon-barrier1"></i>
                </div><!-- /.process-item__icon -->
                <h4 class="process-item__title">Verifikasi oleh Kecamatan dan BPBD.</h4>
                <p class="process-item__desc">02. Verifikasi</p>
              </div><!-- /.process-item -->
            </div><!-- /.col-lg-3 -->
            <!-- process Item #3 -->
            <div class="col-sm-6 col-md-6 col-lg-3">
              <div class="process-item">
                <div class="process-item__icon">
                  <i class="icon-concrete-mixer-1"></i>
                </div><!-- /.process-item__icon -->
                <h4 class="process-item__title">Di Relokasi Ke Hunian Sementara</h4>
                <p class="process-item__desc">03. Relokasi Huntara</p>
              </div><!-- /.process-item -->
            </div><!-- /.col-lg-3 -->
            <!-- process Item #4 -->
            <div class="col-sm-6 col-md-6 col-lg-3">
              <div class="process-item">
                <div class="process-item__icon">
                  <i class="icon-pantone"></i>
                </div><!-- /.process-item__icon -->
                <h4 class="process-item__title">Di Relokasi Penempatan di Hunian Tetap</h4>
                <p class="process-item__desc">04. Penempatan Hunian tetap</p>
              </div><!-- /.process-item -->
            </div><!-- /.col-lg-3 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
      </section><!-- /.Work Process -->

      <!-- ======================
      Blog carousel
    ========================= -->
      <section class="blog-carousel pt-0 pb-50 mt--210 z-index-2">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 heading-wrapper d-flex justify-content-between mb-10">
              <div class="heading heading-light">
                <h2 class="heading__title">Artikel Berita</h2>
              </div><!-- /.heading -->
              <a href="<?php echo base_url('news'); ?>" class="btn btn__white btn__bordered btn__explore">Lihat Semua</a>
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
          <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
              <div class="slick-carousel" data-slick='{"slidesToShow": 3, "slidesToScroll": 3, "arrows": true, "dots": false, "responsive": [ {"breakpoint": 992, "settings": {"slidesToShow": 2}}, {"breakpoint": 767, "settings": {"slidesToShow": 2}}, {"breakpoint": 480, "settings": {"slidesToShow": 1}}]}'>

                <?php foreach ($GetNews as $key => $val) { ?>
                  <div class="post-item">
                    <div class="post-item__img ">
                      <a href="<?php echo base_url('news/detail/') . $val["slug"]; ?>">
                        <img src="<?php echo $val['image']; ?>" height="240px;" alt="<?php echo $val['title']; ?>">
                      </a>
                      <div class="post-item__meta__cat">
                        <a href="#"></a>
                      </div><!-- /.blog-meta-cat -->
                    </div><!-- /.blog-img -->
                    <div class="post-item__content">
                      <span class="post-item__meta__date"><?php echo date('d-m-Y H:i:s', strtotime($val['created_at'])); ?></span>
                      <h4 class="post-item__title"><a href="<?php echo base_url('news/detail/') . $val["slug"]; ?>"><?php echo $val['title']; ?></a>
                      </h4>
                      </p>
                      <a href="<?php echo base_url('news/detail/') . $val["slug"]; ?>" class="btn btn__secondary btn__link">
                        <i class="icon-arrow-right1"></i>
                        <span>Selengkapnya</span>
                      </a>
                    </div><!-- /.blog-content -->
                  </div>
                <?php } ?>



              </div><!-- /.carousel -->
            </div><!-- /.col-lg-12 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
      </section><!-- /.blog carousel -->


    </div>


    <script>
      var appContent = new Vue({
        el: '#appContent',
        filters: {
          capitalize: function(value) {
            if (!value) return ''
            value = value.toString()
            return value.toLowerCase().replace(/(?:^|\s|-)\S/g, x => x.toUpperCase());
          }
        },
        data: {
          homepage: {},
          list_jobs: [],
          banner: []
        },
        mounted: function() {
          this.retrive();
          this.retrive_banner();
        },
        methods: {
          titleCase(value) {
            return value.toLowerCase().replace(/(?:^|\s|-)\S/g, x => x.toUpperCase());
          },
          retrive() {
            axios("<?php echo base_url(); ?>api/homepage").then(response => {
              this.homepage = response.data.data;
            });
          },

          retrive_banner() {
            this.loading = true;

            // var query = "?limit=8&page=1&orderBy=id&sort=desc";
            axios("<?php echo base_url(); ?>api/admin/page_slider").then(response => {
              this.banner = response.data.data;
              this.loading = false;
            });
          },
        }
      });
    </script>