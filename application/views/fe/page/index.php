<div id="appContent">
    
    <section class="page-title page-title-layout7 bg-overlay bg-parallax text-center">
      <div class="bg-img"><img src="<?php echo base_url();?>assets/images/page-titles/3.jpg" alt="background"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 offset-xl-3">
            <h1 class="pagetitle__heading mb-0">{{ data.title }}</h1>
          </div><!-- /.col-xl-6 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.page-title -->

    <section class="text-content-section pb-90">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <h5 class="text__block-title">{{ data.title }}</h5>
                    <p class="text__block-desc" v-html="data.content"></p>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    var appContent = new Vue({
        el: '#appContent',
        data: {
            id: '<?php echo $id; ?>',
            data: {},
        },
        mounted: function () {
            this.retrive();
        },
        methods: {    
            retrive() {
                var query = "?slug=" + this.id;
                axios("<?php echo base_url(); ?>api/admin/page_content/findBy/" + query).then(response => {
                    this.data = response.data.data;
                });
            },
        }
    });
</script>