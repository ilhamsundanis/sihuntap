<div id="appContent">
<section class="careers">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
            <div class="heading text-center mb-50">
              <!-- <h2 class="heading__subtitle">Publikasi Dokumen.</h2> -->
              <h3 class="heading__title">Download Dokumen</h3>
            </div><!-- /.heading -->
          </div><!-- /.col-lg-10 -->
        </div><!-- /.row -->
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="job-items-wrapper">
                
              <div class="job-item" v-for="(data, index) in list_document">
                <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-4">
                    <div class="job-item__meta">
                      <span class="job-item__type">{{ data.master_category_document_name }}</span>
                    </div>
                    <h4 class="job-item__title">{{ data.name }}</h4>
                  </div><!-- /.col-lg-4 -->
                  <div class="col-sm-12 col-md-12 col-lg-5">
                    <p class="job-item__desc">{{ data.description }}</p>
                  </div><!-- /.col-lg-5 -->
                  <div class="col-sm-12 col-md-12 col-lg-3 d-flex align-items-center justify-content-end btn-wrap">
                    <a :href="data.file" target="_blank" class="btn btn__primary"><i class="fa fa-download"></i> Download</a>
                  </div><!-- /.col-lg-3 -->
                </div><!-- /.row -->
              </div><!-- /.job-item -->

            </div><!-- /.job-items-wrapper -->
          </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.careers -->
</div>
    <script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'paginate': VuejsPaginate,
        },
        data: {
            filter: {
                text_search: "",
            },

            list_document: [],
            currentPage: <?php echo empty($this->uri->segment(4)) ? 1 : $this->uri->segment(4);?>,
            totalPage: 1,

            content_document: [],
        },
        mounted: function () {
            this.retrive();

            axios("<?php echo base_url(); ?>api/content/document").then(response => {
                this.content_document = response.data.data;
			});
        },
        methods: {    
            clickCallback(pageNum) {
                window.location.href = "<?php echo base_url(); ?>content/document/" + pageNum;
            },
            retrive() {
                var query = "?orderBy=id&sort=desc&limit=10&page=" + this.currentPage;
                <?php if ( !empty($get['ct']) ) { ?>
                    query += "&master_category_document_id=<?php echo $get['ct']; ?>";
                <?php } ?>
                <?php if ( !empty($get['s']) ) { ?>
                    query += "&title=<?php echo $get['s']; ?>";
                <?php } ?>
                axios("<?php echo base_url(); ?>api/admin/page_document" + query).then(response => {
                    this.list_document = response.data.data;
                    this.totalPage = response.data.meta.totalPage;
                });
            },
            search() {
                window.location.href = "<?php echo base_url(); ?>content/document/1?s=" + this.filter.text_search;
            }
        }
    });
</script>