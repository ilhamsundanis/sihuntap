 <!-- ========================
       page title 
    =========================== -->
<div id="appContent">
    <section class="page-title page-title-layout8 bg-overlay bg-parallax text-center">
      <div class="bg-img"><img src="<?php echo base_url();?>template/fe/assets/images/banner_3.jpg" alt="background"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 offset-xl-3">
            <h1 class="pagetitle__heading">Artikel Berita</h1>
            <nav>
              <ol class="breadcrumb justify-content-center mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Berita</li>
              </ol>
            </nav>
          </div><!-- /.col-xl-6 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.page-title -->

    <!-- ======================
      Blog Grid
    ========================= -->
    <section class="blog-grid">
      <div class="container">
        <div class="row">

          <div class="col-sm-12 col-md-6 col-lg-4" v-for="data in list">
            <div class="post-item">
              <div class="post-item__img">
                <a :href="'<?php echo base_url(); ?>news/detail/' + data.slug">
                  <img :src="data.image" :alt="data.title">
                </a>
                <div class="post-item__meta__cat">
                  <a href="#">Construction</a>
                </div><!-- /.blog-meta-cat -->
              </div><!-- /.blog-img -->
              <div class="post-item__content">
                <span class="post-item__meta__date">Jan 20, 2020</span>
                <h4 class="post-item__title"><a :href="'<?php echo base_url(); ?>news/detail/' + data.slug">{{data.title}}</a>
                </h4>

                <a :href="'<?php echo base_url(); ?>news/detail/' + data.slug" class="btn btn__secondary btn__link">
                  <i class="icon-arrow-right1"></i>
                  <span>Selengkapnya</span>
                </a>
              </div><!-- /.blog-content -->
            </div><!-- /.post-item -->
          </div>
          
         
        </div>
        <!-- <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 text-center">
            <nav class="pagination-area">
              <ul class="pagination justify-content-center">
                <li><a class="current" href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#"><i class="icon-arrow-right1"></i></a></li>
              </ul>
            </nav>
          </div>
        </div> -->

      </div>

    </section>
</div>
    <script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'paginate': VuejsPaginate,
            'v-select': VueSelect.VueSelect,
        },
        data: {
            loading: false,
                       
            list: [],
            currentPage: <?php echo empty($this->uri->segment(4)) ? 1 : $this->uri->segment(4);?>,     
            totalPage: 1,
        },
        mounted: function () {
         
            
            this.retrive();
        },
        methods: {
            clickCallback(pageNum) {
                window.location.href = "<?php echo base_url(); ?>news/index/" + pageNum;
            },    
            
            retrive()
            {
                var query = "?orderBy=id&sort=desc&limit=30&page=" + this.currentPage;
                // query += "&id=" + this.filter.id;
                // query += "&name=" + this.filter.name;
                // query += "&status=" + this.filter.status;
                // query += "&ktp_number=" + this.filter.ktp_number;
                axios("<?php echo base_url(); ?>api/admin/page_news" + query).then(response => {
                    this.list      = response.data.data;
                    this.totalPage = response.data.meta.totalPage;
                    this.loading   = false;
                });
                
            },
           
            
        }
    });
</script>