<div id="appContent">
    <section class="page-title" style="margin-top: 60px;">
        <div class="auto-container">
            <div class="title-outer">
                <h1>Grafik Jumlah Sudah bekerja / belum bekerja</h1>
                <ul class="page-breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
                    <li>Grafik Jumlah Sudah bekerja / belum bekerja</li>
                </ul> 
            </div>
        </div>
    </section>

    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row">
                <!--Content Side-->
                <div class="content-side col-lg-6 col-md-12 col-sm-12">
                    <div class="blog-grid">

                        <!-- News Block -->
                        <div class="news-block-two">
                            <div class="inner-box">
                                <canvas id="myChart" width="200" height="200"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content-side col-lg-6 col-md-12 col-sm-12">
                    <div class="blog-grid">

                        <!-- News Block -->
                        <div class="news-block-two">
                            <div class="inner-box">
                                <canvas id="myChart2" width="200" height="200"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>js/chart.min.js"></script>
<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
        },
        data: {
            chart: [],
        },
        mounted: function () {
            this.retrive();
        },
        methods: {    
            retrive() {
                axios("<?php echo base_url(); ?>api/chart/work_status").then(response => {
                    const ctx = document.getElementById('myChart').getContext('2d');
                    const myChart = new Chart(ctx, {
                        type: 'pie',
                        data: response.data.data,
                        options: {
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                        }
                    });

                    const ctx2 = document.getElementById('myChart2').getContext('2d');
                    const myChart2 = new Chart(ctx2, {
                        type: 'bar',
                        data: response.data.data,
                        options: {
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                        }
                    });

                });
            },
        }
    });
</script>