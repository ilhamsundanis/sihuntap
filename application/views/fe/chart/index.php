<div id="appContent">
    <section class="page-title page-title-layout7 bg-overlay bg-parallax text-center">
      <div class="bg-img">
          <!-- <img src="<?php echo base_url();?>assets/images/page-titles/3.jpg" alt="background"> -->
        </div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 offset-xl-3">
            <h1 class="pagetitle__heading mb-0">Data Statistik</h1>
          </div><!-- /.col-xl-6 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.page-title -->

    <div class="text-content-section pb-90">
        <div class="container">
            <div class="row">
                <!--Content Side-->
                <div class="content-side col-lg-12 col-md-12 col-sm-12">
                    <div class="blog-grid">
                        <div class="news-block-two">
                            <div class="inner-box">
                                <canvas id="myChart" width="200" height="200"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="content-side col-lg-6 col-md-12 col-sm-12">
                    <div class="blog-grid">
                        <div class="news-block-two">
                            <div class="inner-box">
                                <canvas id="myChart2" width="200" height="200"></canvas>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>js/chart.min.js"></script>
<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
        },
        data: {
            chart: [],
        },
        mounted: function () {
            this.retrive();
        },
        methods: {    
            retrive() {
                axios("<?php echo base_url(); ?>api/chart").then(response => {
                    const ctx = document.getElementById('myChart').getContext('2d');
                    const myChart = new Chart(ctx, {
                        type: 'bar',
                        data: response.data.data,
                        options: {
                            indexAxis: 'y',
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                        }
                    });

                    // const ctx2 = document.getElementById('myChart2').getContext('2d');
                    // const myChart2 = new Chart(ctx2, {
                    //     type: 'bar',
                    //     data: response.data.data,
                    //     options: {
                    //         indexAxis: 'y',
                    //         scales: {
                    //             y: {
                    //                 beginAtZero: true
                    //             }
                    //         }
                    //     }
                    // });

                });
            },
        }
    });
</script>