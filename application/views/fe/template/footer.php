
    <!-- ========================
      Footer
    ========================== -->
    <footer class="footer">
      <div class="footer-contact">
        <div class="container">
          <div class="row">
            <div class="offset-lg-4 col-sm-12 col-md-12 col-lg-8 d-flex flex-wrap justify-content-between">
              <div class="footer-contact__item d-flex align-items-center">
                <div class="footer-contact__icon">
                  <i class="icon-phone1"></i>
                </div><!-- /.footer-contact__icon -->
                <div class="footer-contact__text">
                  <span>Call Center:</span>
                  <strong><a href="tel:<?php echo load_apps()["phone_number"];?>"><?php echo load_apps()["phone_number"];?></a></strong>
                </div><!-- /.footer-contact__text -->
              </div><!-- /.footer-contact__item -->
              <div class="footer-contact__item d-flex align-items-center">
                <div class="footer-contact__icon">
                  <i class="icon-envelope1"></i>
                </div><!-- /.footer-contact__icon -->
                <div class="footer-contact__text">
                  <span>Email:</span>
                  <strong><a href="mailto:<?php echo load_apps()["email"];?>"><?php echo load_apps()["email"];?></a></strong>
                </div><!-- /.footer-contact__text -->
              </div><!-- /.footer-contact__item -->
              <div class="footer-contact__item d-flex align-items-center">
                <div class="footer-contact__icon">
                  <i class="icon-pantone"></i>
                </div><!-- /.footer-contact__icon -->
                <div class="footer-contact__text">
                  <span>Kantor:</span>
                  <strong>DPKPP Kabupaten Bogor</strong>
                </div><!-- /.footer-contact__text -->
              </div><!-- /.footer-contact__item -->
            </div><!-- /.col-lg-8 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
      </div><!-- /.footer-contact -->
      <div class="footer-primary bg-primary">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 footer-widget footer-widget__newsletter">
              <form class="footer-widget__newsletter__form">
                <h6 class="footer-widget__newsletter__title">Dinas Perumahan, Kawasan Pemukiman dan Pertanahan Kabupaten Bogor</h6>
                
              </form>
            </div><!-- /.col-xl-4 -->
            <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 footer-widget">
              <h6 class="footer-widget__title">DPKPP Kabupaten Bogor</h6>
              <div class="footer-widget__content">
                <p class="mb-20">Mendukung Program Pancakarsa Bupati Bogor dalam Sektor Pembangunan dengan Karsa Bogor Membangun</p>
              
              </div><!-- /.footer-widget__content -->
            </div><!-- /.col-xl-2 -->
            <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2 footer-widget footer-widget-nav">
              <h6 class="footer-widget__title">Navigasi</h6>
              <div class="footer-widget__content">
                <nav>
                  <ul class="list-unstyled">
                    <li><a href="<?php echo base_url('faq');?>">Bantuan & FAQ</a></li>
                    <li><a href="<?php echo base_url('gallery');?>">Galeri Kegiatan</a></li>
                  </ul>
                </nav>
              </div><!-- /.footer-widget__content -->
            </div><!-- /.col-xl-2 -->
            <div class="col-sm-12 col-md-6 col-lg-3 col-xl-3 footer-widget">
              <h6 class="footer-widget__title">Kontak Kami</h6>
              <div class="footer-widget__content">
                <p><?php echo load_apps()["address"];?></p>
               
                <p class="footer-contact__phone d-flex align-items-center">
                  <i class="icon-phone1"></i>
                  <a href="tel:<?php echo load_apps()["phone_number"];?>"><?php echo load_apps()["phone_number"];?></a>
                </p>
              </div><!-- /.footer-widget__content -->
            </div><!-- /.col-xl-4 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
      </div><!-- /.footer-primary -->
      <div class="footer-secondary">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-sm-12 col-md-12 col-lg-8 d-flex flex-wrap align-items-center">
              <img src="<?php echo load_apps()["logo_fav"];?>" alt="<?php echo load_apps()["name_app"];?>" height="60px">
              <nav>
                <ul class="footer__copyright-links list-unstyled d-flex flex-wrap mb-0">
                  <li><a href="#">Syarat & Ketentuan</a></li>
                  <li><a href="<?php echo base_url('sitemap');?>">Sitemap</a></li>
                  <li><a href="<?php echo base_url('sihuntapadmin/auth');?>">Akses Login</a></li>
                </ul>
                <p class="mb-0"> &copy; 2021 SIHUNTAP |
                  <a class="color-secondary" href="<?php echo base_url();?>"><?php echo load_apps()["author"];?></a>
                </p>
              </nav>
            </div><!-- /.col-lg-8 -->
            <div class="col-sm-12 col-md-12 col-lg-4">
              <ul class="social__icons list-unstyled justify-content-end mb-0">
                <li><a href="<?php echo load_apps()["facebook"];?>"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="<?php echo load_apps()["instagram"];?>"><i class="fab fa-instagram"></i></a></li>
                <li><a href="<?php echo load_apps()["twitter"];?>"><i class="fab fa-twitter"></i></a></li>
              </ul><!-- /.social-icons -->
            </div><!-- /.col-lg-4 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
      </div><!-- /.footer-secondary -->
    </footer><!-- /.Footer -->
    <button id="scrollTopBtn"><i class="fas fa-long-arrow-alt-up"></i></button>
  </div><!-- /.wrapper -->

  <script src="<?php echo base_url();?>template/fe/assets/js/jquery-3.5.1.min.js"></script>
  <script src="<?php echo base_url();?>template/fe/assets/js/plugins.js"></script>
  <script src="<?php echo base_url();?>template/fe/assets/js/main.js"></script>
</body>

</html>