<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"/>
  <link href="<?php echo load_apps()["logo_fav"];?>" rel="icon">
  <title><?php echo $title.' - '.load_apps()['name'];?></title>
  <meta name="keywords" content="<?php echo load_apps()["keyword"];?>">
  <meta name="description" content="<?php echo load_apps()["description"];?>">
  <script src="https://kit.fontawesome.com/7803e36737.js" crossorigin="anonymous"></script>
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Barlow:400,500,600,700%7cHeebo:400,500,700&display=swap">
  <link rel="stylesheet" href="<?php echo base_url();?>template/fe/assets/css/libraries.css">
  <link rel="stylesheet" href="<?php echo base_url();?>template/fe/assets/css/style.css">
  <script src="<?php echo base_url();?>js/vue@2.6.14.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="<?php echo base_url();?>js/sweetalert2@11.js"></script>
    <script src="<?php echo base_url();?>js/vuejs-paginate.js"></script>

    <script src="<?php echo base_url();?>js/vue-select.js"></script>
    <link rel="stylesheet" href="<?php echo base_url();?>js/vue-select.css">

    <!-- summernote -->

  
</head>

<body>
  <div class="wrapper">
    <!-- <div class="preloader">
      <div class="loading">
        <span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>
      </div>
    </div> -->
    <!-- =========================
        Header
    =========================== -->
    <header class="header header-layout2">
      <nav class="navbar navbar-expand-lg">
        <div class="container">
          <a class="navbar-brand" href="<?php echo base_url();?>">
            <img src="<?php echo load_apps()["logo"];?>" class="logo-dark" alt="<?php echo load_apps()["name_app"]; ?>">
          </a>
          <div class="header-topbar d-none d-lg-block">
            <div class="d-flex flex-wrap">
              <ul class="header-topbar__contact d-flex flex-wrap list-unstyled mb-0">
                <li>
                  <i class="icon-phone1"></i>
                  <div>
                    <span>Call Center:</span><strong><a href="tel:<?php echo load_apps()["phone_number"];?>"><?php echo load_apps()["phone_number"];?></a></strong>
                  </div>
                </li>
                <li>
                  <i class="icon-envelope1"></i>
                  <div>
                    <span>Email:</span><strong><a href="mailto:<?php echo load_apps()["email"];?>"><?php echo load_apps()["email"];?></a></strong>
                  </div>
                </li>
                
              </ul>
              <a href="<?php echo base_url('sihuntapadmin/auth');?>" class="btn btn__primary header__btn">
                <i class="icon-arrow-right1"></i><span>Login App</span>
              </a>
            </div>
          </div><!-- /.header-topbar -->
          <button class="navbar-toggler" type="button">
            <span class="menu-lines"><span></span></span>
          </button>
        </div><!-- /.container -->
        <div class="navbar__bottom sticky-navbar" id="appMenus">
          <div class="container">
            <div class="collapse navbar-collapse" id="mainNavigation">
              <ul class="navbar-nav">

                              
                <li class="nav__item with-dropdown">
                  <a href="<?php echo base_url();?>" class="nav__item-link <?= $this->uri->segment(1) == "" ? "active" : "" ?>" >Beranda</a>
                  <i data-toggle="dropdown" class="fa fa-angle-down d-block d-lg-none"></i>
                </li><!-- /.nav-item -->
                
                <li class="nav__item with-dropdown" v-for="val in data">
                    <a href="#" class="dropdown-toggle nav__item-link " v-if="val.menus_level2.length != 0">{{ val.name }}</a>
                    <i v-if="val.menus_level2.length != 0" data-toggle="dropdown" class="fa fa-angle-down d-block d-lg-none"></i>
                    <a :href="'<?php echo base_url();?>' + val.url" v-else class="nav__item-link">{{ val.name }}</a>
                  
                    <ul class="dropdown-menu" v-if="val.menus_level2.length != 0">
                      <li class="nav__item" v-for="val2 in val.menus_level2">
                        <a :href="'<?php echo base_url();?>' + val2.url"  class="nav__item-link ">{{ val2.name }}</a>
                      
                      </li><!-- /.nav-item -->
                    </ul><!-- /.dropdown-menu -->
                </li><!-- /.nav-item -->

                 
              </ul><!-- /.navbar-nav -->
            </div><!-- /.navbar-collapse -->
            <div class="header-actions d-none d-lg-block">
              
            </div><!-- /.header-actions -->
          </div><!-- /.container -->
        </div><!-- /.navbar-bottom -->
      </nav><!-- /.navabr -->
    </header><!-- /.Header -->

    <script>
    var appMenus = new Vue({
        el: '#appMenus',
        data: {
            unique_id: localStorage.getItem("dashboard_unique_id"),
            roleas: localStorage.getItem("dashboard_roleas"),

            data: {}
        },
        mounted: function () {
            this.retrive();
            
        },
        methods: {    
            retrive() {
                axios("<?php echo base_url(); ?>api/homepage/menus").then(response => {
                    this.data = response.data.data;
                });
            }
            
        }
    });
</script>