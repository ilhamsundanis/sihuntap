   

    <!-- ==========================
        contact layout 1
    =========================== -->
    <section class="contact-layout1 py-0">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="contact-panel">
              <div class="contact-panel__banner d-flex align-items-end mb-20">
                <div class="bg-img"><img src="<?php echo base_url();?>template/fe/assets/images/banner_1.jpg" alt="banner"></div>
                <div class="contact-panel__banner__inner">
                  <h4 class="contact-panel__banner__title">
Jalan Terdepan Dalam Bangunan & Konstruksi Sipil!</h4>
                  <p class="contact-panel__banner__desc">Namun mereka yang merangkul perubahan berkembang pesat, membangun lebih besar, lebih baik,
                    produk lebih cepat & lebih kuat dari sebelumnya!</p>
                  <h6 class="contact-panel__banner__desc">Kepala Bidang Perumahan </h6>
                  <!-- <img src="<?php echo base_url();?>template/fe/assets/images/about/singnture2.png" alt="singnture"> -->
                </div>
              </div><!-- /.contact-panel__banner -->
              <form class="contact-panel__form" method="post" action="assets/php/contact.php" id="contactForm">
                <div class="row">
                  <div class="col-sm-12">
                    <h4 class="contact-panel__title">Hubungi Kami</h4>
                    <p class="contact-panel__desc mb-40">Kirim pesan untuk kami perihal Pelayanan Hunian Tetap</p>
                  </div>
                  <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group"><input type="text" class="form-control" placeholder="Nama Lengkap" id="contact-name"
                        name="contact-name" required></div>
                  </div><!-- /.col-lg-6 -->
                  <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group"><input type="email" class="form-control" placeholder="Email"
                        id="contact-email" name="contact-email" required></div>
                  </div><!-- /.col-lg-6 -->
                  <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group"><input type="text" class="form-control" placeholder="No Telp"
                        id="contact-Phone" name="contact-phone" required></div>
                  </div><!-- /.col-lg-6 -->
                  <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <select class="form-control">
                        <option selected>Pilih Pelayanan</option>
                        <option value="1">Konsultasi</option>
                        <option value="2">Keluhan </option>
                      </select>
                    </div>
                  </div><!-- /.col-lg-6 -->
                  <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <textarea class="form-control" placeholder="Isi pesan anda" id="contact-messgae"
                        name="contact-messgae" required></textarea>
                    </div>
                  </div><!-- /.col-lg-12 -->
                  <div class="col-sm-12 col-md-12 col-lg-12">
                    <button type="submit" class="btn btn__secondary btn__block ">Kirim Pesan </button>
                    <div class="contact-result"></div>
                  </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
              </form>
            </div><!-- /.contact__panel -->
          </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.contact layout 1 -->

    <!-- ========================= 
            Google Map
    =========================  -->
    <section class="google-map p-0 mt--120">
      <div id="map" style="height: 440px;">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.3137211819094!2d106.82896761477514!3d-6.481894265178061!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c1ed1566124f%3A0x979d5bc4cc9bad6e!2sDinas%20Perumahan%20Kawasan%20Permukiman%20dan%20Pertanahan%20Kabupaten%20Bogor!5e0!3m2!1sid!2sid!4v1641174585575!5m2!1sid!2sid" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
      </div>
      
    </section><!-- /.GoogleMap -->
