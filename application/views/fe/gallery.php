<div id="appContent">
	<!-- ========================
       page title 
    =========================== -->
    <section class="page-title page-title-layout9 text-center">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-sm-12 col-md-12 col-lg-12">
            <nav>
              <ol class="breadcrumb justify-content-center mb-20">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Beranda</a></li>
                <li class="breadcrumb-item active" aria-current="page">Galeri Kegiatan</li>
              </ol>
            </nav>
          </div><!-- /.col-xl-6 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.page-title -->

	<!-- ======================
     portfolio-gallery
    ========================= -->
    <section class="portfolio-gallery py-0">
      <div class="container">
        <div class="row">
          <!-- portfolio item #1 -->
          <div class="col-sm-6 col-md-6 col-lg-4" v-for="data in list">
            <div class="portfolio-item">
              <div class="portfolio-item__img">
                <a class="popup-gallery-item" :href="data.image">
                  <img :src="data.image" :alt="data.name">
                </a>
              </div><!-- /.portfolio-img -->
            </div><!-- /.portfolio-item -->
          </div><!-- /.col-lg-4 -->
         
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.portfolio gallery -->
</div>

    <script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'paginate': VuejsPaginate,
            'v-select': VueSelect.VueSelect,
        },
        data: {
            loading: false,
                       
            list: [],
            currentPage: <?php echo empty($this->uri->segment(4)) ? 1 : $this->uri->segment(4);?>,     
            totalPage: 1,
        },
        mounted: function () {
         
            
            this.retrive();
        },
        methods: {
            clickCallback(pageNum) {
                window.location.href = "<?php echo base_url(); ?>gallery/index/" + pageNum;
            },    
            
            retrive()
            {
                var query = "?orderBy=id&sort=desc&limit=30&page=" + this.currentPage;
                // query += "&id=" + this.filter.id;
                // query += "&name=" + this.filter.name;
                // query += "&status=" + this.filter.status;
                // query += "&ktp_number=" + this.filter.ktp_number;
                axios("<?php echo base_url(); ?>api/admin/page_gallery" + query).then(response => {
                    this.list      = response.data.data;
                    this.totalPage = response.data.meta.totalPage;
                    this.loading   = false;
                });
                
            },
           
            
        }
    });
</script>