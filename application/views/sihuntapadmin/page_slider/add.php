<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">TAMBAH {{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">Tambah {{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                    <span class="d-none d-sm-block">TAMBAH</span>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">FORM INPUT</h4>
                        <br />
                        <div class="row">
                            
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">Nama</label>
                                    <input type="text" class="form-control" placeholder="Nama" v-model="model.name">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">Deskripsi Sebagian</label>
                                    <textarea v-model="model.description" class="form-control" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">Gambar</label>
                                    <input type="file" class="form-control" ref="image">
                                </div>
                            </div>
                        </div>
                        
                        <div>
                            <div style="text-align: right;">
                               

                                <button type="button" class="btn btn-light waves-effect" v-if="loading">
                                    <i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i>
                                    Loading ...
                                </button>
                                <button type="button" class="btn btn-primary waves-effect waves-light me-1" v-on:click="save" v-else>
                                    Simpan Data
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,
        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },

            loading: false,
            model: 
                { 
                    name: "",
                    description: "",
                    image: ""
                },
        },
        mounted: function () {
            this.validate_access();
        },
        methods: {    
            validate_access() {
                if ( this.access.add == 0 ) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            save() {
                this.loading = true;

                var formData = new FormData();
                formData.append('name', this.model.name);
                formData.append('description', this.model.description);
                formData.append('image', this.$refs.image.files[0]);
                formData.append('created_by', localStorage.getItem("id"));
                error = false;
                for ( data in formData ) {
                    if ( formData[data] == "" || formData[data] == undefined ) {
                        error = true
                    }
                }

                if ( !error ) {
                    axios.post("<?php echo base_url(); ?>api/admin/page_slider", formData, {
                        headers: {
                            'content-type': "application/json",
                            'Authorization': 'Bearer ' + localStorage.getItem("token"),
                        }
                    }).then(response => {
                        if ( response.data.code == 200 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 10000,
                            });

                            this.model   = {name: "", url: "", img: ""};
                            this.loading = false;
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                            this.loading = false;
                        }
                    })
                    .catch(error => {
                        if ( error.response.data.code == 401 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            }).then(function() {
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }
                        this.loading    = false;
                    });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            }
        }
    });
</script>