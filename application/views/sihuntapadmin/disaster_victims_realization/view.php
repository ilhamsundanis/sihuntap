<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">{{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">{{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-list"></i></span>
                                    <span class="d-none d-sm-block"><i class="fas fa-list"></i> DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="fa fa-pencil-alt"></i></span>
                                    <span class="d-none d-sm-block"><i class="fa fa-pencil-alt"></i> EDIT DATA</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>


                <div class="card">
                    <div class="card-body">
                        <h6>FORM EDIT LAPORAN</h6>
                        <div class="row" style="border-bottom:1px solid #a0a0a0;margin-bottom:15px;padding-bottom:5px;">

                            <div class="mb-3 col-lg-12">
                                <label>Nama Penerima (*)</label>
                                <v-select placeholder="--Pilih Nama Penerima--" :options="list_disaster_victims" v-model="model.disaster_victims_id" :reduce="disaster_victims_id => disaster_victims_id.code" class="form-control" style="padding: 3px;"></v-select>
                            </div>

                            <h6 class="col-lg-12">Lampiran Pelaporan</h6>
                            <div class="mb-3 col-lg-4">
                                <label>Surat Laporan Kejadian Bencana Alam (SLKBA) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="slkba_file" required>
                                <object width="100%" height="500" type="application/pdf" :data="model.slkba_file">
                                    <p>Data tidak ada / belum dimuat.</p>
                                </object>
                            </div>
                            <div class="mb-3 col-lg-4">
                                <label>Surat Permohonan Bantuan Penanganan Pasca Bencana (SPBPPB) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="spbppb_file" required>
                                <object width="100%" height="500" type="application/pdf" :data="model.spbppb_file">
                                    <p>Data tidak ada / belum dimuat.</p>
                                </object>
                            </div>
                            <div class="mb-3 col-lg-4">
                                <label>Rencana Anggaran Biaya (RAB) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="rab_file" required>
                                <object width="100%" height="500" type="application/pdf" :data="model.rab_file">
                                    <p>Data tidak ada / belum dimuat.</p>
                                </object>
                            </div>
                            <div class="mb-3 col-lg-4">
                                <label>Bon/Kwitansi Material (KWT-M) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="kwitansi_file" required>
                                <object width="100%" height="500" type="application/pdf" :data="model.kwitansi_file">
                                    <p>Data tidak ada / belum dimuat.</p>
                                </object>
                            </div>
                            <div class="mb-3 col-lg-4">
                                <label>Bon/Kwitansi Upah Tenaga Kerja (KWT-TK) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="kwitansi_tk_file" required>
                                <object width="100%" height="500" type="application/pdf" :data="model.kwitansi_tk_file">
                                    <p>Data tidak ada / belum dimuat.</p>
                                </object>
                            </div>
                            <div class="mb-3 col-lg-4">
                                <label>Daftar Identitas Tenaga Kerja (ID-TK) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="tk_idendity_file" required>
                                <object width="100%" height="500" type="application/pdf" :data="model.tk_idendity_file">
                                    <p>Data tidak ada / belum dimuat.</p>
                                </object>
                            </div>

                        </div>
                        <div class="row">
                            <h6 class="col-lg-12">Foto Foto Dokumentasi</h6>
                            <div class="mb-3 col-lg-4">
                                <label>Dokumentasi Foto (0%) *.img</label>
                                <input class="form-control small" accept="image/*" type="file" ref="documentation_file_1" required>
                            </div>
                            <div class="mb-3 col-lg-4">
                                <label>Dokumentasi Foto (50%) *.img</label>
                                <input class="form-control small" accept="image/*" type="file" ref="documentation_file_2" required>
                            </div>
                            <div class="mb-3 col-lg-4">
                                <label>Dokumentasi Foto (100%) *.img</label>
                                <input class="form-control small" accept="image/*" type="file" ref="documentation_file_3" required>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-12">
                                <br />
                                <button type="submit" class="btn btn-primary" v-on:click="save">SIMPAN DATA</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,

        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },
            id: <?php echo $id; ?>,
            loading: false,
            model: {
                disaster_victims_id: "",
                slkba_file: "",
                spbppb_file: "",
                rab_file: "",
                kwitansi_file: "",
                kwitansi_tk_file: "",
                tk_idendity_file: "",
                documentation_file_1: "",
                documentation_file_2: "",
                documentation_file_3: "",
            },

            list_disaster_victims: [],
        },
        mounted: function() {
            this.validate_access();
            this.retrive();
            axios("<?php echo base_url(); ?>api/admin/disaster_victims").then(response => {
                for (val in response.data.data) {
                    this.list_disaster_victims.push({
                        label: response.data.data[val].name + " - " + response.data.data[val].ktp_number + " [" + response.data.data[val].master_village_name + " - " + response.data.data[val].master_district_name + "]",
                        code: response.data.data[val].id
                    });
                }
            });


        },
        methods: {
            validate_access() {
                if (this.access.add == 0) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            retrive() {
                axios("<?php echo base_url(); ?>api/admin/disaster_victims_realization/" + this.id).then(response => {
                    var data = response.data.data;
                    this.model.disaster_victims_id = data.disaster_victims_id;
                    this.model.slkba_file = data.slkba_file;
                    this.model.spbppb_file = data.spbppb_file;
                    this.model.rab_file = data.rab_file;
                    this.model.kwitansi_file = data.kwitansi_file;
                    this.model.kwitansi_tk_file = data.kwitansi_tk_file;
                    this.model.tk_idendity_file = data.tk_idendity_file;
                    this.model.documentation_file_1 = data.documentation_file_1;
                    this.model.documentation_file_2 = data.documentation_file_2;
                    this.model.documentation_file_3 = data.documentation_file_3;
                });
            },
            save() {
                this.loading = true;
                error = false;
                var formData = new FormData();
                formData.append('disaster_victims_id', this.model.disaster_victims_id);
                formData.append('slkba_file', this.$refs.slkba_file.files[0]);
                formData.append('spbppb_file', this.$refs.spbppb_file.files[0]);
                formData.append('rab_file', this.$refs.rab_file.files[0]);
                formData.append('kwitansi_file', this.$refs.kwitansi_file.files[0]);
                formData.append('kwitansi_tk_file', this.$refs.kwitansi_tk_file.files[0]);
                formData.append('tk_idendity_file', this.$refs.tk_idendity_file.files[0]);
                formData.append('documentation_file_1', this.$refs.documentation_file_1.files[0]);
                formData.append('documentation_file_2', this.$refs.documentation_file_2.files[0]);
                formData.append('documentation_file_3', this.$refs.documentation_file_3.files[0]);

                error = false;
                if (this.model.disaster_victims_id == "") {
                    error = true;
                }

                if (!error) {
                    axios.post("<?php echo base_url(); ?>api/admin/disaster_victims_realization/" + this.id, formData, {
                            headers: {
                                'content-type': "multipart/form-data",
                                'Authorization': 'Bearer ' + localStorage.getItem("token"),
                            }
                        }).then(response => {
                            if (response.data.code == 200) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 10000,
                                });
                                this.retrive();
                                this.loading = false;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                                this.loading = false;
                            }
                        })
                        .catch(error => {
                            if (error.response.data.code == 401) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                }).then(function() {
                                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                            }
                            this.loading = false;
                        });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            }
        }
    });
</script>