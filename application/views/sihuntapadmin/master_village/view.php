<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">EDIT {{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">Edit {{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.edit == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                    <span class="d-none d-sm-block">EDIT</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.import == 1">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module + '/import'" role="tab">
                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                    <span class="d-none d-sm-block">IMPOR DATA</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">FORM INPUT</h4>

                        <div class="row">
                           
                            <div class="col-md-4">
                                <div class="mb-3">
                                    <label class="form-label">Kelurahan / Kecamatan</label>
                                    <v-select :options="list_master_district" v-model="model.master_district_id" :reduce="master_district_id => master_district_id.code" class="form-control" style="padding: 3px;"></v-select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Desa</label>
                                    <input type="text" class="form-control" placeholder="Desa" v-model="model.name">
                                </div>
                            </div>
                        </div>
                        <div>
                            <div style="text-align: right;">
                                <button type="button" class="btn btn-light waves-effect" v-if="loading">
                                    <i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i>
                                    Loading ...
                                </button>
                                <button type="button" class="btn btn-primary waves-effect waves-light me-1" v-on:click="save" v-else>
                                    Simpan Data
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,
        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },

            loading: false,
            id: '<?php echo $id; ?>',
            model: {
                name: "",
                master_province_id: "",
                master_city_id: "",
                master_district_id: "",
            },

            list_master_province: [],
            list_master_city: [],
            list_master_district: [],
        },
        mounted: function () {
            this.validate_access();

            axios("<?php echo base_url(); ?>api/admin/master_district?master_city_id=3201").then(response => {
                for ( val in response.data.data ) {
                    this.list_master_district.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                }
            });

            axios("<?php echo base_url(); ?>api/admin/master_village/" + this.id).then(response => {
                var data = response.data.data;

                this.model.name               = data.name;
                this.model.master_province_id = data.master_province_id;
                // this.get_city_by_province_id(data.master_province_id);
                this.model.master_city_id     = data.master_city_id;
                this.model.master_district_id = data.master_district_id;
            });
            
        },
        methods: {    
            validate_access() {
                if ( this.access.edit == 0 ) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            get_city_by_province_id(id) {
                this.list_master_city = [];
                this.model.master_city_id = "";

                this.list_master_district = [];
                this.model.master_district_id = "";
                
                query = "?master_province_id=" + id;
                axios("<?php echo base_url(); ?>api/admin/master_city" + query).then(response => {
                    for ( val in response.data.data ) {
                        this.list_master_city.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                    }
                });
            },
            get_district_by_city_id(id) {
                this.list_master_district = [];
                this.model.master_district_id = "";
                
                query = "?id=" + id;
                axios("<?php echo base_url(); ?>api/admin/master_district" + query).then(response => {
                    for ( val in response.data.data ) {
                        this.list_master_district.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                    }
                });
            },
            save() {
                this.loading = true;
                var formData = {
                    name: this.model.name,
                    master_province_id: this.model.master_province_id,
                    master_city_id: this.model.master_city_id,
                    master_district_id: this.model.master_district_id,
                }

                error = false;
                for ( data in formData ) {
                    if ( formData[data] == "" || formData[data] == undefined ) {
                        error = true
                    }
                }

                if ( !error ) {
                    axios.put("<?php echo base_url(); ?>api/admin/master_village/" + this.id, formData, {
                        headers: {
                            'content-type': "application/json",
                            'Authorization': 'Bearer ' + localStorage.getItem("token"),
                        }
                    }).then(response => {
                        if ( response.data.code == 200 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 10000,
                            });
                            this.loading = false;
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                            this.loading = false;
                        }
                    })
                    .catch(error => {
                        if ( error.response.data.code == 401 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            }).then(function() {
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }
                        this.loading    = false;
                    });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            }
        }
    });
</script>