<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">TAMBAH {{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">Tambah {{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                    <span class="d-none d-sm-block">TAMBAH</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">FORM INPUT</h4>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label>Nama Proposal</label>
                                    <input type="text" v-model="model.name" placeholder="Tulis nama proposal" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="mb-3">
                                    <label>Kategori Bencana</label>
                                    <v-select placeholder="Jenis Bencana" :options="list_disaster" v-model="model.master_disaster_id" :reduce="master_disaster_id => master_disaster_id.code" class="form-control" style="padding: 3px;"></v-select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-3">
                                    <label>Kategori Kerusakan</label>

                                    <v-select placeholder="Kategori Kerusakan" :options="list_damage" v-model="model.master_damage_id" :reduce="master_damage_id => master_damage_id.code" class="form-control" style="padding: 3px;"></v-select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-3">
                                    <label>Lokasi Bencana</label>

                                    <v-select placeholder="Pilih Kecamatan" :options="list_master_district" v-model="model.master_district_id" :reduce="master_district_id => master_district_id.code" @input="get_village_by_district_id" class="form-control" style="padding: 3px;"></v-select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-3">
                                    <label>Desa</label>

                                    <v-select placeholder="Pilih Desa" :options="list_master_village" v-model="model.master_village_id" :reduce="master_village_id => master_village_id.code" class="form-control" style="padding: 3px;"></v-select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="search_input" id="search_input" placeholder="Ketik Alamat..." style="border: 1px solid black;" value="<?php echo @$post['search_input']; ?>" />
                                    </div>
                                    <div class="col-md-12">
                                        <input name="latlon" id="latlon" type="hidden" value="" />
                                        <div id="map" style="height: 500px; width: 100%;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label>Keterangan</label>
                                    <textarea v-model="model.description" class="form-control summernote" placeholder="Isi keterangan (*bila diperlukan)"></textarea>
                                </div>
                            </div>

                        </div>
                        <div>
                            <div style="text-align: right;">


                                <button type="button" class="btn btn-light waves-effect" v-if="loading">
                                    <i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i>
                                    Loading ...
                                </button>
                                <button type="button" class="btn btn-primary waves-effect waves-light me-1" v-on:click="save" v-else>
                                    Simpan Data
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAEP1L4EzuNYJR83hokTaPEqC8AXoklxmM&callback=initMap&libraries=places&v=weekly" async></script>

<script>
    function initMap() {
        var autocomplete;
        autocomplete = new google.maps.places.Autocomplete((document.getElementById('search_input')), {
            types: ['geocode'],
        });

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var near_place = autocomplete.getPlace();

            document.getElementById('latlon').value = near_place.geometry.location.lat() + "," + near_place.geometry.location.lng();


            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: 15,
                center: {
                    lat: -6.5950181,
                    lng: 106.7218518
                },
            });

            var vMarker = new google.maps.Marker({
                position: {
                    lat: near_place.geometry.location.lat(),
                    lng: near_place.geometry.location.lng()
                },
                map,
                draggable: true,
            });

            google.maps.event.addListener(vMarker, 'dragend', function(evt) {

                $("#latlon").val(evt.latLng.lat().toFixed(6) + "," + evt.latLng.lng().toFixed(6));
                map.panTo(evt.latLng);
            });

            // centers the map on markers coords
            map.setCenter(vMarker.position);

            // adds the marker on the map
            vMarker.setMap(map);
        });
    }
    $(document).ready(function() {
        $("#search_input").keypress(function(e) {
            //Enter key
            if (e.which == 13) {
                return false;
            }
        });
    });
</script>
<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,
        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },

            loading: false,
            model: {
                name: "",
                description: "",
                master_disaster_id: "",
                master_damage_id: "",
                status: 0,
                latlon: "",
                status_description: "",
                master_district_id: "",
                master_village_id: "",
                address: ""
            },
            list_master_district: [],
            list_master_village: [],
            list_damage: [],
            list_disaster: []
        },
        mounted: function() {
            this.validate_access();
            axios("<?php echo base_url(); ?>api/admin/master_disaster").then(response => {
                for (val in response.data.data) {
                    this.list_disaster.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_damage").then(response => {
                for (val in response.data.data) {
                    this.list_damage.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });

            axios("<?php echo base_url(); ?>api/admin/master_district?master_city_id=3201").then(response => {
                for (val in response.data.data) {
                    this.list_master_district.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });

        },
        methods: {
            validate_access() {
                if (this.access.add == 0) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            get_village_by_district_id(id) {
                alert(id);
                this.list_master_village = [];
                axios("<?php echo base_url(); ?>api/admin/master_village?master_district_id=" + id).then(response => {
                    for (val in response.data.data) {
                        this.list_master_village.push({
                            label: response.data.data[val].name,
                            code: response.data.data[val].id
                        });
                    }
                });
            },
            save() {
                this.loading = true;
                this.model.latlon = document.getElementById('latlon').value;
                var formData = new FormData();
                formData.append('name', this.model.name);
                formData.append('description', this.model.description);
                formData.append('master_disaster_id', this.model.master_disaster_id);
                formData.append("latlon", this.model.latlon);
                formData.append('master_damage_id', this.model.master_damage_id);
                formData.append('status', this.model.status);
                formData.append('master_district_id', this.model.master_district_id);

                formData.append('master_village_id', this.model.master_village_id);
                formData.append('created_by', localStorage.getItem("id"));
                error = false;
                // for ( data in formData ) {
                //     if ( formData[data] == "" || formData[data] == undefined ) {
                //         error = true
                //     }
                // }
                if (document.getElementById('latlon').value == "" || this.model.name == '' || this.model.master_disaster_id == '' || this.model.master_damage_id == '' || this.model.master_district_id == '') {
                    error = true;
                }

                if (!error) {
                    axios.post("<?php echo base_url(); ?>api/admin/proposal", formData, {
                            headers: {
                                'content-type': "application/json",
                                'Authorization': 'Bearer ' + localStorage.getItem("token"),
                            }
                        }).then(response => {
                            if (response.data.code == 200) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 10000,
                                });

                                this.model = {
                                    name: "",
                                    description: "",
                                    master_disaster_id: "",
                                    master_damage_id: "",
                                    status: "",
                                    status_description: "",
                                    master_district_id: "",
                                    master_village_id: "",
                                    address: ""
                                };
                                this.loading = false;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                                this.loading = false;
                            }
                        })
                        .catch(error => {
                            if (error.response.data.code == 401) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                }).then(function() {
                                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                            }
                            this.loading = false;
                        });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            }
        }
    });
</script>