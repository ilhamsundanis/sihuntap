<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">TAMBAH {{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">Tambah {{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                    <span class="d-none d-sm-block">TAMBAH</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">FORM INPUT</h4>
                        <br />
                        <div class="row">
                            <div class="mb-3 col-lg-6">
                                <label>Pilih Kecamatan</label>
                                <select class="form-control" v-model="model.master_district_id" @change="get_village_by_district_id($event)" required>
                                    <option selected> -- Pilih --</option>
                                    <option v-for="data in list_master_district" :value="data.id">{{data.name}}</option>
                                </select>
                            </div>
                            <div class="mb-3 col-lg-6">
                                <label>Pilih Desa</label>
                                <select class="form-control" v-model="model.master_village_id" required>
                                    <option v-for="(data,index) in list_master_village" :value="data.id">{{data.name}}</option>
                                </select>
                            </div>

                        </div>
                        <br />
                        <hr>
                        <div class="row">
                            <div class="mb-3 col-lg-3">
                                <label for="nama_lengkap" class="form-label">Nama Lengkap</label>

                            </div>
                            <div class="mb-3 col-lg-2">
                                <label for="nik" class="form-label">NIK</label>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label for="no_kk" class="form-label">No. Kartu Keluarga</label>
                            </div>
                            <div class="mb-3 col-lg-1">
                                <label for="no_kk" class="form-label">JK</label>
                            </div>
                            <div class="mb-3 col-lg-3">
                                <label for="alamat" class="form-label">Alamat</label>
                            </div>

                        </div>

                        <div class="row">

                            <div class="mb-3 col-lg-3">
                                <input class="form-control" type="text" v-model="model.name" placeholder="Input nama lengkap penerima" required>

                            </div>
                            <div class="mb-3 col-lg-2">
                                <input class="form-control" type="text" v-model="model.ktp_number" required placeholder="Nomor induk keluarga">
                            </div>
                            <div class="mb-3 col-lg-2">
                                <input class="form-control" type="text" placeholder="No Kartu Keluarga" v-model="model.kk_number">
                            </div>
                            <div class="mb-3 col-lg-1">
                                <select name="jenis_kelamin" v-model="model.gender" class="form-control" required>
                                    <option selected> -- Pilih --</option>
                                    <option value="L">LAKI LAKI</option>
                                    <option value="P">PEREMPUAN</option>
                                </select>
                            </div>


                            <div class="mb-3 col-lg-3">
                                <input class="form-control" type="text" placeholder="Alamat" v-model="model.address" required>
                            </div>

                        </div>


                        <div class="form-group row">
                            <div class="col-sm-11">
                                <br />
                                <button type="submit" class="btn btn-primary" v-on:click="save">SIMPAN DATA</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    Vue.component('my-input', {
        template: '<input v-attr="name: name" v-model="value" type="text">',
        data() {
            return {
                value: ''
            };
        },
        props: ['name']
    });
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,

        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },
            id: <?php echo $id; ?>,
            loading: false,
            district_id: "",
            village_id: "",
            model: {
                name: "",
                ktp_number: "",
                kk_number: "",
                gender: "",
                status: 1,
                address: "",
                master_district_id: "",
                master_village_id: "",
                proposal_id: ""
            },

            list_company: [],
            list_damage: [],
            list_disaster: [],
            list_master_district: [],
            list_master_village: []
        },
        mounted: function() {
            this.validate_access();
            axios("<?php echo base_url(); ?>api/admin/master_disaster").then(response => {
                for (val in response.data.data) {
                    this.list_disaster.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_damage").then(response => {
                for (val in response.data.data) {
                    this.list_damage.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_district?id_kab=3201").then(response => {
                this.list_master_district = response.data.data;
            });

            axios("<?php echo base_url(); ?>api/admin/residence_recipient/" + this.id).then(response => {
                var data = response.data.data;
                this.model.name = data.name;
                this.model.ktp_number = data.ktp_number;
                this.model.kk_number = data.kk_number;
                this.model.gender = data.gender;
                this.model.address = data.address;
                this.model.master_district_id = data.master_district_id;
                this.model.master_village_id = data.master_village_id;
                axios("<?php echo base_url(); ?>api/admin/master_village?id=" + data.master_village_id).then(response => {
                    this.list_master_village = response.data.data;
                });
                this.model.proposal_id = data.proposal_id;

            });

        },
        methods: {
            validate_access() {
                if (this.access.add == 0) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            get_village_by_district_id(id) {
                query = "?master_district_id=" + id.target.value;
                axios("<?php echo base_url(); ?>api/admin/master_village" + query).then(response => {
                    // for ( val in response.data.data ) {
                    //     this.list_master_village.push({ label: response.data.data[val].nama_desa, code: response.data.data[val].id_kel });
                    // }
                    this.list_master_village = response.data.data;
                });
            },
            add() {
                this.model.push({
                    name: "",
                    ktp_number: "",
                    kk_number: "",
                    gender: "",
                    status: 1,
                    address: "",
                    proposal_id: '<?php echo $id; ?>'
                });
            },
            remove_field(index) {
                this.model.splice(index, 1);
                if (index === 0)
                    this.add()
            },
            save() {
                this.loading = true;
                error = false;
                var formData = {
                    name: this.model.name,
                    ktp_number: this.model.ktp_number,
                    kk_number: this.model.kk_number,
                    gender: this.model.gender,
                    address: this.model.address,
                    master_district_id: this.model.master_district_id,
                    master_village_id: this.model.master_village_id
                }

                error = false;
                for (data in formData) {
                    if (formData[data] == "" || formData[data] == undefined) {
                        error = true
                    }
                }

                if (!error) {
                    axios.put("<?php echo base_url(); ?>api/admin/residence_recipient/" + this.id, formData, {
                            headers: {
                                'content-type': "application/json",
                                'Authorization': 'Bearer ' + localStorage.getItem("token"),
                            }
                        }).then(response => {
                            if (response.data.code == 200) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 10000,
                                });
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/proposal";

                                this.loading = false;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                                this.loading = false;
                            }
                        })
                        .catch(error => {
                            if (error.response.data.code == 401) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                }).then(function() {
                                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                            }
                            this.loading = false;
                        });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            }
        }
    });
</script>