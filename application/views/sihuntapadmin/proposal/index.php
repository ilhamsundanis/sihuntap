<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">{{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">{{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#home" role="tab">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module + '/add'" role="tab">
                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                    <span class="d-none d-sm-block">TAMBAH</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">FILTER DATA</h6>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="mb-4">
                                    <input type="text" class="form-control" placeholder="Nama Proposal" v-model="filter.name" v-on:keyup.enter="retrive">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-4">
                                    <v-select placeholder="Jenis Bencana" :options="list_master_disaster" v-model="filter.master_disaster_id" :reduce="master_disaster_id => master_disaster_id.code" class="form-control" style="padding: 3px;" @input="retrive"></v-select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-4">
                                    <v-select placeholder="Kerusakan" :options="list_master_damage" v-model="filter.master_damage_id" :reduce="master_damage_id => master_damage_id.code" class="form-control" style="padding: 3px;" @input="retrive"></v-select>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <button type="button" class="btn btn-danger waves-effect waves-light me-1" v-on:click="reset_filter">
                                    <i class="fa fa-times"></i> Reset
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="text-center" v-if="loading">
                            <?php $this->load->view('sihuntapadmin/templates/home/loading'); ?>
                        </div>
                        <div class="table-responsive" v-if="!loading">
                            <table class="table mb-0">
                                <thead class="table-light">
                                    <tr>
                                        <th>#</th>
                                        <th>Proposal</th>
                                        <th>Jenis Bencana</th>
                                        <th>Kerusakan</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th v-if="access.edit == 1 || access.delete == 1">Tindakan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(data, index) in list">
                                        <th scope="row">{{ index + 1 }}</th>
                                        <td>{{ data.name }}</td>
                                        <td>{{ data.master_disaster_name }} </td>
                                        <td>{{ data.master_damage_name }} </td>

                                        <td>{{ data.count_recipient}} KK</td>
                                        <td>
                                            <span v-if="data.status == 0" class="badge badge-warning">Diusulkan</span>
                                            <span v-if="data.status == 1" class="badge badge-danger">Ditolak</span>
                                            <span v-if="data.status == 2" class="badge badge-info">Sudah Diverifikasi</span>
                                            <span v-if="data.status == 3" class="badge badge-success">Direlokasi Huntara</span>
                                            <span v-if="data.status == 4" class="badge badge-primary">Direlokasi Huntap</span>
                                        </td>
                                        <td v-if="access.edit == 1 || access.delete == 1">
                                            <a v-if="data.status == 0" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module + '/add_recipient/' + data.id" class="btn btn-outline-info waves-effect waves-light btn-sm" title="Tambah Peserta">
                                                <i class="fas fa-plus"></i>
                                            </a>
                                            <a :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module + '/view/' + data.id" class="btn btn-outline-info waves-effect waves-light btn-sm" title="Edit" v-if="access.edit == 1">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>

                                            <a v-if="data.status == 0" href="javascript:void(0)" class="btn btn-outline-danger waves-effect waves-light btn-sm" title="Hapus" v-on:click="deleteItem(data.id)" v-if="access.delete == 1">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="col-md-12" style="text-align: center;">
                            <template>
                                <paginate v-model="currentPage" :page-count="totalPage" :click-handler="clickCallback" :prev-text="'Prev'" :next-text="'Next'" :container-class="'vue-pagination'"></paginate>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'paginate': VuejsPaginate,
            'v-select': VueSelect.VueSelect,
        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },

            list: [],
            list_master_damage :[],
            list_master_disaster : [],
            currentPage: <?php echo empty($this->uri->segment(4)) ? 1 : $this->uri->segment(4); ?>,
            totalPage: 1,

            loading: false,
            filter: {
                id: (localStorage.getItem("filter_id") == null) ? "" : localStorage.getItem("filter_id"),
                name: (localStorage.getItem("filter_name") == null) ? "" : localStorage.getItem("filter_name"),
                master_disaster_id: (localStorage.getItem("filter_master_disaster_id") == null) ? "" : localStorage.getItem("filter_master_disaster_id"),
                master_damage_id: (localStorage.getItem("filter_master_damage_id") == null) ? "" : localStorage.getItem("filter_master_damage_id"),
            },
        },
        mounted: function() {

            axios("<?php echo base_url(); ?>api/admin/master_disaster").then(response => {
                for ( val in response.data.data ) {
                    this.list_master_disaster.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_damage").then(response => {
                for ( val in response.data.data ) {
                    this.list_master_damage.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                }
            });
            this.retrive();
        },
        methods: {
            clickCallback(pageNum) {
                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/" + this.master_module_module + "/index/" + pageNum;
            },
            retrive() {
                this.loading = true;
                this.filter_data();

                var query = "?orderBy=id&sort=desc&limit=50&page=" + this.currentPage;
                if (localStorage.getItem("id") != 1) {

                    query += "&created_by=" + localStorage.getItem("id");
                }
                query += "&id=" + this.filter.id;
                query += "&name=" + this.filter.name;
                query += "&disaster_id=" + this.filter.master_disaster_id;
                query += "&damage_id=" + this.filter.master_damage_id;
                axios("<?php echo base_url(); ?>api/admin/proposal" + query).then(response => {
                    this.list = response.data.data;
                    this.totalPage = response.data.meta.totalPage;
                    this.loading = false;
                });
            },
            filter_data() {
                localStorage.setItem("filter_id", this.filter.id);
                localStorage.setItem("filter_name", this.filter.name);
                localStorage.setItem("filter_master_disaster_id", this.filter.master_disaster_id);
                localStorage.setItem("filter_master_damage_id", this.filter.master_damage_id);

            },
            reset_filter() {
                this.filter.id = "";
                this.filter.name = "";
                this.filter.master_disaster_id = "";
                this.filter.master_damage_id = "";

                this.retrive();
            },
            deleteItem(id) {
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "Anda akan menghapus data ini",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak'
                }).then((result) => {
                    if (result.isConfirmed) {
                        axios.delete("<?php echo base_url(); ?>api/admin/proposal/" + id, {
                                headers: {
                                    Authorization: 'Bearer ' + localStorage.getItem("token"),
                                },
                            }).then(response => {
                                if (response.data.code == 200) {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        text: "Anda berhasil menghapus data ini",
                                        showConfirmButton: false,
                                        timer: 1500,
                                    });
                                    this.retrive();
                                }
                            })
                            .catch(error => {
                                if (error.response.data.code == 401) {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'warning',
                                        text: error.response.data.message,
                                        showConfirmButton: false,
                                        timer: 1500,
                                    }).then(function() {
                                        window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                                    });
                                } else {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'warning',
                                        text: error.response.data.message,
                                        showConfirmButton: false,
                                        timer: 1500,
                                    });
                                }
                            });
                    }
                });
            }
        }
    });
</script>