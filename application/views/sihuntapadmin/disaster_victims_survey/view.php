<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">{{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">{{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-list"></i></span>
                                    <span class="d-none d-sm-block"><i class="fas fa-list"></i> DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="fa fa-check"></i></span>
                                    <span class="d-none d-sm-block"><i class="fa fa-check"></i> VERIFIKASI DATA</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">FORM VERIFIKASI PROPOSAL</h6>
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home" role="tab"><i class="fa fa-info"></i> Informasi Proposal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#profile" role="tab"><i class="fa fa-file-archive"></i> Lampiran Proposal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#messages" role="tab"><i class="fa fa-images"></i> Foto Dokumentasi</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active p-3" id="home" role="tabpanel">
                                <h6>Informasi Proposal Permohonan</h6>
                                <div class="table-responsive">
                                    <table class="table table-condensed">
                                        <tr>
                                            <th width="200px">Desa / Kecamatan</th>
                                            <th width="1px">:</th>
                                            <th>{{data.master_village_name}} / {{data.master_district_name}}</th>

                                        </tr>
                                        <tr>
                                            <th>Nama Proposal</th>
                                            <th width="1%">:</th>
                                            <th>{{data.name}}</th>
                                        </tr>
                                        <tr>
                                            <th>Jenis Bencana</th>
                                            <th>:</th>
                                            <th>{{data.master_disaster_name}}</th>
                                        </tr>
                                        <tr>
                                            <th>Tingkat Kerusakan</th>
                                            <th>:</th>
                                            <th>{{data.master_damage_name}}</th>
                                        </tr>
                                        <tr>
                                            <th>Tahun</th>
                                            <th>:</th>
                                            <th>{{data.year}}</th>
                                        </tr>
                                        <tr>
                                            <th>Jenis Permohonan</th>
                                            <th>:</th>
                                            <th>{{data.type}}</th>
                                        </tr>
                                        <tr>
                                            <th>Status</th>
                                            <th>:</th>
                                            <th>
                                                <!-- 'UNVERIFIED','ONVERIFIED','VERIFIED','APPROVED','REJECTED','TRANSFERRED' -->
                                                <span class="badge badge-warning" v-if="data.status =='UNVERIFIED'"> BELUM DIVERIFIKASI</span>
                                                <span class="badge badge-info" v-if="data.status =='ONVERIFIED'"> SEDANG DIVERIFIKASI</span>
                                                <span class="badge badge-secondary" v-if="data.status =='VERIFIED'"> SUDAH DIVERIFIKASI</span>
                                                <span class="badge badge-primary" v-if="data.status =='APPROVED'"> DITERIMA</span>
                                                <span class="badge badge-danger" v-if="data.status =='REJECTED'"> DITOLAK</span>
                                                <span class="badge badge-info" v-if="data.status =='TRANSFERRED'"> DITERIMA</span>
                                            </th>
                                        </tr>
                                    </table>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Catatan Verifikasi</label>
                                        <blockquote class="blockquote font-size-16 mb-0">
                                            <p>{{data.notes}}</p>
                                            <footer class="blockquote-footer mt-3">
                                                <cite>ditulis & diverifikasi oleh <b>{{data.usid_notes_name}}</b></cite>
                                            </footer>
                                        </blockquote>
                                    </div>
                                    <br>
                                    <div class="col-md-7" v-if="access.add == 1">
                                        <label>Catatan Penerimaan</label>
                                        <input type="text" class="form-control" placeholder="Tulis catatan penerimaan proposal.........." v-model="model.notes">
                                    </div>
                                    <div class="col-md-5" v-if="access.add == 1">
                                        <label v-if="data.status == 'UNVERIFIED'">Status Saat ini : [BELUM DIVERIFIKASI KECAMATAN]</label>
                                        <label v-if="data.status == 'ONVERIFIED'">Status Saat ini : [SEDANG DIVERIFIKASI KECAMATAN]</label>
                                        <label v-if="data.status == 'VERIFIED'">Status Saat ini : [SUDAH DIVERIFIKASI KECAMATAN]</label>
                                        <label v-if="data.status == 'APPROVED'">Status Saat ini : [SUDAH DITERIMA OLEH DPMD]</label>
                                        <label v-if="data.status == 'REJECTED'">Status Saat ini : [PROPOSAL DITOLAK]</label>

                                        <select class="form-control" v-model="model.status">
                                            <option selected value="">-- Pilih Status Verifikasi --</option>
                                            <option value="UNVERIFIED">Kembalikan Status Belum Verifikasi</option>
                                            <option value="ONVERIFIED">Kembalikan Status Sedang Diverifikasi</option>
                                            <option value="VERIFIED">Sudah Di Verifikasi</option>
                                            <option value="APPROVED">Diterima Oleh DPKPP</option>
                                            <option value="REJECTED">Status Tolak Proposal</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12 text-right" v-if="access.add == 1">
                                        <div class="mb-3">
                                            <br>
                                            <button type="button" v-on:click="" class="btn btn-outline-primary">Simpan Status</button>
                                        </div>
                                    </div>
                                    <div class="col-md-12" v-if="access.add == 1"><br>
                                        <div class="alert alert-danger" role="alert">
                                            Sebagai Administrator, Anda dapat merubah status Proposal ini sesuai dengan kondisi yang berlangsung. <strong>Silahkan pilih Aksi Verifikasi Pada Form diatas</strong>
                                        </div>

                                    </div>
                                    <br>

                                </div>
                            </div>
                            <div class="tab-pane p-3" id="profile" role="tabpanel">
                                <p class="font-14 mb-0">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Surat Laporan Kejadian Bencana Alam (SLKBA)</label>
                                        <object :data="data.slkba_file" width="100%" height="500px" type="application/pdf"> SLKBA Tidak Ada </object>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Surat Permohonan Bantuan Penanganan Pasca Bencana (SPBPPB)</label>
                                        <object :data="data.spbppb_file" width="100%" height="500px" type="application/pdf"> SPBPPB Tidak Ada </object>
                                    </div>
                                    <div class="col-md-6">
                                        <label>SK POKMAS </label>
                                        <object :data="data.sk_pokmas_file" width="100%" height="500px" type="application/pdf"> SK POKMAS Tidak Ada </object>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Rencana Anggaran Biaya (RAB)</label>
                                        <object :data="data.rab_file" width="100%" height="500px" type="application/pdf"> RAB Tidak Ada</object>
                                    </div>
                                </div>
                                </p>
                            </div>
                            <div class="tab-pane p-3" id="messages" role="tabpanel">
                                <p class="font-14 mb-0">
                                    Foto Dokumentasi
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="table-responsive" v-if="!loading">
                                <table class="table mb-0">
                                    <thead class="table-light">
                                        <tr>
                                            <th>#</th>
                                            <th>No</th>
                                            <th>Desa</th>
                                            <th>Nama</th>
                                            <th>No KK</th>
                                            <th>Bencana</th>
                                            <th>Klasifikasi Bantuan</th>
                                            <th>Verifikasi</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data, index) in list_recipient">
                                            <td v-if="access.edit == 1 || access.delete == 1">

                                                <a :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module + '/detailvictim/' + data.id" class="btn btn-outline-info waves-effect waves-light btn-sm" title="Edit" v-if="access.edit == 1">
                                                    <i class="fas fa-eye"></i> Survey
                                                </a>
                                            </td>
                                            <th scope="row">{{ index + 1 }}</th>
                                            <td>{{ data.master_village_name}}<br /><code>{{ data.master_district_name}}</code></td>
                                            <td>{{ data.name }}<br />
                                                <code>{{ data.ktp_number }}</code>
                                            </td>
                                            <td>{{ data.kk_number }} <br />
                                                <code>{{ data.address}} RT {{data.rt}}/{{data.rw}}</code>
                                            </td>
                                            <td>
                                                {{ data.master_disaster_name}}<br />
                                                <code>{{ data.master_damage_name }}</code>
                                            </td>
                                            <td>{{ data.master_help_name }} </td>
                                            <td>{{ data.submission}}</td>

                                            <td>
                                                <span v-if="data.status == 0" class="badge badge-warning">Sedang Diusulkan</span>
                                                <span v-if="data.status == 1" class="badge badge-info">Sudah Diverifikasi</span>
                                                <span v-if="data.status == 2" class="badge badge-info">Sudah Diverifikasi</span>
                                                <span v-if="data.status == 3" class="badge badge-success">Direlokasi Huntara</span>
                                                <span v-if="data.status == 4" class="badge badge-primary">Sudah di Relokasi Huntap</span>
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        Vue.component('my-input', {
            template: '<input v-attr="name: name" v-model="value" type="text">',
            data() {
                return {
                    value: ''
                };
            },
            props: ['name']
        });
        var appContent = new Vue({
            el: '#appContent',
            components: {
                'v-select': VueSelect.VueSelect,

            },
            data: {
                master_module_name: localStorage.getItem("master_module_name"),
                master_module_module: localStorage.getItem("master_module_module"),
                access: {
                    read: localStorage.getItem("read"),
                    add: localStorage.getItem("add"),
                    edit: localStorage.getItem("edit"),
                    delete: localStorage.getItem("delete"),
                    import: localStorage.getItem("import"),
                },
                id: <?php echo $id; ?>,
                loading: false,
                pokmas: false,
                individu: false,
                model: {
                    master_district_id: "",
                    master_village_id: "",
                    name: "",
                    master_disaster_id: "",
                    master_damage_id: "",
                    description: "",
                    year: "",
                    type: "",
                    status: "UNVERIFIED",
                    slkba_file: "",
                    spbppb_file: "",
                    sk_pokmas_file: "",
                    rab_file: ""
                },
                data: {},
                list_recipient: []
            },
            mounted: function() {
                this.validate_access();
                this.retrive();
                this.get_recipient();



            },
            methods: {
                validate_access() {
                    if (this.access.add == 0) {
                        window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                    }
                },
                get_village_by_district_id(id) {
                    query = "?master_district_id=" + id.target.value;
                    axios("<?php echo base_url(); ?>api/admin/master_village" + query).then(response => {
                        // for ( val in response.data.data ) {
                        //     this.list_master_village.push({ label: response.data.data[val].nama_desa, code: response.data.data[val].id_kel });
                        // }
                        this.list_master_village = response.data.data;
                    });
                },
                get_recipient() {
                    axios("<?php echo base_url(); ?>api/admin/disaster_victims?proposal_id=" + this.id).then(response => {
                        this.list_recipient = response.data.data;
                        this.totalPage = response.data.meta.totalPage;
                        this.loading = false;
                    });
                },
                retrive() {
                    axios("<?php echo base_url(); ?>api/admin/disaster_proposal/" + this.id).then(response => {
                        this.data = response.data.data;

                    });
                },
                verification(id) {
                    this.loading = true;
                    var status = '';
                    var text = '';
                    var text_button = '';
                    var formData = new FormData();
                    formData.append("notes_goverment", this.model.notes_goverment);

                    formData.append("usid_notes_goverment", localStorage.getItem("id"));
                    formData.append("status", this.model.status_verification);

                    if (this.model.notes_goverment == "" || this.model.notes_goverment == undefined || this.model.status_verification == "" || this.model.status_verification == undefined) {
                        Swal.fire('Error...', 'Silahkan Tulis Catatan Verifikasi atau Pilih Aksi terlebih dahulu!', 'error')
                    } else {
                        Swal.fire({
                            title: 'Apakah Anda yakin ?',
                            text: 'Ingin merubah status ini',
                            icon: 'info',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ya, Simpan!',
                            cancelButtonText: 'Tidak'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                axios.post("<?php echo base_url(); ?>api/admin/disaster_proposal/verification/" + this.id, formData, {
                                    headers: {
                                        'content-type': "application/json",
                                        'Authorization': 'Bearer ' + localStorage.getItem("token"),
                                    },
                                }).then(response => {
                                    if (response.data.code == 200) {
                                        Swal.fire({
                                            position: 'center',
                                            icon: 'success',
                                            text: "Anda berhasil melakukan Perubahan Status ini",
                                            showConfirmButton: false,
                                            timer: 1500,
                                        });
                                        this.retrive();
                                        this.get_rab();
                                        // this.list_proposal_step();
                                    } else {
                                        Swal.fire({
                                            position: 'center',
                                            icon: 'error',
                                            text: response.data.message,
                                            showConfirmButton: false,
                                            timer: 1500,
                                        });
                                        this.loading = false;
                                    }
                                });
                            }
                        })
                    }

                },
                save() {
                    this.loading = true;
                    error = false;
                    var formData = new FormData();
                    formData.append('master_district_id', this.model.master_district_id);
                    formData.append('master_village_id', this.model.master_village_id);
                    formData.append('name', this.model.name);
                    formData.append('master_disaster_id', this.model.master_disaster_id);
                    formData.append('master_damage_id', this.model.master_damage_id);
                    formData.append('description', this.model.description);
                    formData.append('year', this.model.year);
                    formData.append('status', this.model.status);
                    formData.append('type', this.model.type);
                    formData.append('slkba_file', this.$refs.slkba_file.files[0]);
                    formData.append('spbppb_file', this.$refs.spbppb_file.files[0]);
                    formData.append('rab_file', this.$refs.rab_file.files[0]);
                    formData.append('sk_pokmas_file', this.$refs.sk_pokmas_file.files[0]);
                    formData.append('created_by', localStorage.getItem("id"));
                    formData.append('created_dt', '<?= date('Y-m-d H:i:s'); ?>');

                    if (this.model.master_district_id == "" || this.model.master_village_id == "" || this.model.name == "" ||
                        this.model.master_disaster_id == "" || this.model.master_damage_id == "" || this.model.description == "") {
                        error = true;
                    }

                    if (!error) {
                        axios.post("<?php echo base_url(); ?>api/admin/disaster_proposal/" + this.id, formData, {
                                headers: {
                                    'content-type': "multipart/form-data",
                                    'Authorization': 'Bearer ' + localStorage.getItem("token"),
                                }
                            }).then(response => {
                                if (response.data.code == 200) {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        text: response.data.message,
                                        showConfirmButton: false,
                                        timer: 10000,
                                    });
                                    this.retrive();
                                    this.loading = false;
                                } else {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'error',
                                        text: response.data.message,
                                        showConfirmButton: false,
                                        timer: 1500,
                                    });
                                    this.loading = false;
                                }
                            })
                            .catch(error => {
                                if (error.response.data.code == 401) {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'warning',
                                        text: error.response.data.message,
                                        showConfirmButton: false,
                                        timer: 1500,
                                    }).then(function() {
                                        window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                                    });
                                } else {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'warning',
                                        text: error.response.data.message,
                                        showConfirmButton: false,
                                        timer: 1500,
                                    });
                                }
                                this.loading = false;
                            });
                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'warning',
                            text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        this.loading = false;
                    }
                }
            }
        });
    </script>