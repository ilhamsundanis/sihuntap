<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">{{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">{{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-list"></i></span>
                                    <span class="d-none d-sm-block"><i class="fas fa-list"></i> DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="fa fa-check"></i></span>
                                    <span class="d-none d-sm-block"><i class="fa fa-check"></i> DATA SURVEY</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">FORM SURVEY</h4>
                        <div class="col-md-12">
                            <div class="row">
                                <table class="table">
                                    <tr>
                                        <th>Nama</th>
                                        <th>:</th>
                                        <th>{{model.name}}</th>
                                    </tr>
                                    <tr>
                                        <td>KTP</td>
                                        <td>:</td>
                                        <td>{{model.ktp_number}}</td>
                                    </tr>
                                    <tr>
                                        <td>KK</td>
                                        <td>:</td>
                                        <td>{{model.kk_number}}</td>
                                    </tr>
                                    <tr>
                                        <td>Kelamin</td>
                                        <td>:</td>
                                        <td>
                                            <span v-if="model.gender == 'L'">LAKI LAKI</span>
                                            <span v-if="model.gender == 'P'">PEREMPUAN</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td>{{model.address}}</td>
                                    </tr>
                                    <tr>
                                        <td>RT / RW</td>
                                        <td>:</td>
                                        <td>{{model.rt}} / {{model.rw}}</td>
                                    </tr>
                                    <tr>
                                        <td>Desa/Kel</td>
                                        <td>:</td>
                                        <td>{{model.master_village_name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>:</td>
                                        <td>{{model.master_district_name}}</td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Bencana</th>
                                        <th>:</th>
                                        <th>{{model.master_disaster_name}}</th>
                                    </tr>
                                    <tr>
                                        <td>Tingkatan</td>
                                        <td>:</td>
                                        <td>{{model.master_damage_name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tgl Kejadian</td>
                                        <td>:</td>
                                        <td>{{model.incident_date}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tahun</td>
                                        <td>:</td>
                                        <td>{{model.year}}</td>
                                    </tr>
                                    <tr>
                                        <td>Penerimaan</td>
                                        <td>:</td>
                                        <td>{{model.submission}}</td>
                                    </tr>
                                    <tr>
                                        <td>Kategori Bantuan</td>
                                        <td>:</td>
                                        <td>{{model.master_help_name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah (Rp)</td>
                                        <td>:</td>
                                        <td>Rp. {{model.master_help_ammount}}</td>
                                    </tr>
                                    <tr>
                                        <td>Nomor Rekening</td>
                                        <td>:</td>
                                        <td>{{model.account_number}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tgl Transfer</td>
                                        <td>:</td>
                                        <td>{{model.transfer_date}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h6>ISIAN HASIL SURVEY</h6>
                        <div class="row">

                            <div class="mb-3 col-lg-2">
                                <label>Kategori Korban</label>
                                <select v-model="model.non_category" class="form-control small" required>
                                    <option value="" selected> -- Pilih Kategori--</option>
                                    <option value="TERDAMPAK">TERDAMPAK</option>
                                    <option value="TERANCAM">TERANCAM</option>
                                    <option value="TIDAK TERDAMPAK">TIDAK TERDAMPAK</option>
                                </select>
                            </div>

                            <!-- <div class="mb-3 col-lg-2">
                                <label>Status Ancaman</label>
                                <select v-model="model.threatened" class="form-control small" required>
                                    <option value="" selected> -- Pilih Status--</option>
                                    <option value="TERANCAM">TERANCAM</option>
                                    <option value="TIDAK TERANCAM">TIDAK TERANCAM</option>
                                </select>
                            </div> -->
                            <div class="mb-3 col-lg-4">
                                <label>Tingkat Kerusakan</label>
                                <v-select placeholder="Kerusakan" :options="list_master_damage" v-model="model.master_damage_id" :reduce="master_damage_id => master_damage_id.code" class="form-control" style="padding: 3px;"></v-select>
                            </div>
                            <div class="mb-3 col-lg-6">
                                <label>Klasifikasi Penerimaan Bantuan</label>
                                <v-select placeholder="--Pilih Klasifikasi--" :options="list_master_help" v-model="model.master_help_id" :reduce="master_help_id => master_help_id.code" class="form-control" style="padding: 3px;"></v-select>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>Status SIPD</label>
                                <select v-model="model.entered" class="form-control small" required>
                                    <option value="" selected> -- Pilih Status--</option>
                                    <option value="SUDAH MASUK">SUDAH MASUK</option>
                                    <option value="BELUM MASUK">BELUM MASUK</option>
                                </select>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>Status Survey</label>
                                <select v-model="model.surveyed" class="form-control small" required>
                                    <option value="" selected> -- Pilih Status--</option>
                                    <option value="SUDAH DISURVEY">SUDAH DISURVEY</option>
                                    <option value="BELUM DISURVEY">BELUM DISURVEY</option>
                                </select>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>Tanggal Survey</label>
                                <input class="form-control" type="date" placeholder="RW" v-model="model.survey_date" required>
                            </div>


                            <div class="mb-3 col-lg-2">
                                <label>Status Usulan</label>
                                <select v-model="model.submission" class="form-control small" required>
                                    <option value="" selected>-- Pilih Status--</option>
                                    <option value="DITERIMA">DITERIMA</option>
                                    <option value="DITOLAK">DITOLAK</option>
                                </select>
                            </div>

                        </div>
                        <div class="row">
                            <h6 class="col-lg-12">Dokumentasi</h6>
                            <div class="mb-3 col-lg-2">
                                <label>Foto Dokumentasi [1]</label>
                                <input class="form-control" type="file" accept="image/*" placeholder="RW" ref="survey_file_1" required>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>Foto Dokumentasi [2]</label>
                                <input class="form-control" type="file" accept="image/*" placeholder="RW" ref="survey_file_2" required>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>Foto Dokumentasi [3]</label>
                                <input class="form-control" type="file" accept="image/*" placeholder="RW" ref="survey_file_3" required>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>Foto Dokumentasi [4]</label>
                                <input class="form-control" type="file" accept="image/*" placeholder="RW" ref="survey_file_4" required>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>Foto Dokumentasi [5]</label>
                                <input class="form-control" type="file" accept="image/*" placeholder="RW" ref="survey_file_5" required>
                            </div>

                        </div>


                        <div class="form-group row">
                            <div class="col-sm-11">
                                <br />
                                <button type="submit" class="btn btn-primary" v-on:click="save">SIMPAN DATA</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    Vue.component('my-input', {
        template: '<input v-attr="name: name" v-model="value" type="text">',
        data() {
            return {
                value: ''
            };
        },
        props: ['name']
    });
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,

        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },
            id: <?php echo $id; ?>,
            loading: false,
            district_id: "",
            village_id: "",
            model: {
                name: "",
                ktp_number: "",
                kk_number: "",
                gender: "",
                status: 1,
                address: "",
                master_district_id: "",
                master_village_id: "",
                master_district_name: "",
                master_village_name: "",
                rt: "",
                rw: "",
                master_disaster_id: "",
                non_category: "",
                threatened: "",
                master_damage_id: "",
                year: "",
                entered: "",
                surveyed: "",
                incident_date: "",
                survey_date: "",
                master_help_id: "",
                master_help_name: "",
                submission: ""
            },

            list_company: [],
            list_master_damage: [],
            list_master_help: [],
            list_master_disaster: [],
            list_master_district: [],
            list_master_village: []
        },
        mounted: function() {
            this.validate_access();
            axios("<?php echo base_url(); ?>api/admin/master_disaster").then(response => {
                for (val in response.data.data) {
                    this.list_master_disaster.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_damage").then(response => {
                for (val in response.data.data) {
                    this.list_master_damage.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_help").then(response => {
                for (val in response.data.data) {
                    this.list_master_help.push({
                        label: response.data.data[val].name + " - [Rp. " + response.data.data[val].ammount + "]",
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_district?master_city_id=3201").then(response => {
                this.list_master_district = response.data.data;
            });

            axios("<?php echo base_url(); ?>api/admin/disaster_victims_survey/" + this.id).then(response => {
                var data = response.data.data;
                this.model.name = data.name;
                this.model.ktp_number = data.ktp_number;
                this.model.kk_number = data.kk_number;
                this.model.gender = data.gender;
                this.model.address = data.address;
                this.model.master_district_id = data.master_district_id;
                this.model.master_village_id = data.master_village_id;
                this.model.master_district_name = data.master_district_name;
                this.model.master_village_name = data.master_village_name;
                this.model.master_disaster_id = data.master_disaster_id;
                this.model.master_help_name = data.master_help_name;
                this.model.rt = data.rt;
                this.model.rw = data.rw;
                this.model.non_category = data.non_category;
                this.model.threatened = data.threatened;
                this.model.master_damage_id = data.master_damage_id;
                this.model.year = data.year;
                this.model.entered = data.entered;
                this.model.surveyed = data.surveyed;
                this.model.incident_date = data.incident_date;
                this.model.survey_date = data.survey_date;
                this.model.master_help_id = data.master_help_id;
                this.model.submission = data.submission;
            });

        },
        methods: {
            validate_access() {
                if (this.access.add == 0) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            get_village_by_district_id(id) {
                query = "?master_district_id=" + id.target.value;
                axios("<?php echo base_url(); ?>api/admin/master_village" + query).then(response => {
                    // for ( val in response.data.data ) {
                    //     this.list_master_village.push({ label: response.data.data[val].nama_desa, code: response.data.data[val].id_kel });
                    // }
                    this.list_master_village = response.data.data;
                });
            },

            save() {
                this.loading = true;
                error = false;
                var formData = new FormData();
                formData.append('threatened', this.model.threatened);
                formData.append('non_category', this.model.non_category);
                formData.append('master_damage_id', this.model.master_damage_id);
                formData.append("surveyed", this.model.surveyed);
                formData.append('master_help_id', this.model.master_help_id);
                formData.append('status', this.model.status);
                // formData.append('ktp_file', this.$refs.ktp_file.files[0]);
                // formData.append('kk_file', this.$refs.kk_file.files[0]);
                formData.append('survey_date', this.model.survey_date);
                formData.append('submission', this.model.submission);
                formData.append('entered', this.model.entered);

                formData.append('survey_file_1', this.$refs.survey_file_1.files[0]);
                formData.append('survey_file_2', this.$refs.survey_file_2.files[0]);
                formData.append('survey_file_3', this.$refs.survey_file_3.files[0]);
                formData.append('survey_file_4', this.$refs.survey_file_4.files[0]);
                formData.append('survey_file_5', this.$refs.survey_file_5.files[0]);


                error = false;
                if (this.model.threatened == "" || this.model.non_category == "" || this.model.master_damage_id == "" || this.model.surveyed == "" || this.model.master_help_id == "") {
                    error = true;
                }

                if (!error) {
                    axios.post("<?php echo base_url(); ?>api/admin/disaster_victims_survey/" + this.id, formData, {
                            headers: {
                                'content-type': "multipart/form-data",
                                'Authorization': 'Bearer ' + localStorage.getItem("token"),
                            }
                        }).then(response => {
                            if (response.data.code == 200) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 10000,
                                });
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/" + master_module_module;

                                this.loading = false;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                                this.loading = false;
                            }
                        })
                        .catch(error => {
                            if (error.response.data.code == 401) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                }).then(function() {
                                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                            }
                            this.loading = false;
                        });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            }
        }
    });
</script>