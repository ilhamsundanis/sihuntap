<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">{{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">{{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module" role="tab">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">DATA HUNIAN TETAP</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module + '/huntara'" role="tab">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">DATA HUNIAN SEMENTARA</span>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>

            

                <div class="card">
                    <div class="card-body">
                        <div class="text-center" v-if="loading">
                            <?php $this->load->view('sihuntapadmin/templates/home/loading'); ?>
                        </div>
                        <div class="table-responsive" v-if="!loading">
                            <table class="table mb-0">
                                <thead class="table-light">
                                    <tr>
                                        <th>#</th>
                                        <th>Proposal</th>
                                        <th>Alamat Relokasi</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th v-if="access.edit == 1 || access.delete == 1">Tindakan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(data, index) in list">
                                        <th scope="row">{{ index + 1 }}</th>
                                        <td>{{ data.proposal_name }}</td>
                                        <td>{{ data.master_village_name }}, {{data.master_district_name}} </td>
                                        
                                        <td>{{ data.count_recipient}} KK</td>
                                        <td>
                                            <span v-if="data.status == 0" class="badge badge-warning">Diusulkan</span>
                                            <span v-if="data.status == 1" class="badge badge-danger">Ditolak</span>
                                            <span v-if="data.status == 2" class="badge badge-info">Sudah Diverifikasi</span>
                                            <span v-if="data.status == 3" class="badge badge-success">Direlokasi Huntara</span>
                                            <span v-if="data.status == 4" class="badge badge-primary">Sudah di Relokasi Huntap</span>
                                        </td>
                                        <td v-if="access.edit == 1 || access.delete == 1">
                                            
                                            <a :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module + '/view/' + data.proposal_id" 
                                                class="btn btn-outline-info waves-effect waves-light btn-sm" 
                                                title="Edit" v-if="access.edit == 1">
                                                    <i class="fas fa-eye"></i> Lihat
                                            </a>
                                            <a :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module + '/attachment/' + data.proposal_id" 
                                                class="btn btn-outline-primary waves-effect waves-light btn-sm" 
                                                title="Edit" v-if="data.status == 4">
                                                    <i class="fas fa-plus"></i> Tambah Lampiran
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="col-md-12" style="text-align: center;">
                            <template>
                                <paginate v-model="currentPage" :page-count="totalPage" :click-handler="clickCallback" :prev-text="'Prev'" :next-text="'Next'" :container-class="'vue-pagination'"></paginate>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'paginate': VuejsPaginate,
            'v-select': VueSelect.VueSelect,
        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },

            list: [],
            currentPage: <?php echo empty($this->uri->segment(4)) ? 1 : $this->uri->segment(4);?>,     
            totalPage: 1,

            loading: false,
            filter: {
                id: (localStorage.getItem("filter_id") == null) ? "" : localStorage.getItem("filter_id"),
                proposal_id: (localStorage.getItem("filter_proposal_id") == null) ? "" : localStorage.getItem("filter_proposal_id"),
                master_village_id: (localStorage.getItem("filter_master_village_id") == null) ? "" : localStorage.getItem("filter_master_village_id"),
                master_district_id: (localStorage.getItem("filter_master_district_id") == null) ? "" : localStorage.getItem("filter_master_district_id"),
            },
        },
        mounted: function () {
            

            this.retrive();
        },
        methods: {    
            clickCallback(pageNum) {
                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/"+ this.master_module_module +"/index/" + pageNum;
            },
            retrive() {
                this.loading = true;
                this.filter_data();

                var query = "?orderBy=id&sort=desc&limit=50&page=" + this.currentPage;
                query += "&id=" + this.filter.id;
                query += "&proposal_id=" + this.filter.proposal_id;
                query += "&master_village_id=" + this.filter.master_village_id;
                query += "&master_district_id=" + this.filter.master_district_id;
                axios("<?php echo base_url(); ?>api/admin/huntap" + query).then(response => {
                    this.list      = response.data.data;
                    this.totalPage = response.data.meta.totalPage;
                    this.loading   = false;
                });
            },
            filter_data() {
                localStorage.setItem("filter_id", this.filter.id);
                localStorage.setItem("filter_company_id", this.filter.company_id);
                localStorage.setItem("filter_date_from", this.filter.date_from);
                localStorage.setItem("filter_date_to", this.filter.date_to);

            },
            reset_filter() {
                this.filter.id   = "";
                this.filter.company_id = "";
                this.filter.date_from = "";
                this.filter.date_to = "";

                this.retrive();
            },
            deleteItem(id) {
                Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Anda akan menghapus data ini",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                    if (result.isConfirmed) {
                        axios.delete("<?php echo base_url(); ?>api/admin/huntap/" + id, {
                            headers: {
                                Authorization: 'Bearer ' + localStorage.getItem("token"),
                            },
                        }).then(response => {
                            if ( response.data.code == 200 ) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: "Anda berhasil menghapus data ini",
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                                this.retrive();
                            }
                        })
                        .catch(error => {
                            if ( error.response.data.code == 401 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            }).then(function() {
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }
                        });
                    }
                });
            }
        }
    });
</script>