<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">{{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">{{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">DATA HUNIAN TETAP</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                    <span class="d-none d-sm-block">RELOKASI KE HUNIAN TETAP</span>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Data Proposal</h4>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                        <label>Nama Proposal</label>
                                        <input readonly type="text" v-model="model.name" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                        <label>Kategori Bencana</label>
                                        <v-select disabled placeholder="Jenis Bencana" :options="list_disaster" v-model="model.master_disaster_id" :reduce="master_disaster_id => master_disaster_id.code" class="form-control" style="padding: 3px;"></v-select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                        <label>Kategori Kerusakan</label>
                                        <v-select disabled placeholder="Jenis Bencana" :options="list_damage" v-model="model.master_damage_id" :reduce="master_damage_id => master_damage_id.code" class="form-control" style="padding: 3px;"></v-select>
                                </div>
                            </div>
                            
                               
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <br/><br><h4>Relokasi Ke Hunian Tetap</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                        <label>Lokasi Kecamatan</label>
                                        <v-select placeholder="Pilih Kecamatan" :options="list_master_district" v-model="huntap.master_district_id" :reduce="master_district_id => master_district_id.code" class="form-control" style="padding: 3px;" @input="get_village_by_district_id"></v-select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                        <label>Lokasi Desa</label>
                                        <v-select placeholder="Nama Desa / Kelurahan" :options="list_master_village" v-model="huntap.master_village_id" :reduce="master_village_id => master_village_id.code" class="form-control" style="padding: 3px;"></v-select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                        <label>Alamat Hunian Tetap</label>
                                        <summernote v-model="huntap.address"></summernote>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <dv class="mb-3">
                                    <label>File Lampiran</label>
                                    <input type="file" ref="file" class="form-control">
                                </dv>
                            </div>
                        </div>
                        <div>
                            <div style="text-align: center;margin-top: 50px;">                                
                                <button type="button" class="btn btn-light waves-effect" v-if="loading">
                                    <i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i>
                                    Loading ...
                                </button>
                                <button type="button" v-if="model.status == 3" v-on:click="save" class="btn btn-primary waves-effect waves-light me-1" v-else>
                                    Relokasi Ke Huntap
                                </button>

                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>

<script>
    $('#summernote').summernote('disable');
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,
        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },
            id: '<?php echo $id;?>',
            loading: false,
            model: {
                    name:"", 
                    description: "",
                    master_disaster_id: "",
                    master_damage_id: "",
                    status: "",
                    status_description: ""
                },
            huntap: {
                proposal_id: "",
                master_village_id: "",
                master_district_id: "",
                address: "",
                file: ""
            },
            list_company: [],
            list_damage :[],
            list_recipient: [],
            list_disaster : [],
            list_master_district: [],
            list_master_village: []
        },
        mounted: function () {
            this.validate_access();
            axios("<?php echo base_url(); ?>api/admin/master_disaster").then(response => {
                for ( val in response.data.data ) {
                    this.list_disaster.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_damage").then(response => {
                for ( val in response.data.data ) {
                    this.list_damage.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/proposal/"+ this.id).then(response => {
                var data = response.data.data;
                this.model.name = data.name;
                this.model.master_damage_id = data.master_damage_id;
                this.model.master_disaster_id = data.master_disaster_id;
                this.model.description = data.description;
                this.model.status = data.status;
                this.huntap.proposal_id = this.id;
                this.huntap.master_district_id = data.master_district_id;
                // this.get_village_by_district_id(data.master_district_id);
            });
            
            axios("<?php echo base_url(); ?>api/admin/master_district?master_city_id=3201").then(response => {
                for ( val in response.data.data ) {
                    this.list_master_district.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                }
            });

            this.retrive_recipient();
        },
        methods: {    
            validate_access() {
                if ( this.access.add == 0 ) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            retrive_recipient()
            {
                axios("<?php echo base_url(); ?>api/admin/residence_recipient?proposal_id="+ this.id).then(response => {
                    this.list_recipient = response.data.data;
                });
            },
            get_village_by_district_id(id)
            {
                this.huntap.master_village_id = "";
                this.list_master_village = [];
                axios("<?php echo base_url(); ?>api/admin/master_village?master_district_id="+ id).then(response => {
                    for ( val in response.data.data ) {
                        this.list_master_village.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                    }
                });
            },
           
            save() {
                this.loading = true;

                var formData = new FormData();
                formData.append('proposal_id', this.huntap.proposal_id);
                formData.append('master_village_id', this.huntap.master_village_id);
                formData.append('master_district_id', this.huntap.master_district_id)
                formData.append('address', this.huntap.address);
                formData.append('file', this.$refs.file.files[0]);
                
                error = false;

                if(this.huntap.proposal_id == '' || this.huntap.master_village_id == '' || this.huntap.master_district_id == ''){
                    error = true;
                }

                if ( !error ) {
                    axios.post("<?php echo base_url(); ?>api/admin/huntap", formData, {
                        headers: {
                            'content-type': "multipart/form-data",
                            'Authorization': 'Bearer ' + localStorage.getItem("token"),
                        }
                    }).then(response => {
                        if ( response.data.code == 200 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 10000,
                            });
                            // $('#showModal').modal('hide');
                            window.location.href = "<?php echo base_url(); ?>sihuntapadmin/huntap";
                            this.loading = false;
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                            this.loading = false;
                        }
                    })
                    .catch(error => {
                        if ( error.response.data.code == 401 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            }).then(function() {
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }
                        this.loading    = false;
                    });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            },
            
        }
    });
</script>