<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">{{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">{{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                    <span class="d-none d-sm-block">VERIFIKASI DATA</span>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Relokasi Ke Huntara</h4>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                        <label>Nama Proposal</label>
                                        <input readonly type="text" v-model="model.name" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                        <label>Kategori Bencana</label>
                                        <v-select disabled placeholder="Jenis Bencana" :options="list_disaster" v-model="model.master_disaster_id" :reduce="master_disaster_id => master_disaster_id.code" class="form-control" style="padding: 3px;"></v-select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                        <label>Kategori Kerusakan</label>
                                        <v-select disabled placeholder="Jenis Bencana" :options="list_damage" v-model="model.master_damage_id" :reduce="master_damage_id => master_damage_id.code" class="form-control" style="padding: 3px;"></v-select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                        <label>Keterangan</label>
                                        <summernote readonly disabled v-model="model.description"></summernote>
                                </div>
                            </div>
                               
                        </div>
                        
                        <div class="row">
                            <div class="table-responsive" v-if="!loading">
                                <table class="table mb-0">
                                    <thead class="table-light">
                                        <tr>
                                            <th>#</th>
                                            <th>Nama</th>
                                            <th>NIK</th>
                                            <th>No KK</th>
                                            <th>Alamat</th>
                                            <th v-if="access.edit == 1 || access.delete == 1">Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data, index) in list_recipient">
                                            <th scope="row">{{ index + 1 }}</th>
                                            <td>{{ data.name }}</td>
                                            <td>{{ data.ktp_number }} </td>
                                            <td>{{ data.kk_number }} </td>
                                            
                                            <td>{{ data.master_village_name}}, {{data.master_district_name}} </td>
                                            <td v-if="access.edit == 1 || access.delete == 1">
                                                
                                                <!-- <a :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module + '/view_recipient/' + data.id" 
                                                    class="btn btn-outline-info waves-effect waves-light btn-sm" 
                                                    title="Edit" v-if="access.edit == 1">
                                                        <i class="fas fa-pencil-alt"></i>
                                                </a>

                                                <a href="javascript:void(0)" 
                                                    class="btn btn-outline-danger waves-effect waves-light btn-sm" 
                                                    title="Hapus" 
                                                    v-on:click="deleteItem(data.id)" v-if="access.delete == 1">
                                                        <i class="fas fa-trash"></i>
                                                </a> -->
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div>
                            <div style="text-align: center;margin-top: 50px;">                                
                                <span v-if="model.status == 0">DATA BARU DIUSULKAN</span>
                                <span v-if="model.status == 2">DATA SUDAH DIVERIFIKASI</span>
                                <span v-if="model.status == 1">PERMOHONAN DITOLAK</span>
                                <span v-if="model.status == 3">DATA SUDAH DIHUNIAN SEMENTARA</span>
                                <span v-if="model.status == 4">DATA SUDAH DIHUNIAN TETAP</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-center" tabindex="-1" id="showModal" role="dialog" aria-labelledby="showModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" >
            <div class="modal-content">
                <form action="" > 
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Verifikasi Berkas</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" v-model="modal.id">
                        <input type="hidden" v-model="modal.id_status">
                        <label>Isi Keterangan Verifikasi</label>
                        <textarea v-model="modal.status_description" class="form-control" placeholder="Isikan keterangan verifikasi"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button v-if="modal.id_status == 2" type="button" v-on:click="accept(modal.id)" class="btn btn-primary">Setujui dan Verifikasi</button>
                        <button v-if="modal.id_status == 1" type="button" v-on:click="reject(modal.id)" class="btn btn-warning">Tolak Proposal</button>
                        
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

<script>
    $('#summernote').summernote('disable');
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,
        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },
            id: '<?php echo $id;?>',
            loading: false,
            model: {
                    name:"", 
                    description: "",
                    master_disaster_id: "",
                    master_damage_id: "",
                    status: "",
                    status_description: "",
                    master_district_id: "",
                    master_village_id: "",
                    address: "",
                },
            modal: {
                id: "",
                id_status: "",
                status_description: ""
            },
            list_company: [],
            list_damage :[],
            list_recipient: [],
            list_disaster : []
        },
        mounted: function () {
            this.validate_access();
            axios("<?php echo base_url(); ?>api/admin/master_disaster").then(response => {
                for ( val in response.data.data ) {
                    this.list_disaster.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_damage").then(response => {
                for ( val in response.data.data ) {
                    this.list_damage.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/proposal/"+ this.id).then(response => {
                var data = response.data.data;
                this.model.name = data.name;
                this.model.master_damage_id = data.master_damage_id;
                this.model.master_disaster_id = data.master_disaster_id;
                this.model.description = data.description;
                this.model.status = data.status;
            });
            
            this.retrive_recipient();
        },
        methods: {    
            validate_access() {
                if ( this.access.add == 0 ) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            retrive_recipient()
            {
                axios("<?php echo base_url(); ?>api/admin/residence_recipient?proposal_id="+ this.id).then(response => {
                    this.list_recipient = response.data.data;
                });
            },
            showModal(id,id_status){
                this.modal.id = id;
                this.modal.id_status = id_status;
                $('#showModal').modal('show');
            },
            accept(id) {
                this.loading = true;

                var formData = new FormData();
                formData.append('status', this.modal.id_status);
                formData.append('status_description', this.modal.status_description);
                
                error = false;

                if ( !error ) {
                    axios.post("<?php echo base_url(); ?>api/admin/proposal/accept/" + this.id, formData, {
                        headers: {
                            'content-type': "application/json",
                            'Authorization': 'Bearer ' + localStorage.getItem("token"),
                        }
                    }).then(response => {
                        if ( response.data.code == 200 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 10000,
                            });
                            $('#showModal').modal('hide');
                            
                            this.loading = false;
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                            this.loading = false;
                        }
                    })
                    .catch(error => {
                        if ( error.response.data.code == 401 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            }).then(function() {
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }
                        this.loading    = false;
                    });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            },
            reject(id) {
                this.loading = true;

                var formData = new FormData();
                formData.append('status', this.modal.id_status);
                formData.append('status_description', this.modal.status_description);
                
                error = false;

                if ( !error ) {
                    axios.post("<?php echo base_url(); ?>api/admin/proposal/accept/" + this.id, formData, {
                        headers: {
                            'content-type': "application/json",
                            'Authorization': 'Bearer ' + localStorage.getItem("token"),
                        }
                    }).then(response => {
                        if ( response.data.code == 200 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 10000,
                            });

                             $('#showModal').modal('hide');

                            this.loading = false;
                            window.location.href = "<?php echo base_url(); ?>sihuntapadmin/verification";
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                            this.loading = false;
                        }
                    })
                    .catch(error => {
                        if ( error.response.data.code == 401 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            }).then(function() {
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }
                        this.loading    = false;
                    });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            }
        }
    });
</script>