<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Lampiran {{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">Lampiran {{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">DATA HUNIAN TETAP</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                    <span class="d-none d-sm-block">TAMBAH DATA LAMPIRAN</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Tambah Lampiran</h4>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label>Nama Proposal</label>
                                    <input readonly type="text" v-model="model.name" class="form-control">
                                </div>
                            </div>


                        </div>

                        <div class="row">
                            <div class="alert-success" role="alert">
                                <strong>Tetapkan Penerima Hunian Tetap!</strong> Silahkan tekan tombol tetapkan untuk penerima.
                            </div>
                            <div class="table-responsive" v-if="!loading">
                                <table class="table mb-0">
                                    <thead class="table-light">
                                        <tr>
                                            <th>#</th>
                                            <th>Nama</th>
                                            <th>NIK</th>
                                            <th>No KK</th>
                                            <th>Alamat</th>
                                            <th v-if="access.edit == 1 || access.delete == 1">Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data, index) in list_recipient">
                                            <th scope="row">{{ index + 1 }}</th>
                                            <td>{{ data.name }}</td>
                                            <td>{{ data.ktp_number }} </td>
                                            <td>{{ data.kk_number }} </td>

                                            <td>{{ data.master_village_name}}, {{data.master_district_name}} </td>
                                            <td v-if="access.edit == 1 || access.delete == 1">

                                                <a href="javascript:void(0)" class="btn btn-outline-info waves-effect waves-light btn-sm" title="Edit" v-if="data.status == 3" v-on:click="action(data.id,4)">
                                                    <i class="fas fa-check"></i> Tetapkan
                                                </a>

                                                <a href="javascript:void(0)" class="btn btn-outline-danger waves-effect waves-light btn-sm" title="Hapus" v-on:click="action(data.id,3)" v-if="data.status == 4">
                                                    <i class="fas fa-trash"></i> Batal
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div>
                            <div style="text-align: center;margin-top: 50px;">
                                <span v-if="model.status == 0">DATA BARU DIUSULKAN</span>
                                <span v-if="model.status == 2">DATA SUDAH DIVERIFIKASI</span>
                                <span v-if="model.status == 1">PERMOHONAN DITOLAK</span>
                                <span v-if="model.status == 3">DATA SUDAH DIHUNIAN SEMENTARA</span>
                                <span v-if="model.status == 4">DATA SUDAH DIHUNIAN TETAP</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $('#summernote').summernote('disable');
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,
        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },
            id: '<?php echo $id; ?>',
            loading: false,
            model: {
                name: "",
                description: "",
                master_disaster_id: "",
                master_damage_id: "",
                status: "",
                status_description: "",
                master_district_id: "",
                master_village_id: "",
                address: "",
            },
            modal: {
                id: "",
                id_status: "",
                status_description: ""
            },
            list_company: [],
            list_damage: [],
            list_recipient: [],
            list_disaster: []
        },
        mounted: function() {
            this.validate_access();
            axios("<?php echo base_url(); ?>api/admin/master_disaster").then(response => {
                for (val in response.data.data) {
                    this.list_disaster.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_damage").then(response => {
                for (val in response.data.data) {
                    this.list_damage.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/proposal/" + this.id).then(response => {
                var data = response.data.data;
                this.model.name = data.name;
                this.model.master_damage_id = data.master_damage_id;
                this.model.master_disaster_id = data.master_disaster_id;
                this.model.description = data.description;
                this.model.status = data.status;
            });

            this.retrive_recipient();
        },
        methods: {
            validate_access() {
                if (this.access.add == 0) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            retrive_recipient() {
                axios("<?php echo base_url(); ?>api/admin/residence_recipient?proposal_id=" + this.id).then(response => {
                    this.list_recipient = response.data.data;
                });
            },
            action(id, id_status) {
                this.loading = true;

                var formData = {
                    status: id_status
                }

                error = false;
                for (data in formData) {
                    if (formData[data] == "" || formData[data] == undefined) {
                        error = true
                    }
                }
                if (!error) {
                    axios.put("<?php echo base_url(); ?>api/admin/residence_recipient/" + id, formData, {
                            headers: {
                                'content-type': "application/json",
                                'Authorization': 'Bearer ' + localStorage.getItem("token"),
                            }
                        }).then(response => {
                            if (response.data.code == 200) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 10000,
                                });
                                this.retrive_recipient();
                                this.loading = false;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                                this.loading = false;
                            }
                        })
                        .catch(error => {
                            if (error.response.data.code == 401) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                }).then(function() {
                                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                            }
                            this.loading = false;
                        });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            },

        }
    });
</script>