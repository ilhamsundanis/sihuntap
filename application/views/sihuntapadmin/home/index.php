<div class="page-content-wrapper ">

    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
                            <!-- <li class="breadcrumb-item active">Dashboard</li> -->
                        </ol>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- end page title end breadcrumb -->



        <div class="row">
            <div class="col-xl-12">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="col-3 align-self-center">
                                        <div class="round">
                                            <i class="mdi mdi-image"></i>
                                        </div>
                                    </div>
                                    <div class="col-9 align-self-center text-right">
                                        <div class="m-l-10">
                                            <h5 class="mt-0"><?= total_slider(); ?></h5>
                                            <p class="mb-0 text-muted">Slider Beranda</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-3" style="height:3px;">
                                    <div class="progress-bar  bg-success" role="progressbar" style="width: 35%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="col-3 align-self-center">
                                        <div class="round">
                                            <i class="mdi mdi-account-multiple-plus"></i>
                                        </div>
                                    </div>
                                    <div class="col-9 text-right align-self-center">
                                        <div class="m-l-10 ">
                                            <h5 class="mt-0"><?= total_users(); ?></h5>
                                            <p class="mb-0 text-muted">Akun Pengguna</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-3" style="height:3px;">
                                    <div class="progress-bar bg-warning" role="progressbar" style="width: 48%;" aria-valuenow="48" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="search-type-arrow"></div>
                                <div class="d-flex flex-row">
                                    <div class="col-3 align-self-center">
                                        <div class="round ">
                                            <i class="mdi mdi-file"></i>
                                        </div>
                                    </div>
                                    <div class="col-9 align-self-center text-right">
                                        <div class="m-l-10 ">
                                            <h5 class="mt-0"><?= total_document(); ?></h5>
                                            <p class="mb-0 text-muted">Dokumen Unduhan</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-3" style="height:3px;">
                                    <div class="progress-bar bg-danger" role="progressbar" style="width: 61%;" aria-valuenow="61" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="search-type-arrow"></div>
                                <div class="d-flex flex-row">
                                    <div class="col-3 align-self-center">
                                        <div class="round ">
                                            <i class="mdi mdi-newspaper"></i>
                                        </div>
                                    </div>
                                    <div class="col-9 align-self-center text-right">
                                        <div class="m-l-10 ">
                                            <h5 class="mt-0"><?= total_news(); ?></h5>
                                            <p class="mb-0 text-muted">Artikel Berita</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-3" style="height:3px;">
                                    <div class="progress-bar bg-danger" role="progressbar" style="width: 61%;" aria-valuenow="61" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <h6>Data Hunian Tetap</h6>
            </div>
            <div class="col-xl-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="col-3 align-self-center">
                                        <div class="round">
                                            <i class="mdi mdi-road"></i>
                                        </div>
                                    </div>
                                    <div class="col-9 align-self-center text-right">
                                        <div class="m-l-10">
                                            <h5 class="mt-0"><?php echo total_data(1); ?> KK</h5>
                                            <p class="mb-0 text-muted">Proses Diusulkan</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-3" style="height:3px;">
                                    <div class="progress-bar  bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="col-3 align-self-center">
                                        <div class="round">
                                            <i class="mdi mdi-home-variant"></i>
                                        </div>
                                    </div>
                                    <div class="col-9 align-self-center text-right">
                                        <div class="m-l-10">
                                            <h5 class="mt-0"><?php echo total_penerima(); ?> KK</h5>
                                            <p class="mb-0 text-muted">Total Penerima</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-3" style="height:3px;">
                                    <div class="progress-bar  bg-primary" role="progressbar" style="width: 100%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                    <!--end col-->

                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="col-3 align-self-center">
                                        <div class="round">
                                            <i class="mdi mdi-home"></i>
                                        </div>
                                    </div>
                                    <div class="col-9 align-self-center text-right">
                                        <div class="m-l-10">
                                            <h5 class="mt-0"><?php echo total_data(4); ?> KK</h5>
                                            <p class="mb-0 text-muted">Hunian Tetap</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-3" style="height:3px;">
                                    <div class="progress-bar  bg-success" role="progressbar" style="width: 100%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                    <!--end col-->

                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="col-3 align-self-center">
                                        <div class="round">
                                            <i class="mdi mdi-home-map-marker"></i>
                                        </div>
                                    </div>
                                    <div class="col-9 align-self-center text-right">
                                        <div class="m-l-10">
                                            <h5 class="mt-0"><?php echo total_data(3); ?> KK</h5>
                                            <p class="mb-0 text-muted">Hunian Sementara</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-3" style="height:3px;">
                                    <div class="progress-bar  bg-info" role="progressbar" style="width: 100%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                    <!--end col-->

                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="col-3 align-self-center">
                                        <div class="round">
                                            <i class="mdi mdi-road"></i>
                                        </div>
                                    </div>
                                    <div class="col-9 align-self-center text-right">
                                        <div class="m-l-10">
                                            <h5 class="mt-0"><?php echo total_data(2); ?> KK</h5>
                                            <p class="mb-0 text-muted">Terverifikasi</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-3" style="height:3px;">
                                    <div class="progress-bar  bg-warning" role="progressbar" style="width: 100%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                    <!--end col-->


                    <!--end col-->
                </div>
                <!--end row-->


            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                <h6>Data Korban Bencana (Rehabilitasi, Relokasi)</h6>
            </div>
            <div class="col-xl-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="col-3 align-self-center">
                                        <div class="round">
                                            <i class="mdi mdi-file-tree"></i>
                                        </div>
                                    </div>
                                    <div class="col-9 align-self-center text-right">
                                        <div class="m-l-10">
                                            <h5 class="mt-0"><?= total_rehabilitasi(0); ?></h5>
                                            <p class="mb-0 text-muted">Total Korban Bencana</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-3" style="height:3px;">
                                    <div class="progress-bar  bg-success" role="progressbar" style="width: 35%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="col-3 align-self-center">
                                        <div class="round">
                                            <i class="mdi mdi-database"></i>
                                        </div>
                                    </div>
                                    <div class="col-9 text-right align-self-center">
                                        <div class="m-l-10 ">
                                            <h5 class="mt-0"><?= total_rehabilitasi(0); ?></h5>
                                            <p class="mb-0 text-muted">Sedang Diusulkan</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-3" style="height:3px;">
                                    <div class="progress-bar bg-warning" role="progressbar" style="width: 48%;" aria-valuenow="48" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="search-type-arrow"></div>
                                <div class="d-flex flex-row">
                                    <div class="col-3 align-self-center">
                                        <div class="round ">
                                            <i class="mdi mdi-file"></i>
                                        </div>
                                    </div>
                                    <div class="col-9 align-self-center text-right">
                                        <div class="m-l-10 ">
                                            <h5 class="mt-0"><?= total_rehabilitasi(1); ?></h5>
                                            <p class="mb-0 text-muted">Total Diverifikasi</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-3" style="height:3px;">
                                    <div class="progress-bar bg-danger" role="progressbar" style="width: 61%;" aria-valuenow="61" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="search-type-arrow"></div>
                                <div class="d-flex flex-row">
                                    <div class="col-3 align-self-center">
                                        <div class="round ">
                                            <i class="mdi mdi-newspaper"></i>
                                        </div>
                                    </div>
                                    <div class="col-9 align-self-center text-right">
                                        <div class="m-l-10 ">
                                            <h5 class="mt-0"><?= total_rehabilitasi(2); ?></h5>
                                            <p class="mb-0 text-muted">Total Sudah Ditransfer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-3" style="height:3px;">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 61%;" aria-valuenow="61" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="search-type-arrow"></div>
                                <div class="d-flex flex-row">
                                    <div class="col-3 align-self-center">
                                        <div class="round ">
                                            <i class="mdi mdi-newspaper"></i>
                                        </div>
                                    </div>
                                    <div class="col-9 align-self-center text-right">
                                        <div class="m-l-10 ">
                                            <h5 class="mt-0"><?= total_rehabilitasi(2); ?></h5>
                                            <p class="mb-0 text-muted">Total Laporan Selesai</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-3" style="height:3px;">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 61%;" aria-valuenow="61" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Statistik Hunian Tetap</h4>
                        <p class="text-muted mb-4 font-14 d-inline-block text-truncate w-100">Data Statistik Berdasarkan jenis bencana alam.</p>

                        <div id="bar-chart" class="h-300"></div>

                    </div>
                </div>
            </div> <!-- end col -->

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Grafik Hunian Tetap</h4>
                        <p class="text-muted mb-4 font-14 d-inline-block text-truncate w-100">Berikut ini merupaka Grafik Data Hunian tetap.</p>

                        <div id="line-chart" class="h-300"></div>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
        <!-- <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Area Chart</h4>
                        <p class="text-muted mb-4 font-14 d-inline-block text-truncate w-100">Create an area chart using
                            Morris.Area(options). Area charts take all the same options as line
                            charts.</p>

                        <div id="revenue-chart" class="h-300"></div>

                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Donut Chart</h4>
                        <p class="text-muted mb-4 font-14 d-inline-block text-truncate w-100">This really couldn't be easier. Create
                            a Donut chart using Morris.Donut(options).</p>

                        <div id="sales-chart" class="h-300"></div>

                    </div>
                </div>
            </div>
        </div> -->



    </div><!-- container -->

</div> <!-- Page content Wrapper -->

</div> <!-- content -->
<script>
    $(function() {
        "use strict";

        // AREA CHART
        // var area = new Morris.Area({
        //     element: 'revenue-chart',
        //     resize: true,
        //     data: [{
        //             y: '2011 Q1',
        //             item1: 2666,
        //             item2: 2666
        //         },
        //         {
        //             y: '2011 Q2',
        //             item1: 2778,
        //             item2: 2294
        //         },
        //         {
        //             y: '2011 Q3',
        //             item1: 4912,
        //             item2: 1969
        //         },
        //         {
        //             y: '2011 Q4',
        //             item1: 3767,
        //             item2: 3597
        //         },
        //         {
        //             y: '2012 Q1',
        //             item1: 6810,
        //             item2: 1914
        //         },
        //         {
        //             y: '2012 Q2',
        //             item1: 5670,
        //             item2: 4293
        //         },
        //         {
        //             y: '2012 Q3',
        //             item1: 4820,
        //             item2: 3795
        //         },
        //         {
        //             y: '2012 Q4',
        //             item1: 15073,
        //             item2: 5967
        //         },
        //         {
        //             y: '2013 Q1',
        //             item1: 10687,
        //             item2: 4460
        //         },
        //         {
        //             y: '2013 Q2',
        //             item1: 8432,
        //             item2: 5713
        //         }
        //     ],
        //     xkey: 'y',
        //     ykeys: ['item1', 'item2'],
        //     labels: ['Item 1', 'Item 2'],
        //     lineColors: ['#a0d0e0', '#3c8dbc'],
        //     hideHover: 'auto'
        // });

        // LINE CHART
        var line = new Morris.Line({
            element: 'line-chart',
            resize: true,
            data: [{
                    y: '2011 Q1',
                    item1: 2666
                },
                {
                    y: '2011 Q2',
                    item1: 2778
                },
                {
                    y: '2011 Q3',
                    item1: 4912
                },
                {
                    y: '2011 Q4',
                    item1: 3767
                },
                {
                    y: '2012 Q1',
                    item1: 6810
                },
                {
                    y: '2012 Q2',
                    item1: 5670
                },
                {
                    y: '2012 Q3',
                    item1: 4820
                },
                {
                    y: '2012 Q4',
                    item1: 15073
                },
                {
                    y: '2013 Q1',
                    item1: 10687
                },
                {
                    y: '2013 Q2',
                    item1: 8432
                }
            ],
            xkey: 'y',
            ykeys: ['item1'],
            labels: ['Item 1'],
            lineColors: ['#3c8dbc'],
            hideHover: 'auto'
        });

        //DONUT CHART
        // var donut = new Morris.Donut({
        //     element: 'sales-chart',
        //     resize: true,
        //     colors: ["#3c8dbc", "#f56954", "#00a65a"],
        //     data: [{
        //             label: "Download Sales",
        //             value: 12
        //         },
        //         {
        //             label: "In-Store Sales",
        //             value: 30
        //         },
        //         {
        //             label: "Mail-Order Sales",
        //             value: 20
        //         }
        //     ],
        //     hideHover: 'auto'
        // });
        //BAR CHART
        var bar = new Morris.Bar({
            element: 'bar-chart',
            resize: true,
            data: [{
                    y: '2006',
                    a: 100,
                    b: 90
                },
                {
                    y: '2007',
                    a: 75,
                    b: 65
                },
                {
                    y: '2008',
                    a: 50,
                    b: 40
                },
                {
                    y: '2009',
                    a: 75,
                    b: 65
                },
                {
                    y: '2010',
                    a: 50,
                    b: 40
                },
                {
                    y: '2011',
                    a: 75,
                    b: 65
                },
                {
                    y: '2012',
                    a: 100,
                    b: 90
                }
            ],
            barColors: ['#00a65a', '#f56954'],
            xkey: 'y',
            ykeys: ['a', 'b'],
            labels: ['L', 'P', 'OO'],
            hideHover: 'auto'
        });
    });
</script>