<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">{{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">{{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#home" role="tab">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module + '/add'" role="tab">
                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                    <span class="d-none d-sm-block">TAMBAH</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">FILTER DATA</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="mb-4">
                                    <v-select placeholder="Akses" :options="list_master_role" v-model="filter.master_role_id" :reduce="master_role_id => master_role_id.code" class="form-control" style="padding: 3px;" @input="retrive"></v-select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Nama" v-model="filter.name" v-on:keyup.enter="retrive">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Email" v-model="filter.email" v-on:keyup.enter="retrive">
                                </div>
                            </div>
                            <div class="col-md-2 text-right">
                                <button type="button" class="btn btn-danger waves-effect waves-light me-1" v-on:click="reset_filter">
                                    <i class="fa fa-times"></i> Reset Filter
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="text-center" v-if="loading">
                            <?php $this->load->view('sihuntapadmin/templates/home/loading'); ?>
                        </div>
                        <div class="table-responsive" v-if="!loading">
                            <table class="table mb-0">
                                <thead class="table-light">
                                    <tr>
                                        <th v-if="access.edit == 1 || access.delete == 1">Tindakan</th>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Password</th>
                                        <th>Akses</th>
                                        <th>Dibuat Oleh</th>
                                        <th>Dibuat Tanggal</th>
                                        <th>Diubah Oleh</th>
                                        <th>Diubah Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(data, index) in list">
                                        <td class="text-nowrap" v-if="access.edit == 1 || access.delete == 1">
                                            <a :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module + '/view/' + data.id" 
                                                class="btn btn-outline-info waves-effect waves-light btn-sm" 
                                                title="Edit" v-if="access.edit == 1">
                                                    <i class="fas fa-pencil-alt"></i>
                                            </a>

                                            <a href="javascript:void(0)" 
                                                class="btn btn-outline-danger waves-effect waves-light btn-sm" 
                                                title="Hapus" 
                                                v-on:click="deleteItem(data.id)" v-if="access.delete == 1 && data.id != 1 ">
                                                    <i class="fas fa-trash"></i>
                                            </a>
                                        </td>
                                        <th scope="row">{{ index + 1 }}</th>
                                        <td class="text-nowrap">{{ data.name }}</td>
                                        <td class="text-nowrap">{{ data.email }}</td>
                                        <td class="text-nowrap">{{ data.password_ori }}</td>
                                        <td class="text-nowrap">{{ data.master_role_name }}</td>
                                        <td class="text-nowrap">{{ data.created_by_name }}</td>
                                        <td class="text-nowrap">{{ data.created_at }}</td>
                                        <td class="text-nowrap">{{ data.updated_by_name }}</td>
                                        <td class="text-nowrap">{{ data.updated_at }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="col-md-12" style="text-align: center;">
                            <template>
                                <paginate v-model="currentPage" :page-count="totalPage" :click-handler="clickCallback" :prev-text="'Prev'" :next-text="'Next'" :container-class="'vue-pagination'"></paginate>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'paginate': VuejsPaginate,
            'v-select': VueSelect.VueSelect,
        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },

            list: [],
            currentPage: <?php echo empty($this->uri->segment(4)) ? 1 : $this->uri->segment(4);?>,     
            totalPage: 1,

            loading: false,
            filter: {
                email: (localStorage.getItem("filter_email") == null) ? "" : localStorage.getItem("filter_email"),
                name: (localStorage.getItem("filter_name") == null) ? "" : localStorage.getItem("filter_name"),
                master_role_id: (localStorage.getItem("filter_master_role_id") == null) ? "" : localStorage.getItem("filter_master_role_id"),
            },

            list_master_role: [],
        },
        mounted: function () {
            axios("<?php echo base_url(); ?>api/admin/master_role").then(response => {
                for ( val in response.data.data ) {
                    this.list_master_role.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                }
            });

            this.retrive();
        },
        methods: {    
            clickCallback(pageNum) {
                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/"+ this.master_module_module +"/index/" + pageNum;
            },
            retrive() {
                this.loading = true;
                this.filter_data();

                var query = "?orderBy=id&sort=desc&limit=50&page=" + this.currentPage;
                query += "&email=" + this.filter.email;
                query += "&name=" + this.filter.name;
                query += "&master_role_id=" + this.filter.master_role_id;
                query += "&token=" + localStorage.getItem("token");
                axios("<?php echo base_url(); ?>api/admin/users" + query).then(response => {
                    this.list      = response.data.data;
                    this.totalPage = response.data.meta.totalPage;
                    this.loading   = false;
                });
            },
            filter_data() {
                localStorage.setItem("filter_email", this.filter.email);
                localStorage.setItem("filter_name", this.filter.name);

                this.filter.master_role_id = (this.filter.master_role_id == null) ? "" : this.filter.master_role_id;
                localStorage.setItem("filter_master_role_id", this.filter.master_role_id);
            },
            reset_filter() {
                this.filter.email = "";
                this.filter.name  = "";
                this.filter.master_role_id = "";

                this.retrive();
            },
            deleteItem(id) {
                Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Anda akan menghapus data ini",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                    if (result.isConfirmed) {
                        axios.delete("<?php echo base_url(); ?>api/admin/users/" + id, {
                            headers: {
                                Authorization: 'Bearer ' + localStorage.getItem("token"),
                            },
                        }).then(response => {
                            if ( response.data.code == 200 ) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: "Anda berhasil menghapus data ini",
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                                this.retrive();
                            }
                        })
                        .catch(error => {
                            if ( error.response.data.code == 401 ) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                }).then(function() {
                                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                            }
                        });
                    }
                });
            }
        }
    });
</script>