<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">EDIT {{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">Edit {{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.edit == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                    <span class="d-none d-sm-block">EDIT</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">FORM INPUT</h4>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Nama Pengguna</label>
                                    <input type="text" class="form-control" placeholder="Nama" v-model="model.name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Email</label>
                                    <input type="text" class="form-control" placeholder="Email" v-model="model.email" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Password</label>
                                    <input type="password" class="form-control" placeholder="Password" v-model="model.password">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Hak Akses</label>
                                    <v-select :options="listMasterRole" v-if="model.master_role_id != 1" v-model="model.master_role_id" :reduce="master_role_id => master_role_id.code" class="form-control" style="padding: 3px;"></v-select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Role Kecamatan</label>
                                    <v-select :options="list_district" v-model="model.master_district_id" :reduce="master_district_id => master_district_id.code" class="form-control" style="padding: 3px;" @input="get_village_by_district_id"></v-select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Role Desa</label>
                                    <v-select :options="list_village" v-model="model.master_village_id" :reduce="master_village_id => master_village_id.code" class="form-control" style="padding: 3px;"></v-select>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div style="text-align: right;">
                                <button type="button" class="btn btn-light waves-effect" v-if="loading">
                                    <i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i>
                                    Loading ...
                                </button>
                                <button type="button" class="btn btn-primary waves-effect waves-light me-1" v-on:click="save" v-else>
                                    Simpan Data
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,
        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },

            loading: false,
            id: '<?php echo $id; ?>',
            model: {
                name: "",
                email: "",
                password: "",
                master_role_id: "",
                master_district_id: "",
                master_village_id: ""

            },

            listMasterRole: [],
            list_district: [],
            list_village: []
        },
        mounted: function () {
            this.validate_access();
            var query = localStorage.getItem("token");
            axios("<?php echo base_url(); ?>api/admin/users/" + this.id +'/'+ query).then(response => {
                var data = response.data.data;

                this.model.name           = data.name;
                this.model.email          = data.email;
                this.model.master_role_id = data.master_role_id;
                this.model.master_district_id = data.master_district_id;
                this.model.master_village_id = data.master_village_id;
                this.get_village_by_district_id(data.master_district_id);
            });

            axios("<?php echo base_url(); ?>api/admin/master_role").then(response => {
                for ( val in response.data.data ) {
                    this.listMasterRole.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_district?master_city_id=3201").then(response => {
                for ( val in response.data.data ) {
                    this.list_district.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                }
            });
        },
        methods: {    
            validate_access() {
                if ( this.access.edit == 0 ) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            get_village_by_district_id(id){
                axios("<?php echo base_url(); ?>api/admin/master_village?master_district_id=" + id).then(response => {
                    for ( val in response.data.data ) {
                        this.list_village.push({ label: response.data.data[val].name, code: response.data.data[val].id });
                    }
                });
            },
            save() {
                this.loading = true;
                var formData = {
                    name: this.model.name,
                    email: this.model.email,
                    password: this.model.password,
                    master_district_id: this.model.master_district_id,
                    master_village_id: this.model.master_village_id,
                    master_role_id: this.model.master_role_id,
                    updated_by: localStorage.getItem("id"),
                }

                error = false;
                for ( data in formData ) {
                    if ( data != "password" && data != "master_district_id" && data != "master_village_id") {
                        if ( formData[data] == "" || formData[data] == undefined ) {
                            error = true
                        }
                    }
                }

                if ( this.password == "" ) {
                    formData = {
                        name: this.model.name,
                        master_role_id: this.model.master_role_id,
                        updated_by: localStorage.getItem("id"),
                    }
                }
                if(this.master_role_id == 2){
                    if(this.model.master_district_id == ''){
                        error = true;
                    }
                }
                if(this.master_role_id == 3){
                    if(this.model.master_district_id == '' || this.model.master_village_id == ''){
                        error = true;
                    }
                }

                if ( !error ) {
                    axios.put("<?php echo base_url(); ?>api/admin/users/" + this.id, formData, {
                        headers: {
                            'content-type': "application/json",
                            'Authorization': 'Bearer ' + localStorage.getItem("token"),
                        }
                    }).then(response => {
                        if ( response.data.code == 200 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 10000,
                            });
                            this.loading = false;
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                            this.loading = false;
                        }
                    })
                    .catch(error => {
                        if ( error.response.data.code == 401 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            }).then(function() {
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }
                        this.loading = false;
                    });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            }
        }
    });
</script>