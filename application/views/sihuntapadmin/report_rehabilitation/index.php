<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">{{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">{{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">

                <div class="card">
                    <div class="card-body">
                        <h6>CETAK DATA BERDASARKAN </h6>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="mb-3">
                                    <select v-model="filter.status" class="form-control" @change="retrive">
                                        <option selected value="">--Pilih Status--</option>
                                        <option :value="0"> Diusulkan</option>
                                        <option :value="1"> Terverifikasi</option>
                                        <option :value="2"> Ditranfer</option>
                                        <option :value="3"> Laporan Selesai</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Nama Penerima" v-model="filter.name" v-on:keyup.enter="retrive">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="NIK" v-model="filter.ktp_number" v-on:keyup.enter="retrive">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-4">
                                    <v-select placeholder="Kerusakan" :options="list_master_damage" v-model="filter.master_damage_id" :reduce="master_damage_id => master_damage_id.code" class="form-control" style="padding: 3px;" @input="get_village"></v-select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-4">
                                    <v-select placeholder="Jenis Bencana" :options="list_master_disaster" v-model="filter.master_disaster_id" :reduce="master_disaster_id => master_disaster_id.code" class="form-control" style="padding: 3px;" @input="get_village"></v-select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="mb-4">
                                    <v-select placeholder="Kecamatan" :options="list_master_district" v-model="filter.master_district_id" :reduce="master_district_id => master_district_id.code" class="form-control" style="padding: 3px;" @input="get_village"></v-select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-4">
                                    <v-select placeholder="Pilih Desa/Kelurahan" :options="list_master_village" v-model="filter.master_village_id" :reduce="master_village_id => master_village_id.code" class="form-control" style="padding: 3px;" @input="retrive"></v-select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-danger waves-effect waves-light me-1" v-on:click="reset_filter">
                                    <i class="fa fa-times"></i> Reset Filter
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 text-right ">
                                <a href="javascript:void(0)" class="btn btn-primary" v-on:click="export_excel">
                                    <i class="fa fa-file-export"></i> Export PDF
                                </a>
                                <a href="javascript:void(0)" class="btn btn-success" v-on:click="export_excel">
                                    <i class="fa fa-download"></i> Export Excel
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h6>Data Rekapitulasi</h6>
                        <div class="row">
                            <div class="table-responsive" v-if="!loading">
                                <table class="table mb-0 small table-responsive">
                                    <thead class="table-light">
                                        <tr>
                                            <th>#</th>
                                            <th class="text-nowrap">Nama</th>
                                            <th class="text-nowrap">NIK</th>
                                            <th class="text-nowrap">No KK</th>
                                            <th>Alamat</th>
                                            <th>Bencana</th>
                                            <th>Kondisi</th>
                                            <th>Tgl Kejadian</th>
                                            <th>Tgl Survey</th>
                                            <th>Survey</th>
                                            <th>Klasifikasi Bantuan</th>
                                            <th>SIPD</th>
                                            <th>Verifikasi</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data, index) in list_recipient">
                                            <th scope="row">{{ index + 1 }}</th>
                                            <td class="text-nowrap">{{ data.name }}</td>
                                            <td class="text-nowrap">{{ data.ktp_number }} </td>
                                            <td class="text-nowrap">{{ data.kk_number }} </td>

                                            <td class="text-nowrap">{{ data.address }}, Ds.{{ data.master_village_name}}, Kec.{{data.master_district_name}} </td>

                                            <td class="text-nowrap">{{ data.master_disaster_name }} </td>
                                            <td class="text-nowrap">{{ data.master_damage_name }} </td>
                                            <td class="text-nowrap">{{ data.incident_date }} </td>
                                            <td class="text-nowrap">{{ data.survey_date }} </td>
                                            <td class="text-nowrap">{{ data.surveyed }} </td>
                                            <td class="text-nowrap">{{ data.master_help_name }} </td>
                                            <td>{{ data.entered }} </td>
                                            <td>{{ data.submission }} </td>
                                            <td>
                                                <span v-if="data.status == 0" class="badge badge-warning">Diusulkan</span>
                                                <span v-if="data.status == 1" class="badge badge-danger">Ditolak</span>
                                                <span v-if="data.status == 2" class="badge badge-info">Sudah Diverifikasi</span>
                                                <span v-if="data.status == 3" class="badge badge-success">Direlokasi Huntara</span>
                                                <span v-if="data.status == 4" class="badge badge-primary">Sudah di Relokasi Huntap</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-12" style="text-align: center;">
                            <template>
                                <paginate v-model="currentPage" :page-count="totalPage" :click-handler="clickCallback" :prev-text="'Prev'" :next-text="'Next'" :container-class="'vue-pagination'"></paginate>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'paginate': VuejsPaginate,
            'v-select': VueSelect.VueSelect,
        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },
            loading: false,
            model: {
                name: "",
                description: "",
                master_disaster_id: "",
                master_damage_id: "",
                status: 1,
                status_description: "",
                master_district_id: "",
                master_village_id: "",
                address: ""
            },
            filter: {
                id: (localStorage.getItem("filter_id") == null) ? "" : localStorage.getItem("filter_id"),
                name: (localStorage.getItem("filter_name") == null) ? "" : localStorage.getItem("filter_name"),
                status: (localStorage.getItem("filter_status") == null) ? "" : localStorage.getItem("filter_status"),
                ktp_number: (localStorage.getItem("filter_ktp_number") == null) ? "" : localStorage.getItem("filter_ktp_number"),
                master_district_id: (localStorage.getItem("filter_master_district_id") == null) ? "" : localStorage.getItem("filter_master_district_id"),
                master_village_id: (localStorage.getItem("filter_master_village_id") == null) ? "" : localStorage.getItem("filter_master_village_id"),
                master_damage_id: (localStorage.getItem("filter_master_damage_id") == null) ? "" : localStorage.getItem("filter_master_damage_id"),
                master_disaster_id: (localStorage.getItem("filter_master_disaster_id") == null) ? "" : localStorage.getItem("filter_master_disaster_id")
            },
            list_recipient: [],
            list_master_district: [],
            list_master_village: [],
            list_master_damage: [],
            list_master_disaster: [],
            currentPage: <?php echo empty($this->uri->segment(4)) ? 1 : $this->uri->segment(4); ?>,
            totalPage: 1,
        },
        mounted: function() {
            this.validate_access();
            axios("<?php echo base_url(); ?>api/admin/master_district?master_city_id=3201").then(response => {
                for (val in response.data.data) {
                    this.list_master_district.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_damage").then(response => {
                for (val in response.data.data) {
                    this.list_master_damage.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_disaster").then(response => {
                for (val in response.data.data) {
                    this.list_master_disaster.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            this.retrive();
        },
        methods: {
            clickCallback(pageNum) {
                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/" + this.master_module_module + "/index/" + pageNum;
            },
            validate_access() {
                if (this.access.add == 0) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            get_village(id) {
                this.retrive();
                this.model.master_village_id = "";
                this.list_master_village = [];
                axios("<?php echo base_url(); ?>api/admin/master_village?master_district_id=" + id).then(response => {
                    for (val in response.data.data) {
                        this.list_master_village.push({
                            label: response.data.data[val].name,
                            code: response.data.data[val].id
                        });
                    }
                });
            },
            retrive() {
                this.filter_data();
                var query = "?orderBy=id&sort=asc&limit=100&page=" + this.currentPage;
                query += "&id=" + this.filter.id;
                query += "&name=" + this.filter.name;
                query += "&status=" + this.filter.status;
                query += "&ktp_number=" + this.filter.ktp_number;
                query += "&master_district_id=" + this.filter.master_district_id;
                query += "&master_village_id=" + this.filter.master_village_id;
                query += "&master_damage_id=" + this.filter.master_damage_id;
                query += "&master_disaster_id=" + this.filter.master_disaster_id;
                axios("<?php echo base_url(); ?>api/admin/report_rehabilitation" + query).then(response => {
                    this.list_recipient = response.data.data;
                    this.totalPage = response.data.meta.totalPage;
                    this.loading = false;
                });

            },
            filter_data() {
                localStorage.setItem("filter_id", this.filter.id);
                localStorage.setItem("filter_name", this.filter.name);
                localStorage.setItem("filter_status", this.filter.status);
                localStorage.setItem("filter_ktp_number", this.filter.ktp_number);
                localStorage.setItem("filter_master_district_id", this.filter.master_district_id);
                localStorage.setItem("filter_master_village_id", this.filter.master_village_id);
                localStorage.setItem("filter_master_damage_id", this.filter.master_damage_id);
                localStorage.setItem("filter_master_disaster_id", this.filter.master_disaster_id);

                // this.filter.master_company_size_id = (this.filter.master_company_size_id == null) ? "" : this.filter.master_company_size_id;
                // localStorage.setItem("filter_master_company_size_id", this.filter.master_company_size_id);


            },
            reset_filter() {
                this.filter.id = "";
                this.filter.name = "";
                this.filter.status = "";
                this.filter.ktp_number = "";
                this.filter.master_district_id = "";
                this.filter.master_village_id = "";
                this.filter.master_damage_id = "";
                this.filter.master_disaster_id = "";

                this.retrive();
            },
            export_excel() {

                var query = "?orderBy=id&sort=desc&limit=30&page=" + this.currentPage;
                query += "&id=" + this.filter.id;
                query += "&name=" + this.filter.name;
                query += "&status=" + this.filter.status;
                query += "&ktp_number=" + this.filter.ktp_number;
                query += "&master_district_id=" + this.filter.master_district_id;
                query += "&master_village_id=" + this.filter.master_village_id;
                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/" + this.master_module_module + "/export?" + query;
            }

        }
    });
</script>