<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Buat {{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">Buat {{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-list"></i></span>
                                    <span class="d-none d-sm-block"><i class="fas fa-list"></i> DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="fa fa-plus"></i></span>
                                    <span class="d-none d-sm-block"><i class="fa fa-plus"></i> BUAT PERMOHONAN</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">FORM INPUT</h6>


                        <div class="row" style="border-bottom:1px solid #a0a0a0;margin-bottom:15px;padding-bottom:5px;">

                            <div class="mb-3 col-lg-12">
                                <label>Pilih Proposal (*)</label>
                                <v-select placeholder="--Pilih Proposal--" :options="list_disaster_proposal" v-model="model.disaster_proposal_id" :reduce="disaster_proposal_id => disaster_proposal_id.code" @input="get_proposal" class="form-control" style="padding: 3px;"></v-select>
                            </div>
                            <div class="mb-3 col-lg-8">
                                <label>Sumber Dana (*)</label>
                                <v-select placeholder="--Pilih Sumber--" :options="list_master_disaster_funds" v-model="model.master_disaster_funds_id" :reduce="master_disaster_funds_id => master_disaster_funds_id.code" class="form-control" style="padding: 3px;"></v-select>
                            </div>
                            <div class="mb-3 col-lg-4">
                                <label>Jenis Kegiatan :</label>
                                <h6>{{model.type}}</h6>
                            </div>
                            <h6 class="col-lg-12">Lampiran Permohonan Pencairan</h6>

                            <div class="mb-3 col-lg-6" v-if="type_pokmas || type_individu">
                                <label>Berita Acara Verifikasi dan Validasi Korban *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="ba_verif_file" required>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="type_pokmas || type_individu">
                                <label>Pakta Integritas *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="integrity_file" required>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="type_pokmas">
                                <label>Kontrak Swakelola *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="contract_file" required>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="type_pokmas">
                                <label>Surat Perjanjian Kerjasaman DPKPP & POKMAS (SPK) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="spk_file" required>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="type_pokmas">
                                <label>Surat Perintah Mulai Kerja (SPMK) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="spmk_file" required>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="type_pokmas || type_individu">
                                <label>Surat Kesanggupan Melaksanakan Pekerjaan (SKMP) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="skmp_file" required>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="type_pokmas || type_individu">
                                <label>Surat Pertanggungjawaban (SPj) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="spj_file" required>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="type_pokmas">
                                <label>Surat Permohonan Pencairan BTT (SPP-BTT) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="sppbtt_file" required>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="type_individu">
                                <label>Surat Keabsahaan Dokumen (SKD) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="document_validity_file" required>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="type_individu">
                                <label>Surat Keterangan Kepemilikan Tanah (SKKT) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="ownership_land_file" required>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="type_individu">
                                <label>SK Tidak Sedang Diusulkan RUTILAHU *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="not_rutilahu_file" required>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="type_pokmas || type_individu">
                                <label>Buku Rekening *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="account_book_file" required>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="type_pokmas || type_individu">
                                <label>Kwitansi Penerimaan *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="acceptance_receipt_file" required>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="type_pokmas || type_individu">
                                <label>BA Belanja Tak Terduga *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="ba_unexpected_file" required>
                            </div>
                        </div>

                        <div class="form-group row" style="text-align:right">
                            <div class="col-sm-12">
                                <br />
                                <button type="submit" class="btn btn-primary" v-on:click="save">SIMPAN DATA</button>
                            </div>
                        </div>
                        <div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,

        },
        data: {
            inputs: [{
                type: 'my-input'
            }],
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },

            loading: false,
            type_pokmas: false,
            type_individu: false,
            model: {
                disaster_proposal_id: "",
                master_disaster_funds_id: "",
                type: "",
                ba_verif_file: "",
                integrity_file: "",
                contract_file: "",
                spk_file: "",
                spmk_file: "",
                skmp_file: "",
                spj_file: "",
                sppbtt_file: "",
                document_validity_file: "",
                ownership_land_file: "",
                not_rutilahu_file: "",
                account_book_file: "",
                acceptance_receipt_file: "",
                ba_unexpected_file: "",
                status: "UNVERIFIED",
                created_by: localStorage.getItem("id"),
                created_dt: "<?= date('Y-m-d H:i:s'); ?>"
            },
            list_disaster_proposal: [],
            list_master_disaster_funds: []
        },
        mounted: function() {
            this.validate_access();
            axios("<?php echo base_url(); ?>api/admin/disaster_proposal").then(response => {
                for (val in response.data.data) {
                    this.list_disaster_proposal.push({
                        label: response.data.data[val].name + " [" + response.data.data[val].master_village_name + " - " + response.data.data[val].master_district_name + "]",
                        code: response.data.data[val].id
                    });
                }
            });

            axios("<?php echo base_url(); ?>api/admin/master_disaster_funds").then(response => {
                for (val in response.data.data) {
                    this.list_master_disaster_funds.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });

        },
        methods: {
            validate_access() {
                if (this.access.add == 0) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            get_proposal(id) {
                axios("<?php echo base_url(); ?>api/admin/disaster_proposal/" + id).then(response => {
                    var data = response.data.data;
                    if (data.type == "POKMAS") {
                        this.type_pokmas = true;
                        this.type_individu = false;
                    }
                    if (data.type == "INDIVIDU") {

                        this.type_pokmas = false;
                        this.type_individu = true;
                    }
                    this.model.type = data.type;
                });
            },
            save() {
                this.loading = true;
                error = false;
                var formData = new FormData();
                formData.append('disaster_proposal_id', this.model.disaster_proposal_id);
                formData.append('master_disaster_funds_id', this.model.master_disaster_funds_id);
                formData.append('type', this.model.type);
                formData.append('ba_verif_file', this.$refs.ba_verif_file.files[0]);
                formData.append('integrity_file', this.$refs.integrity_file.files[0]);
                formData.append('contract_file', this.$refs.contract_file.files[0]);
                formData.append('spk_file', this.$refs.spk_file.files[0]);
                formData.append('spmk_file', this.$refs.spmk_file.files[0]);
                formData.append('skmp_file', this.$refs.skmp_file.files[0]);
                formData.append('spj_file', this.$refs.spj_file.files[0]);
                formData.append('sppbtt_file', this.$refs.sppbtt_file.files[0]);
                formData.append('document_validity_file', this.$refs.document_validity_file.files[0]);
                formData.append('ownership_land_file', this.$refs.ownership_land_file.files[0]);
                formData.append('not_rutilahu_file', this.$refs.not_rutilahu_file.files[0]);
                formData.append('account_book_file', this.$refs.account_book_file.files[0]);
                formData.append('acceptance_receipt_file', this.$refs.acceptance_receipt_file.files[0]);
                formData.append('ba_unexpected_file', this.$refs.ba_unexpected_file.files[0]);
                formData.append('status', this.model.status);
                formData.append('created_by', this.model.created_by);
                formData.append('created_dt', this.model.created_dt);
                if (this.model.disaster_proposal_id == "" || this.model.master_disaster_funds_id == "") {
                    error = true;
                }

                if (!error) {
                    axios.post("<?php echo base_url(); ?>api/admin/disaster_disbursement_request", formData, {
                            headers: {
                                'content-type': "multipart/form-data",
                                'Authorization': 'Bearer ' + localStorage.getItem("token"),
                            }
                        }).then(response => {
                            if (response.data.code == 200) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 10000,
                                });
                                this.loading = false;
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/disaster_disbursement_request";
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                                this.loading = false;
                            }
                        })
                        .catch(error => {
                            if (error.response.data.code == 401) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                }).then(function() {
                                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                            }
                            this.loading = false;
                        });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            }
        }
    });
</script>