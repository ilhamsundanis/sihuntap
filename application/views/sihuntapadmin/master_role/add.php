<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">TAMBAH {{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">Tambah {{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                    <span class="d-none d-sm-block">TAMBAH</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">FORM INPUT</h4>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">Nama</label>
                                    <input type="text" class="form-control" placeholder="Nama Akses" v-model="model.name">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <h4 class="card-title">PILIH HAK AKSES</h4>

                <div class="card" v-for="data in list_group_module">
                    <h5 class="card-header mt-0">- # - {{ data.name }}</h5>
                    <div class="card-body">
                        <table class="table table-sm">
                            <thead class="table-light">
                                <tr>
                                    <th>#</th>
                                    <th style="width: 50%;">Nama Module</th>
                                    <th class="text-center">Data</th>
                                    <th class="text-center">Tambah</th>
                                    <th class="text-center">Edit</th>
                                    <th class="text-center">Hapus</th>
                                    <th class="text-center">Impor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(subdata, index) in data.modules">
                                    <td>{{ index + 1 }}</td>
                                    <td>{{ subdata.name }}</td>
                                    <td class="text-center">
                                        <input type="hidden" v-model="subdata.id">
                                        <input v-model="subdata.access_read" type="checkbox">
                                    </td>
                                    <td class="text-center">
                                        <input v-model="subdata.access_add" type="checkbox">
                                    </td>
                                    <td class="text-center">
                                        <input v-model="subdata.access_edit" type="checkbox">
                                    </td>
                                    <td class="text-center">
                                        <input v-model="subdata.access_delete" type="checkbox">
                                    </td>
                                    <td class="text-center">
                                        <input v-model="subdata.access_import" type="checkbox">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div>
                            <div style="text-align: right;">
                                <button type="button" class="btn btn-light waves-effect" v-if="loading">
                                    <i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i>
                                    Loading ...
                                </button>
                                <button type="button" class="btn btn-primary waves-effect waves-light me-1" v-on:click="save" v-else>
                                    Simpan Data
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var appContent = new Vue({
        el: '#appContent',
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },

            loading: false,
            list_group_module: [],
            model: {
                name: ""
            },
            modules: [],
        },
        mounted: function () {
            this.validate_access();

            axios("<?php echo base_url(); ?>api/admin/master_role/group_module").then(response => {
                this.list_group_module = response.data.data;
            });
        },
        methods: {    
            validate_access() {
                if ( this.access.add == 0 ) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            save() {
                this.modules = [];
                for ( data in this.list_group_module ) {
                    var modules = this.list_group_module[data].modules;
                    for (subdata in modules) {
                        var access_read = (modules[subdata].access_read == undefined) ? false : true;
                        var access_add = (modules[subdata].access_add == undefined) ? false : true;
                        var access_edit = (modules[subdata].access_edit == undefined) ? false : true;
                        var access_delete = (modules[subdata].access_delete == undefined) ? false : true;
                        var access_import = (modules[subdata].access_import == undefined) ? false : true;

                        this.modules.push({
                            master_module_id: modules[subdata].id,
                            access_read: access_read,
                            access_add: access_add,
                            access_edit: access_edit,
                            access_delete: access_delete,
                            access_import: access_import,
                        });
                    }
                }

                this.loading = true;
                var formData = {
                    name: this.model.name,
                }

                error = false;
                for ( data in formData ) {
                    if ( formData[data] == "" || formData[data] == undefined ) {
                        error = true
                    }
                }

                if ( !error ) {
                    var params = {
                        name: this.model.name,
                        modules: this.modules
                    };

                    axios.post("<?php echo base_url(); ?>api/admin/master_role", params, {
                        headers: {
                            'content-type': "application/json",
                            'Authorization': 'Bearer ' + localStorage.getItem("token"),
                        }
                    }).then(response => {
                        if ( response.data.code == 200 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 10000,
                            });

                            this.model.name = "";
                            this.modules    = [];
                            this.loading    = false;
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                            this.loading = false;
                        }
                    })
                    .catch(error => {
                        if ( error.response.data.code == 401 ) {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            }).then(function() {
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }
                        this.loading    = false;
                    });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            }
        }
    });
</script>