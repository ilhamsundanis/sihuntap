<div class="main-content">
    <div class="page-content">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">-</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item">
                                <a href="javascript: void(0);">-</a>
                            </li>
                            <li class="breadcrumb-item active">-</li>
                        </ol>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card overflow-hidden">
                            <div class="card-body">
                                <div class="text-center p-3">
                                    <div class="img">
                                        <img src="<?php echo base_url(); ?>/assets/images/error-img.png" class="img-fluid" alt="">
                                    </div>
                                    <h1 class="error-page mt-5"><span>403!</span></h1>
                                    <h4 class="mb-4 mt-5">Pembatan Akses!</h4>
                                    <p class="mb-4 w-75 mx-auto">
                                        Anda tidak memiliki akses untuk mengelola halaman ini.
                                    </p>
                                    <a class="btn btn-primary mb-4 waves-effect waves-light" href="<?php echo base_url(); ?>sihuntapadmin/home">
                                        <i class="mdi mdi-home"></i> Kembali
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>