<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">{{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">{{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#home" role="tab">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module + '/add'" role="tab">
                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                    <span class="d-none d-sm-block">TAMBAH</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead class="table-light">
                                    <tr>
                                        <th>#</th>
                                        <th>Judul Konten</th>
                                        <th>Slug</th>
                                        <th v-if="access.edit == 1 || access.delete == 1">Tindakan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(data, index) in list">
                                        <th scope="row">{{ index + 1 }}</th>
                                        <td>{{ data.title }}</td>
                                        <td>{{ data.slug }}</td>
                                        <td v-if="access.edit == 1 || access.delete == 1">
                                            <a :href="'<?php echo base_url(); ?>page/index/' + data.slug" target="_blank" class="btn btn-outline-info waves-effect waves-light btn-sm">
                                                <i class="fas fa-eye"></i> Tinjau Langsung
                                            </a>

                                            <a :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module + '/view/' + data.id" 
                                                class="btn btn-outline-info waves-effect waves-light btn-sm" 
                                                title="Edit" v-if="access.edit == 1">
                                                    <i class="fas fa-pencil-alt"></i>
                                            </a>

                                            <a href="javascript:void(0)" 
                                                class="btn btn-outline-danger waves-effect waves-light btn-sm" 
                                                title="Hapus" 
                                                v-on:click="deleteItem(data.id)" v-if="access.delete == 1">
                                                    <i class="fas fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="col-md-12" style="text-align: center;">
                            <template>
                                <paginate v-model="currentPage" :page-count="totalPage" :click-handler="clickCallback" :prev-text="'Prev'" :next-text="'Next'" :container-class="'vue-pagination'"></paginate>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'paginate': VuejsPaginate,
        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },

            list: [],
            currentPage: <?php echo empty($this->uri->segment(4)) ? 1 : $this->uri->segment(4);?>,     
            totalPage: 1,
        },
        mounted: function () {
            this.retrive();
        },
        methods: {    
            clickCallback(pageNum) {
                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/"+ this.master_module_module +"/index/" + pageNum;
            },
            retrive() {
                var query = "?orderBy=id&sort=desc&limit=50&page=" + this.currentPage;
                axios("<?php echo base_url(); ?>api/admin/page_content" + query).then(response => {
                    this.list      = response.data.data;
                    this.totalPage = response.data.meta.totalPage;
                });
            },
            deleteItem(id) {
                Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Anda akan menghapus data ini",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                    if (result.isConfirmed) {
                        axios.delete("<?php echo base_url(); ?>api/admin/page_content/" + id, {
                            headers: {
                                Authorization: 'Bearer ' + localStorage.getItem("token"),
                            },
                        }).then(response => {
                            if ( response.data.code == 200 ) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: "Anda berhasil menghapus data ini",
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                                this.retrive();
                            }
                        })
                        .catch(error => {
                            if ( error.response.data.code == 401 ) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                }).then(function() {
                                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                            }
                        });
                    }
                });
            }
        }
    });
</script>