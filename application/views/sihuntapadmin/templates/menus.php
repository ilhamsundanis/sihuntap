<body class="fixed-left">
    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>
    <!-- Begin page -->
    <div id="wrapper">

        <!-- ========== Left Sidebar Start ========== -->
        <div class="left side-menu">
            <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                <i class="ion-close"></i>
            </button>

            <!-- LOGO -->
            <div class="topbar-left">
                <div class="text-center">
                    <!-- <a href="index.html" class="logo" style="font-size: 30px;color: #fff;">SIHUNTAP</a> -->
                    <a href="<?php echo base_url("sihuntapadmin/home"); ?>" class="logo">
                        <img src="<?php echo base_url(); ?>template/assets/images/logo.png" alt="" class="logo-large">
                    </a>
                </div>
            </div>

            <div class="sidebar-inner niceScrollleft" id="appMenus">

                <div id="sidebar-menu">
                    <ul>
                        <li class="menu-title">{{ name }}</li>

                        <li>
                            <a href="<?php echo base_url('sihuntapadmin/home'); ?>" class="waves-effect">
                                <i class="mdi mdi-home"></i><span>Beranda</span>
                            </a>
                        </li>
                        <li class="has_sub" v-for="data in menus" v-if="data.submenus.length != 0">
                            <a href="javascript:void(0);" class="waves-effect"><i :class="data.icon"></i><span>{{ data.name }}</span> <span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="list-unstyled">
                                <li v-for="subdata in data.submenus"><a :href="'<?php echo base_url(); ?>sihuntapadmin/' + subdata.module">{{ subdata.name }}</a></li>
                            </ul>
                        </li>
                        <li v-if="id == 1">
                            <a href="<?php echo base_url('sihuntapadmin/setting'); ?>" class="waves-effect">
                                <i class="mdi mdi-settings"></i><span>Pengaturan</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('sihuntapadmin/auth/logout'); ?>" class="waves-effect">
                                <i class="mdi mdi-logout-variant"></i><span>Log Out</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div> <!-- end sidebarinner -->
        </div>
        <!-- Left Sidebar End -->

        <!-- Start right Content here -->

        <div class="content-page">
            <!-- Start content -->
            <div class="content">

                <!-- Top Bar Start -->
                <div class="topbar" id="appMenus">

                    <nav class="navbar-custom"  >

                        <ul class="list-inline float-right mb-0">

                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <img src="<?php echo base_url(); ?>template/assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5>Navigasi</h5>
                                    </div>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5 text-muted"></i> Profil</a>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-settings m-r-5 text-muted"></i> Pengaturan</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="<?= base_url('auth/logout') ?>"><i class="mdi mdi-logout m-r-5 text-muted"></i> Log out</a>
                                </div>
                            </li>

                        </ul>

                        <ul class="list-inline menu-left mb-0">
                            <li class="float-left">
                                <button class="button-menu-mobile open-left waves-light waves-effect">
                                    <i class="mdi mdi-menu"></i>
                                </button>
                            </li>
                        </ul>

                        <div class="clearfix"></div>

                    </nav>

                </div>
                <!-- Top Bar End -->
                <script>
                    var appMenus = new Vue({
                        el: '#appMenus',
                        data: {
                            id: localStorage.getItem("id"),
                            email: localStorage.getItem("email"),
                            name: localStorage.getItem("name"),
                            master_role_name: localStorage.getItem("master_role_name"),
                            menus: JSON.parse(localStorage.getItem("menus")),
                            modules: JSON.parse(localStorage.getItem("modules")),
                            currentModule: '<?php echo $this->uri->segment(2); ?>',
                            list_message: [],
                            count_contact: ""
                        },
                        mounted: function() {
                            if (this.id == null) {
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth";
                            }

                            if (this.currentModule != "access" && this.currentModule != "home" && this.currentModule != "setting" && this.currentModule != "contact") {
                                var access = 0;
                                for (data in this.modules) {
                                    if (this.currentModule == this.modules[data].master_module_module) {
                                        access = 1;
                                        localStorage.read = this.modules[data].access_read;
                                        localStorage.add = this.modules[data].access_add;
                                        localStorage.edit = this.modules[data].access_edit;
                                        localStorage.delete = this.modules[data].access_delete;
                                        localStorage.import = this.modules[data].access_import;
                                        localStorage.master_module_name = this.modules[data].master_module_name;
                                        localStorage.master_module_module = this.modules[data].master_module_module;
                                    }
                                }

                                if (access == 0) {
                                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                                }
                            }
                        },
                        methods: {}
                    });
                </script>