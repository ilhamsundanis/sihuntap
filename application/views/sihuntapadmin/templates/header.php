<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>DASHBOARD - <?=load_apps()["name_app"];?></title>
    <meta content="<?php echo load_apps()["description"];?>" name="description" />
    <meta content="@ilhamsundanis" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="<?php echo base_url(); ?>template/assets/images/favicon.png">

    <?php
    if (!empty($css)) {
        foreach ($css as $style) echo '<link rel="stylesheet" type="text/css" href="' . base_url() . $style . '.css" />', "\n";
    }
    ?>
    <script src="<?php echo base_url(); ?>template/assets/js/jquery.min.js"></script>

    <script src="<?php echo base_url(); ?>js/vue@2.6.14.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="<?php echo base_url(); ?>js/sweetalert2@11.js"></script>
    <script src="<?php echo base_url(); ?>js/vuejs-paginate.js"></script>

    <script src="<?php echo base_url(); ?>js/vue-select.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>js/vue-select.css">

    <!-- summernote -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/summernote@0.8.10/dist/summernote-bs4.css">
    <script src="<?php echo base_url(); ?>js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <script src='<?php echo base_url(); ?>js/summernote-bs4.min.js'></script>
    <script src="<?php echo base_url(); ?>js/custome/vue-summernote.es5.js"></script>

    <style>
        .vue-pagination {
            display: inline-block;
            padding: 0;
            margin: 0;
        }

        .vue-pagination li {
            display: inline;
        }

        .vue-pagination li a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #ddd;
            margin: 0 4px;
        }

        .vue-pagination li.active a {
            background-color: #263c92;
            color: white;
            border: 1px solid #263c92;
        }

        .vue-pagination li:hover:not(.active) a {
            background-color: #ddd;
        }

        .block-loader {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            background: rgba(158, 130, 77, 0.5);
            width: 100%;
            height: 100%;
            z-index: 1;
            text-align: center;
            line-height: 700px;
        }

        .block-loader p {
            line-height: 1.5;
            display: inline-block;
            vertical-align: middle;
            font-weight: bold;
            font-size: 20px;
            color: white;
        }

        .text-right {
            text-align: right;
        }
    </style>

</head>