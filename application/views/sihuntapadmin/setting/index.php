<div class="main-content" id="app">
    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Pengaturan Dasar Aplikasi</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Beranda</a></li>
                            <li class="breadcrumb-item active">Pengaturan Aplikasi</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <h4 class="card-title">Form Pengaturan Aplikasi</h4>
                        <p class="card-title-desc">Mohon untuk tidak merubah rubah data dengan sengaja. <code>Karena akan mempengaruhi</code> pada sistem  yang sedang berjalan.

                        <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Nama</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" v-model="model.name">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-search-input" class="col-md-2 col-form-label">Nama Aplikasi</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" placeholder="Nama Aplikasi" v-model="model.name_app">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-email-input" class="col-md-2 col-form-label">Deskripsi</label>
                            <div class="col-md-10">
                                <textarea rows="10" v-model="model.description" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-url-input" class="col-md-2 col-form-label">Keyword SEO</label>
                            <div class="col-md-10">
                            <textarea rows="5" v-model="model.keyword" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-tel-input" class="col-md-2 col-form-label">Deskripsi Profil</label>
                            <div class="col-md-10">
                            <textarea rows="20" v-model="model.description_profile" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-password-input" class="col-md-2 col-form-label">Alamat</label>
                            <div class="col-md-10">
                                <textarea v-model="model.address" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-number-input" class="col-md-2 col-form-label">Email</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" v-model="model.email">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-datetime-local-input" class="col-md-2 col-form-label">No. Telp</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" v-model="model.phone_number">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-date-input" class="col-md-2 col-form-label">Call Center</label>
                            <div class="col-md-10">
                                <input class="form-control" type="number" v-model="model.call_center">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-month-input" class="col-md-2 col-form-label">URL Video Profil</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" v-model="model.video_profile">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-week-input" class="col-md-2 col-form-label">Logo</label>
                            <div class="col-md-10">
                                <input class="form-control" type="file" ref="logo">
                                <div style="background:#b0b0b0;padding:20px;">
                                    <img width="500px;" :src="model.logo"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-time-input" class="col-md-2 col-form-label">Favicon</label>
                            <div class="col-md-10">
                                <input class="form-control" type="file" ref="logo_fav">
                                <div style="background:#b0b0b0;padding:20px;">
                                    <img width="100px;" :src="model.logo_fav"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-color-input" class="col-md-2 col-form-label">Peta Google</label>
                            <div class="col-md-10">
                                <textarea rows="6" v-model="model.maps" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="mb-3 row">
                            <label for="example-month-input" class="col-md-2 col-form-label">Whatsapp</label>
                            <div class="col-md-10">
                                <input class="form-control" type="number" v-model="model.whatsapp">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-month-input" class="col-md-2 col-form-label">URL Facebook</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" v-model="model.facebook">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-month-input" class="col-md-2 col-form-label">URL Twitter</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" v-model="model.twitter">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-month-input" class="col-md-2 col-form-label">URL Linkedin</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" v-model="model.linkedin">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-month-input" class="col-md-2 col-form-label">URL Youtube</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" v-model="model.youtube">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-month-input" class="col-md-2 col-form-label">URL Instagram</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" v-model="model.instagram">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <button type="button" v-on:click="save" class="btn btn-outline-primary">PERBAHARUI PENGATURAN</button>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->


    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> © Bogor Career Center.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
                        Design & Develop by Indo Akses Media
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<script>
    var app = new Vue({
        el: '#app',
        components: {
            'v-select': VueSelect.VueSelect,
        },
        data: {
            loading: false,
            id: localStorage.getItem("id"),
            setting_id: "",
            model: { 
				name: "",
                name_app: "",
                description: "",
                keyword: "",
				description_profile: "",
                address:"",
                email:"",
                phone_number: "",
                call_center: "",
                video_profile: "",
                logo: "",
                logo_fav: "",
                maps: "",
                whatsapp: "",
                facebook: "",
                twitter: "",
                linkedin: "",
                youtube: "",
                instagram: ""

			}
        },
        mounted: function () {
            if(this.id != 1){
                window.location.href = "<?php echo base_url(); ?>bccadmin/access";
            }

			axios("<?php echo base_url(); ?>api/admin/setting/1/" + localStorage.getItem("token")).then(response => {
                var data = response.data.data;
                this.setting_id = data.id;
                this.model.name = data.name;
                this.model.name_app = data.name_app;
                this.model.description = data.description;
                this.model.keyword = data.keyword;
                this.model.description_profile = data.description_profile;
                this.model.address = data.address;
                this.model.email = data.email;
                this.model.phone_number = data.phone_number;
                this.model.call_center = data.call_center;
                this.model.video_profile = data.video_profile;
                this.model.logo = data.logo;
                this.model.logo_fav = data.logo_fav;
                this.model.maps = data.maps;
                this.model.whatsapp = data.whatsapp;
                this.model.facebook = data.facebook;
                this.model.twitter = data.twitter;
                this.model.youtube = data.youtube;
                this.model.linkedin = data.linkedin;
                this.model.instagram = data.instagram;
            });

            // this.retrive();
        },
        methods: {    
            
            save() {
                this.loading = true;
                if (this.model.name.length < 5 || this.model.name_app.length < 6 ){
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Nama Harus Benar",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                
                }else{
                    var formData = new FormData();
                    
                    formData.append('name', this.model.name);
                    formData.append('name_app', this.model.name_app);
                    formData.append('description', this.model.description);
                    formData.append('phone_number', this.model.phone_number);
                    formData.append('keyword', this.model.keyword);
                    formData.append('description_profile', this.model.description_profile);
                    formData.append('address', this.model.address);
                    formData.append('email', this.model.email);
                    formData.append('call_center', this.model.call_center);
                    formData.append('video_profile', this.model.video_profile);

                    formData.append('logo', this.$refs.logo.files[0]);
                    formData.append('logo_fav', this.$refs.logo_fav.files[0]);
                    formData.append('whatsapp', this.model.whatsapp);
                    formData.append('facebook', this.model.facebook);
                    formData.append('twitter', this.model.twitter);
                    formData.append('instagram', this.model.instagram);
                    formData.append('youtube', this.model.youtube);

                    formData.append('maps', this.model.maps);

                    error = false;
                    for(var pair of formData.entries()) {
                        if ( pair[1] == "" || pair[1] == undefined) {
                            error = true;
                        }
                    }

                    if ( !error ) {
                        axios.post("<?php echo base_url(); ?>api/admin/setting/"+this.setting_id, formData, {
                            headers: {
                                'content-type': "multipart/form-data",
                                'Authorization': 'Bearer ' + localStorage.getItem("token"),
                            }
                        }).then(response => {
                            if ( response.data.code == 200 ) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 10000,
                                });

                                
                                // window.location.href = "<?php echo base_url(); ?>bccadmin/setting";
                                this.loading = false;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                                this.loading = false;
                            }
                        })
                        .catch(error => {
                            if ( error.response.data.code == 401 ) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                }).then(function() {
                                    window.location.href = "<?php echo base_url(); ?>bccadmin/auth/logout";
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                            }
                            this.loading    = false;
                        });
                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'warning',
                            text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        this.loading = false;
                    }
                }
            }
        }
    });
</script>