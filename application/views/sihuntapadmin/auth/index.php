<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Login - Sistem Informasi Hunian Tetap</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="Mannatthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="<?php echo base_url(); ?>template/assets/images/favicon.png">

    <link href="<?php echo base_url(); ?>template/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>template/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>template/assets/css/style.css" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>
<body>
    <div class="accountbg"></div>
    <div class="wrapper-page" id="app">

        <div class="card">
            <div class="card-body">
                <?= alert(); ?>
                <div class="text-center">
                    <a href="<?php echo base_url(); ?>" class="logo logo-admin"><img src="<?php echo load_apps()["logo"]; ?>" height="60" alt="logo"></a>
                    <h5>Halaman Dashboard<br> Sistem Informasi Hunian Tetap<br><br></h5>
                </div>
                <div class="px-3 pb-3">
                    <form class="form-horizontal m-t-20" method="" autcomplete="off">
                        <div class="form-group row">
                            <div class="col-12">
                                <label>Username</label>
                                <input class="form-control" type="text" v-model="email" required="" placeholder="Username" minlength="4">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label>Password</label>
                                <input class="form-control" type="password" v-on:keyup.enter="login" v-model="password" required="" placeholder="Password" minlength="6">
                            </div>
                        </div>
                        <div class="form-group text-center row m-t-20">
                            <div class="col-12">
                                <button class="btn btn-primary btn-block waves-effect waves-light" type="button" v-on:click="login" name="btn_login">Log In</button>
                            </div>
                        </div>
                        <div class="form-group m-t-10 mb-0 row">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url(); ?>template/assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/js/modernizr.min.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/js/detect.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/js/fastclick.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/js/jquery.blockUI.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/js/waves.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/js/jquery.nicescroll.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/js/app.js"></script>

</body>

</html>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            email: "",
            password: "",
        },
        mounted: function() {},
        methods: {
            login() {
                var formData = {
                    email: this.email,
                    password: this.password,
                }

                error = false;
                for (data in formData) {
                    if (formData[data] == "" || formData[data] == undefined) {
                        error = true
                    }
                }

                if (!error) {
                    var query = "?email=" + this.email + "&password=" + this.password;
                    axios("<?php echo base_url(); ?>api/admin/auth" + query).then(response => {
                            var resp = response.data.data;
                            localStorage.id = resp.id;
                            localStorage.name = resp.name;
                            localStorage.email = resp.email;
                            localStorage.master_role_id = resp.master_role_id;
                            localStorage.master_role_name = resp.master_role_name;
                            if (resp.master_role_id == 2) {
                                localStorage.master_district_id = resp.master_district_id;
                            }
                            if (resp.master_role_id == 3) {
                                localStorage.master_district_id = resp.master_district_id;
                                localStorage.master_village_id = resp.master_village_id;
                            }
                            localStorage.token = resp.token;
                            localStorage.menus = JSON.stringify(resp.menus);
                            localStorage.modules = JSON.stringify(resp.modules);
                            window.location.href = "<?php echo base_url(); ?>sihuntapadmin/home";
                        })
                        .catch(error => {
                            if (error.response.data.code == 401) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                }).then(function() {
                                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                            }
                        });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Input email/password terlebih dahulu",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                }
            }
        }
    });
</script>