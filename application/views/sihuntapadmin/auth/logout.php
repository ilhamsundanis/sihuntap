
<!doctype html>
<html lang="en">
    <head>
        <title>Logout</title>
        <!-- others -->
        <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    </head>

    <body>
        <div id="appLogout">
        </div>
    </body>
</html>

<script>
    var appLogout = new Vue({
        el: '#appLogout',
        data: {
        },
        mounted: function () {
            localStorage.clear()
            window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth";
        },
        methods: {
        }
    });
</script>