<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">{{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">{{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-list"></i></span>
                                    <span class="d-none d-sm-block"><i class="fas fa-list"></i> DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="fa fa-pencil-alt"></i></span>
                                    <span class="d-none d-sm-block"><i class="fa fa-eye"></i> LIHAT PROPOSAL</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">LIHAT DETAIL PROPOSAL</h5>
                        <br />
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#informasi" role="tab"><i class="fa fa-info"></i> Informasi Proposal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#lampiran" role="tab"><i class="fa fa-file-archive"></i> Lampiran Proposal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#foto" role="tab"><i class="fa fa-images"></i> Foto Dokumentasi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#victims" role="tab"><i class="fa fa-users"></i> Data Korban Bencana</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active p-3" id="informasi" role="tabpanel">
                                <h6>Informasi Proposal Permohonan</h6>
                                <div class="table-responsive">
                                    <table class="table table-condensed">
                                        <tr>
                                            <th width="200px">Desa / Kecamatan</th>
                                            <th width="1px">:</th>
                                            <th>{{data.master_village_name}} / {{data.master_district_name}}</th>

                                        </tr>
                                        <tr>
                                            <th>Nama Proposal</th>
                                            <th width="1%">:</th>
                                            <th>{{data.name}}</th>
                                        </tr>
                                        <tr>
                                            <th>Jenis Bencana</th>
                                            <th>:</th>
                                            <th>{{data.master_disaster_name}}</th>
                                        </tr>
                                        <tr>
                                            <th>Tingkat Kerusakan</th>
                                            <th>:</th>
                                            <th>{{data.master_damage_name}}</th>
                                        </tr>
                                        <tr>
                                            <th>Tahun</th>
                                            <th>:</th>
                                            <th>{{data.year}}</th>
                                        </tr>
                                        <tr>
                                            <th>Jenis Permohonan</th>
                                            <th>:</th>
                                            <th>{{data.type}}</th>
                                        </tr>
                                        <tr>
                                            <th>Status</th>
                                            <th>:</th>
                                            <th>
                                                <!-- 'UNVERIFIED','ONVERIFIED','VERIFIED','APPROVED','REJECTED','TRANSFERRED' -->
                                                <span class="badge badge-warning" v-if="data.status =='UNVERIFIED'"> BELUM DIVERIFIKASI</span>
                                                <span class="badge badge-info" v-if="data.status =='ONVERIFIED'"> SEDANG DIVERIFIKASI</span>
                                                <span class="badge badge-secondary" v-if="data.status =='VERIFIED'"> SUDAH DIVERIFIKASI</span>
                                                <span class="badge badge-primary" v-if="data.status =='APPROVED'"> DITERIMA</span>
                                                <span class="badge badge-danger" v-if="data.status =='REJECTED'"> DITOLAK</span>
                                                <span class="badge badge-info" v-if="data.status =='TRANSFERRED'"> DITERIMA</span>
                                            </th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane p-3" id="lampiran" role="tabpanel">
                                <p class="font-14 mb-0">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Surat Laporan Kejadian Bencana Alam (SLKBA)</label>
                                        <object :data="data.slkba_file" width="100%" height="500px" type="application/pdf"> SLKBA Tidak Ada </object>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Surat Permohonan Bantuan Penanganan Pasca Bencana (SPBPPB)</label>
                                        <object :data="data.spbppb_file" width="100%" height="500px" type="application/pdf"> SPBPPB Tidak Ada </object>
                                    </div>
                                    <div class="col-md-6">
                                        <label>SK POKMAS </label>
                                        <object :data="data.sk_pokmas_file" width="100%" height="500px" type="application/pdf"> SK POKMAS Tidak Ada </object>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Rencana Anggaran Biaya (RAB)</label>
                                        <object :data="data.rab_file" width="100%" height="500px" type="application/pdf"> RAB Tidak Ada</object>
                                    </div>
                                </div>
                                </p>
                            </div>
                            <div class="tab-pane p-3" id="foto" role="tabpanel">
                                <p class="font-14 mb-0">
                                    Foto Dokumentasi
                                </p>
                            </div>
                            <div class="tab-pane p-3" id="victims" role="tabpanel">
                                <p class="font-14 mb-0">
                                <div class="table-responsive" v-if="!loading">
                                    <table class="table mb-0">
                                        <thead class="table-light">
                                            <tr>
                                                <th>#</th>
                                                <th>No</th>
                                                <th>Desa</th>
                                                <th>Nama</th>
                                                <th>No KK</th>
                                                <th>Bencana</th>
                                                <th>Klasifikasi Bantuan</th>
                                                <th>Verifikasi</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data, index) in list_recipient">
                                                <td v-if="access.edit == 1 || access.delete == 1">

                                                    <a v-on:click="viewVictims(data.id)" class="btn btn-outline-info waves-effect waves-light btn-sm" title="Edit" v-if="access.edit == 1">
                                                        <i class="fas fa-eye"></i>
                                                    </a>
                                                </td>
                                                <th scope="row">{{ index + 1 }}</th>
                                                <td>{{ data.master_village_name}}<br /><code>{{ data.master_district_name}}</code></td>
                                                <td>{{ data.name }}<br />
                                                    <code>{{ data.ktp_number }}</code>
                                                </td>
                                                <td>{{ data.kk_number }} <br />
                                                    <code>{{ data.address}} RT {{data.rt}}/{{data.rw}}</code>
                                                </td>
                                                <td>
                                                    {{ data.master_disaster_name}}<br />
                                                    <code>{{ data.master_damage_name }}</code>
                                                </td>
                                                <td>{{ data.master_help_name }} </td>
                                                <td>{{ data.submission}}</td>

                                                <td>
                                                    <span v-if="data.status == 0" class="badge badge-warning">Sedang Diusulkan</span>
                                                    <span v-if="data.status == 1" class="badge badge-info">Sudah Diverifikasi</span>
                                                    <span v-if="data.status == 2" class="badge badge-info">Sudah Diverifikasi</span>
                                                    <span v-if="data.status == 3" class="badge badge-success">Direlokasi Huntara</span>
                                                    <span v-if="data.status == 4" class="badge badge-primary">Sudah di Relokasi Huntap</span>
                                                </td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade bs-example-modal-lg" id="modalVictims" tabindex="-1" role="dialog" aria-labelledby="modalVictimsDetail" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalVictimsDetail">Lihat Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="main-tabs">
                        <div class="tab-6">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#home-6" data-toggle="tab" aria-expanded="false"><i class="fa fa-user mr-1"></i> Info</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="#lampKTP" data-toggle="tab" aria-expanded="false"><i class="fa fa-vr-cardboard"></i> Lampiran</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#keterangan" data-toggle="tab" aria-expanded="false"><i class="fa fa-file-code"></i> Keterangan</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="#profile-6" data-toggle="tab" aria-expanded="false"><i class="fa fa-mars-stroke-v"></i> Survey</a>
                                </li>
                            </ul>
                            <div class="tab-content bg-white">
                                <div class="tab-pane active home-text p-4" id="home-6">
                                    <div class="row">
                                        <table class="table">
                                            <tr>
                                                <th>Nama</th>
                                                <th>:</th>
                                                <th>{{dataVictims.name}}</th>
                                            </tr>
                                            <tr>
                                                <td>KTP</td>
                                                <td>:</td>
                                                <td>{{dataVictims.ktp_number}}</td>
                                            </tr>
                                            <tr>
                                                <td>KK</td>
                                                <td>:</td>
                                                <td>{{dataVictims.kk_number}}</td>
                                            </tr>
                                            <tr>
                                                <td>Kelamin</td>
                                                <td>:</td>
                                                <td>
                                                    <span v-if="dataVictims.gender == 'L'">LAKI LAKI</span>
                                                    <span v-if="dataVictims.gender == 'P'">PEREMPUAN</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td>:</td>
                                                <td>{{dataVictims.address}}</td>
                                            </tr>
                                            <tr>
                                                <td>RT / RW</td>
                                                <td>:</td>
                                                <td>{{dataVictims.rt}} / {{dataVictims.rw}}</td>
                                            </tr>
                                            <tr>
                                                <td>Desa/Kel</td>
                                                <td>:</td>
                                                <td>{{dataVictims.master_village_name}}</td>
                                            </tr>
                                            <tr>
                                                <td>Kecamatan</td>
                                                <td>:</td>
                                                <td>{{dataVictims.master_district_name}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane p-4" id="profile-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Hasil Survey [1]</label>
                                            <img :src="dataVictims.survey_file_1">
                                        </div>
                                        <div class="col-md-12">
                                            <label>Hasil Survey [2]</label>
                                            <img :src="dataVictims.survey_file_2">
                                        </div>
                                        <div class="col-md-12">
                                            <label>Hasil Survey [3]</label>
                                            <img :src="dataVictims.survey_file_3">
                                        </div>
                                        <div class="col-md-12">
                                            <label>Hasil Survey [4]</label>
                                            <img :src="dataVictims.survey_file_4">
                                        </div>
                                        <div class="col-md-12">
                                            <label>Hasil Survey [5]</label>
                                            <img :src="dataVictims.survey_file_5">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane  p-4" id="lampKTP">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>KTP</label>
                                            <img :src="dataVictims.ktp_file">
                                        </div>
                                        <div class="col-md-12">
                                            <label>Kartu Keluarga</label>
                                            <img :src="dataVictims.kk_file">
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane p-4" id="keterangan">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table">
                                                <tr>
                                                    <th>Jenis Bencana</th>
                                                    <th>:</th>
                                                    <th>{{dataVictims.master_disaster_name}}</th>
                                                </tr>
                                                <tr>
                                                    <td>Tingkatan</td>
                                                    <td>:</td>
                                                    <td>{{dataVictims.master_damage_name}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tgl Kejadian</td>
                                                    <td>:</td>
                                                    <td>{{dataVictims.incident_date}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tahun</td>
                                                    <td>:</td>
                                                    <td>{{dataVictims.year}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Penerimaan</td>
                                                    <td>:</td>
                                                    <td>{{dataVictims.submission}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kategori Bantuan</td>
                                                    <td>:</td>
                                                    <td>{{dataVictims.master_help_name}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah (Rp)</td>
                                                    <td>:</td>
                                                    <td>Rp. {{dataVictims.master_help_ammount}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nomor Rekening</td>
                                                    <td>:</td>
                                                    <td>{{dataVictims.account_number}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tgl Transfer</td>
                                                    <td>:</td>
                                                    <td>{{dataVictims.transfer_date}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    Vue.component('my-input', {
        template: '<input v-attr="name: name" v-model="value" type="text">',
        data() {
            return {
                value: ''
            };
        },
        props: ['name']
    });
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,

        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },
            id: <?php echo $id; ?>,
            loading: false,
            data: {},
            dataVictims: {},
            list_recipient: []
        },
        mounted: function() {
            this.validate_access();
            this.retrive();
            this.get_recipient();



        },
        methods: {
            validate_access() {
                if (this.access.add == 0) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            get_recipient() {
                axios("<?php echo base_url(); ?>api/admin/disaster_victims?proposal_id=" + this.id).then(response => {
                    this.list_recipient = response.data.data;
                    this.totalPage = response.data.meta.totalPage;
                    this.loading = false;
                });
            },
            retrive() {
                axios("<?php echo base_url(); ?>api/admin/disaster_proposal/" + this.id).then(response => {
                    this.data = response.data.data;
                });
            },
            viewVictims(id) {
                axios("<?php echo base_url(); ?>api/admin/disaster_victims/" + id).then(response => {
                    this.dataVictims = response.data.data;
                });
                $('#modalVictims').modal('show');
            }
        }
    });
</script>