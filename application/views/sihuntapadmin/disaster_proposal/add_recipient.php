<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">{{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">{{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-list"></i></span>
                                    <span class="d-none d-sm-block"><i class="fas fa-list"></i> DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="fa fa-pencil-alt"></i></span>
                                    <span class="d-none d-sm-block"><i class="fa fa-pencil-alt"></i> EDIT DATA</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">FORM TAMBAH LAMPIRAN</h4>
                        <br />
                        <div class="row">
                            <div class="mb-3 col-lg-12">
                                <table class="table table-condensed">
                                    <tr>
                                        <th width="10%">Kecamatan</th>
                                        <th width="1%">:</th>
                                        <th width="30%">{{view_data.master_district_name}}</th>

                                        <th width="10%">Desa/Kelurahan</th>
                                        <th width="1%">:</th>
                                        <th width="30%">{{view_data.master_village_name}}</th>
                                    </tr>
                                    <tr>
                                        <th>Nama Proposal</th>
                                        <th width="1%">:</th>
                                        <th colspan="4">{{view_data.name}}</th>
                                    </tr>
                                    <tr>
                                        <td>Jenis Bencana</td>
                                        <td>:</td>
                                        <td>{{view_data.master_disaster_name}}</td>

                                        <td>Tingkat Kerusakan</td>
                                        <td>:</td>
                                        <td>{{view_data.master_damage_name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tahun</td>
                                        <td>:</td>
                                        <td>{{view_data.year}}</td>

                                        <td>Jenis Permohonan</td>
                                        <td>:</td>
                                        <td>{{view_data.type}}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>:</td>
                                        <td colspan="4">
                                            <!-- 'UNVERIFIED','ONVERIFIED','VERIFIED','APPROVED','REJECTED','TRANSFERRED' -->
                                            <span class="badge badge-warning" v-if="view_data.status =='UNVERIFIED'"> BELUM DIVERIFIKASI</span>
                                            <span class="badge badge-info" v-if="view_data.status =='ONVERIFIED'"> SEDANG DIVERIFIKASI</span>
                                            <span class="badge badge-secondary" v-if="view_data.status =='VERIFIED'"> SUDAH DIVERIFIKASI</span>
                                            <span class="badge badge-primary" v-if="view_data.status =='APPROVED'"> DITERIMA</span>
                                            <span class="badge badge-danger" v-if="view_data.status =='REJECTED'"> DITOLAK</span>
                                            <span class="badge badge-info" v-if="view_data.status =='TRANSFERRED'"> DITERIMA</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <h6 class="col-lg-12">Lampiran Korban Bencana</h6>
                            <div class="table-responsive" v-if="!loading">
                                <table class="table mb-0">
                                    <thead class="table-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>NIK</th>
                                            <th>No KK</th>
                                            <th>Alamat</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data, index) in list_recipient">
                                            <th scope="row">{{ index + 1 }}</th>
                                            <td>{{ data.name }}</td>
                                            <td>{{ data.ktp_number }} </td>
                                            <td>{{ data.kk_number }} </td>

                                            <td>{{ data.master_village_name}}, {{data.master_district_name}} </td>
                                            <td>
                                                <a v-if="data.proposal_id == 0 && view_data.status == 'UNVERIFIED' " href="" v-on:click="pilih_korban(data.id)" class="btn small btn-outline-info waves-effect waves-light btn-sm" title="Edit">
                                                    <i class="fas fa-check"></i> Pilih
                                                </a>
                                                <a v-if="data.proposal_id != 0 && view_data.status == 'UNVERIFIED'" href="" v-on:click="batal_korban(data.id)" class="btn small btn-outline-danger waves-effect waves-light btn-sm" title="Edit">
                                                    <i class="fas fa-undo"></i> Kembali
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    Vue.component('my-input', {
        template: '<input v-attr="name: name" v-model="value" type="text">',
        data() {
            return {
                value: ''
            };
        },
        props: ['name']
    });
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,

        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },
            id: <?php echo $id; ?>,
            loading: false,
            pokmas: false,
            individu: false,
            model: {
                master_district_id: "",
                master_village_id: "",
                name: "",
                master_disaster_id: "",
                master_damage_id: "",
                description: "",
                year: "",
                type: "",
                status: "UNVERIFIED",
                slkba_file: "",
                spbppb_file: "",
                sk_pokmas_file: "",
                rab_file: ""
            },
            view_data: {},
            list_master_damage: [],
            list_recipient: [],
            list_master_disaster: [],
            list_master_district: [],
            list_master_village: []
        },
        mounted: function() {
            this.validate_access();
            this.retrive();

            axios("<?php echo base_url(); ?>api/admin/master_disaster").then(response => {
                for (val in response.data.data) {
                    this.list_master_disaster.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_damage").then(response => {
                for (val in response.data.data) {
                    this.list_master_damage.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_district?master_city_id=3201").then(response => {
                this.list_master_district = response.data.data;
            });



        },
        methods: {
            validate_access() {
                if (this.access.add == 0) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            get_village_by_district_id(id) {
                query = "?master_district_id=" + id.target.value;
                axios("<?php echo base_url(); ?>api/admin/master_village" + query).then(response => {
                    // for ( val in response.data.data ) {
                    //     this.list_master_village.push({ label: response.data.data[val].nama_desa, code: response.data.data[val].id_kel });
                    // }
                    this.list_master_village = response.data.data;
                });
            },
            get_type() {
                if (this.model.type == 'POKMAS') {
                    this.pokmas = true;
                    this.individu = false;
                }
                if (this.model.type == 'INDIVIDU') {
                    this.individu = true;
                    this.pokmas = false;
                }
            },
            retrive() {
                axios("<?php echo base_url(); ?>api/admin/disaster_proposal/" + this.id).then(response => {
                    this.view_data = response.data.data;
                    axios("<?php echo base_url(); ?>api/admin/disaster_victims?master_village_id=" + this.view_data.master_village_id).then(response => {
                        this.list_recipient = response.data.data;
                    });
                });
            },
            pilih_korban(id) {
                this.loading = true;
                var formData = {
                    proposal_status: "SUDAH",
                    proposal_id: this.id,
                };

                axios.put("<?php echo base_url(); ?>api/admin/disaster_victims/" + id, formData, {
                        headers: {
                            'content-type': "application/json",
                            'Authorization': 'Bearer ' + localStorage.getItem("token"),
                        }
                    }).then(response => {
                        if (response.data.code == 200) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 10000,
                            });

                            this.loading = false;
                            this.retrive();
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                            this.loading = false;
                        }
                    })
                    .catch(error => {
                        if (error.response.data.code == 401) {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            }).then(function() {
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }
                        this.loading = false;
                    });
            },
            batal_korban(id) {
                this.loading = true;
                var formData = {
                    proposal_status: "BELUM",
                    proposal_id: 0,
                };

                axios.put("<?php echo base_url(); ?>api/admin/disaster_victims/" + id, formData, {
                        headers: {
                            'content-type': "application/json",
                            'Authorization': 'Bearer ' + localStorage.getItem("token"),
                        }
                    }).then(response => {
                        if (response.data.code == 200) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 10000,
                            });

                            this.loading = false;
                            this.retrive();
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                            this.loading = false;
                        }
                    })
                    .catch(error => {
                        if (error.response.data.code == 401) {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            }).then(function() {
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }
                        this.loading = false;
                    });
            },

        }
    });
</script>