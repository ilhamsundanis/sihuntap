<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">{{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">{{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-list"></i></span>
                                    <span class="d-none d-sm-block"><i class="fas fa-list"></i> DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="fa fa-pencil-alt"></i></span>
                                    <span class="d-none d-sm-block"><i class="fa fa-pencil-alt"></i> EDIT DATA</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">FORM EDIT</h4>
                        <br />
                        <div class="row">
                            <div class="mb-3 col-lg-6">
                                <label>Pilih Kecamatan</label>
                                <select class="form-control" v-model="model.master_district_id" @change="get_village_by_district_id($event)" required>
                                    <option value="" selected>--Pilih Kecamatan--</option>
                                    <option v-for="data in list_master_district" :value="data.id">{{data.name}}</option>
                                </select>
                            </div>
                            <div class="mb-3 col-lg-6">
                                <label>Pilih Desa</label>
                                <select class="form-control" v-model="model.master_village_id" required>
                                    <option value="" selected>--Pilih Desa--</option>
                                    <option v-for="(data,index) in list_master_village" :value="data.id">{{data.name}}</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="border-bottom:1px solid #a0a0a0;margin-bottom:15px;padding-bottom:5px;">

                            <div class="mb-3 col-lg-12">
                                <label>Nama Proposal *</label>
                                <input class="form-control small" type="text" v-model="model.name" placeholder="Nama Proposal" required>

                            </div>

                            <div class="mb-3 col-lg-4">
                                <label>Jenis Bencana *</label>
                                <v-select placeholder="Jenis Bencana" :options="list_master_disaster" v-model="model.master_disaster_id" :reduce="master_disaster_id => master_disaster_id.code" class="form-control" style="padding: 3px;"></v-select>
                            </div>
                            <div class="mb-3 col-lg-3">
                                <label>Tingkat Kerusakan *</label>
                                <v-select placeholder="Kerusakan" :options="list_master_damage" v-model="model.master_damage_id" :reduce="master_damage_id => master_damage_id.code" class="form-control" style="padding: 3px;"></v-select>
                            </div>
                            <div class="mb-3 col-lg-3">
                                <label>Jenis Permohonan</label>
                                <select v-model="model.type" class="form-control" @change="get_type">
                                    <option selected value="">--Jenis--</option>
                                    <option value="POKMAS">POKMAS</option>
                                    <option value="INDIVIDU">INDIVIDU</option>
                                </select>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>Tahun</label>
                                <select v-model="model.year" class="form-control">
                                    <option selected value="">--Tahun--</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                                    <option value="2025">2025</option>
                                </select>
                            </div>
                            <div class="mb-3 col-lg-12">
                                <label>Kronologi Bencana *</label>
                                <textarea class="form-control" v-model="model.description" placeholder="Tulis kronologi kejadian bencana"></textarea>
                            </div>


                        </div>
                        <div class="row">
                            <h6 class="col-lg-12">Lampiran Dokumen</h6>
                            <div class="mb-3 col-lg-6" v-if="pokmas == true || individu == true">
                                <label>Surat Laporan Kejadian Bencana Alam (SLKBA) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="slkba_file" required>
                                <object :data="model.slkba_file" type="application/pdf" style="width: 100%;" height="600"></object>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="pokmas == true">
                                <label>Surat Permohonan Bantuan Penanganan Pasca Bencana (SPBPPB) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="spbppb_file" required>
                                <object :data="model.spbppb_file" type="application/pdf" style="width: 100%;" height="600"></object>
                            </div>
                            <div class="mb-3 col-lg-6" v-if="pokmas == true">
                                <label>Surat Keterangan Kelompok Masyarakat (SK Pokmas) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="sk_pokmas_file" required>
                                <object :data="model.sk_pokmas_file" type="application/pdf" style="width: 100%;" height="600"></object>
                            </div>
                            <div class="mb-3 col-lg-6">
                                <label>Rencana Anggaran Biaya (RAB) *.pdf</label>
                                <input class="form-control small" accept="application/pdf" type="file" ref="rab_file" required>
                                <object :data="model.rab_file" type="application/pdf" style="width: 100%;" height="600"></object>
                            </div>
                        </div>


                        <div class="form-group row " style="text-align:right">
                            <div class="col-sm-12">
                                <br />
                                <button type="submit" class="btn btn-primary" v-on:click="save"><i class="fa fa-save"></i> UPDATE DATA</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    Vue.component('my-input', {
        template: '<input v-attr="name: name" v-model="value" type="text">',
        data() {
            return {
                value: ''
            };
        },
        props: ['name']
    });
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,

        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },
            id: <?php echo $id; ?>,
            loading: false,
            pokmas: false,
            individu: false,
            model: {
                master_district_id: "",
                master_village_id: "",
                name: "",
                master_disaster_id: "",
                master_damage_id: "",
                description: "",
                year: "",
                type: "",
                status: "UNVERIFIED",
                slkba_file: "",
                spbppb_file: "",
                sk_pokmas_file: "",
                rab_file: ""
            },
            list_master_damage: [],
            list_master_help: [],
            list_master_disaster: [],
            list_master_district: [],
            list_master_village: []
        },
        mounted: function() {
            this.validate_access();
            this.retrive();
            axios("<?php echo base_url(); ?>api/admin/master_disaster").then(response => {
                for (val in response.data.data) {
                    this.list_master_disaster.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_damage").then(response => {
                for (val in response.data.data) {
                    this.list_master_damage.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_district?master_city_id=3201&id=" + this.model.master_district_id).then(response => {
                this.list_master_district = response.data.data;
            });



        },
        methods: {
            validate_access() {
                if (this.access.add == 0) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            get_village_by_district_id(id) {
                query = "?master_district_id=" + id.target.value;
                axios("<?php echo base_url(); ?>api/admin/master_village" + query).then(response => {
                    // for ( val in response.data.data ) {
                    //     this.list_master_village.push({ label: response.data.data[val].nama_desa, code: response.data.data[val].id_kel });
                    // }
                    this.list_master_village = response.data.data;
                });
            },
            get_type() {
                if (this.model.type == 'POKMAS') {
                    this.pokmas = true;
                    this.individu = false;
                }
                if (this.model.type == 'INDIVIDU') {
                    this.individu = true;
                    this.pokmas = false;
                }
            },
            retrive() {
                axios("<?php echo base_url(); ?>api/admin/disaster_proposal/" + this.id).then(response => {
                    var data = response.data.data;
                    this.model.name = data.name;
                    this.model.type = data.type;
                    this.model.description = data.description;
                    this.model.master_district_id = data.master_district_id;
                    this.model.master_village_id = data.master_village_id;
                    axios("<?php echo base_url(); ?>api/admin/master_village?id=" + data.master_village_id).then(response => {
                        this.list_master_village = response.data.data;
                    });
                    this.model.master_disaster_id = data.master_disaster_id;
                    this.model.master_damage_id = data.master_damage_id;
                    this.model.year = data.year;
                    this.get_type(this.model.type);
                    this.model.slkba_file = data.slkba_file;
                    this.model.spbppb_file = data.spbppb_file;
                    this.model.sk_pokmas_file = data.sk_pokmas_file;
                    this.model.rab_file = data.rab_file;
                });
            },
            save() {
                this.loading = true;
                error = false;
                var formData = new FormData();
                formData.append('master_district_id', this.model.master_district_id);
                formData.append('master_village_id', this.model.master_village_id);
                formData.append('name', this.model.name);
                formData.append('master_disaster_id', this.model.master_disaster_id);
                formData.append('master_damage_id', this.model.master_damage_id);
                formData.append('description', this.model.description);
                formData.append('year', this.model.year);
                formData.append('status', this.model.status);
                formData.append('type', this.model.type);
                formData.append('slkba_file', this.$refs.slkba_file.files[0]);
                formData.append('spbppb_file', this.$refs.spbppb_file.files[0]);
                formData.append('rab_file', this.$refs.rab_file.files[0]);
                formData.append('sk_pokmas_file', this.$refs.sk_pokmas_file.files[0]);
                formData.append('created_by', localStorage.getItem("id"));
                formData.append('created_dt', '<?= date('Y-m-d H:i:s'); ?>');

                if (this.model.master_district_id == "" || this.model.master_village_id == "" || this.model.name == "" ||
                    this.model.master_disaster_id == "" || this.model.master_damage_id == "" || this.model.description == "") {
                    error = true;
                }

                if (!error) {
                    axios.post("<?php echo base_url(); ?>api/admin/disaster_proposal/" + this.id, formData, {
                            headers: {
                                'content-type': "multipart/form-data",
                                'Authorization': 'Bearer ' + localStorage.getItem("token"),
                            }
                        }).then(response => {
                            if (response.data.code == 200) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 10000,
                                });
                                this.retrive();
                                this.loading = false;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                                this.loading = false;
                            }
                        })
                        .catch(error => {
                            if (error.response.data.code == 401) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                }).then(function() {
                                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                            }
                            this.loading = false;
                        });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            }
        }
    });
</script>