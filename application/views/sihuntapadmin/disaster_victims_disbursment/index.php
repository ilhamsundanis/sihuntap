<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">{{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">{{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="" role="tab">
                                    <span class="d-block d-sm-none"><i class="fas fa-list"></i></span>
                                    <span class="d-none d-sm-block"><i class="fas fa-list"></i> DATA</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="page-title-box">
                            <h4 class="page-title">FILTER DATA</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Nama Penerima" v-model="filter.name" v-on:keyup.enter="retrive">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="NIK" v-model="filter.ktp_number" v-on:keyup.enter="retrive">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="NO KK" v-model="filter.no_kk" v-on:keyup.enter="retrive">
                                </div>
                            </div>
                            <!-- <div class="col-md-4">
                                <div class="mb-3">
                                    <select v-model="filter.status" class="form-control" @change="retrive">
                                        <option selected value="">--Berdasarkan Status--</option>
                                        <option :value="0"> Diusulkan</option>
                                        <option :value="1"> Ditolak</option>
                                        <option :value="2"> Diterima</option>
                                        <option :value="3"> Direlokasi Huntara</option>
                                        <option :value="4"> Direlokasi Huntap</option>
                                    </select>
                                </div>
                            </div> -->
                            <div class="col-md-3">
                                <div class="mb-4">
                                    <v-select placeholder="Kecamatan" :options="list_master_district" v-model="filter.master_district_id" :reduce="master_district_id => master_district_id.code" class="form-control" style="padding: 3px;" @input="get_village"></v-select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-4">
                                    <v-select placeholder="Pilih Desa/Kelurahan" :options="list_master_village" v-model="filter.master_village_id" :reduce="master_village_id => master_village_id.code" class="form-control" style="padding: 3px;" @input="retrive"></v-select>
                                </div>
                            </div>
                            <div class="col-md-2 text-right">
                                <button type="button" class="btn btn-danger waves-effect waves-light me-1" v-on:click="reset_filter">
                                    <i class="fa fa-times"></i> Reset Filter
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 text-right mb-4">
                                <a href="javascript:void(0)" class="btn btn-success" v-on:click="export_excel">
                                    <i class="fa fa-download"></i> Export Ke Excel
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="table-responsive" v-if="!loading">
                                <table class="table mb-0">
                                    <thead class="table-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Desa</th>
                                            <th>Nama</th>
                                            <th>Bencana</th>
                                            <th>Klasifikasi Bantuan</th>
                                            <th>No Rekening</th>
                                            <th>Tgl Transfer</th>
                                            <!-- <th>#</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data, index) in list_recipient">
                                            <th scope="row">{{ index + 1 }}</th>
                                            <td>{{ data.master_village_name}}<br /><code>{{ data.master_district_name}}</code></td>
                                            <td>{{ data.name }} <br /> <code>{{ data.ktp_number }}</code></td>
                                            <td>
                                                {{ data.master_disaster_name}}<br />
                                                <code>{{ data.master_damage_name }}</code>
                                            </td>
                                            <td>{{ data.master_help_name }} <br />
                                                <code>Rp. {{ data.master_help_ammount }}</code>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" :value="data.account_number" v-on:keyup.enter="update_account(data.id, event)">
                                            </td>
                                            <td>
                                                <input type="date" class="form-control" :value="data.transfer_date" v-on:change="update_transfer(data.id, event)">
                                            </td>
                                            <!-- <td v-if="access.edit == 1 || access.delete == 1">
                                                
                                                <a :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module + '/view/' + data.id" class="btn btn-outline-info waves-effect waves-light btn-sm" title="Edit" v-if="access.edit == 1">
                                                    <i class="fas fa-eye"></i> Lihat
                                                </a>
                                            </td> -->
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-12" style="text-align: center;">
                            <template>
                                <paginate v-model="currentPage" :page-count="totalPage" :click-handler="clickCallback" :prev-text="'Prev'" :next-text="'Next'" :container-class="'vue-pagination'"></paginate>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'paginate': VuejsPaginate,
            'v-select': VueSelect.VueSelect,
        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },
            loading: false,
            model: {
                name: "",
                description: "",
                master_disaster_id: "",
                master_damage_id: "",
                status: 1,
                status_description: "",
                master_district_id: "",
                master_village_id: "",
                address: ""
            },
            filter: {
                id: (localStorage.getItem("filter_id") == null) ? "" : localStorage.getItem("filter_id"),
                name: (localStorage.getItem("filter_name") == null) ? "" : localStorage.getItem("filter_name"),
                status: (localStorage.getItem("filter_status") == null) ? "" : localStorage.getItem("filter_status"),
                ktp_number: (localStorage.getItem("filter_ktp_number") == null) ? "" : localStorage.getItem("filter_ktp_number"),
                master_district_id: (localStorage.getItem("filter_master_district_id") == null) ? "" : localStorage.getItem("filter_master_district_id"),
                master_village_id: (localStorage.getItem("filter_master_village_id") == null) ? "" : localStorage.getItem("filter_master_village_id")
            },
            list_recipient: [],
            list_master_district: [],
            list_master_village: [],
            currentPage: <?php echo empty($this->uri->segment(4)) ? 1 : $this->uri->segment(4); ?>,
            totalPage: 1,
        },
        mounted: function() {
            this.validate_access();
            axios("<?php echo base_url(); ?>api/admin/master_district?master_city_id=3201").then(response => {
                for (val in response.data.data) {
                    this.list_master_district.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });

            this.retrive();
        },
        methods: {
            clickCallback(pageNum) {
                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/" + this.master_module_module + "/index/" + pageNum;
            },
            validate_access() {
                if (this.access.add == 0) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            get_village(id) {
                this.retrive();
                this.model.master_village_id = "";
                this.list_master_village = [];
                axios("<?php echo base_url(); ?>api/admin/master_village?master_district_id=" + id).then(response => {
                    for (val in response.data.data) {
                        this.list_master_village.push({
                            label: response.data.data[val].name,
                            code: response.data.data[val].id
                        });
                    }
                });
            },
            retrive() {
                this.filter_data();
                var query = "?orderBy=id&sort=desc&limit=30&page=" + this.currentPage;
                query += "&submission=DITERIMA";
                query += "&status=1";
                query += "&id=" + this.filter.id;
                query += "&name=" + this.filter.name;
                query += "&ktp_number=" + this.filter.ktp_number;
                query += "&master_district_id=" + this.filter.master_district_id;
                query += "&master_village_id=" + this.filter.master_village_id;
                axios("<?php echo base_url(); ?>api/admin/disaster_victims" + query).then(response => {
                    this.list_recipient = response.data.data;
                    this.totalPage = response.data.meta.totalPage;
                    this.loading = false;
                });

            },
            filter_data() {
                localStorage.setItem("filter_id", this.filter.id);
                localStorage.setItem("filter_name", this.filter.name);
                localStorage.setItem("filter_status", this.filter.status);
                localStorage.setItem("filter_ktp_number", this.filter.ktp_number);
                localStorage.setItem("filter_master_district_id", this.filter.master_district_id);
                localStorage.setItem("filter_master_village_id", this.filter.master_village_id);

                // this.filter.master_company_size_id = (this.filter.master_company_size_id == null) ? "" : this.filter.master_company_size_id;
                // localStorage.setItem("filter_master_company_size_id", this.filter.master_company_size_id);


            },
            reset_filter() {
                this.filter.id = "";
                this.filter.name = "";
                this.filter.status = "";
                this.filter.ktp_number = "";
                this.filter.master_district_id = "";
                this.filter.master_village_id = "";

                this.retrive();
            },
            update_account(id, event) {
                this.loading = true;
                var formData = {
                    account_number: event.target.value,
                    status: 2
                };

                axios.put("<?php echo base_url(); ?>api/admin/disaster_victims/" + id, formData, {
                        headers: {
                            'content-type': "application/json",
                            'Authorization': 'Bearer ' + localStorage.getItem("token"),
                        }
                    }).then(response => {
                        if (response.data.code == 200) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 10000,
                            });

                            this.loading = false;
                            this.retrive();
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                            this.loading = false;
                        }
                    })
                    .catch(error => {
                        if (error.response.data.code == 401) {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            }).then(function() {
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }
                        this.loading = false;
                    });
            },
            update_transfer(id, event) {
                this.loading = true;
                var formData = {
                    transfer_date: event.target.value,
                    status: 2
                };

                axios.put("<?php echo base_url(); ?>api/admin/disaster_victims/" + id, formData, {
                        headers: {
                            'content-type': "application/json",
                            'Authorization': 'Bearer ' + localStorage.getItem("token"),
                        }
                    }).then(response => {
                        if (response.data.code == 200) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 10000,
                            });

                            this.loading = false;
                            this.retrive();
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                            this.loading = false;
                        }
                    })
                    .catch(error => {
                        if (error.response.data.code == 401) {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            }).then(function() {
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: error.response.data.message,
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }
                        this.loading = false;
                    });
            },
            export_excel() {

                var query = "?orderBy=id&sort=desc&limit=30&page=" + this.currentPage;
                query += "&id=" + this.filter.id;
                query += "&name=" + this.filter.name;
                query += "&status=" + this.filter.status;
                query += "&ktp_number=" + this.filter.ktp_number;
                query += "&master_district_id=" + this.filter.master_district_id;
                query += "&master_village_id=" + this.filter.master_village_id;
                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/" + this.master_module_module + "/export?" + query;
            }

        }
    });
</script>