<div class="main-content" id="appContent">
    <div class="page-content">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">EDIT {{ master_module_name }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                            <li class="breadcrumb-item active">Edit {{ master_module_name }}</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" :href="'<?php echo base_url(); ?>sihuntapadmin/' + master_module_module">
                                    <span class="d-block d-sm-none"><i class="fas fa-list"></i></span>
                                    <span class="d-none d-sm-block"><i class="fas fa-list"></i> DATA</span>
                                </a>
                            </li>
                            <li class="nav-item" v-if="access.add == 1">
                                <a class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="fas fa-pencil-alt"></i></span>
                                    <span class="d-none d-sm-block"><i class="fas fa-pencil-alt"></i> EDIT</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">FORM EDIT</h4>
                        <br />
                        <div class="row">
                            <div class="mb-3 col-lg-6">
                                <label>Nama Kecamatan</label>
                                <v-select placeholder="Pilih Kecamatan" :options="list_master_district" v-model="model.master_district_id" :reduce="master_district_id => master_district_id.code" @input="get_village_by_district_id" class="form-control" style="padding: 3px;"></v-select>

                            </div>
                            <div class="mb-3 col-lg-6">
                                <label>Nama Desa</label>
                                <v-select placeholder="Pilih Desa" :options="list_master_village" v-model="model.master_village_id" :reduce="master_village_id => master_village_id.code" class="form-control" style="padding: 3px;"></v-select>

                            </div>

                        </div>
                        <hr>

                        <div class="row" style="border-bottom:1px solid #a0a0a0;margin-bottom:15px;padding-bottom:5px;">

                            <div class="mb-3 col-lg-4">
                                <label>Nama Lengkap</label>
                                <input class="form-control small" type="text" v-model="model.name" placeholder="Nama Lengkap" required>

                            </div>
                            <div class="mb-3 col-lg-3">
                                <label>NIK</label>
                                <input class="form-control small" type="number" v-model="model.ktp_number" required placeholder="NIK">
                            </div>
                            <div class="mb-3 col-lg-3">
                                <label>No. Kartu Keluarga</label>
                                <input class="form-control small" type="number" placeholder="No Kartu Keluarga" v-model="model.kk_number">
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>Jenis Kelamin</label>
                                <select name="jenis_kelamin" v-model="model.gender" class="form-control small" required>
                                    <option value="" selected> -- Pilih JK--</option>
                                    <option value="L">LAKI LAKI</option>
                                    <option value="P">PEREMPUAN</option>
                                </select>
                            </div>
                            <div class="mb-3 col-lg-3">
                                <label>Alamat Tinggal</label>
                                <input class="form-control" type="text" placeholder="KP/JL/DUSUN" v-model="model.address" required>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>RT</label>
                                <input class="form-control" type="number" placeholder="000" v-model="model.rt" required>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>RW</label>
                                <input class="form-control" type="number" placeholder="000" v-model="model.rw" required>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>Tanggal Kejadian</label>
                                <input class="form-control" type="date" placeholder="RW" v-model="model.incident_date" required>
                            </div>
                            <div class="mb-3 col-lg-3">
                                <label>Jenis Bencana</label>
                                <v-select placeholder="Jenis Bencana" :options="list_master_disaster" v-model="model.master_disaster_id" :reduce="master_disaster_id => master_disaster_id.code" class="form-control" style="padding: 3px;"></v-select>
                            </div>
                            <div class="mb-3 col-lg-3">
                                <label>Tingkat Kerusakan</label>
                                <v-select placeholder="Kerusakan" :options="list_master_damage" v-model="model.master_damage_id" :reduce="master_damage_id => master_damage_id.code" class="form-control" style="padding: 3px;"></v-select>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>Tahun</label>
                                <input class="form-control" type="number" placeholder="Tahun" v-model="model.year" required>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>KTP File</label>
                                <input class="form-control" type="file" accept="image/*" placeholder="RW" ref="ktp_file" required>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>KK File</label>
                                <input class="form-control" type="file" accept="image/*" placeholder="RW" ref="kk_file" required>
                            </div>
                            <div class="mb-3 col-lg-2">
                                <label>Dokumentasi</label>
                                <input class="form-control" type="file" accept="image/*" placeholder="RW" ref="dokumentasi" required>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-11">
                                <br />
                                <button type="submit" class="btn btn-primary" v-on:click="save">SIMPAN DATA</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    Vue.component('my-input', {
        template: '<input v-attr="name: name" v-model="value" type="text">',
        data() {
            return {
                value: ''
            };
        },
        props: ['name']
    });
    var appContent = new Vue({
        el: '#appContent',
        components: {
            'v-select': VueSelect.VueSelect,

        },
        data: {
            master_module_name: localStorage.getItem("master_module_name"),
            master_module_module: localStorage.getItem("master_module_module"),
            access: {
                read: localStorage.getItem("read"),
                add: localStorage.getItem("add"),
                edit: localStorage.getItem("edit"),
                delete: localStorage.getItem("delete"),
                import: localStorage.getItem("import"),
            },
            id: <?php echo $id; ?>,
            loading: false,
            district_id: "",
            village_id: "",
            model: {
                name: "",
                ktp_number: "",
                kk_number: "",
                gender: "",
                status: 0,
                address: "",
                rt: "",
                rw: "",
                master_district_id: "",
                master_village_id: "",
                master_damage_id: "",
                master_disaster_id: "",
                non_category: "BELUM TERKLASIFIKASI",
                year: "",
                entered: "BELUM MASUK",
                submission: "BELUM",
                incident_date: ""
            },

            list_company: [],
            list_master_damage: [],
            list_master_disaster: [],
            list_master_district: [],
            list_master_village: []
        },
        mounted: function() {
            this.validate_access();
            axios("<?php echo base_url(); ?>api/admin/master_disaster").then(response => {
                for (val in response.data.data) {
                    this.list_master_disaster.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });
            axios("<?php echo base_url(); ?>api/admin/master_damage").then(response => {
                for (val in response.data.data) {
                    this.list_master_damage.push({
                        label: response.data.data[val].name,
                        code: response.data.data[val].id
                    });
                }
            });


            axios("<?php echo base_url(); ?>api/admin/disaster_victims/" + this.id).then(response => {
                var data = response.data.data;
                axios("<?php echo base_url(); ?>api/admin/master_district?id=" + data.master_district_id).then(response => {
                    for (val in response.data.data) {
                        this.list_master_district.push({
                            label: response.data.data[val].name,
                            code: response.data.data[val].id
                        });
                    }
                });
                axios("<?php echo base_url(); ?>api/admin/master_village?id=" + data.master_village_id).then(response => {
                    for (val in response.data.data) {
                        this.list_master_village.push({
                            label: response.data.data[val].name,
                            code: response.data.data[val].id
                        });
                    }
                });
                this.model.name = data.name;
                this.model.ktp_number = data.ktp_number;
                this.model.kk_number = data.kk_number;
                this.model.gender = data.gender;
                this.model.address = data.address;
                this.model.master_district_id = data.master_district_id;
                this.model.master_village_id = data.master_village_id;
                this.model.master_disaster_id = data.master_disaster_id;
                this.model.rt = data.rt;
                this.model.rw = data.rw;
                this.model.master_damage_id = data.master_damage_id;
                this.model.year = data.year;
                this.model.incident_date = data.incident_date;



            });

        },
        methods: {
            validate_access() {
                if (this.access.add == 0) {
                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/access";
                }
            },
            get_village_by_district_id(id) {
                this.list_master_village = [];
                if (localStorage.getItem("master_role_id") == 3) {
                    query = "?id=" + localStorage.getItem("master_village_id");
                } else {
                    query = "?master_district_id=" + id;
                }
                axios("<?php echo base_url(); ?>api/admin/master_village" + query).then(response => {
                    for (val in response.data.data) {
                        this.list_master_village.push({
                            label: response.data.data[val].name,
                            code: response.data.data[val].id
                        });
                    }
                });
            },

            save() {
                this.loading = true;
                error = false;
                var formData = new FormData();
                formData.append('name', this.model.name);
                formData.append('ktp_number', this.model.ktp_number);
                formData.append('kk_number', this.model.kk_number);
                formData.append('gender', this.model.gender);
                formData.append("address", this.model.address);
                formData.append('rt', this.model.rt);
                formData.append('rw', this.model.rw);
                formData.append('master_disaster_id', this.model.master_disaster_id);
                formData.append('master_village_id', this.model.master_village_id);
                formData.append('master_district_id', this.model.master_district_id);
                formData.append('master_damage_id', this.model.master_damage_id);
                formData.append('year', this.model.year);
                formData.append('incident_date', this.model.incident_date);
                formData.append('submission', 'BELUM');

                error = false;
                if (this.model.name == "" || this.model.ktp_number == "" || this.model.kk_number == "") {
                    error = true;
                }

                if (!error) {
                    axios.post("<?php echo base_url(); ?>api/admin/disaster_victims/" + this.id, formData, {
                            headers: {
                                'content-type': "multipart/form-data",
                                'Authorization': 'Bearer ' + localStorage.getItem("token"),
                            }
                        }).then(response => {
                            if (response.data.code == 200) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 10000,
                                });
                                window.location.href = "<?php echo base_url(); ?>sihuntapadmin/" + master_module_module;

                                this.loading = false;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    text: response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                                this.loading = false;
                            }
                        })
                        .catch(error => {
                            if (error.response.data.code == 401) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                }).then(function() {
                                    window.location.href = "<?php echo base_url(); ?>sihuntapadmin/auth/logout";
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: error.response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                });
                            }
                            this.loading = false;
                        });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: "Lengkapi data terlebih dahulu sebelum menyimpan data",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    this.loading = false;
                }
            }
        }
    });
</script>