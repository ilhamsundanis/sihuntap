<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_auth extends CI_Model {

    public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['email'] ) ) {
            array_push($where, "users.email = '" . $filter['email'] . "'");
        }

        if ( !empty( $filter['password'] ) ) {
            array_push($where, "users.password = '" . $filter['password']. "'");
        }

        return $where;
    }    
	
	public function get_detail( $filter = array() )
	{
		$query = 'SELECT 
            users.id,
            users.name,
            users.email,
            users.master_role_id,
            users.master_district_id,
            users.master_village_id,
            master_role.name as master_role_name
        FROM users 
        INNER JOIN master_role on users.master_role_id = master_role.id';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        $result = $this->db->query($query)->row_array();
		return $result;
    }
    
    public function get_menus( $master_role_id )
	{
		$query = 'SELECT 
            master_module_group.id, 
            master_module_group.name,
            master_module_group.icon 
        FROM master_role_access 
        INNER JOIN master_module on master_role_access.master_module_id = master_module.id 
        INNER JOIN master_module_group on master_module.master_module_group_id = master_module_group.id 
        WHERE master_role_access.master_role_id = ' . $master_role_id . ' 
        GROUP BY master_module_group.id, master_module_group.name 
        ORDER BY master_module_group.sort ASC';
        $result = $this->db->query($query)->result_array();
        foreach ( $result as $idx => $val ) {
            $result[$idx]["submenus"] = $this->get_submenus($master_role_id, $val["id"]);
        }
		return $result;
    }

    public function get_submenus( $master_role_id, $master_module_group_id )
	{
		$query = 'SELECT 
            master_module.id, 
            master_module.name, 
            master_module.module
        FROM master_role_access 
        INNER JOIN master_module on master_role_access.master_module_id = master_module.id 
        INNER JOIN master_module_group on master_module.master_module_group_id = master_module_group.id 
        WHERE master_role_access.master_role_id = ' . $master_role_id . ' 
        AND master_module.master_module_group_id = ' . $master_module_group_id . ' 
        AND master_role_access.access_read = 1  
        ORDER BY master_module.sort ASC';
        $result = $this->db->query($query)->result_array();
		return $result;
    }

}

/* End of file Model_auth.php */
