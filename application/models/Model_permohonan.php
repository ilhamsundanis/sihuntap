<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_permohonan extends CI_Model
{

	public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "id_permohonan = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['nama_permohonan'] ) ) {
            array_push($where, "nama_permohonan LIKE '%" . $filter['nama_permohonan'] . "%'");
        }

        if ( !empty( $filter['nik'] ) ) {
            array_push($where, "nik = '" . $filter['nik'] . "'");
        }


        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT 
            tb_permohonan.*, 
            tr_kecamatan.nama as nama_kecamatan,
            tr_kelurahan.nama_desa as nama_desa,
            tr_jenis_bencana.nama as jenis_bencana,
            tr_kerusakan.nama as kerusakan
        FROM tb_permohonan
        LEFT JOIN tr_kelurahan on tb_permohonan.desa_id = tr_kelurahan.id_kel 
        LEFT JOIN tr_kecamatan on tb_permohonan.kec_id = tr_kecamatan.id_kec
        LEFT JOIN tr_jenis_bencana on tb_permohonan.bencana_id = tr_jenis_bencana.id
        LEFT JOIN tr_kerusakan on tb_permohonan.kerusakan_id = tr_kerusakan.id';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

	function get_desa_by_kec($id_kec)
	{
		$this->db->where('id_kec', $id_kec);
		$this->db->order_by('id_kel', 'ASC');
		$query = $this->db->get('tr_kelurahan')->result_array();
		return $query;
	}
	public function get_all( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}
	
	public function get_detail( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->row_array();
		return $result;
    }	

	function add($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->affected_rows() > 0;
	}
	function update($table, $data, $where)
	{
		$this->db->where($where);
		$q = $this->db->get($table);
		$this->db->reset_query();
			
		if ($q->num_rows() > 0 ) 
		{
			$this->db->where($where)->update($table, $data);
			return $this->db->affected_rows() > 0;
		} else {
			$this->db->insert($table, $data);
			return $this->db->affected_rows() > 0;
		}
	}

	function delete($table, $where)
	{
		$this->db->where($where);
		return $this->db->delete($table);
	}
}
