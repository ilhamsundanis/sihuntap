<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_news extends CI_Model
{
	function get_desa_by_kec($id_kec)
	{
		$this->db->where('id_kec', $id_kec);
		$this->db->order_by('id_kel', 'ASC');
		$query = $this->db->get('tr_kelurahan')->result_array();
		return $query;
	}
	function get_data()
	{	
		$this->db->join('master_category_news', 'page_news.master_category_news_id = master_category_news.id');
		$query = $this->db->get('page_news')->result_array();
		return $query;
	}
	function get_artikel_by_id($id)
	{
		$this->db->select("page_news.*, master_category_news.kategori_berita as 'kategori', tb_user.nama_user as 'nama_user'");
		$this->db->join('master_category_news', 'page_news.master_category_news_id = master_category_news.id');
		$this->db->join('tb_user', 'page_news.created_by = tb_user.id_user');
		$this->db->where('id_berita',$id);
		$query = $this->db->get('page_news')->result_array();
		return $query;
	}
	function get_kategori()
	{
		return $this->db->get('master_category_news')->result_array();
	}

	function save($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->affected_rows() > 0;
	}
	function update_data_huntap($table, $data, $id)
	{
		$this->db->where('id_penerima', $id);
		$this->db->update($table, $data);
		return $this->db->affected_rows();
	}

	function delete_huntap($id)
	{
		$this->db->where('id_penerima', $id);
		return $this->db->delete('tm_penerima');
	}
}
