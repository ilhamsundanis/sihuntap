<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_master_role_access extends CI_Model {

    public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "id = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['master_role_id'] ) ) {
            array_push($where, "master_role_id = '" . $filter['master_role_id'] . "'");
        }

        if ( !empty( $filter['master_module_id'] ) ) {
            array_push($where, "master_module_id = '" . $filter['master_module_id'] . "'");
        }

        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT 
            master_role_access.*,
            master_role.name as master_role_name,
            master_module.name as master_module_name,
            master_module.module as master_module_module
        FROM master_role_access 
        INNER JOIN master_role on master_role_access.master_role_id = master_role.id 
        INNER JOIN master_module on master_role_access.master_module_id = master_module.id';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

    public function get_all( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}
	
	public function get_detail( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->row_array();
		return $result;
    }	

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}

    public function delete( $table, $id )
	{
        $this->db->where('id', $id);
        $delete = $this->db->delete($table);
		return $delete;
	}

}

/* End of file ModelName.php */
