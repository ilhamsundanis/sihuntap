<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_disaster_disbursement_request extends CI_Model
{

    public function build($filter)
    {
        $where = array();
        if (!empty($filter['id'])) {
            array_push($where, "disaster_disbursement_request.id = '" . $filter['id'] . "'");
        }

        if (!empty($filter['name'])) {
            array_push($where, "disaster_proposal.name LIKE '%" . $filter['name'] . "%'");
        }
        if (!empty($filter['type'])) {
            array_push($where, "disaster_disbursement_request.type = '" . $filter['type'] . "'");
        }
        if (!empty($filter['status'])) {
            array_push($where, "disaster_disbursement_request.status = '" . $filter['status'] . "'");
        }
        return $where;
    }

    public function query($filter = array())
    {
        $query = 'SELECT disaster_disbursement_request.*,
        disaster_proposal.name as disaster_proposal_name,
        master_village.name as master_village_name,
		master_district.name as master_district_name,
        master_disaster_funds.name as master_disaster_funds_name
        FROM disaster_disbursement_request
		LEFT JOIN disaster_proposal on disaster_disbursement_request.disaster_proposal_id = disaster_proposal.id
		LEFT JOIN master_village on disaster_proposal.master_village_id = master_village.id
        LEFT JOIN master_district on disaster_proposal.master_district_id = master_district.id
        LEFT JOIN master_disaster_funds on disaster_disbursement_request.master_disaster_funds_id = master_disaster_funds.id';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

    public function get_all($filter = array())
    {
        $result = $this->db->query($this->query($filter))->result_array();
        return $result;
    }

    public function get_detail($filter = array())
    {
        $result = $this->db->query($this->query($filter))->row_array();
        return $result;
    }

    function add($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    function update($table, $data, $where)
    {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }

    public function delete($table, $id)
    {
        $this->db->where('id', $id);
        $delete = $this->db->delete($table);
        return $delete;
    }
}

/* End of file ModelName.php */
