<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_menus_level3 extends CI_Model {

    public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "menus_level3.id = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['menus_level2_id'] ) ) {
            array_push($where, "menus_level3.menus_level2_id = '" . $filter['menus_level2_id'] . "'");
        }

        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT 
            menus_level3.*,
            menus_level2.name as menus_level2_name,
            menus_level1.name as menus_level1_name
        FROM menus_level3
        INNER JOIN menus_level2 on menus_level3.menus_level2_id = menus_level2.id
        INNER JOIN menus_level1 on menus_level2.menus_level1_id = menus_level1.id';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

    public function get_all( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}
	
	public function get_detail( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->row_array();
		return $result;
    }	

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}

    public function delete( $table, $id )
	{
        $this->db->where('id', $id);
        $delete = $this->db->delete($table);
		return $delete;
	}

}

/* End of file ModelName.php */
