<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_pemakaman extends CI_Model
{
	function get_desa_by_kec($id_kec)
	{
		$this->db->where('id_kec', $id_kec);
		$this->db->order_by('id_kel', 'ASC');
		$query = $this->db->get('tr_kelurahan')->result_array();
		return $query;
	}
	function get_data_pemakaman($kecamatan, $role)
	{
		if ($role != 1) {
			$this->db->join('tr_kelurahan', 'tb_pemakaman.desa_id = tr_kelurahan.id_kel');
			$this->db->join('tr_kecamatan', 'tr_kecamatan.id_kec = tr_kelurahan.id_kec');
			// $this->db->order_by('tb_pemakaman.id_pemakaman','ASC');
			$this->db->where('tr_kecamatan.id_kec', $kecamatan);
			$query = $this->db->get('tb_pemakaman')->result_array();
			return $query;
		} else {
			$this->db->join('tr_kelurahan', 'tb_pemakaman.desa_id = tr_kelurahan.id_kel');
			$this->db->join('tr_kecamatan', 'tr_kecamatan.id_kec = tr_kelurahan.id_kec');
			// $this->db->order_by('tb_pemakaman.id_pemakaman','ASC');
			$query = $this->db->get('tb_pemakaman')->result_array();
			return $query;
		}
	}
	function get_pemakaman_by_id($id)
	{
		$this->db->join('tr_kelurahan', 'tb_pemakaman.desa_id = tr_kelurahan.id_kel');
		$this->db->join('tr_kecamatan', 'tr_kecamatan.id_kec = tr_kelurahan.id_kec');
		$this->db->where('id_pemakaman', $id);
		$query = $this->db->get('tb_pemakaman')->result_array();
		return $query;
	}

	function save_data_pemakaman($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->affected_rows() > 0;
	}
	function update_data_pemakaman($table, $data, $id)
	{
		$this->db->where('id_pemakaman', $id);
		$this->db->update($table, $data);
		return $this->db->affected_rows();
	}

	function delete_pemakaman($id)
	{
		$this->db->where('id_pemakaman', $id);
		return $this->db->delete('tb_pemakaman');
	}
}
