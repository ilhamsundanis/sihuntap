<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_page_document extends CI_Model {

    public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "page_document.id = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['name'] ) ) {
            array_push($where, "page_document.name = '" . $filter['name'] . "'");
        }

        if ( !empty( $filter['description'] ) ) {
            array_push($where, "page_document.description = '" . $filter['description'] . "'");
        }

        if ( !empty( $filter['master_category_document_id'] ) ) {
            array_push($where, "page_document.master_category_document_id = '" . $filter['master_category_document_id'] . "'");
        }

        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT 
            page_document.*,
            master_category_document.name as master_category_document_name,
            users.name as users_name
        FROM page_document
        LEFT JOIN master_category_document on page_document.master_category_document_id = master_category_document.id
        LEFT JOIN users on page_document.created_by = users.id';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

    public function get_all( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}
	
	public function get_detail( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->row_array();
		return $result;
    }	

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}

    public function delete( $table, $id )
	{
        $this->db->where('id', $id);
        $delete = $this->db->delete($table);
		return $delete;
	}

}

/* End of file ModelName.php */
