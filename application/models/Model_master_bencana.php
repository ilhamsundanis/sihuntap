<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_master_bencana extends CI_Model
{

	public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "id = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['nama'] ) ) {
            array_push($where, "nama LIKE '%" . $filter['nama'] . "%'");
        }


        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT *
        FROM tr_jenis_bencana';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

	function get_desa_by_kec($id_kec)
	{
		$this->db->where('id_kec', $id_kec);
		$this->db->order_by('id_kel', 'ASC');
		$query = $this->db->get('tr_kelurahan')->result_array();
		return $query;
	}
	public function get_all( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}
	
	public function get_detail( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->row_array();
		return $result;
    }	

	function add($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->affected_rows() > 0;
	}
	function update($table, $data, $where)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
		return $this->db->affected_rows();
	}

	function delete($table, $id)
	{
		$this->db->where($where);
		return $this->db->delete($table);
	}
}
