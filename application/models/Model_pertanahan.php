<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_pertanahan extends CI_Model
{
	function get_desa_by_kec($id_kec)
	{
		$this->db->where('id_kec', $id_kec);
		$this->db->order_by('id_kel', 'ASC');
		$query = $this->db->get('tr_kelurahan')->result_array();
		return $query;
	}
	function get_data_pertanahan($kecamatan, $role)
	{
		if ($role != 1) {
			$this->db->join('tr_kelurahan', 'tb_pertanahan.desa_id = tr_kelurahan.id_kel');
			$this->db->join('tr_kecamatan', 'tr_kecamatan.id_kec = tr_kelurahan.id_kec');
			// $this->db->order_by('tb_pertanahan.id_pertanahan','ASC');
			$this->db->where('tr_kecamatan.id_kec', $kecamatan);
			$query = $this->db->get('tb_pertanahan')->result_array();
			return $query;
		} else {
			$this->db->join('tr_kelurahan', 'tb_pertanahan.desa_id = tr_kelurahan.id_kel');
			$this->db->join('tr_kecamatan', 'tr_kecamatan.id_kec = tr_kelurahan.id_kec');
			// $this->db->order_by('tb_pertanahan.id_pertanahan','ASC');
			$query = $this->db->get('tb_pertanahan')->result_array();
			return $query;
		}
	}
	function get_pertanahan_by_id($id)
	{
		$this->db->join('tr_kelurahan', 'tb_pertanahan.desa_id = tr_kelurahan.id_kel');
		$this->db->join('tr_kecamatan', 'tr_kecamatan.id_kec = tr_kelurahan.id_kec');
		$this->db->where('id_pertanahan', $id);
		$query = $this->db->get('tb_pertanahan')->result_array();
		return $query;
	}

	function save_data_pertanahan($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->affected_rows() > 0;
	}
	function update_data_pertanahan($table, $data, $id)
	{
		$this->db->where('id_pertanahan', $id);
		$this->db->update($table, $data);
		return $this->db->affected_rows();
	}

	function delete_pertanahan($id)
	{
		$this->db->where('id_pertanahan', $id);
		return $this->db->delete('tb_pertanahan');
	}
}
