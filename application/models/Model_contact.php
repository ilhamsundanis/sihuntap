<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_contact extends CI_Model {

    public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "id = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['full_name'] ) ) {
            array_push($where, "full_name = '" . $filter['full_name'] . "'");
        }

        if ( !empty( $filter['subject'] ) ) {
            array_push($where, "subject = '" . $filter['subject'] . "'");
        }

        if ( !empty( $filter['email'] ) ) {
            array_push($where, "email = '" . $filter['email'] . "'");
        }

        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT *
        FROM contact';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

    public function get_all( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}
	
	public function get_detail( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->row_array();
		return $result;
    }	

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}

    public function delete( $table, $id )
	{
        $this->db->where('id', $id);
        $delete = $this->db->delete($table);
		return $delete;
	}

}

/* End of file ModelName.php */
