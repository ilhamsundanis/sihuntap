<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_page_news extends CI_Model {

    public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "page_news.id = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['title'] ) ) {
            array_push($where, "page_news.title = '" . $filter['title'] . "'");
        }

        if ( !empty( $filter['slug'] ) ) {
            array_push($where, "page_news.slug = '" . $filter['slug'] . "'");
        }

        if ( !empty( $filter['master_category_news_id'] ) ) {
            array_push($where, "page_news.master_category_news_id = '" . $filter['master_category_news_id'] . "'");
        }
        if ( !empty( $filter['created_by'] ) ) {
            array_push($where, "page_news.created_by = '" . $filter['created_by'] . "'");
        }
        

        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT 
            page_news.*,
            master_category_news.name as master_category_news_name,
            users.name as users_name
        FROM page_news
        LEFT JOIN master_category_news on page_news.master_category_news_id = master_category_news.id
        LEFT JOIN users on page_news.created_by = users.id';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

    public function get_all( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}
	
	public function get_detail( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->row_array();
		return $result;
    }	

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}

    public function delete( $table, $id )
	{
        $this->db->where('id', $id);
        $delete = $this->db->delete($table);
		return $delete;
	}

}

/* End of file ModelName.php */
