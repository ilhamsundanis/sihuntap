<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_users extends CI_Model {

    public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "us.id = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['name'] ) ) {
            array_push($where, "us.name LIKE '%" . $filter['name'] . "%'");
        }

        if ( !empty( $filter['email'] ) ) {
            array_push($where, "us.email = '" . $filter['email'] . "'");
        }
        if ( !empty( $filter['password'] ) ) {
            array_push($where, "us.password = '" . $filter['password'] . "'");
        }


        if ( !empty( $filter['master_role_id'] ) ) {
            array_push($where, "us.master_role_id = '" . $filter['master_role_id'] . "'");
        }

        if ( !empty( $filter['master_role_name'] ) ) {
            array_push($where, "master_role.name = '" . $filter['master_role_name'] . "'");
        }

        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT 
            us.*,
            master_role.name as master_role_name,
            (SELECT name FROM users WHERE id=us.created_by) as created_by_name,
            (SELECT name FROM users WHERE id=us.updated_by) as updated_by_name
        FROM users AS us 
        INNER JOIN master_role on us.master_role_id = master_role.id';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

    public function get_all( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}
	
	public function get_detail( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->row_array();
		return $result;
    }	

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}

    public function delete( $table, $id )
	{
        $this->db->where('id', $id);
        $delete = $this->db->delete($table);
		return $delete;
	}

}

/* End of file ModelName.php */
