<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_proposal extends CI_Model {

    public function build( $filter )
    {
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "proposal.id = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['unique_id'] ) ) {
            array_push($where, "proposal.unique_id = '" . $filter['unique_id'] . "'");
        }
        if ( !empty( $filter['damage_id'] ) ) {
            array_push($where, "proposal.master_damage_id = '" . $filter['damage_id'] . "'");
        }

        if ( !empty( $filter['name'] ) ) {
            array_push($where, "proposal.name LIKE '%" . $filter['name'] . "%'");
        }
        if ( !empty( $filter['disaster_id'] ) ) {
            array_push($where, "proposal.master_disaster_id = '" . $filter['disaster_id'] . "'");
        }
        if ( !empty( $filter['master_village_id'] ) ) {
            array_push($where, "proposal.master_village_id = '" . $filter['master_village_id'] . "'");
        }
        if ( !empty( $filter['master_district_id'] ) ) {
            array_push($where, "proposal.master_district_id = '" . $filter['master_district_id'] . "'");
        }
        if ( !empty( $filter['created_by'] ) ) {
            array_push($where, "proposal.created_by = '" . $filter['created_by'] . "'");
        }
        if ( !empty( $filter['status'] ) ) {
            array_push($where, "proposal.status = '" . $filter['status'] . "'");
        }

        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT 
            proposal.*,
            master_disaster.name as master_disaster_name,
            master_damage.name as master_damage_name
        FROM proposal 
        INNER JOIN master_damage on proposal.master_damage_id = master_damage.id
        INNER JOIN master_disaster on proposal.master_disaster_id = master_disaster.id';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

    public function get_all( $filter = array() )
    {
        $result = $this->db->query($this->query($filter))->result_array();
        return $result;
    }
    
    public function get_detail( $filter = array() )
    {
        $result = $this->db->query($this->query($filter))->row_array();
        return $result;
    }   

    function add($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    function update($table, $data, $where)
    {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }

    public function delete( $table, $id )
    {
        $this->db->where('id', $id);
        $delete = $this->db->delete($table);
        return $delete;
    }

}

/* End of file ModelName.php */
