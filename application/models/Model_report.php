<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_report extends CI_Model
{
    public function build( $filter )
    {
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "id_penerima = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['nama_lengkap'] ) ) {
            array_push($where, "nama_lengkap LIKE '%" . $filter['nama_lengkap'] . "%'");
        }

        if ( !empty( $filter['nik'] ) ) {
            array_push($where, "nik = '" . $filter['nik'] . "'");
        }


        return $where;
    }
    public function query( $filter = array() ) {
        $query = 'SELECT 
            tm_penerima.*, 
            tr_kecamatan.nama as nama_kecamatan,
            tr_kelurahan.nama_desa as nama_desa
        FROM tm_penerima
        LEFT JOIN tr_kelurahan on tm_penerima.desa_id = tr_kelurahan.id_kel 
        LEFT JOIN tr_kecamatan on tm_penerima.kec_id = tr_kecamatan.id_kec';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }
    public function get_all( $filter = array() )
    {
        $result = $this->db->query($this->query($filter))->result_array();
        return $result;
    }
    
    public function get_detail( $filter = array() )
    {
        $result = $this->db->query($this->query($filter))->row_array();
        return $result;
    }   

    function get_desa_by_kec($id_kec)
    {
        $this->db->where('id_kec', $id_kec);
        $this->db->order_by('id_kel', 'ASC');
        $query = $this->db->get('tr_kelurahan')->result_array();
        return $query;
    }
    function get_data_huntap($kecamatan, $role)
    {
        if ($role != 1) {
            $this->db->join('tr_kelurahan', 'tm_penerima.desa_id = tr_kelurahan.id_kel');
            $this->db->join('tr_kecamatan', 'tr_kecamatan.id_kec = tr_kelurahan.id_kec');
            // $this->db->order_by('tm_penerima.id_penerima','ASC');
            $this->db->where('tr_kecamatan.id_kec', $kecamatan);
            $query = $this->db->get('tm_penerima')->result_array();
            return $query;
        } else {
            $this->db->join('tr_kelurahan', 'tm_penerima.desa_id = tr_kelurahan.id_kel');
            $this->db->join('tr_kecamatan', 'tr_kecamatan.id_kec = tr_kelurahan.id_kec');
            // $this->db->order_by('tm_penerima.id_penerima','ASC');
            $query = $this->db->get('tm_penerima')->result_array();
            return $query;
        }
    }
    function get_huntap_by_id($id)
    {
        $this->db->join('tr_kelurahan', 'tm_penerima.desa_id = tr_kelurahan.id_kel');
        $this->db->join('tr_kecamatan', 'tr_kecamatan.id_kec = tr_kelurahan.id_kec');
        $this->db->where('id_penerima', $id);
        $query = $this->db->get('tm_penerima')->result_array();
        return $query;
    }

    function save_data_huntap($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->affected_rows() > 0;
    }
    function update_data_huntap($table, $data, $id)
    {
        $this->db->where('id_penerima', $id);
        $this->db->update($table, $data);
        return $this->db->affected_rows();
    }

    function delete_huntap($id)
    {
        $this->db->where('id_penerima', $id);
        return $this->db->delete('tm_penerima');
    }
}
