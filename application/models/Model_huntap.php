<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_huntap extends CI_Model
{

	public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "huntap.id = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['proposal_id'] ) ) {
            array_push($where, "huntap.proposal_id = '" . $filter['proposal_id'] . "'");
        }


        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT 
            huntap.*, 
            master_district.name as master_district_name,
            master_village.name as master_village_name,
            proposal.name as proposal_name,
            proposal.status as status
        FROM huntap
        LEFT JOIN master_village on huntap.master_village_id = master_village.id 
        LEFT JOIN master_district on huntap.master_district_id = master_district.id
        LEFT JOIN proposal on huntap.proposal_id = proposal.id';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

	function get_desa_by_kec($id_kec)
	{
		$this->db->where('id_kec', $id_kec);
		$this->db->order_by('id_kel', 'ASC');
		$query = $this->db->get('tr_kelurahan')->result_array();
		return $query;
	}
	public function get_all( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}
	
	public function get_detail( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->row_array();
		return $result;
    }	

	function add($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->affected_rows() > 0;
	}
	function update($table, $data, $where)
	{
		// $this->db->where($where);
		// $this->db->update($table, $data);
		// if($this->db->affected_rows() > 0){
		// 	return TRUE;
		// }else{
		// 	return FALSE;
		// }
		$this->db->where($where);
		$q = $this->db->get($table);
		$this->db->reset_query();
			
		if ($q->num_rows() > 0 ) 
		{
			$this->db->where($where)->update($table, $data);
			return $this->db->affected_rows() > 0;
		} else {
			$this->db->insert($table, $data);
			return $this->db->affected_rows() > 0;
		}
	}

	function delete($table, $where)
	{
		$this->db->where($where);
		return $this->db->delete($table);
	}
}
