<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_rutilahu extends CI_Model
{
	function get_desa_by_kec($id_kec)
	{
		$this->db->where('id_kec', $id_kec);
		$this->db->order_by('id_kel', 'ASC');
		$query = $this->db->get('tr_kelurahan')->result_array();
		return $query;
	}
	function get_data_rutilahu($kecamatan, $role)
	{
		if ($role != 1) {
			$this->db->join('tr_kelurahan', 'tb_rutilahu.desa_id = tr_kelurahan.id_kel');
			$this->db->join('tr_kecamatan', 'tr_kecamatan.id_kec = tr_kelurahan.id_kec');
			// $this->db->order_by('tb_rutilahu.id_penerima','ASC');
			$this->db->where('tr_kecamatan.id_kec', $kecamatan);
			$query = $this->db->get('tb_rutilahu')->result_array();
			return $query;
		} else {
			$this->db->join('tr_kelurahan', 'tb_rutilahu.desa_id = tr_kelurahan.id_kel');
			$this->db->join('tr_kecamatan', 'tr_kecamatan.id_kec = tr_kelurahan.id_kec');
			// $this->db->order_by('tb_rutilahu.id_penerima','ASC');
			$query = $this->db->get('tb_rutilahu')->result_array();
			return $query;
		}
	}
	function get_rutilahu_by_id($id)
	{
		$this->db->join('tr_kelurahan', 'tb_rutilahu.desa_id = tr_kelurahan.id_kel');
		$this->db->join('tr_kecamatan', 'tr_kecamatan.id_kec = tr_kelurahan.id_kec');
		$this->db->where('id_rutilahu', $id);
		$query = $this->db->get('tb_rutilahu')->result_array();
		return $query;
	}

	function save_data_rutilahu($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->affected_rows() > 0;
	}
	function update_data_rutilahu($table, $data, $id)
	{
		$this->db->where('id_rutilahu', $id);
		$this->db->update($table, $data);
		return $this->db->affected_rows();
	}

	function delete_rutilahu($id)
	{
		$this->db->where('id_rutilahu', $id);
		return $this->db->delete('tb_rutilahu');
	}
}
