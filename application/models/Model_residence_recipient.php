<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_residence_recipient extends CI_Model {

    public function build( $filter )
    {
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "residence_recipient.id = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['unique_id'] ) ) {
            array_push($where, "residence_recipient.unique_id = '" . $filter['unique_id'] . "'");
        }
        if ( !empty( $filter['proposal_id'] ) ) {
            array_push($where, "residence_recipient.proposal_id = '" . $filter['proposal_id'] . "'");
        }
        if ( !empty( $filter['ktp_number'] ) ) {
            array_push($where, "residence_recipient.ktp_number = '" . $filter['ktp_number'] . "'");
        }

        if ( !empty( $filter['name'] ) ) {
            array_push($where, "residence_recipient.name LIKE '%" . $filter['name'] . "%'");
        }
        if ( !empty( $filter['gender'] ) ) {
            array_push($where, "proposal.gender = '" . $filter['gender'] . "'");
        }
        if ( !empty( $filter['master_village_id'] ) ) {
            array_push($where, "residence_recipient.master_village_id = '" . $filter['master_village_id'] . "'");
        }
        if ( !empty( $filter['master_district_id'] ) ) {
            array_push($where, "residence_recipient.master_district_id = '" . $filter['master_district_id'] . "'");
        }
        if ( !empty( $filter['created_by'] ) ) {
            array_push($where, "proposal.created_by = '" . $filter['created_by'] . "'");
        }
        if ( !empty( $filter['status'] ) ) {
            array_push($where, "residence_recipient.status = '" . $filter['status'] . "'");
        }

        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT 
            residence_recipient.*,
            proposal.name as proposal_name,
            master_district.name as master_district_name,
            master_village.name as master_village_name
        FROM residence_recipient 
        INNER JOIN proposal on residence_recipient.proposal_id = proposal.id
        INNER JOIN master_district on residence_recipient.master_district_id = master_district.id
        INNER JOIN master_village on residence_recipient.master_village_id = master_village.id';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

    public function get_all( $filter = array() )
    {
        $result = $this->db->query($this->query($filter))->result_array();
        return $result;
    }
    
    public function get_detail( $filter = array() )
    {
        $result = $this->db->query($this->query($filter))->row_array();
        return $result;
    }   

    function add($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    function update($table, $data, $where)
    {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }

    public function delete( $table, $id )
    {
        $this->db->where('id', $id);
        $delete = $this->db->delete($table);
        return $delete;
    }

}

/* End of file ModelName.php */
