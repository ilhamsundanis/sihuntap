<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_page_faq extends CI_Model {

    public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "page_faq.id = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['question'] ) ) {
            array_push($where, "page_faq.question LIKE '%" . $filter['question'] . "%'");
        }

        if ( !empty( $filter['master_queue_id'] ) ) {
            array_push($where, "page_faq.master_queue_id = '" . $filter['master_queue_id'] . "'");
        }

        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT 
            page_faq.*
        FROM page_faq';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

    public function get_all( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}
	
	public function get_detail( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->row_array();
		return $result;
    }	

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}

    public function delete( $table, $id )
	{
        $this->db->where('id', $id);
        $delete = $this->db->delete($table);
		return $delete;
	}

}

/* End of file ModelName.php */
