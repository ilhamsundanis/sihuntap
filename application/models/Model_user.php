<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_user extends CI_Model
{

	function get_data_user()
	{
		$this->db->join('tr_kecamatan', 'tr_kecamatan.id_kec = tb_user.kec_id');
		$query = $this->db->get('tb_user')->result_array();
		return $query;
	}

	function check_username($username)
	{
		$this->db->where('username', $username);
		return $this->db->get('tb_user');
	}

	function save_data_user($table, $data)
	{
		$this->db->set('id_user', 'UUID()', FALSE);
		$this->db->insert($table, $data);
		return $this->db->affected_rows() > 0;
	}

	function get_user_by_id($id)
	{
		$this->db->join('tr_kecamatan', 'tr_kecamatan.id_kec = tb_user.kec_id');
		$this->db->where('id_user', $id);
		$query = $this->db->get('tb_user')->result_array();
		return $query;
	}

	function update_data_user($table, $data, $id)
	{
		$this->db->where('id_user', $id);
		$this->db->update($table, $data);
		return $this->db->affected_rows();
	}

	function delete_user($id)
	{
		$this->db->where('id_user', $id);
		return $this->db->delete('tb_user');
	}
}
