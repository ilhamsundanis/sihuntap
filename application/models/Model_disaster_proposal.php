<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_disaster_proposal extends CI_Model
{

	public function build($filter)
	{
		$where = array();
		if (!empty($filter['id'])) {
			array_push($where, "disaster_proposal.id = '" . $filter['id'] . "'");
		}

		if (!empty($filter['name'])) {
			array_push($where, "disaster_proposal.name LIKE '%" . $filter['name'] . "%'");
		}

		if (!empty($filter['submission'])) {
			array_push($where, "disaster_proposal.submission = '" . $filter['submission'] . "'");
		}
		if (!empty($filter['status'])) {
			array_push($where, "disaster_proposal.status = '" . $filter['status'] . "'");
		}
		if (!empty($filter['master_village_id'])) {
			array_push($where, "disaster_proposal.master_village_id = '" . $filter['master_village_id'] . "'");
		}
		if (!empty($filter['master_district_id'])) {
			array_push($where, "disaster_proposal.master_district_id = '" . $filter['master_district_id'] . "'");
		}
		if (!empty($filter['master_damage_id'])) {
			array_push($where, "disaster_proposal.master_damage_id = '" . $filter['master_damage_id'] . "'");
		}
		if (!empty($filter['master_disaster_id'])) {
			array_push($where, "disaster_proposal.master_disaster_id = '" . $filter['master_disaster_id'] . "'");
		}
		if (!empty($filter['status'])) {
			array_push($where, "disaster_proposal.status = '" . $filter['status'] . "'");
		}


		return $where;
	}

	public function query($filter = array())
	{
		$query = 'SELECT disaster_proposal.*,
		master_district.name as master_district_name,
        master_village.name as master_village_name,
		master_disaster.name as master_disaster_name,
        (SELECT name FROM users WHERE users.id = disaster_proposal.usid_notes) as usid_notes_name,
        -- master_help.ammount as master_help_ammount,
        master_damage.name as master_damage_name
        FROM disaster_proposal
		LEFT JOIN master_disaster on disaster_proposal.master_disaster_id = master_disaster.id 
        LEFT JOIN master_damage on disaster_proposal.master_damage_id = master_damage.id
        -- LEFT JOIN master_help on disaster_proposal.master_help_id = master_help.id
		LEFT JOIN master_village on disaster_proposal.master_village_id = master_village.id 
        LEFT JOIN master_district on disaster_proposal.master_district_id = master_district.id';
		$query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
		return $query;
	}

	function get_desa_by_kec($id_kec)
	{
		$this->db->where('id_kec', $id_kec);
		$this->db->order_by('id_kel', 'ASC');
		$query = $this->db->get('tr_kelurahan')->result_array();
		return $query;
	}
	public function get_all($filter = array())
	{
		$result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}

	public function get_detail($filter = array())
	{
		$result = $this->db->query($this->query($filter))->row_array();
		return $result;
	}

	function add($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->affected_rows() > 0;
	}
	function update($table, $data, $where)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
		return $this->db->affected_rows();
	}

	function delete($table, $id)
	{
		$this->db->where('id', $id);
		$delete = $this->db->delete($table);
		return $delete;
	}
}
