<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_master_village extends CI_Model {

    public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "master_village.id = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['master_district_id'] ) ) {
            array_push($where, "master_village.master_district_id = '" . $filter['master_district_id'] . "'");
        }
        if ( !empty( $filter['master_city_id'] ) ) {
            array_push($where, "master_district.master_city_id = '" . $filter['master_city_id'] . "'");
        }

        if ( !empty( $filter['name'] ) ) {
            array_push($where, "master_village.name LIKE '%" . $filter['name'] . "%'");
        }

        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT 
            master_village.*,
            master_village.name as master_village_name,
            master_district.name as master_district_name 
        FROM master_village 
        INNER JOIN master_district on master_village.master_district_id = master_district.id';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

    public function get_all( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}
	
	public function get_detail( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->row_array();
		return $result;
    }	

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}

    public function delete( $table, $id )
	{
        $this->db->where('id', $id);
        $delete = $this->db->delete($table);
		return $delete;
	}

}

/* End of file ModelName.php */
