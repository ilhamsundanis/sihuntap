<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_usulan extends CI_Model
{

	public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "tm_penerima.id_penerima = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['nama_lengkap'] ) ) {
            array_push($where, "tm_penerima.nama_lengkap LIKE '%" . $filter['nama_lengkap'] . "%'");
        }
        if ( !empty( $filter['permohonan_id'] ) ) {
            array_push($where, "tm_penerima.permohonan_id = '" . $filter['permohonan_id'] . "'");
        }

        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT 
            tm_penerima.*, 
            tr_kecamatan.nama as nama_kecamatan,
            tr_kelurahan.nama_desa as nama_desa,
            tb_permohonan.nama_permohonan as nama_permohonan
        FROM tm_penerima
        LEFT JOIN tr_kelurahan on tm_penerima.desa_id = tr_kelurahan.id_kel 
        LEFT JOIN tr_kecamatan on tm_penerima.kec_id = tr_kecamatan.id_kec
        LEFT JOIN tb_permohonan on tm_penerima.permohonan_id = tb_permohonan.id_permohonan';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

	function get_desa_by_kec($id_kec)
	{
		$this->db->where('id_kec', $id_kec);
		$this->db->order_by('id_kel', 'ASC');
		$query = $this->db->get('tr_kelurahan')->result_array();
		return $query;
	}
	public function get_all( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}
	
	public function get_detail( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->row_array();
		return $result;
    }	

	function add($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->affected_rows() > 0;
	}
	function update($table, $data, $where)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
		return $this->db->affected_rows();
	}

	function delete($table, $where)
	{
		$this->db->where($where);
		return $this->db->delete($table);
	}
}
