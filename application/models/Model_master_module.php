<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_master_module extends CI_Model {

    public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "master_module.id = '" . $filter['id'] . "'");
        }

        if ( !empty( $filter['master_module_group_id'] ) ) {
            array_push($where, "master_module.master_module_group_id = '" . $filter['master_module_group_id'] . "'");
        }

        if ( !empty( $filter['name'] ) ) {
            array_push($where, "master_module.name LIKE '%" . $filter['name'] . "%'");
        }

        if ( !empty( $filter['module'] ) ) {
            array_push($where, "master_module.module LIKE '%" . $filter['module'] . "%'");
        }

        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT 
            master_module.*,
            master_module_group.name as master_module_group_name
        FROM master_module
        INNER JOIN master_module_group on master_module.master_module_group_id = master_module_group.id';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

    public function get_all( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}
	
	public function get_detail( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->row_array();
		return $result;
    }	

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}

    public function delete( $table, $id )
	{
        $this->db->where('id', $id);
        $delete = $this->db->delete($table);
		return $delete;
	}

}

/* End of file ModelName.php */
