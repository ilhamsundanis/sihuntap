<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_master_role extends CI_Model {

    public function build( $filter )
	{
        $where = array();
        if ( !empty( $filter['id'] ) ) {
            array_push($where, "id = '" . $filter['id'] . "'");
        }

        return $where;
    }    

    public function query( $filter = array() ) {
        $query = 'SELECT * FROM master_role';
        $query .= RawQuery($query, $this->build($filter), @$filter['page'], @$filter['limit'], @$filter['orderBy'], @$filter['sort']);
        return $query;
    }

    public function get_all( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->result_array();
		return $result;
	}
	
	public function get_detail( $filter = array() )
	{
        $result = $this->db->query($this->query($filter))->row_array();
		return $result;
    }	

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}

    public function delete( $table, $id )
	{
        $this->db->where('id', $id);
        $delete = $this->db->delete($table);
		return $delete;
	}

    public function get_group_module() {
        $result = $this->db->query("SELECT * FROM master_module_group ORDER BY sort ASC")->result_array();
        foreach ($result as $idx => $val) {
            $result[$idx]["modules"] = $this->db->query("SELECT * FROM master_module WHERE master_module_group_id = '".$val['id']."' ORDER BY sort ASC")->result_array();
        }

        return $result;
    }

    public function get_group_module_by_master_role_id( $id ) {
        $result = $this->db->query("SELECT * FROM master_module_group ORDER BY sort ASC")->result_array();
        foreach ($result as $idx => $val) {
            $result[$idx]["modules"] = $this->db->query("SELECT * FROM master_module WHERE master_module_group_id = '".$val['id']."' ORDER BY sort ASC")->result_array();
            foreach ($result[$idx]["modules"] as $subidx => $subvalue) {
                $masterRoleAccess = $this->db->query("SELECT * FROM master_role_access WHERE master_role_id = '".$id."' AND master_module_id = '".$subvalue['id']."'")->row_array();
                
                $result[$idx]["modules"][$subidx] = $subvalue;
                $result[$idx]["modules"][$subidx]["access_read"]   = empty($masterRoleAccess["access_read"]) || ($masterRoleAccess["access_read"] == false) ? false : true;
                $result[$idx]["modules"][$subidx]["access_add"]    = empty($masterRoleAccess["access_add"]) || ($masterRoleAccess["access_add"] == false) ? false : true;
                $result[$idx]["modules"][$subidx]["access_edit"]   = empty($masterRoleAccess["access_edit"]) || ($masterRoleAccess["access_edit"] == false) ? false : true;
                $result[$idx]["modules"][$subidx]["access_delete"] = empty($masterRoleAccess["access_delete"]) || ($masterRoleAccess["access_delete"] == false) ? false : true;
                $result[$idx]["modules"][$subidx]["access_import"] = empty($masterRoleAccess["access_import"]) || ($masterRoleAccess["access_import"] == false) ? false : true;
            }
        }

        return $result;
    }

}

/* End of file ModelName.php */
