<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $asset = [
            "title" => "PORTAL INFORMASI HUNIAN TETAP"
        ];
        $data = array(
            "id" => $this->uri->segment(3)
        );
        $this->load->view('fe/template/header', $asset);
        $this->load->view('fe/page/index',$data);
        $this->load->view('fe/template/footer');
    }

    public function document()
    {
        $asset = [
            "title" => "Download Dokumen"
        ];
        $data = array(
            "id" => $this->uri->segment(3)
        );
        $this->load->view('fe/template/header', $asset);
        $this->load->view('fe/page/document');
        $this->load->view('fe/template/footer');
    }
}
