<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gallery extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = [
            "title" => "Galeri Kegiatan"
        ];
        $this->load->view('fe/template/header', $data);
        $this->load->view('fe/gallery');
        $this->load->view('fe/template/footer');
    }
}
