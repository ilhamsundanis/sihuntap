<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kontak extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = [
            "title" => "Kontak Kami"
        ];
        $this->load->view('fe/template/header', $data);
        $this->load->view('fe/contact');
        $this->load->view('fe/template/footer');
    }
}
