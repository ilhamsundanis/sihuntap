<?php
    require APPPATH . 'libraries/REST_Controller.php';

    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Homepage extends REST_Controller {

        var $table           = '';
        var $model_page_news = 'Model_page_news';
        var $model_master_category_news = 'Model_master_category_news';
        var $model_residence_recipient = 'Model_residence_recipient';
        

        var $model_menus_level1 = 'Model_menus_level1';
        var $model_menus_level2 = 'Model_menus_level2';
        var $model_menus_level3 = 'Model_menus_level3';

        public function __construct()
        {
            parent::__construct();
            // check_login();
            $this->load->model('Model_page_news');
            $this->load->model('Model_master_category_news');
            $this->load->model('Model_residence_recipient');
            $this->load->model('Model_menus_level1');
            $this->load->model('Model_menus_level2');
            $this->load->model('Model_menus_level3');
        }

        public function index_get() 
        {
            // GET homepage
            $resp                           = Response();
            $result                         = array();

            $result['news']            = $this->{$this->model_page_news}->get_all(array("page" => 1, "limit" => 6, "orderBy" => "id", "sort" => "desc"));
            $result['total_penerima_kk']   = count($this->{$this->model_residence_recipient}->get_all());
            $result['total_huntap'] = count($this->{$this->model_residence_recipient}->get_all(array("status" => 4)));
            $result['total_huntara'] = count($this->{$this->model_residence_recipient}->get_all(array("statsu" => 3)));
            $result['total_sudah_verifikasi'] = count($this->{$this->model_residence_recipient}->get_all(array("status" => 2)));
            $result['total_usulan'] = count($this->{$this->model_residence_recipient}->get_all(array("status" => 0)));
            // $result['total_company']   = count($this->{$this->model_company}->get_all());
            // $result['total_job_application']   = count($this->{$this->model_company_job_application}->get_all());
            // $result['banner']          = $this->{$this->model_page_banner}->get_all(array("page" => 1, "orderBy" => "id", "sort" => "desc"));
            
            $resp['success']           = true;
            $resp['code']              = 200;
            $resp['message']           = "Success";
            $resp['data']              = $result;
            $this->response($resp, REST_Controller::HTTP_OK);
        }
    
        public function news_get() 
        {
            // GET auth
            $resp   = Response();
            $result = array();
            $listCategory = $this->{$this->model_master_category_news}->get_all();
            foreach ( $listCategory as $idx => $val ) {
                $result['category'][$idx]['id']    = $val['id'];
                $result['category'][$idx]['name']  = $val['name'];
                $result['category'][$idx]['total'] = count($this->{$this->model_page_news}->get_all(array("master_category_news_id" => $val['id'])));
            }

            $result['recent_posts'] = $this->{$this->model_page_news}->get_all(array("page" => 1, "limit" => 8, "orderBy" => "id", "sort" => "DESC"));

            $resp['success']           = true;
            $resp['code']              = 200;
            $resp['message']           = "Success";
            $resp['data']              = $result;
            $this->response($resp, REST_Controller::HTTP_OK);
        }

        public function menus_get() 
        {
            // GET auth
            $resp   = Response();
            $result = array();
            $list_menus_level1 = $this->{$this->model_menus_level1}->get_all();
            foreach ( $list_menus_level1 as $idx => $val ) {
                $result[$idx]['id']           = $val['id'];
                $result[$idx]['name']         = $val['name'];
                $result[$idx]['url']          = $val['url'];
                $result[$idx]['menus_level2'] = [];

                $list_menus_level2 = $this->{$this->model_menus_level2}->get_all(array("menus_level1_id" => $val["id"]));
                if ( !empty($list_menus_level2)) {
                    foreach ( $list_menus_level2 as $idx2 => $val2 ) {
                        $result[$idx]['menus_level2'][$idx2]["id"]           = $val2["id"];
                        $result[$idx]['menus_level2'][$idx2]["name"]         = $val2["name"];
                        $result[$idx]['menus_level2'][$idx2]["url"]          = $val2["url"];
                        $result[$idx]['menus_level2'][$idx2]["menus_level3"] = [];

                        $list_menus_level3 = $this->{$this->model_menus_level3}->get_all(array("menus_level2_id" => $val2["id"]));
                        if ( !empty($list_menus_level3)) {
                            foreach ( $list_menus_level3 as $idx3 => $val3 ) {
                                $result[$idx]['menus_level2'][$idx2]["menus_level3"][$idx3]["id"]   = $val3["id"];
                                $result[$idx]['menus_level2'][$idx2]["menus_level3"][$idx3]["name"] = $val3["name"];
                                $result[$idx]['menus_level2'][$idx2]["menus_level3"][$idx3]["url"]  = $val3["url"];
                            }
                        }
                    }
                }
            }

            $resp['success']           = true;
            $resp['code']              = 200;
            $resp['message']           = "Success";
            $resp['data']              = $result;
            $this->response($resp, REST_Controller::HTTP_OK);
        }

        public function visitor_post() 
        {
            // POST visitor
            $resp = Response();
            $post = json_decode(file_get_contents('php://input'), TRUE);

            if ($this->agent->is_browser()) {
                $agent = $this->agent->browser().' '.$this->agent->version();
            } elseif ($this->agent->is_robot()) {
                $agent = $this->agent->robot();
            } elseif ($this->agent->is_mobile()) {
                $agent = $this->agent->mobile();
            } else {
                $agent = 'Unidentified User Agent';
            }

            $visitor = $this->{$this->modelVisitor}->get_detail(array("ip" => $post['ip'], "date" => date("Y-m-d")));
            if ( !$visitor ) {
                $params = array(
                    "ip"         => $post['ip'],
                    "user_agent" => $agent . " " . $this->agent->platform(),
                    "date"       => date("Y-m-d"),
                );
                $this->{$this->modelVisitor}->add("webvisitor", $params);
            }

            $result = array();
            // $result['total_visitor']     = count($this->{$this->modelVisitor}->get_all(array()));
            // $result['yesterday_visitor'] = count($this->{$this->modelVisitor}->get_all(array("date" => date("Y-m-d",strtotime("-1 days")))));
            // $result['today_visitor']     = count($this->{$this->modelVisitor}->get_all(array("date" => date("Y-m-d"))));
            // $result['month_visitor']     = count($this->{$this->modelVisitor}->get_all(array("month" => date("m"), "year" => date("Y"))));

            $resp['success']           = true;
            $resp['code']              = 200;
            $resp['message']           = "Success";
            $resp['data']              = $result;
            $this->response($resp, REST_Controller::HTTP_OK);
        }

    }
    
    /* End of file Test.php */
    
?>