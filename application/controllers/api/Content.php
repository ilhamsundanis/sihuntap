<?php
    require APPPATH . 'libraries/REST_Controller.php';

    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Content extends REST_Controller {

        var $table           = '';
        var $model_page_document = 'Model_page_document';
        var $model_master_category_document = 'Model_master_category_document';

        var $model_page_faq = 'Model_page_faq';
        var $model_residence_recipient = 'Model_residence_recipient';
        var $model_master_district = 'Model_master_district';
        var $model_master_village = 'Model_master_village';

        public function __construct()
        {
            parent::__construct();
            // check_login();
            $this->load->model('Model_page_document');
            $this->load->model('Model_master_category_document');
            $this->load->model('Model_page_faq');
            $this->load->model('Model_contact');
            $this->load->model('Model_master_district');
            $this->load->model('Model_residence_recipient');
            $this->load->model('Model_master_village');
        }

        public function index_get() 
        {
            // GET Content
            $resp                           = Response();
            $result                         = array();

            $result['document']            = $this->{$this->model_page_document}->get_all(array("page" => 1, "limit" => 6, "orderBy" => "id", "sort" => "desc"));
            $result['total_vacancy']   = count($this->{$this->model_company_job}->get_all());
            

            $resp['success']           = true;
            $resp['code']              = 200;
            $resp['message']           = "Success";
            $resp['data']              = $result;
            $this->response($resp, REST_Controller::HTTP_OK);
        }
    
        public function document_get() 
        {
            // GET auth
            $resp   = Response();
            $result = array();
            $listCategory = $this->{$this->model_master_category_document}->get_all();
            foreach ( $listCategory as $idx => $val ) {
                $result['category'][$idx]['id']    = $val['id'];
                $result['category'][$idx]['name']  = $val['name'];
                $result['category'][$idx]['total'] = count($this->{$this->model_page_document}->get_all(array("master_category_document_id" => $val['id'])));
            }

            $result['recent_posts'] = $this->{$this->model_page_document}->get_all(array("page" => 1, "limit" => 8, "orderBy" => "id", "sort" => "DESC"));

            $resp['success']           = true;
            $resp['code']              = 200;
            $resp['message']           = "Success";
            $resp['data']              = $result;
            $this->response($resp, REST_Controller::HTTP_OK);
        }

        public function faq_get() 
        {
            // GET auth
            $resp   = Response();
            $result = array();
            $result = $this->{$this->model_page_faq}->get_all();

            // $result['recent_posts'] = $this->{$this->model_page_faq}->get_all(array("page" => 1, "limit" => 8, "orderBy" => "id", "sort" => "DESC"));

            $resp['success']           = true;
            $resp['code']              = 200;
            $resp['message']           = "Success";
            $resp['data']              = $result;
            $this->response($resp, REST_Controller::HTTP_OK);
        }

        public function statistic_get($id = 0)
        {
            $resp   = Response();
            $result = array();
            
            $total = 0;
            if(empty($id)){
                $ListDistrict = $this->{$this->model_master_district}->get_all(array("master_city_id" => 3201));
                foreach ( $ListDistrict as $idx => $val ) {
                    $ListResidenceRecipient = count($this->{$this->model_residence_recipient}->get_all(array("master_district_id" => $val["id"])));
                    $ListDistrict[$idx]['total_recipient']    = $ListResidenceRecipient;
                    $total += $ListResidenceRecipient;
                }
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Success";
                $resp['data']              = $ListDistrict;
                $resp['total']             = $total;
                $this->response($resp, REST_Controller::HTTP_OK);
            }else{
                $ListVillage = $this->{$this->model_master_village}->get_all(array("master_district_id" => $id));
                foreach ( $ListVillage as $idx => $val ) {
                    $ListResidenceRecipient = count($this->{$this->model_residence_recipient}->get_all(array("master_village_id" => $val["id"])));
                    $ListVillage[$idx]['total_recipient']    = $ListResidenceRecipient;
                    $total += $ListResidenceRecipient;
                }
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Success";
                $resp['data']              = $ListVillage;
                $resp['total']             = $total;
                $this->response($resp, REST_Controller::HTTP_OK);
            }
        }

        public function maps_get($id = 0)
        {
            $resp   = Response();
            $result = array();
            
            $total = 0;
            if(empty($id)){
                $ListDistrict = $this->{$this->model_master_district}->get_all(array("master_city_id" => 3201));
                foreach ( $ListDistrict as $idx => $val ) {
                    $ListResidenceRecipient = count($this->{$this->model_residence_recipient}->get_all(array("master_district_id" => $val["id"])));
                    $ListDistrict[$idx]['total_recipient']    = $ListResidenceRecipient;
                    $total += $ListResidenceRecipient;
                }
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Success";
                $resp['data']              = $ListDistrict;
                $resp['total']             = $total;
                $this->response($resp, REST_Controller::HTTP_OK);
            }else{
                $ListVillage = $this->{$this->model_master_village}->get_all(array("master_district_id" => $id));
                foreach ( $ListVillage as $idx => $val ) {
                    $ListResidenceRecipient = count($this->{$this->model_residence_recipient}->get_all(array("master_village_id" => $val["id"])));
                    $ListVillage[$idx]['total_recipient']    = $ListResidenceRecipient;
                    $total += $ListResidenceRecipient;
                }
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Success";
                $resp['data']              = $ListVillage;
                $resp['total']             = $total;
                $this->response($resp, REST_Controller::HTTP_OK);
            }
        }
        public function contact_post()
        {
            // POST
            $resp         = Response();
            $post         = json_decode(file_get_contents('php://input'), TRUE);
            // $post["slug"] = url_title($post["title"], 'dash', true);
            if ($this->agent->is_browser()) {
                $agent = $this->agent->browser().' '.$this->agent->version();
            } elseif ($this->agent->is_robot()) {
                $agent = $this->agent->robot();
            } elseif ($this->agent->is_mobile()) {
                $agent = $this->agent->mobile();
            } else {
                $agent = 'Unidentified User Agent';
            }
            $data = array(
                'category_contact' => $post['category_contact'],
                'subject'           => ucwords($post['subject']),
                'full_name'         => ucwords($post['full_name']),
                'email'             => strtolower($post['email']),
                'phone_number'      => $post['phone_number'],
                'message'           => ucwords($post['message']),
                'user_agent' => $agent . " " . $this->agent->platform(),
                'ip'        => $post['ip']

            );
            $message = "Terimakasih, ".strtoupper($post['full_name'])." Sudah Menghubungi Kami dengan mengirimkan pertanyaan <br /> ".$post['message'].". Segera Kami Akan Balas Pertanyaan Anda, Terimakasih";
            $email = array(
                "to" 	  => strtolower($post["email"]),
                "subject" => "Auto Reply - Bogor Career Center",
                "message" => $message,
            );
            BackgroundProcessAPI("POST", "notification/email", $email);

            $save = $this->{$this->model_contact}->add('contact', $data);
            if ( $save ) {
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Pesan Anda Berhasil dikirim, balasan pesan Anda akan dikirim melalui Email yang Anda tulis.";
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "Pesan gagal dikirim";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            }
            
        }


    }
    
    /* End of file Test.php */
    
?>