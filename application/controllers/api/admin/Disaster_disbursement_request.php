<?php
require_once APPPATH . "third_party/PHPExcel.php";
require APPPATH . 'libraries/REST_Controller.php';

defined('BASEPATH') or exit('No direct script access allowed');

class Disaster_disbursement_request extends REST_Controller
{

    var $table = 'disaster_disbursement_request';
    var $model = 'Model_disaster_disbursement_request';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_disaster_disbursement_request');
    }

    public function index_get($id = 0)
    {
        // GET
        if (empty($id)) {
            $resp         = Response(true);
            $post         = $this->input->get();
            $filter       = array();
            if (!empty($post['id'])) {
                $filter['id'] = $post['id'];
            }

            if (!empty($post['type'])) {
                $filter['type'] = $post['type'];
            }

            if (!empty($post['name'])) {
                $filter['name'] = $post['name'];
            }
            if (!empty($post['status'])) {
                $filter['status'] = $post['status'];
            }

            if (!empty($post['created_by'])) {
                $filter['created_by'] = $post['created_by'];
            }

            $totalRow = $this->{$this->model}->get_all($filter);

            // pagination
            if (!empty($post['page'])) {
                $filter['page'] = $post['page'];
            }

            if (!empty($post['limit'])) {
                $filter['limit'] = $post['limit'];
            }

            if (!empty($post['orderBy'])) {
                $filter['orderBy'] = $post['orderBy'];
            }

            if (!empty($post['sort'])) {
                $filter['sort'] = $post['sort'];
            }

            $filter['offset'] = ($resp['meta']['page'] - 1) * $resp['meta']['limit'];
            $list = $this->{$this->model}->get_all($filter);

            $resp['success']           = true;
            $resp['code']              = 200;
            $resp['message']           = "Success";
            $resp['data']              = $list;

            $resp['meta']['page']      = @$filter['page'];
            $resp['meta']['limit']     = @$filter['limit'];
            $resp['meta']['totalData'] = count($totalRow);
            $resp['meta']['totalPage'] = ceil(count($totalRow) / (empty($filter['limit']) ? 1 : $filter['limit']));
            $resp['meta']['orderBy']   = @$filter['orderBy'];
            $resp['meta']['sort']      = @$filter['sort'];

            $this->response($resp, REST_Controller::HTTP_OK);
        } else {
            // GET /:id
            $resp         = Response();
            $filter       = array(
                'id' => $id,
            );
            $data = $this->{$this->model}->get_detail($filter);
            if (!$data) {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "Data tidak ditemukan";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            } else {
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Success";
                $resp['data']              = $data;
                $this->response($resp, REST_Controller::HTTP_OK);
            }
        }
    }

    public function index_post($id = 0)
    {
        // POST faq
        $resp = Response();

        // validate token
        $token = $this->input->get_request_header("Authorization");
        $jwt            = new JWT();
        $jwt_secret_key = secret_key();
        $validate_jwt   = validate_token($token);
        if ($validate_jwt["status"] == 401) {
            $resp['success']           = false;
            $resp['code']              = $validate_jwt["status"];
            $resp['message']           = $validate_jwt["message"];
            return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $post = $this->input->post();
        if (empty($id)) {
            $check = $this->{$this->model}->get_detail(array("disaster_proposal_id" => $post["disaster_proposal_id"]));

            if ($check) {
                $resp['success']           = true;
                $resp['code']              = 400;
                $resp['message']           = "Permohonan Sudah Dibuat Untuk Proposal Ini!";
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {

                if (!empty($_FILES['ba_verif_file']['name'])) {
                    $post['ba_verif_file'] = UploadDocumentByName($_FILES['ba_verif_file'], "rehabilitasi/ba", "BAVERIF-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
                } else {
                    unset($post['ba_verif_file']);
                }
                if (!empty($_FILES['integrity_file']['name'])) {
                    $post['integrity_file'] = UploadDocumentByName($_FILES['integrity_file'], "rehabilitasi/integritas", "PI-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
                } else {
                    unset($post['integrity_file']);
                }
                if (!empty($_FILES['contract_file']['name'])) {
                    $post['contract_file'] = UploadDocumentByName($_FILES['contract_file'], "rehabilitasi/contract", "kontrak-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
                } else {
                    unset($post['contract_file']);
                }
                if (!empty($_FILES['spk_file']['name'])) {
                    $post['spk_file'] = UploadDocumentByName($_FILES['spk_file'], "rehabilitasi/spk", "spk-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
                } else {
                    unset($post['spk_file']);
                }
                if (!empty($_FILES['spmk_file']['name'])) {
                    $post['spmk_file'] = UploadDocumentByName($_FILES['spmk_file'], "rehabilitasi/spmk", "spmk-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
                } else {
                    unset($post['spmk_file']);
                }
                if (!empty($_FILES['skmp_file']['name'])) {
                    $post['skmp_file'] = UploadDocumentByName($_FILES['skmp_file'], "rehabilitasi/skmp", "skmp-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
                } else {
                    unset($post['skmp_file']);
                }
                if (!empty($_FILES['spj_file']['name'])) {
                    $post['spj_file'] = UploadDocumentByName($_FILES['spj_file'], "rehabilitasi/spj", "spj-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
                } else {
                    unset($post['spj_file']);
                }
                if (!empty($_FILES['sppbtt_file']['name'])) {
                    $post['sppbtt_file'] = UploadDocumentByName($_FILES['sppbtt_file'], "rehabilitasi/sppbtt", "sppbtt-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
                } else {
                    unset($post['sppbtt_file']);
                }
                if (!empty($_FILES['document_validity_file']['name'])) {
                    $post['document_validity_file'] = UploadDocumentByName($_FILES['document_validity_file'], "rehabilitasi/validity", "vldty-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
                } else {
                    unset($post['document_validity_file']);
                }
                if (!empty($_FILES['ownership_land_file']['name'])) {
                    $post['ownership_land_file'] = UploadDocumentByName($_FILES['ownership_land_file'], "rehabilitasi/kemepilikan", "hakmilik-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
                } else {
                    unset($post['ownership_land_file']);
                }
                if (!empty($_FILES['not_rutilahu_file']['name'])) {
                    $post['not_rutilahu_file'] = UploadDocumentByName($_FILES['not_rutilahu_file'], "rehabilitasi/norutilahu", "norutilahu-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
                } else {
                    unset($post['not_rutilahu_file']);
                }
                if (!empty($_FILES['account_book_file']['name'])) {
                    $post['account_book_file'] = UploadDocumentByName($_FILES['account_book_file'], "rehabilitasi/rekening", "rekening-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
                } else {
                    unset($post['account_book_file']);
                }
                if (!empty($_FILES['acceptance_receipt_file']['name'])) {
                    $post['acceptance_receipt_file'] = UploadDocumentByName($_FILES['acceptance_receipt_file'], "rehabilitasi/kwtpenerimaan", "KWP-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
                } else {
                    unset($post['acceptance_receipt_file']);
                }
                if (!empty($_FILES['ba_unexpected_file']['name'])) {
                    $post['ba_unexpected_file'] = UploadDocumentByName($_FILES['ba_unexpected_file'], "rehabilitasi/batakterduga", "BATT-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
                } else {
                    unset($post['ba_unexpected_file']);
                }
                $save = $this->{$this->model}->add($this->table, $post);

                if ($save) {
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = "Data berhasil disimpan";
                    $this->response($resp, REST_Controller::HTTP_OK);
                } else {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data gagal disimpan";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        } else {
            $filter = array(
                "id" => $id,
            );
            if (!empty($_FILES['ba_verif_file']['name'])) {
                $post['ba_verif_file'] = UploadDocumentByName($_FILES['ba_verif_file'], "rehabilitasi/ba", "BAVERIF-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
            } else {
                unset($post['ba_verif_file']);
            }
            if (!empty($_FILES['integrity_file']['name'])) {
                $post['integrity_file'] = UploadDocumentByName($_FILES['integrity_file'], "rehabilitasi/integritas", "PI-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
            } else {
                unset($post['integrity_file']);
            }
            if (!empty($_FILES['contract_file']['name'])) {
                $post['contract_file'] = UploadDocumentByName($_FILES['contract_file'], "rehabilitasi/contract", "kontrak-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
            } else {
                unset($post['contract_file']);
            }
            if (!empty($_FILES['spk_file']['name'])) {
                $post['spk_file'] = UploadDocumentByName($_FILES['spk_file'], "rehabilitasi/spk", "spk-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
            } else {
                unset($post['spk_file']);
            }
            if (!empty($_FILES['spmk_file']['name'])) {
                $post['spmk_file'] = UploadDocumentByName($_FILES['spmk_file'], "rehabilitasi/spmk", "spmk-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
            } else {
                unset($post['spmk_file']);
            }
            if (!empty($_FILES['skmp_file']['name'])) {
                $post['skmp_file'] = UploadDocumentByName($_FILES['skmp_file'], "rehabilitasi/skmp", "skmp-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
            } else {
                unset($post['skmp_file']);
            }
            if (!empty($_FILES['spj_file']['name'])) {
                $post['spj_file'] = UploadDocumentByName($_FILES['spj_file'], "rehabilitasi/spj", "spj-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
            } else {
                unset($post['spj_file']);
            }
            if (!empty($_FILES['sppbtt_file']['name'])) {
                $post['sppbtt_file'] = UploadDocumentByName($_FILES['sppbtt_file'], "rehabilitasi/sppbtt", "sppbtt-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
            } else {
                unset($post['sppbtt_file']);
            }
            if (!empty($_FILES['document_validity_file']['name'])) {
                $post['document_validity_file'] = UploadDocumentByName($_FILES['document_validity_file'], "rehabilitasi/validity", "vldty-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
            } else {
                unset($post['document_validity_file']);
            }
            if (!empty($_FILES['ownership_land_file']['name'])) {
                $post['ownership_land_file'] = UploadDocumentByName($_FILES['ownership_land_file'], "rehabilitasi/kemepilikan", "hakmilik-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
            } else {
                unset($post['ownership_land_file']);
            }
            if (!empty($_FILES['not_rutilahu_file']['name'])) {
                $post['not_rutilahu_file'] = UploadDocumentByName($_FILES['not_rutilahu_file'], "rehabilitasi/norutilahu", "norutilahu-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
            } else {
                unset($post['not_rutilahu_file']);
            }
            if (!empty($_FILES['account_book_file']['name'])) {
                $post['account_book_file'] = UploadDocumentByName($_FILES['account_book_file'], "rehabilitasi/rekening", "rekening-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
            } else {
                unset($post['account_book_file']);
            }
            if (!empty($_FILES['acceptance_receipt_file']['name'])) {
                $post['acceptance_receipt_file'] = UploadDocumentByName($_FILES['acceptance_receipt_file'], "rehabilitasi/kwtpenerimaan", "KWP-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
            } else {
                unset($post['acceptance_receipt_file']);
            }
            if (!empty($_FILES['ba_unexpected_file']['name'])) {
                $post['ba_unexpected_file'] = UploadDocumentByName($_FILES['ba_unexpected_file'], "rehabilitasi/batakterduga", "BATT-" . $post["type"] . "_" . $post["disaster_proposal_id"] . "_" . date('YmdHis'));
            } else {
                unset($post['ba_unexpected_file']);
            }

            $save   = $this->{$this->model}->update($this->table, $post, $filter);

            if ($save) {
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Data berhasil diubah";
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "Data gagal diubah";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function index_put($id = 0)
    {
        // PUT faq
        $resp = Response();

        // validate token
        $token = $this->input->get_request_header("Authorization");
        $jwt            = new JWT();
        $jwt_secret_key = secret_key();
        $validate_jwt   = validate_token($token);
        if ($validate_jwt["status"] == 401) {
            $resp['success']           = false;
            $resp['code']              = $validate_jwt["status"];
            $resp['message']           = $validate_jwt["message"];
            return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
        }

        if (empty($id)) {
            $resp['success']           = false;
            $resp['code']              = 400;
            $resp['message']           = "ID tidak ditemukan";
            $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $post   = json_decode(file_get_contents('php://input'), TRUE);
            // $post = $this->input->post();
            $filter = array(
                "id" => $id,
            );
            $save   = $this->{$this->model}->update($this->table, $post, $filter);
            if ($save) {
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Data berhasil dirubah";
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "Data gagal dirubah";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function index_delete($id = 0)
    {
        // DELETE faq
        $resp = Response();

        // validate token
        $token = $this->input->get_request_header("Authorization");
        $jwt            = new JWT();
        $jwt_secret_key = secret_key();
        $validate_jwt   = validate_token($token);
        if ($validate_jwt["status"] == 401) {
            $resp['success']           = false;
            $resp['code']              = $validate_jwt["status"];
            $resp['message']           = $validate_jwt["message"];
            return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
        }

        if (empty($id)) {
            $resp['success']           = false;
            $resp['code']              = 400;
            $resp['message']           = "ID tidak ditemukan";
            $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $save   = $this->{$this->model}->delete($this->table, $id);
            if ($save) {
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Data berhasil dihapus";
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "Data gagal dihapus";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function import_post()
    {
        $resp = Response();

        // validate token
        $token = $this->input->get_request_header("Authorization");
        $jwt            = new JWT();
        $jwt_secret_key = secret_key();
        $validate_jwt   = validate_token($token);
        if ($validate_jwt["status"] == 401) {
            $resp['success']           = false;
            $resp['code']              = $validate_jwt["status"];
            $resp['message']           = $validate_jwt["message"];
            return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $post = $this->convert_import($_FILES["file"]);
        if (empty($post["data"])) {
            $resp['success']           = false;
            $resp['code']              = 400;
            $resp['message']           = $post["message"];
            $this->response($resp, REST_Controller::HTTP_OK);
        } else {
            foreach ($post["data"] as $val) {
                $val["name"] = strtoupper($val["name"]);
                $save = $this->{$this->model}->add($this->table, $val);
            }
            if ($save) {
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = $post["message"];
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                $resp['success']           = false;
                $resp['code']              = 200;
                $resp['message']           = "Data gagal disimpan";
                $this->response($resp, REST_Controller::HTTP_OK);
            }
        }
    }

    public function convert_import($file)
    {
        $result = array();
        $path = $file["tmp_name"];

        $objPHPExcel = PHPExcel_IOFactory::load($path, $encode = 'utf-8');
        $sheet = $objPHPExcel->getSheet(0); //Activate first sheet table
        $highestRow = $sheet->getHighestRow(); //Get total number
        $highestColumn = $sheet->getHighestColumn(); //Get total columns
        $tempData = array();
        for ($i = 3; $i <= $highestRow; $i++) {

            $name               = $objPHPExcel->getActiveSheet()->getCell("B" . $i)->getValue();
            $ktp_number         = $objPHPExcel->getActiveSheet()->getCell("C" . $i)->getValue();
            $kk_number              = $objPHPExcel->getActiveSheet()->getCell("D" . $i)->getValue();
            $gender             = $objPHPExcel->getActiveSheet()->getCell("E" . $i)->getValue();
            $address            = $objPHPExcel->getActiveSheet()->getCell("F" . $i)->getValue();
            $rt                 = $objPHPExcel->getActiveSheet()->getCell("G" . $i)->getValue();
            $rw                 = $objPHPExcel->getActiveSheet()->getCell("H" . $i)->getValue();
            $master_disaster_id = $objPHPExcel->getActiveSheet()->getCell("I" . $i)->getValue();
            $master_damage_id   = $objPHPExcel->getActiveSheet()->getCell("J" . $i)->getValue();
            $year               = $objPHPExcel->getActiveSheet()->getCell("K" . $i)->getValue();
            $non_category       = $objPHPExcel->getActiveSheet()->getCell("L" . $i)->getValue();
            $threatened         = $objPHPExcel->getActiveSheet()->getCell("M" . $i)->getValue();

            $master_district_id = $objPHPExcel->getActiveSheet()->getCell("Q" . $i)->getValue();
            $master_village_id  = $objPHPExcel->getActiveSheet()->getCell("R" . $i)->getValue();


            array_push($tempData, array(
                "name"                  => $name,
                "ktp_number"            => $ktp_number,
                "kk_number"                 => $kk_number,
                "gender"                => $gender,
                "address"               => $address, //M
                "rt"                    => $rt,
                "rw"                    => $rw,
                "master_disaster_id"    => $master_disaster_id,
                "master_damage_id"      => $master_damage_id,
                "year"                  => $year,
                "non_category"          => $non_category,
                "threatened"            => $threatened,
                "master_village_id"     => $master_village_id,
                "master_district_id"    => $master_district_id
            ));
        }

        if (empty($tempData)) {
            $result["message"] = "Data yang anda impor kosong.";
            $result["data"]    = array();
            return $result;
        } else {
            $result["message"] = "Data yang anda impor berhasil";
            $result["data"]    = $tempData;
        }

        return $result;
    }

    public function verification_post($id = 0)
    {
        // POST faq
        $resp = Response();

        // validate token
        $token = $this->input->get_request_header("Authorization");
        $jwt            = new JWT();
        $jwt_secret_key = secret_key();
        $validate_jwt   = validate_token($token);
        if ($validate_jwt["status"] == 401) {
            $resp['success']           = false;
            $resp['code']              = $validate_jwt["status"];
            $resp['message']           = $validate_jwt["message"];
            return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $post = $this->input->post();
        if (empty($id)) {
            $resp['success']           = true;
            $resp['code']              = 400;
            $resp['message']           = "ID Tidak Ditemukan";
            $this->response($resp, REST_Controller::HTTP_OK);
        } else {
            $filter = array(
                "id" => $id,
            );
            $save   = $this->{$this->model}->update($this->table, $post, $filter);

            if ($save) {
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Proposal Berhasil di Verifikasi";
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "Proposal Gagal di Verifikasi";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }
}
    
    /* End of file Test.php */
