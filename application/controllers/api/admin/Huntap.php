<?php
    require APPPATH . 'libraries/REST_Controller.php';

    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Huntap extends REST_Controller {

        var $table = 'huntap';
        var $model = 'Model_huntap';
        var $model_proposal = 'Model_proposal';
        var $model_residence_recipient = 'Model_residence_recipient';
        public function __construct()
        {
            parent::__construct();
            $this->load->model('Model_huntap');
            $this->load->model('Model_proposal');
            $this->load->model('Model_residence_recipient');
        }
    
        public function index_get($id = 0) 
        {
            // GET
            if ( empty( $id ) ) {
                $resp         = Response(true);
                $post         = $this->input->get();
                $filter       = array();
                if ( !empty( $post['id'] ) ) {
                    $filter['id'] = $post['id'];
                }

                if ( !empty( $post['proposal_id'] ) ) {
                    $filter['proposal_id'] = $post['proposal_id'];
                }

                
                if ( !empty( $post['master_village_id'] ) ) {
                    $filter['master_village_id'] = $post['master_village_id'];
                }
                if ( !empty( $post['master_district_id'] ) ) {
                    $filter['master_district_id'] = $post['master_district_id'];
                }
                

                $totalRow = $this->{$this->model}->get_all($filter);

                // pagination
                if ( !empty( $post['page'] ) ) {
                    $filter['page'] = $post['page'];
                }

                if ( !empty( $post['limit'] ) ) {
                    $filter['limit'] = $post['limit'];
                }

                if ( !empty( $post['orderBy'] ) ) {
                    $filter['orderBy'] = $post['orderBy'];
                }

                if ( !empty( $post['sort'] ) ) {
                    $filter['sort'] = $post['sort'];
                }

                $filter['offset'] = ($resp['meta']['page'] - 1) * $resp['meta']['limit'];
                $list = $this->{$this->model}->get_all($filter);
                foreach($list as $idx => $val){
                        $list[$idx]['count_recipient'] = count($this->{$this->model_residence_recipient}->get_all(array("proposal_id" => $val['proposal_id'], "status" => 4)));
                }
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Success";
                $resp['data']              = $list;

                $resp['meta']['page']      = @$filter['page']; 
                $resp['meta']['limit']     = @$filter['limit']; 
                $resp['meta']['totalData'] = count($totalRow); 
                $resp['meta']['totalPage'] = ceil(count($totalRow) / (empty($filter['limit']) ? 1 : $filter['limit'])); 
                $resp['meta']['orderBy']   = @$filter['orderBy'];
                $resp['meta']['sort']      = @$filter['sort'];
                
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                // GET /:id
                $resp         = Response();
                $filter       = array(
                    'id' => $id, 
                );
                $data = $this->{$this->model}->get_detail($filter);
                if ( !$data ) {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data tidak ditemukan";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                } else {
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = "Success";
                    $resp['data']              = $data;
                    $this->response($resp, REST_Controller::HTTP_OK);
                }
            }
        }

        public function index_post($id = 0) 
        {
            // POST faq
            $resp = Response();

            // validate token
            $token = $this->input->get_request_header("Authorization");
            $jwt            = new JWT();
            $jwt_secret_key = secret_key();
            $validate_jwt   = validate_token($token);
            if ( $validate_jwt["status"] == 401 ) {
                $resp['success']           = false;
                $resp['code']              = $validate_jwt["status"];
                $resp['message']           = $validate_jwt["message"];
                return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
            }
            $post = $this->input->post();
            if ( !empty($_FILES['file']['name']) ) {
                $post['file'] = UploadDocumentByName($_FILES['file'], "huntap", 'huntap-'.$post["proposal_id"]);
            } else {
                unset($post['file']);
            }
            // $post = json_decode(file_get_contents('php://input'), TRUE);
            
            if(empty($id)){
                $save = $this->{$this->model}->add($this->table, $post);
                // $post["unique_id"] = hash('sha256', $post['name'] . "@" . date("Ymdhis"));
                $get_proposal = $this->{$this->model_proposal}->get_detail(array("proposal_id" => $post["proposal_id"]));
                $upProposal = array(
                    "status" => 4
                );
                $filter_proposal = array(
                    "id" => $post['proposal_id']
                );
                $filter_recipient = array(
                    "proposal_id" => $post["proposal_id"]
                );
                
                if ( $save ) {
                    $update_proposal = $this->{$this->model_proposal}->update('proposal',$upProposal,$filter_proposal);
                    // $update_recipient = $this->{$this->model_residence_recipient}->update('residence_recipient',$upProposal,$filter_recipient);
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = "Data berhasil disimpan";
                    $this->response($resp, REST_Controller::HTTP_OK);
                } else {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data gagal disimpan";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                }
            }else{
                $filter = array(
                    "id" => $id,
                );
                $save   = $this->{$this->model}->update($this->table, $post, $filter);
                if ( $save ) {
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = "Data berhasil dirubah";
                    $this->response($resp, REST_Controller::HTTP_OK);
                } else {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data gagal dirubah";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }

        public function index_put($id = 0) 
        {
            // PUT faq
            $resp = Response();

            // validate token
            $token = $this->input->get_request_header("Authorization");
            $jwt            = new JWT();
            $jwt_secret_key = secret_key();
            $validate_jwt   = validate_token($token);
            if ( $validate_jwt["status"] == 401 ) {
                $resp['success']           = false;
                $resp['code']              = $validate_jwt["status"];
                $resp['message']           = $validate_jwt["message"];
                return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
            }

            if ( empty( $id ) ) {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "ID tidak ditemukan";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            } else {
                $post   = json_decode(file_get_contents('php://input'), TRUE);
                $filter = array(
                    "id" => $id,
                );
                $save   = $this->{$this->model}->update($this->table, $post, $filter);
                if ( $save ) {
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = "Data berhasil dirubah";
                    $this->response($resp, REST_Controller::HTTP_OK);
                } else {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data gagal dirubah";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }

        public function index_delete($id = 0) 
        {
            // DELETE faq
            $resp = Response();

            // validate token
            $token = $this->input->get_request_header("Authorization");
            $jwt            = new JWT();
            $jwt_secret_key = secret_key();
            $validate_jwt   = validate_token($token);
            if ( $validate_jwt["status"] == 401 ) {
                $resp['success']           = false;
                $resp['code']              = $validate_jwt["status"];
                $resp['message']           = $validate_jwt["message"];
                return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
            }
            
            if ( empty( $id ) ) {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "ID tidak ditemukan";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            } else {
                $save   = $this->{$this->model}->delete($this->table, $id);
                if ( $save ) {
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = "Data berhasil dihapus";
                    $this->response($resp, REST_Controller::HTTP_OK);
                } else {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data gagal dihapus";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }

    }
    
    /* End of file Test.php */
    
?>