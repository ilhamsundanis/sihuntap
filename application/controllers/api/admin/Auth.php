<?php
    require APPPATH . 'libraries/REST_Controller.php';
    require APPPATH . 'libraries/Format.php';

    defined('BASEPATH') OR exit('No direct script access allowed');
    class Auth extends REST_Controller {

        var $table                 = 'users';
        var $model                 = 'Model_auth';
        var $modelMasterRoleAccess = 'Model_master_role_access';
        public function __construct()
        {
            parent::__construct();
            $this->load->model('Model_auth');
            $this->load->model('Model_master_role_access');
        }
        public function index_get() 
        {
            // GET auth
            $resp            = Response();
            $get             = $this->input->get();
            $get['password'] = md5($get['password']);
            $response = array();
            $auth     = $this->{$this->model}->get_detail($get);
            if ( $auth ) {
                // generate token
                $jwt            = new JWT();
                $jwt_secret_key = secret_key();
                $payload        = array(
                    "adm"        => true,
                    "aud"        => "sihuntap",
                    "authorised" => true,
                    "did"        => "sihuntap",
                    "dli"        => "WEB",
                    "email"      => $auth["email"],
                    "exp"        => strtotime("+120 minutes"),
                    "iat"        => time(),
                    "iss"        => "sihuntap.bogorkab.go.id",
                    "jti"        => guidv4($auth["email"]),
                    "memberType" => "administrator",
                    "sub"        => $auth["id"]
                );
                $auth["token"]   = $jwt->encode($payload, $jwt_secret_key);
                $auth["menus"]   = $this->{$this->model}->get_menus($auth["master_role_id"]);
                $auth["modules"] = $this->{$this->modelMasterRoleAccess}->get_all(array("master_role_id" => $auth["master_role_id"]));
            }
            if ( !$auth ) {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "Email/Password salah";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            } else {
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Success";
                $resp['data']              = $auth;
                $this->response($resp, REST_Controller::HTTP_OK);
            }
        }
    }
