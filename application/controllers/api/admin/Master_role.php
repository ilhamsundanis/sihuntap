<?php
    require APPPATH . 'libraries/REST_Controller.php';

    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Master_role extends REST_Controller {

        var $table                 = 'master_role';
        var $model                 = 'Model_master_role';
        var $modelMasterRoleAccess = 'Model_master_role_access';

        public function __construct()
        {
            parent::__construct();
            $this->load->model('Model_master_role');
            $this->load->model('Model_master_role_access');
        }
    
        public function index_get($id = 0) 
        {
            // GET
            if ( empty( $id ) ) {
                $resp         = Response(true);
                $post         = $this->input->get();
                $filter       = array();
                if ( !empty( $post['id'] ) ) {
                    $filter['id'] = $post['id'];
                }

                $totalRow = $this->{$this->model}->get_all($filter);

                // pagination
                if ( !empty( $post['page'] ) ) {
                    $filter['page'] = $post['page'];
                }

                if ( !empty( $post['limit'] ) ) {
                    $filter['limit'] = $post['limit'];
                }

                if ( !empty( $post['orderBy'] ) ) {
                    $filter['orderBy'] = $post['orderBy'];
                }

                if ( !empty( $post['sort'] ) ) {
                    $filter['sort'] = $post['sort'];
                }

                $filter['offset'] = ($resp['meta']['page'] - 1) * $resp['meta']['limit'];
                $list = $this->{$this->model}->get_all($filter);
                foreach ( $list as $index => $val ) {
                    $filterRoleAccess = array(
                        "master_role_id" => $val["id"]
                    );
                    $list[$index]["modules"] = $this->{$this->modelMasterRoleAccess}->get_all($filterRoleAccess);
                }

                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Success";
                $resp['data']              = $list;

                $resp['meta']['page']      = @$filter['page']; 
                $resp['meta']['limit']     = @$filter['limit']; 
                $resp['meta']['totalData'] = count($totalRow); 
                $resp['meta']['totalPage'] = ceil(count($totalRow) / (empty($filter['limit']) ? 1 : $filter['limit'])); 
                $resp['meta']['orderBy']   = @$filter['orderBy'];
                $resp['meta']['sort']      = @$filter['sort'];
                
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                // GET /:id
                $resp         = Response();
                $filter       = array(
                    'id' => $id, 
                );
                $data = $this->{$this->model}->get_detail($filter);

                $filterRoleAccess = array(
                    "master_role_id" => $data["id"]
                );
                $data["modules"] = $this->{$this->modelMasterRoleAccess}->get_all($filterRoleAccess);
                if ( !$data ) {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data tidak ditemukan";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                } else {
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = "Success";
                    $resp['data']              = $data;
                    $this->response($resp, REST_Controller::HTTP_OK);
                }
            }
        }

        public function index_post() 
        {
            // POST
            $resp      = Response();

            // validate token
            $token = $this->input->get_request_header("Authorization");
            $jwt            = new JWT();
            $jwt_secret_key = secret_key();
            $validate_jwt   = validate_token($token);
            if ( $validate_jwt["status"] == 401 ) {
                $resp['success']           = false;
                $resp['code']              = $validate_jwt["status"];
                $resp['message']           = $validate_jwt["message"];
                return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
            }

            $post      = json_decode(file_get_contents('php://input'), TRUE);

            $params    = $post;
            unset($params["modules"]);

            $save      = $this->{$this->model}->add($this->table, $params);
            $insert_id = $this->db->insert_id();
            foreach ( $post["modules"] as $val ) {
                $val["master_role_id"] = $insert_id;

                $filter = array(
                    "master_role_id"   => $insert_id,
                    "master_module_id" => $val['master_module_id'],
                );
                $check = $this->{$this->modelMasterRoleAccess}->get_detail($filter);
                if ( $check ) {
                    $this->{$this->model}->update("master_role_access", $val, $filter);
                } else {
                    $this->{$this->model}->add("master_role_access", $val);
                }
            }

            if ( $save ) {
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Data berhasil disimpan";
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "Data gagal disimpan";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            }
        }

        public function index_put($id = 0) 
        {
            // PUT
            $resp = Response();

            // validate token
            $token = $this->input->get_request_header("Authorization");
            $jwt            = new JWT();
            $jwt_secret_key = secret_key();
            $validate_jwt   = validate_token($token);
            if ( $validate_jwt["status"] == 401 ) {
                $resp['success']           = false;
                $resp['code']              = $validate_jwt["status"];
                $resp['message']           = $validate_jwt["message"];
                return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
            }

            if ( empty( $id ) ) {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "ID tidak ditemukan";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            } else {
                $post   = json_decode(file_get_contents('php://input'), TRUE);
                $params = $post;
                unset($params["modules"]);
                
                $filter = array(
                    "id" => $id,
                );
                $save   = $this->{$this->model}->update($this->table, $params, $filter);
                foreach ( $post["modules"] as $val ) {
                    $val["master_role_id"] = $id;
    
                    $filter = array(
                        "master_role_id"   => $id,
                        "master_module_id" => $val['master_module_id'],
                    );
                    $check = $this->{$this->modelMasterRoleAccess}->get_detail($filter);
                    if ( $check ) {
                        $this->{$this->model}->update("master_role_access", $val, $filter);
                    } else {
                        $this->{$this->model}->add("master_role_access", $val);
                    }
                }

                if ( $save ) {
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = "Data berhasil dirubah";
                    $this->response($resp, REST_Controller::HTTP_OK);
                } else {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data gagal dirubah";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }

        public function index_delete($id = 0) 
        {
            // DELETE
            $resp = Response();

            // validate token
            $token = $this->input->get_request_header("Authorization");
            $jwt            = new JWT();
            $jwt_secret_key = secret_key();
            $validate_jwt   = validate_token($token);
            if ( $validate_jwt["status"] == 401 ) {
                $resp['success']           = false;
                $resp['code']              = $validate_jwt["status"];
                $resp['message']           = $validate_jwt["message"];
                return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
            }
            
            if ( empty( $id ) ) {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "ID tidak ditemukan";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            } else {
                $save   = $this->{$this->model}->delete($this->table, $id);
                if ( $save ) {
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = "Data berhasil dihapus";
                    $this->response($resp, REST_Controller::HTTP_OK);
                } else {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data gagal dihapus";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }

        public function group_module_get($id = 0) 
        {
            $resp = Response(true);
            if ( empty( $id ) ) {
                $list = $this->{$this->model}->get_group_module();
            } else {
                $list = $this->{$this->model}->get_group_module_by_master_role_id($id);
            }

            $resp['success']           = true;
            $resp['code']              = 200;
            $resp['message']           = "Success";
            $resp['data']              = $list;
            
            $this->response($resp, REST_Controller::HTTP_OK);
        }

    }
    
    /* End of file Test.php */
    
?>