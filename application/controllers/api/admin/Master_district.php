<?php
    require_once APPPATH . "third_party/PHPExcel.php";
    require APPPATH . 'libraries/REST_Controller.php';

    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Master_district extends REST_Controller {

        var $table = 'master_district';
        var $model = 'Model_master_district';

        public function __construct()
        {
            parent::__construct();
            $this->load->model('Model_master_district');
        }
    
        public function index_get($id = 0) 
        {
            // GET
            if ( empty( $id ) ) {
                $resp         = Response(true);
                $post         = $this->input->get();
                $filter       = array();
                if ( !empty( $post['id'] ) ) {
                    $filter['id'] = $post['id'];
                }

                if ( !empty( $post['master_city_id'] ) ) {
                    $filter['master_city_id'] = $post['master_city_id'];
                }

                if ( !empty( $post['name'] ) ) {
                    $filter['name'] = $post['name'];
                }

                $totalRow = $this->{$this->model}->get_all($filter);

                // pagination
                if ( !empty( $post['page'] ) ) {
                    $filter['page'] = $post['page'];
                }

                if ( !empty( $post['limit'] ) ) {
                    $filter['limit'] = $post['limit'];
                }

                if ( !empty( $post['orderBy'] ) ) {
                    $filter['orderBy'] = $post['orderBy'];
                }

                if ( !empty( $post['sort'] ) ) {
                    $filter['sort'] = $post['sort'];
                }

                $filter['offset'] = ($resp['meta']['page'] - 1) * $resp['meta']['limit'];
                $list = $this->{$this->model}->get_all($filter);

                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Success";
                $resp['data']              = $list;

                $resp['meta']['page']      = @$filter['page']; 
                $resp['meta']['limit']     = @$filter['limit']; 
                $resp['meta']['totalData'] = count($totalRow); 
                $resp['meta']['totalPage'] = ceil(count($totalRow) / (empty($filter['limit']) ? 1 : $filter['limit'])); 
                $resp['meta']['orderBy']   = @$filter['orderBy'];
                $resp['meta']['sort']      = @$filter['sort'];
                
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                // GET /:id
                $resp         = Response();
                $filter       = array(
                    'id' => $id, 
                );
                $data = $this->{$this->model}->get_detail($filter);
                if ( !$data ) {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data tidak ditemukan";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                } else {
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = "Success";
                    $resp['data']              = $data;
                    $this->response($resp, REST_Controller::HTTP_OK);
                }
            }
        }

        public function index_post() 
        {
            // POST faq
            $resp = Response();

            // validate token
            $token = $this->input->get_request_header("Authorization");
            $jwt            = new JWT();
            $jwt_secret_key = secret_key();
            $validate_jwt   = validate_token($token);
            if ( $validate_jwt["status"] == 401 ) {
                $resp['success']           = false;
                $resp['code']              = $validate_jwt["status"];
                $resp['message']           = $validate_jwt["message"];
                return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
            }

            $post = json_decode(file_get_contents('php://input'), TRUE);
            foreach ( $post as $val ) {
                $save = $this->{$this->model}->add($this->table, $val);
            }
            if ( $save ) {
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Data berhasil disimpan";
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "Data gagal disimpan";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            }
        }

        public function index_put($id = 0) 
        {
            // PUT faq
            $resp = Response();

            // validate token
            $token = $this->input->get_request_header("Authorization");
            $jwt            = new JWT();
            $jwt_secret_key = secret_key();
            $validate_jwt   = validate_token($token);
            if ( $validate_jwt["status"] == 401 ) {
                $resp['success']           = false;
                $resp['code']              = $validate_jwt["status"];
                $resp['message']           = $validate_jwt["message"];
                return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
            }

            if ( empty( $id ) ) {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "ID tidak ditemukan";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            } else {
                $post   = json_decode(file_get_contents('php://input'), TRUE);
                $filter = array(
                    "id" => $id,
                );
                $save   = $this->{$this->model}->update($this->table, $post, $filter);
                if ( $save ) {
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = "Data berhasil dirubah";
                    $this->response($resp, REST_Controller::HTTP_OK);
                } else {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data gagal dirubah";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }

        public function index_delete($id = 0) 
        {
            // DELETE faq
            $resp = Response();

            // validate token
            $token = $this->input->get_request_header("Authorization");
            $jwt            = new JWT();
            $jwt_secret_key = secret_key();
            $validate_jwt   = validate_token($token);
            if ( $validate_jwt["status"] == 401 ) {
                $resp['success']           = false;
                $resp['code']              = $validate_jwt["status"];
                $resp['message']           = $validate_jwt["message"];
                return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
            }

            if ( empty( $id ) ) {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "ID tidak ditemukan";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            } else {
                $save   = $this->{$this->model}->delete($this->table, $id);
                if ( $save ) {
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = "Data berhasil dihapus";
                    $this->response($resp, REST_Controller::HTTP_OK);
                } else {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data gagal dihapus";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }

        public function import_post() 
        {
            $resp = Response();

            // validate token
            $token = $this->input->get_request_header("Authorization");
            $jwt            = new JWT();
            $jwt_secret_key = secret_key();
            $validate_jwt   = validate_token($token);
            if ( $validate_jwt["status"] == 401 ) {
                $resp['success']           = false;
                $resp['code']              = $validate_jwt["status"];
                $resp['message']           = $validate_jwt["message"];
                return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
            }
            
            $post = $this->convert_import($_FILES["file"]);
            if ( empty( $post["data"] ) ) {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = $post["message"];
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            } else {
                foreach ( $post["data"] as $val ) {
                    $save = $this->{$this->model}->add($this->table, $val);
                }
                if ( $save ) {
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = $post["message"];
                    $this->response($resp, REST_Controller::HTTP_OK);
                } else {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data gagal disimpan";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }

        public function convert_import($file) 
        {
            $result = array();
            $path = $file["tmp_name"];

            $objPHPExcel = PHPExcel_IOFactory::load($path, $encode = 'utf-8');
            $sheet =$objPHPExcel->getSheet(0);//Activate first sheet table
            $highestRow = $sheet->getHighestRow();//Get total number
            $highestColumn =$sheet->getHighestColumn(); //Get total columns

            //Verify that the header data is aligned
            $J1 = $objPHPExcel->getActiveSheet()->getCell("J1")->getValue();
            if($J1 != 'BCC' ){
                $result["message"] = "File ditolak!, Silahkan menggunakan file yang kami sediakan.";
                $result["data"]    = array();
                return $result;
            }

            $tempData = array();
            for ( $i=3; $i <= $highestRow; $i++ ) {
                $master_province_id = $objPHPExcel->getActiveSheet()->getCell("B" .$i)->getValue();
                $master_city_id     = $objPHPExcel->getActiveSheet()->getCell("C" .$i)->getValue();
                $name               = $objPHPExcel->getActiveSheet()->getCell("D" .$i)->getValue();
                if ( !empty( $name ) && !empty( $master_province_id ) && !empty( $master_city_id ) ) {
                    array_push($tempData, array(
                        "master_province_id" => $master_province_id,
                        "master_city_id"     => $master_city_id,
                        "name"               => $name,
                    ));
                }
            }

            if ( empty( $tempData ) ) {
                $result["message"] = "Data yang anda impor kosong.";
                $result["data"]    = array();
                return $result;
            } else {
                $result["message"] = "Data yang anda impor berhasil";
                $result["data"]    = $tempData;
            }

            return $result;
        }

    }
    
    /* End of file Test.php */
    
?>