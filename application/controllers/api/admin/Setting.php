<?php
    require APPPATH . 'libraries/REST_Controller.php';

    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Setting extends REST_Controller {

        var $table = 'settings';
        var $model = 'Model_setting';

        public function __construct()
        {
            parent::__construct();
            $this->load->model('Model_setting');
        }
    
        public function index_get($id = 1, $token = "") 
        {
            // GET
            if ( empty( $id ) ) {
                $resp         = Response(true);
                $post         = $this->input->get();
                if(!empty($post['token'])){
                    $token        = $post['token'];
                }else{
                    $token = "";
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data tidak ditemukan";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                }
                $jwt            = new JWT();
                $jwt_secret_key = secret_key();
                $validate_jwt   = validate_token($token);
                if ( $validate_jwt["status"] == 401 ) {
                    $resp['success']           = false;
                    $resp['code']              = $validate_jwt["status"];
                    $resp['message']           = $validate_jwt["message"];
                    return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
                }

                $filter       = array();
                
               
                $list = $this->{$this->model}->get_all($filter);

                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Success";
                $resp['data']              = $list;
                
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                // GET /:id
                $resp         = Response();
                if(!empty($token)){
                    $token        = $token;
                }else{
                    $token = "";
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data tidak ditemukan";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                }
                $jwt            = new JWT();
                $jwt_secret_key = secret_key();
                $validate_jwt   = validate_token($token);
                if ( $validate_jwt["status"] == 401 ) {
                    $resp['success']           = false;
                    $resp['code']              = $validate_jwt["status"];
                    $resp['message']           = $validate_jwt["message"];
                    return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
                }
                $filter       = array(
                    'id' => $id, 
                );
                $data = $this->{$this->model}->get_detail($filter);
                if ( !$data ) {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data tidak ditemukan";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                } else {
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = "Success";
                    $resp['data']              = $data;
                    $this->response($resp, REST_Controller::HTTP_OK);
                }
            }
        }

        public function index_post($id = 0) 
        {
            // POST
            $resp         = Response();

            // validate token
            $token = $this->input->get_request_header("Authorization");
            $jwt            = new JWT();
            $jwt_secret_key = secret_key();
            $validate_jwt   = validate_token($token);
            if ( $validate_jwt["status"] == 401 ) {
                $resp['success']           = false;
                $resp['code']              = $validate_jwt["status"];
                $resp['message']           = $validate_jwt["message"];
                return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
            }

            $post         = $this->input->post();

            if ( !empty($_FILES['logo']['name']) ) {
                $post['logo'] = UploadImageByName($_FILES['logo'], "logo", 'Logo-Perumahan-'.date('YmdHis'));
            } else {
                unset($post['logo']);
            }
            if ( !empty($_FILES['logo_fav']['name']) ) {
                $post['logo_fav'] = UploadImageByName($_FILES['logo_fav'], "logo", 'Logo-Perumahan-'.date('YmdHis'));
            } else {
                unset($post['logo_fav']);
            }

            if ( empty( $id ) ) {
                $filter       = array(
                    'id' => $post["id"], 
                );
                $check = $this->{$this->model}->get_detail($filter);
                if ( $check < 0) {
                    
                    $save = $this->{$this->model}->add($this->table, $post);
                    if ( $save ) {
                        $resp['success']           = true;
                        $resp['code']              = 200;
                        $resp['message']           = "Data berhasil disimpan";
                        $this->response($resp, REST_Controller::HTTP_OK);
                    } else {
                        $resp['success']           = false;
                        $resp['code']              = 400;
                        $resp['message']           = "Data gagal disimpan";
                        $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                    }
                }
            } else {
                $filter = array(
                    "id" => $id,
                );
                $save   = $this->{$this->model}->update($this->table, $post, $filter);
                if ( $save ) {
                    $resp['success']           = true;
                    $resp['code']              = 200;
                    $resp['message']           = "Data berhasil dirubah";
                    $this->response($resp, REST_Controller::HTTP_OK);
                } else {
                    $resp['success']           = false;
                    $resp['code']              = 400;
                    $resp['message']           = "Data gagal dirubah";
                    $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }

    }
    
    /* End of file Test.php */
