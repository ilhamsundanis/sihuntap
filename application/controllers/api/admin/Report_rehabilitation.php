<?php
require_once APPPATH . "third_party/PHPExcel.php";
require APPPATH . 'libraries/REST_Controller.php';

defined('BASEPATH') or exit('No direct script access allowed');

class Report_rehabilitation extends REST_Controller
{

    var $table = 'disaster_victims';
    var $model = 'Model_disaster_victims';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_disaster_victims');
    }

    public function index_get($id = 0)
    {
        // GET
        if (empty($id)) {
            $resp         = Response(true);
            $post         = $this->input->get();
            $filter       = array();
            if (!empty($post['id'])) {
                $filter['id'] = $post['id'];
            }

            if (!empty($post['master_damage_id'])) {
                $filter['master_damage_id'] = $post['master_damage_id'];
            }

            if (!empty($post['name'])) {
                $filter['name'] = $post['name'];
            }

            if (!empty($post['master_disaster_id'])) {
                $filter['master_disaster_id'] = $post['master_disaster_id'];
            }
            if (!empty($post['status'])) {
                $filter['status'] = $post['status'];
            }
            if (!empty($post['ktp_number'])) {
                $filter['ktp_number'] = $post['ktp_number'];
            }
            if (!empty($post['master_village_id'])) {
                $filter['master_village_id'] = $post['master_village_id'];
            }
            if (!empty($post['master_district_id'])) {
                $filter['master_district_id'] = $post['master_district_id'];
            }
            if (!empty($post['submission'])) {
                $filter['submission'] = $post['submission'];
            }
            if (!empty($post['created_by'])) {
                $filter['created_by'] = $post['created_by'];
            }

            $totalRow = $this->{$this->model}->get_all($filter);

            // pagination
            if (!empty($post['page'])) {
                $filter['page'] = $post['page'];
            }

            if (!empty($post['limit'])) {
                $filter['limit'] = $post['limit'];
            }

            if (!empty($post['orderBy'])) {
                $filter['orderBy'] = $post['orderBy'];
            }

            if (!empty($post['sort'])) {
                $filter['sort'] = $post['sort'];
            }

            $filter['offset'] = ($resp['meta']['page'] - 1) * $resp['meta']['limit'];
            $list = $this->{$this->model}->get_all($filter);

            $resp['success']           = true;
            $resp['code']              = 200;
            $resp['message']           = "Success";
            $resp['data']              = $list;

            $resp['meta']['page']      = @$filter['page'];
            $resp['meta']['limit']     = @$filter['limit'];
            $resp['meta']['totalData'] = count($totalRow);
            $resp['meta']['totalPage'] = ceil(count($totalRow) / (empty($filter['limit']) ? 1 : $filter['limit']));
            $resp['meta']['orderBy']   = @$filter['orderBy'];
            $resp['meta']['sort']      = @$filter['sort'];

            $this->response($resp, REST_Controller::HTTP_OK);
        } else {
            // GET /:id
            $resp         = Response();
            $filter       = array(
                'id' => $id,
            );
            $data = $this->{$this->model}->get_detail($filter);
            if (!$data) {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "Data tidak ditemukan";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            } else {
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Success";
                $resp['data']              = $data;
                $this->response($resp, REST_Controller::HTTP_OK);
            }
        }
    }

    public function index_post()
    {
        // POST faq
        $resp = Response();

        // validate token
        $token = $this->input->get_request_header("Authorization");
        $jwt            = new JWT();
        $jwt_secret_key = secret_key();
        $validate_jwt   = validate_token($token);
        if ($validate_jwt["status"] == 401) {
            $resp['success']           = false;
            $resp['code']              = $validate_jwt["status"];
            $resp['message']           = $validate_jwt["message"];
            return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $post = json_decode(file_get_contents('php://input'), TRUE);
        $save = "";
        foreach ($post as $val) {
            // $val["name"] = strtoupper($val["name"]);
            // $check = $this->{$this->model}->get_detail(array("ktp_number" => $val["ktp_number"]));
            // if(!$check){
            $save = $this->{$this->model}->add($this->table, $val);
            // }
        }
        if ($save) {
            $resp['success']           = true;
            $resp['code']              = 200;
            $resp['message']           = "Data berhasil disimpan";
            $this->response($resp, REST_Controller::HTTP_OK);
        } else {
            $resp['success']           = false;
            $resp['code']              = 400;
            $resp['message']           = "Data gagal disimpan";
            $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_put($id = 0)
    {
        // PUT faq
        $resp = Response();

        // validate token
        $token = $this->input->get_request_header("Authorization");
        $jwt            = new JWT();
        $jwt_secret_key = secret_key();
        $validate_jwt   = validate_token($token);
        if ($validate_jwt["status"] == 401) {
            $resp['success']           = false;
            $resp['code']              = $validate_jwt["status"];
            $resp['message']           = $validate_jwt["message"];
            return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
        }

        if (empty($id)) {
            $resp['success']           = false;
            $resp['code']              = 400;
            $resp['message']           = "ID tidak ditemukan";
            $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $post   = json_decode(file_get_contents('php://input'), TRUE);
            // $post = $this->input->post();
            $filter = array(
                "id" => $id,
            );
            $save   = $this->{$this->model}->update($this->table, $post, $filter);
            if ($save) {
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Data berhasil dirubah";
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "Data gagal dirubah";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function index_delete($id = 0)
    {
        // DELETE faq
        $resp = Response();

        // validate token
        $token = $this->input->get_request_header("Authorization");
        $jwt            = new JWT();
        $jwt_secret_key = secret_key();
        $validate_jwt   = validate_token($token);
        if ($validate_jwt["status"] == 401) {
            $resp['success']           = false;
            $resp['code']              = $validate_jwt["status"];
            $resp['message']           = $validate_jwt["message"];
            return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
        }

        if (empty($id)) {
            $resp['success']           = false;
            $resp['code']              = 400;
            $resp['message']           = "ID tidak ditemukan";
            $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $save   = $this->{$this->model}->delete($this->table, $id);
            if ($save) {
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = "Data berhasil dihapus";
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                $resp['success']           = false;
                $resp['code']              = 400;
                $resp['message']           = "Data gagal dihapus";
                $this->response($resp, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function import_post()
    {
        $resp = Response();

        // validate token
        $token = $this->input->get_request_header("Authorization");
        $jwt            = new JWT();
        $jwt_secret_key = secret_key();
        $validate_jwt   = validate_token($token);
        if ($validate_jwt["status"] == 401) {
            $resp['success']           = false;
            $resp['code']              = $validate_jwt["status"];
            $resp['message']           = $validate_jwt["message"];
            return $this->response($resp, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $post = $this->convert_import($_FILES["file"]);
        if (empty($post["data"])) {
            $resp['success']           = false;
            $resp['code']              = 400;
            $resp['message']           = $post["message"];
            $this->response($resp, REST_Controller::HTTP_OK);
        } else {
            foreach ($post["data"] as $val) {
                $val["name"] = strtoupper($val["name"]);
                $save = $this->{$this->model}->add($this->table, $val);
            }
            if ($save) {
                $resp['success']           = true;
                $resp['code']              = 200;
                $resp['message']           = $post["message"];
                $this->response($resp, REST_Controller::HTTP_OK);
            } else {
                $resp['success']           = false;
                $resp['code']              = 200;
                $resp['message']           = "Data gagal disimpan";
                $this->response($resp, REST_Controller::HTTP_OK);
            }
        }
    }

    public function convert_import($file)
    {
        $result = array();
        $path = $file["tmp_name"];

        $objPHPExcel = PHPExcel_IOFactory::load($path, $encode = 'utf-8');
        $sheet = $objPHPExcel->getSheet(0); //Activate first sheet table
        $highestRow = $sheet->getHighestRow(); //Get total number
        $highestColumn = $sheet->getHighestColumn(); //Get total columns
        $tempData = array();
        for ($i = 3; $i <= $highestRow; $i++) {

            $name               = $objPHPExcel->getActiveSheet()->getCell("B" . $i)->getValue();
            $ktp_number         = $objPHPExcel->getActiveSheet()->getCell("C" . $i)->getValue();
            $kk_number              = $objPHPExcel->getActiveSheet()->getCell("D" . $i)->getValue();
            $gender             = $objPHPExcel->getActiveSheet()->getCell("E" . $i)->getValue();
            $address            = $objPHPExcel->getActiveSheet()->getCell("F" . $i)->getValue();
            $rt                 = $objPHPExcel->getActiveSheet()->getCell("G" . $i)->getValue();
            $rw                 = $objPHPExcel->getActiveSheet()->getCell("H" . $i)->getValue();
            $master_disaster_id = $objPHPExcel->getActiveSheet()->getCell("I" . $i)->getValue();
            $master_damage_id   = $objPHPExcel->getActiveSheet()->getCell("J" . $i)->getValue();
            $year               = $objPHPExcel->getActiveSheet()->getCell("K" . $i)->getValue();
            $non_category       = $objPHPExcel->getActiveSheet()->getCell("L" . $i)->getValue();
            $threatened         = $objPHPExcel->getActiveSheet()->getCell("M" . $i)->getValue();

            $master_district_id = $objPHPExcel->getActiveSheet()->getCell("Q" . $i)->getValue();
            $master_village_id  = $objPHPExcel->getActiveSheet()->getCell("R" . $i)->getValue();


            array_push($tempData, array(
                "name"                  => $name,
                "ktp_number"            => $ktp_number,
                "kk_number"                 => $kk_number,
                "gender"                => $gender,
                "address"               => $address, //M
                "rt"                    => $rt,
                "rw"                    => $rw,
                "master_disaster_id"    => $master_disaster_id,
                "master_damage_id"      => $master_damage_id,
                "year"                  => $year,
                "non_category"          => $non_category,
                "threatened"            => $threatened,
                "master_village_id"     => $master_village_id,
                "master_district_id"    => $master_district_id
            ));
        }

        if (empty($tempData)) {
            $result["message"] = "Data yang anda impor kosong.";
            $result["data"]    = array();
            return $result;
        } else {
            $result["message"] = "Data yang anda impor berhasil";
            $result["data"]    = $tempData;
        }

        return $result;
    }
}
    
    /* End of file Test.php */
