<?php
    require APPPATH . 'libraries/REST_Controller.php';

    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Chart extends REST_Controller {

        var $table           = '';
        var $model_master_district = 'Model_master_district';
        var $model_residence_recipient     = 'Model_residence_recipient';

        public function __construct()
        {
            parent::__construct();
            // check_login();
            $this->load->model('Model_master_district');
            $this->load->model('Model_residence_recipient');
        }

        public function index_get() 
        {
            // GET homepage
            $resp                           = Response();

            $master_district = $this->{$this->model_master_district}->get_all(array("master_city_id" => 3201));

            $labels = array();
            $color  = array();
            $data   = array();
            // "#".substr(md5(rand()), 0, 6)
            foreach ( $master_district as $value ) {
                array_push($labels, $value["name"]);
                array_push($color, "#".substr(md5(rand()), 0, 6));
                $jobseeker = $this->{$this->model_residence_recipient}->get_all(array("master_district_id" => $value["id"]));
                array_push($data, count($jobseeker));
            }
            $result        = array(
                "labels" => $labels,
                "datasets" => array(
                    array(
                        "label" => "Total",
                        "data" => $data,
                        "backgroundColor" => $color,
                        "borderColor" => $color,
                        "borderWidth" => 1,
                    ),
                )
            );
            
            $resp['success']           = true;
            $resp['code']              = 200;
            $resp['message']           = "Success";
            $resp['data']              = $result;
            $this->response($resp, REST_Controller::HTTP_OK);
        }

        public function work_status_get() 
        {
            // GET homepage
            $resp = Response();

            $work_status = array(
                'BELUM BEKERJA',
                'SUDAH BEKERJA',
                'SEDANG PELATIHAN'
            );

            $labels = array();
            $color  = array();
            $data   = array();
            // "#".substr(md5(rand()), 0, 6)
            foreach ( $work_status as $value ) {
                array_push($labels, $value);
                array_push($color, "#".substr(md5(rand()), 0, 6));

                $jobseeker = $this->{$this->model_jobseeker}->get_all(array("work_status" => $value));
                array_push($data, count($jobseeker));
            }
            $result        = array(
                "labels" => $labels,
                "datasets" => array(
                    array(
                        "label" => "Total",
                        "data" => $data,
                        "backgroundColor" => $color,
                        "borderColor" => $color,
                        "borderWidth" => 1,
                    ),
                )
            );
            
            $resp['success']           = true;
            $resp['code']              = 200;
            $resp['message']           = "Success";
            $resp['data']              = $result;
            $this->response($resp, REST_Controller::HTTP_OK);
        }
    
    }
    
    /* End of file Test.php */
    
?>