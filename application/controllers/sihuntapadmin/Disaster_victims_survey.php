<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Disaster_victims_survey extends CI_Controller {

    public function index()
    {
        $asset = array(
            'title' => "BERANDA SIHUNTAP",
            'css'   => array(
                'template/assets/css/bootstrap.min',
                'template/assets/css/icons',
                'template/assets/css/style'

            ),
            'js'    => array(
                'template/assets/js/jquery.min',
                'template/assets/js/popper.min',
                'template/assets/js/bootstrap.min',
                'template/assets/js/modernizr.min',
                'template/assets/js/detect',
                'template/assets/js/fastclick',
                'template/assets/js/jquery.blockUI',
                'template/assets/js/waves',
                'template/assets/js/jquery.nicescroll',


                'template/assets/js/app'

            ),
        );
        $this->load->view('sihuntapadmin/templates/header',$asset);
        $this->load->view('sihuntapadmin/templates/menus');
        $this->load->view('sihuntapadmin/disaster_victims_survey/index');
        $this->load->view('sihuntapadmin/templates/footer',$asset);
    }

    

    public function view()
    {
        $asset = array(
            'title' => "BERANDA SIHUNTAP",
            'css'   => array(
                'template/assets/css/bootstrap.min',
                'template/assets/css/icons',
                'template/assets/css/style',
                'template/assets/plugins/summernote/summernote-bs4'

            ),
            'js'    => array(
                'template/assets/js/jquery.min',
                'template/assets/js/popper.min',
                'template/assets/js/bootstrap.min',
                'template/assets/js/modernizr.min',
                'template/assets/js/detect',
                'template/assets/js/fastclick',
                'template/assets/js/jquery.blockUI',
                'template/assets/js/waves',
                'template/assets/js/jquery.nicescroll',
                'template/assets/plugins/summernote/summernote-bs4.min',
                'template/assets/pages/summernote.init',
                'template/assets/js/app'

            ),
        );
        $data = array(
            "id" => $this->uri->segment(4)
        );

        $this->load->view('sihuntapadmin/templates/header',$asset);
        $this->load->view('sihuntapadmin/templates/menus');
        $this->load->view('sihuntapadmin/disaster_victims_survey/view', $data);
        $this->load->view('sihuntapadmin/templates/footer',$asset);
    }
    public function detailvictim()
    {
        $asset = array(
            'title' => "BERANDA SIHUNTAP",
            'css'   => array(
                'template/assets/css/bootstrap.min',
                'template/assets/css/icons',
                'template/assets/css/style',
                'template/assets/plugins/summernote/summernote-bs4'

            ),
            'js'    => array(
                'template/assets/js/jquery.min',
                'template/assets/js/popper.min',
                'template/assets/js/bootstrap.min',
                'template/assets/js/modernizr.min',
                'template/assets/js/detect',
                'template/assets/js/fastclick',
                'template/assets/js/jquery.blockUI',
                'template/assets/js/waves',
                'template/assets/js/jquery.nicescroll',
                'template/assets/plugins/summernote/summernote-bs4.min',
                'template/assets/pages/summernote.init',
                'template/assets/js/app'

            ),
        );
        $data = array(
            "id" => $this->uri->segment(4)
        );

        $this->load->view('sihuntapadmin/templates/header',$asset);
        $this->load->view('sihuntapadmin/templates/menus');
        $this->load->view('sihuntapadmin/disaster_victims_survey/detail_victim', $data);
        $this->load->view('sihuntapadmin/templates/footer',$asset);
    }

}

/* End of file Home.php */
