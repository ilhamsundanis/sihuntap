<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Page_gallery extends CI_Controller {

    public function index()
    {
        $asset = array(
            'title' => "BERANDA SIHUNTAP",
            'css'   => array(
                'template/assets/css/bootstrap.min',
                'template/assets/css/icons',
                'template/assets/css/style'

            ),
            'js'    => array(
                'template/assets/js/jquery.min',
                'template/assets/js/popper.min',
                'template/assets/js/bootstrap.min',
                'template/assets/js/modernizr.min',
                'template/assets/js/detect',
                'template/assets/js/fastclick',
                'template/assets/js/jquery.blockUI',
                'template/assets/js/waves',
                'template/assets/js/jquery.nicescroll',


                'template/assets/js/app'

            ),
        );
        $this->load->view('sihuntapadmin/templates/header',$asset);
        $this->load->view('sihuntapadmin/templates/menus');
        $this->load->view('sihuntapadmin/page_gallery/index');
        $this->load->view('sihuntapadmin/templates/footer',$asset);
    }

    public function add()
    {
        $asset = array(
            'title' => "BERANDA SIHUNTAP",
            'css'   => array(
                'template/assets/css/bootstrap.min',
                'template/assets/css/icons',
                'template/assets/css/style'

            ),
            'js'    => array(
                'template/assets/js/jquery.min',
                'template/assets/js/popper.min',
                'template/assets/js/bootstrap.min',
                'template/assets/js/modernizr.min',
                'template/assets/js/detect',
                'template/assets/js/fastclick',
                'template/assets/js/jquery.blockUI',
                'template/assets/js/waves',
                'template/assets/js/jquery.nicescroll',


                'template/assets/js/app'

            ),
        );
        $this->load->view('sihuntapadmin/templates/header',$asset);
        $this->load->view('sihuntapadmin/templates/menus');
        $this->load->view('sihuntapadmin/page_gallery/add');
        $this->load->view('sihuntapadmin/templates/footer',$asset);
    }

    public function view()
    {
        $asset = array(
            'title' => "BERANDA SIHUNTAP",
            'css'   => array(
                'template/assets/css/bootstrap.min',
                'template/assets/css/icons',
                'template/assets/css/style'

            ),
            'js'    => array(
                'template/assets/js/jquery.min',
                'template/assets/js/popper.min',
                'template/assets/js/bootstrap.min',
                'template/assets/js/modernizr.min',
                'template/assets/js/detect',
                'template/assets/js/fastclick',
                'template/assets/js/jquery.blockUI',
                'template/assets/js/waves',
                'template/assets/js/jquery.nicescroll',


                'template/assets/js/app'

            ),
        );
        $data = array(
            "id" => $this->uri->segment(4)
        );

        $this->load->view('sihuntapadmin/templates/header',$asset);
        $this->load->view('sihuntapadmin/templates/menus');
        $this->load->view('sihuntapadmin/page_gallery/view', $data);
        $this->load->view('sihuntapadmin/templates/footer',$asset);
    }

}

/* End of file Home.php */
