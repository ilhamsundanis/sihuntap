<?php


// require_once APPPATH . "third_party/PhpSpreadsheet/Spreadsheet.php";
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_rehabilitation extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // check_login();
        $this->load->model('Model_disaster_victims');
    }
    public function index()
    {
        $asset = array(
            'title' => "BERANDA SIHUNTAP",
            'css'   => array(
                'template/assets/css/bootstrap.min',
                'template/assets/css/icons',
                'template/assets/css/style'

            ),
            'js'    => array(
                'template/assets/js/jquery.min',
                'template/assets/js/popper.min',
                'template/assets/js/bootstrap.min',
                'template/assets/js/modernizr.min',
                'template/assets/js/detect',
                'template/assets/js/fastclick',
                'template/assets/js/jquery.blockUI',
                'template/assets/js/waves',
                'template/assets/js/jquery.nicescroll',


                'template/assets/js/app'

            ),
        );
        $this->load->view('sihuntapadmin/templates/header',$asset);
        $this->load->view('sihuntapadmin/templates/menus');
        $this->load->view('sihuntapadmin/report_rehabilitation/index');
        $this->load->view('sihuntapadmin/templates/footer',$asset);
    }
    public function relocation()
    {
        $asset = array(
            'title' => "BERANDA SIHUNTAP",
            'css'   => array(
                'template/assets/css/bootstrap.min',
                'template/assets/css/icons',
                'template/assets/css/style',
                'template/assets/plugins/summernote/summernote-bs4'

            ),
            'js'    => array(
                'template/assets/js/jquery.min',
                'template/assets/js/popper.min',
                'template/assets/js/bootstrap.min',
                'template/assets/js/modernizr.min',
                'template/assets/js/detect',
                'template/assets/js/fastclick',
                'template/assets/js/jquery.blockUI',
                'template/assets/js/waves',
                'template/assets/js/jquery.nicescroll',
                'template/assets/plugins/summernote/summernote-bs4.min',
                'template/assets/pages/summernote.init',
                'template/assets/js/app'

            ),
        );
        $data = array(
            "id" => $this->uri->segment(4)
        );
        $this->load->view('sihuntapadmin/templates/header',$asset);
        $this->load->view('sihuntapadmin/templates/menus');
        $this->load->view('sihuntapadmin/report_rehabilitation/relocation',$data);
        $this->load->view('sihuntapadmin/templates/footer',$asset);
    }

    public function view()
    {
        $asset = array(
            'title' => "BERANDA SIHUNTAP",
            'css'   => array(
                'template/assets/css/bootstrap.min',
                'template/assets/css/icons',
                'template/assets/css/style',
                'template/assets/plugins/summernote/summernote-bs4'

            ),
            'js'    => array(
                'template/assets/js/jquery.min',
                'template/assets/js/popper.min',
                'template/assets/js/bootstrap.min',
                'template/assets/js/modernizr.min',
                'template/assets/js/detect',
                'template/assets/js/fastclick',
                'template/assets/js/jquery.blockUI',
                'template/assets/js/waves',
                'template/assets/js/jquery.nicescroll',
                'template/assets/plugins/summernote/summernote-bs4.min',
                'template/assets/pages/summernote.init',
                'template/assets/js/app'

            ),
        );
        $data = array(
            "id" => $this->uri->segment(4)
        );

        $this->load->view('sihuntapadmin/templates/header',$asset);
        $this->load->view('sihuntapadmin/templates/menus');
        $this->load->view('sihuntapadmin/report_rehabilitation/view', $data);
        $this->load->view('sihuntapadmin/templates/footer',$asset);
    }
    public function attachment()
    {
        $asset = array(
            'title' => "BERANDA SIHUNTAP",
            'css'   => array(
                'template/assets/css/bootstrap.min',
                'template/assets/css/icons',
                'template/assets/css/style',
                'template/assets/plugins/summernote/summernote-bs4'

            ),
            'js'    => array(
                'template/assets/js/jquery.min',
                'template/assets/js/popper.min',
                'template/assets/js/bootstrap.min',
                'template/assets/js/modernizr.min',
                'template/assets/js/detect',
                'template/assets/js/fastclick',
                'template/assets/js/jquery.blockUI',
                'template/assets/js/waves',
                'template/assets/js/jquery.nicescroll',
                'template/assets/plugins/summernote/summernote-bs4.min',
                'template/assets/pages/summernote.init',
                'template/assets/js/app'

            ),
        );
        $data = array(
            "id" => $this->uri->segment(4)
        );

        $this->load->view('sihuntapadmin/templates/header',$asset);
        $this->load->view('sihuntapadmin/templates/menus');
        $this->load->view('sihuntapadmin/report_rehabilitation/attachment', $data);
        $this->load->view('sihuntapadmin/templates/footer',$asset);
    }
    
    public function view_recipient()
    {
        $asset = array(
            'title' => "BERANDA SIHUNTAP",
            'css'   => array(
                'template/assets/css/bootstrap.min',
                'template/assets/css/icons',
                'template/assets/css/style'

            ),
            'js'    => array(
                'template/assets/js/jquery.min',
                'template/assets/js/popper.min',
                'template/assets/js/bootstrap.min',
                'template/assets/js/modernizr.min',
                'template/assets/js/detect',
                'template/assets/js/fastclick',
                'template/assets/js/jquery.blockUI',
                'template/assets/js/waves',
                'template/assets/js/jquery.nicescroll',
                'template/assets/plugins/jquery.repeater/jquery.repeater.min',
                'template/assets/pages/form-repeater.init',
                'template/assets/js/app'

            ),
        );
        $data = array(
            "id" => $this->uri->segment(4)
        );

        $this->load->view('sihuntapadmin/templates/header',$asset);
        $this->load->view('sihuntapadmin/templates/menus');
        $this->load->view('sihuntapadmin/report_rehabilitation/view_recipient', $data);
        $this->load->view('sihuntapadmin/templates/footer',$asset);
    }

    public function export()
    {
        $objPHPExcel = new Spreadsheet();
        $objPHPExcel->getActiveSheet();
        

        $tableBorder = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('rgb' => '000')
				)
			)
		);

        $styleCenter = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            )
        );

        $post         = $this->input->get();
        $filter       = array();
        $output = "FilterBy-";
        if ( !empty( $post['id'] ) ) {
            $filter['id'] = $post['id'];
            $output += "ByID";
        }

        if ( !empty( $post['name'] ) ) {
            $filter['name'] = $post['name'];
            $output += $post["name"];
        }

        if ( !empty( $post['ktp_number'] ) ) {
            $filter['ktp_number'] = $post['ktp_number'];
            $output += $post["ktp_number"];
        }

        if ( !empty( $post['status'] ) ) {
            $filter['status'] = $post['status'];
            $output += $post["status"];
        }

            

        if ( !empty( $post['master_district_id'] ) ) {
            $filter['master_district_id'] = $post['master_district_id'];
            // $output += "ByKecamatan";
        }

        if ( !empty( $post['master_village_id'] ) ) {
            $filter['master_village_id'] = $post['master_village_id'];
            $output += "ByDesa";
        }

        
        $result = $this->Model_disaster_victims->get_all($filter);

        $objPHPExcel->getActiveSheet()->setCellValue("A1", "NO");
        $objPHPExcel->getActiveSheet()->setCellValue("B1", "NAMA");
        $objPHPExcel->getActiveSheet()->setCellValue("C1", "NIK");
        $objPHPExcel->getActiveSheet()->setCellValue("D1", "KK");
        $objPHPExcel->getActiveSheet()->setCellValue("E1", "KECAMATAN");
        $objPHPExcel->getActiveSheet()->setCellValue("F1", "DESA / KELURAHAN");
        $objPHPExcel->getActiveSheet()->setCellValue("G1", "STATUS");

        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($tableBorder);
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($styleCenter);
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getAlignment()->setWrapText(true); 

        $index = 2;
        $no = 1;
        foreach ( $result as $data ) {
            $index++;

            $objPHPExcel->getActiveSheet()->setCellValue("A" . $index, $no);
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $index, $data["name"]);
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $index, "'".$data["ktp_number"],\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $index, "'".$data["kk_number"],\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $index, $data["master_district_name"]);
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $index, $data["master_village_name"]);
            $objPHPExcel->getActiveSheet()->setCellValue("G" . $index, $data["status"]);
            
            $no++;
        }

        $namaFile = "Data Rehabilitasi - ".$output. "-" . date('YmdHis');
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'. $namaFile .'.xlsx"'); 
        header('Cache-Control: max-age=0'); //no cache
        // $objWriter = \PhpOffice\PhpSpreadsheet\IOFactor::createWriter($objPHPExcel); 
        $writer = new Xlsx($objPHPExcel);
        $writer->save('php://output'); 
        //force user to download the Excel file without writing it to server's HD
        // $objWriter->save('php://output'); 
       
        // // $objPHPExcel->setTitle("Laporan Data Siswa");
        // $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($namaFile);
        // // Proses file excel
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment; filename="Data Siswa.xlsx"'); // Set nama file excel nya
        // header('Cache-Control: max-age=0');
        // $writer = new Xlsx($spreadsheet);
        // $writer->save('php://output');
    }


}

/* End of file Home.php */
