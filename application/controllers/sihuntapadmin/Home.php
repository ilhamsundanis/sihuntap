<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index()
    {
        $asset = array(
            'title' => "BERANDA SIHUNTAP",
            'css'   => array(
                'template/assets/plugins/jvectormap/jquery-jvectormap-2.0.2',
                'template/assets/plugins/fullcalendar/vanillaCalendar',
                'template/assets/plugins/morris/morris',
                'template/assets/css/bootstrap.min',
                'template/assets/css/icons',
                'template/assets/css/style'

            ),
            'js'    => array(
                // 'template/assets/js/jquery.min',
                'template/assets/js/popper.min',
                // 'template/assets/js/bootstrap.min',
                'template/assets/js/modernizr.min',
                'template/assets/js/detect',
                'template/assets/js/fastclick',
                'template/assets/js/jquery.blockUI',
                'template/assets/js/waves',
                'template/assets/js/jquery.nicescroll',
                // 'template/assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min',
                // 'template/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en',
                // 'template/assets/plugins/skycons/skycons.min',
                // 'template/assets/plugins/fullcalendar/vanillaCalendar',
                // 'template/assets/plugins/chart.js/chart.min',
                'template/assets/plugins/raphael/raphael-min',
                'template/assets/plugins/morris/morris.min',
                // 'template/assets/pages/morris.init',
                'template/assets/js/app'

            ),
        );
        $this->load->view('sihuntapadmin/templates/header',$asset);
        $this->load->view('sihuntapadmin/templates/menus');
        $this->load->view('sihuntapadmin/home/index');
        $this->load->view('sihuntapadmin/templates/footer',$asset);
    }

    public function dashboard()
    {
        $this->load->view('sihuntapadmin/templates/header');
        $this->load->view('sihuntapadmin/templates/menus');
        $this->load->view('sihuntapadmin/home/dashboard');
        $this->load->view('sihuntapadmin/templates/footer');
    }


    public function logout()
    {
        $this->load->view('sihuntapadmin/logout');
    }

}

/* End of file Home.php */
?>