<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    var $url = "auth";

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('Model_auth');
    }

    public function index()
    {
        $asset = array(
            'title' => "Data Hunian Tetap",
            'description'   => 'Data calon penerima hunian tetap',
            'css'   => array(
                'template/assets/css/bootstrap.min',
                'template/assets/css/icons',
                'template/assets/css/style'

            ),
            'js'    => array(
                'template/assets/js/jquery.min',
                'template/assets/js/popper.min',
                'template/assets/js/bootstrap.min',
                'template/assets/js/modernizr.min',
                'template/assets/js/detect',
                'template/assets/js/fastclick',
                'template/assets/js/jquery.blockUI',
                'template/assets/js/waves',
                'template/assets/js/jquery.nicescroll',

                'template/assets/js/app'

            ),
        );
        $this->load->view('sihuntapadmin/auth/index');
    }
     public function logout()
    {
        $this->load->view('sihuntapadmin/auth/logout');
    }
    private function _check_username($username)
    {
        return $this->db->get_where('tb_user', ['username' => $username]);
    }
}
