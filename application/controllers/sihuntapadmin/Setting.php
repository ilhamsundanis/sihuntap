<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

    public function index()
    {
        $asset = array(
            'title' => "BERANDA SIHUNTAP",
            'css'   => array(
                'template/assets/css/bootstrap.min',
                'template/assets/css/icons',
                'template/assets/css/style'

            ),
            'js'    => array(
                'template/assets/js/jquery.min',
                'template/assets/js/popper.min',
                'template/assets/js/bootstrap.min',
                'template/assets/js/modernizr.min',
                'template/assets/js/detect',
                'template/assets/js/fastclick',
                'template/assets/js/jquery.blockUI',
                'template/assets/js/waves',
                'template/assets/js/jquery.nicescroll',


                'template/assets/js/app'

            ),
        );
        $this->load->view('sihuntapadmin/templates/header',$asset);
        $this->load->view('sihuntapadmin/templates/menus');
        $this->load->view('sihuntapadmin/setting/index');
        $this->load->view('sihuntapadmin/templates/footer',$asset);
    }
}