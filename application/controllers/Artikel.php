<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Artikel extends CI_Controller
{

    var $url = "news";
    var $model = "Model_news";
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_news');
    }

    public function index()
    {
        $asset = [
            "title" => "Artikel dan Berita"
        ];
        $data = array(
            "GetData"   => $this->{$this->model}->get_data()
        );
        $this->load->view('fe/template/header', $asset);
        $this->load->view('fe/article',$data);
        $this->load->view('fe/template/footer');
    }

    public function detail($id)
    {
        $asset = [
            "title" => "Artikel dan Berita"
        ];
        $data = array(
            "GetArtikelById"  => $this->{$this->model}->get_artikel_by_id($id)
        );
        $this->load->view('fe/template/header', $asset);
        $this->load->view('fe/article_detail',$data);
        $this->load->view('fe/template/footer');
    }
}
