<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Faq extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = [
            "title" => "Layanan Bantuan"
        ];
        $this->load->view('fe/template/header', $data);
        $this->load->view('fe/faq');
        $this->load->view('fe/template/footer');
    }
}
