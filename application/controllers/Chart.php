<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Chart extends CI_Controller {

    public function index()
    {
        $data = array(
            "get" => $this->input->get(),
        );
        $assets = array(
			"title" => "Grafik"
		);

        $this->load->view('fe/template/header',$assets);
        $this->load->view('fe/chart/index', $data);
        $this->load->view('fe/template/footer');
    }

    public function work_status()
    {
        $data = array(
            "get" => $this->input->get(),
        );
        $assets = array(
			"title" => "Grafik"
		);

        $this->load->view('templates/home/header',$assets);
        $this->load->view('templates/home/menus');
        $this->load->view('chart/work_status', $data);
        $this->load->view('templates/home/footer');
    }

}

/* End of file Home.php */
