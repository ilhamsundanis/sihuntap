<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends CI_Controller
{
    var $url = "beranda";
    var $model_news = 'Model_page_news';
    var $model_slider = "Model_page_slider";
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_page_slider');
        $this->load->model('Model_page_news');
    }

    public function index()
    {
        $assets = [
            "title" => "Beranda"
        ];
        $data = array(
            "GetSlider"   => $this->{$this->model_slider}->get_all(),
            "GetNews"     => $this->{$this->model_news}->get_all()
        );
        $this->load->view('fe/template/header', $assets);
        $this->load->view('fe/index',$data);
        $this->load->view('fe/template/footer');
    }
}
