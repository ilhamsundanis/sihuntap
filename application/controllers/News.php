<?php
defined('BASEPATH') or exit('No direct script access allowed');

class News extends CI_Controller
{

    var $url = "news";
    var $model = "Model_news";
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_news');
    }

    public function index()
    {
        $asset = [
            "title" => "Artikel dan Berita"
        ];
        $data = array(
            "GetData"   => $this->{$this->model}->get_data()
        );
        $this->load->view('fe/template/header', $asset);
        $this->load->view('fe/news',$data);
        $this->load->view('fe/template/footer');
    }

    public function detail($id)
    {
        $asset = [
            "title" => "Artikel dan Berita"
        ];
        $data = array(
            "id" => $this->uri->segment(3)
        );
        $this->load->view('fe/template/header', $asset);
        $this->load->view('fe/news_detail',$data);
        $this->load->view('fe/template/footer');
    }
}
