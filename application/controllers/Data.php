<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data extends CI_Controller
{

    var $url = "news";
    var $model = "Model_news";
    var $model_proposal ='Model_proposal';
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_news');
        $this->load->model('Model_proposal');
    }

    public function index()
    {
        $asset = [
            "title" => "Data Statistik"
        ];
        // $data = array(
        //     "GetData"   => $this->{$this->model}->get_data()
        // );
        $this->load->view('fe/template/header', $asset);
        $this->load->view('fe/data/index');
        $this->load->view('fe/template/footer');
    }

    public function detail($id)
    {
        $asset = [
            "title" => "Data Statistik Berdasarkan Kecamatan"
        ];
        $data = array(
            "id" => $this->uri->segment(3)
        );
        $this->load->view('fe/template/header', $asset);
        $this->load->view('fe/data/data_detail',$data);
        $this->load->view('fe/template/footer');
    }

    public function geomaps()
    {
        $asset = [
            "title" => "Peta Lokasi"
        ];
        $data = array(
            "get_maps"  => $this->{$this->model_proposal}->get_all()
        );
        $this->load->view('fe/template/header', $asset);
        $this->load->view('fe/data/geomaps',$data);
        $this->load->view('fe/template/footer');
    }
}
